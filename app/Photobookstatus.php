<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Photobookstatus extends Model
{
    //
    protected $fillable = ['name'];

    public function event()
    {
        return $this->belongsTo('Field\Event');
    }
}
