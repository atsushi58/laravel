<?php

namespace Field;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Field\Insurance;

class Admin extends Authenticatable
{
    use Notifiable;

    protected $guard = 'admin';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'kana', 'nickname', 'tel', 'reference', 'active', 'birthday', 'email', 'password', 'role_id', 'basesalary', 'basetransportation', 'insurancerank', 'nursinginsurance', 'taxsection', 'inhabitanttax'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function bankaccount(){
        return $this->hasOne('Field\Bankaccount');
    }

    public function payments(){
        return $this->hasMany('Field\Payment');
    }

    public function role(){
        return $this->belongsTo('Field\Role');
    }

    public function salaries(){
        return $this->hasMany('Field\Salary');
    }

    public function shifts(){
        return $this->hasMany('Field\Shift');
    }

    public function events(){
        return $this->hasMany('Field\Event');
    }

    public function getPensionamountAttribute(){
        $insurance = Insurance::where('rank', $this->insurancerank)->first();
        if(isset($this->insurancerank)){
            return $insurance->pension;
        }else{
            return 0;
        }
    }
    
    public function getHealthinsuranceamountAttribute(){
        $insurance = Insurance::where('rank', $this->insurancerank)->first();
        if(isset($this->insurancerank)){
            return $insurance->healthinsurance;
        }else{
            return 0;
        }
    }

    public function getNursinginsuranceamountAttribute(){
        if($this->nursinginsurance == 1 && isset($this->insurancerank)){
            $insurance = Insurance::where('rank', $this->insurancerank)->first();
            return $insurance->nursinginsurance;
        }else{
            return 0;
        }
    }

    public function getTcPaymenttAttribute(){
        if($this->payments->where('workplace_id', 2)->first() != null){
            return $this->payments->where('workplace_id', 2)->first()->payment;
        }else{
            return 0;
        }
    }
    public function getUrawaPaymentAttribute(){
        if($this->payments->where('workplace_id', 3)->first() != null){
            return $this->payments->where('workplace_id', 3)->first()->payment;
        }else{
            return 0;
        }
    }
    public function getShinjukuPaymentAttribute(){
        if($this->payments->where('workplace_id', 4)->first() != null){
            return $this->payments->where('workplace_id', 4)->first()->payment;
        }else{
            return 0;
        }
    }
    public function getKawaguchikoPaymentAttribute(){
        if($this->payments->where('workplace_id', 5)->first() != null){
            return $this->payments->where('workplace_id', 5)->first()->payment;
        }else{
            return 0;
        }
    }
    public function getYakushimaPaymentAttribute(){
        if($this->payments->where('workplace_id', 6)->first() != null){
            return $this->payments->where('workplace_id', 6)->first()->payment;
        }else{
            return 0;
        }
    }
}