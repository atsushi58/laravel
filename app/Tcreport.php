<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Tcreport extends Model
{
    protected $fillable = ['admin_id','body', 'event_id'];

    public function tctimereports()
    {
    	return $this->hasMany('Field\Tctimereport');
    }

    public function admin()
    {
    	return $this->belongsTo('Field\Admin');
    }

    public function Event()
    {
        return $this->belongsTo('Field\Event');
    }
}
