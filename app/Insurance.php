<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Insurance extends Model
{
    protected $fillable = [
        'rank', 'basewage', 'minwage', 'maxwage', 'healthinsurance', 'nursinginsurance', 'pension'
    ];
}
