<?php

namespace Field\Yamarent;

use Illuminate\Database\Eloquent\Model;

class Lostfoundcategory extends Model
{
	protected $fillable = [
        'name'
    ];

    public function founds()
    {
    	return $this->hasMany('Field\Yamarent\Found');
    }

    public function losts()
    {
    	return $this->hasMany('Field\Yamarent\Lost');
    }

}
