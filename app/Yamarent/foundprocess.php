<?php

namespace Field\Yamarent;

use Illuminate\Database\Eloquent\Model;

class Foundprocess extends Model
{
	protected $fillable = [
        'name'
    ];

    public function founds()
    {
    	return $this->hasMany('Field\Yamarent\Found');
    }

}
