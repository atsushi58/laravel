<?php

namespace Field\Yamarent;

use Illuminate\Database\Eloquent\Model;

class Lost extends Model
{
	protected $fillable = [
        'lostfoundcategory_id', 'name', 'tel', 'orderid', 'item', 'comment', 'lostprocess_id', 'voucharno'
    ];

    public function lostprocess()
    {
    	return $this->belongsTo('Field\Yamarent\Lostprocess');
    }

    public function lostfoundcategory()
    {
    	return $this->belongsTo('Field\Yamarent\Lostfoundcategory');
    }



}
