<?php

namespace Field\Yamarent;

use Illuminate\Database\Eloquent\Model;

class Lostprocess extends Model
{
	protected $fillable = [
        'name'
    ];

    public function losts()
    {
    	return $this->hasMany('Field\Yamarent\Lost');
    }

}
