<?php

namespace Field\Yamarent;

use Illuminate\Database\Eloquent\Model;

class Found extends Model
{
	protected $fillable = [
        'lostfoundcategory_id', 'name', 'tel', 'orderid', 'item', 'comment', 'foundprocess_id', 'voucharno', 'processeddate',
    ];

    public function foundprocess()
    {
    	return $this->belongsTo('Field\Yamarent\Foundprocess');
    }

    public function lostfoundcategory()
    {
    	return $this->belongsTo('Field\Yamarent\Lostfoundcategory');
    }

}
