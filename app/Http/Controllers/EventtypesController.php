<?php

namespace Field\Http\Controllers;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Field\Eventtype;

class EventtypesController extends Controller
{
    //
    public function index(){
    	$eventtypes = Eventtype::all();
        $eventtypes = Eventtype::Paginate(10);

    	return view('eventtypes.index')->with('eventtypes', $eventtypes);
    }

    public function show($id){
    	$eventtypes = Eventtype::findOrFail($id);

    	return view('eventtypes.show');
    }

    public function create(){
    	return view('eventtypes.create');
    }

    public function store(Request $request){
    	$eventtype = new Eventtype();
    	$eventtype->name = $request->name;
    	$eventtype->save();
    	return redirect('/eventtypes');
    }

    public function edit($id) {
      $eventtype = Eventtype::findOrFail($id);
      return view('eventtypes.edit')->with(compact('eventtype'));
    }

    public function update(Request $request, $id) {
      $eventtype = Eventtype::findOrFail($id);
      $eventtype->name = $request->name;
      $eventtype->save();
      return redirect('/eventtypes')->with('flash_message', 'Eventtype Updated!');
    }

    public function destroy($id) {
      $eventtype = Eventtype::findOrFail($id);
      $eventtype->delete();
      return redirect('/eventtypes')->with('flash_message', 'Deleted!');
  }
}
