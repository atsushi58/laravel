<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Field\Balancetype;

class BalancetypesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index()
	{
		$balancetypes = Balancetype::all();
		return view('yamakara.balancetypes.index')->with(compact('balancetypes'));
	}

	public function create()
	{
		return view('yamakara.balancetypes.create');
	}

	public function store(Request $request)
	{
		$balancetype = new Balancetype();
		$balancetype->fill($request->all())->save();
		return redirect()->route('balancetypes.index');
	}

	public function edit($id)
	{
		$balancetype = Balancetype::findOrFail($id);
		return view('yamakara.balancetypes.edit')->with(compact('balancetype'));
	}

	public function update(Request $request, $id)
	{
		$balancetype = Balancetype::findOrFail($id);
		$balancetype->fill($request->all())->save();
		return redirect()->route('balancetypes.index');
	}
}
