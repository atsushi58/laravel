<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;

use Field\Tctimereport;

class TctimereportsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $tctimereports = Tctimereport::all();
        return view('yamakara.tctimereports.index')->with(compact('tctimereports'));
    }

    public function create()
    {
        return view('yamakara.tctimereports.create');
    }


    public function store(Request $request)
    {
        $tctimereport = new Tctimereport();
        $tctimereport->fill($request->all())->save();
        return redirect()->route('tctimereports.index');
    }

    public function edit($id)
    {
        $tctimereport = Tctimereport::findOrFail($id);
        return view('yamakara.tctimereports.edit')->with(compact('tctimereport'));
    }

    public function update(Request $request, $id) {
      $tctimereport = Tctimereport::findOrFail($id);
      $tctimereport->fill($request->all())->save();        
      return redirect()->route('tctimereports.index');
    }
}
