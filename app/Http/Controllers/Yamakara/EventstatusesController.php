<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Field\Eventstatus;

class EventstatusesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
    	$eventstatuses = Eventstatus::all();
    	return view('yamakara.eventstatuses.index')->with(compact('eventstatuses'));
    }

    public function create()
    {
        return view('yamakara.eventstatuses.create');
    }

    public function store(Request $request)
    {
    	$eventstatus = new Eventstatus();
    	$eventstatus->fill($request->all())->save();
    	return redirect()->route('eventstatuses.index');
    }

    public function edit($id)
    {
        $eventstatus = Eventstatus::findOrFail($id);
        return view('yamakara.eventstatuses.edit')->with(compact('eventstatus'));
    }

    public function update(Request $request, $id)
    {
      $eventstatus = Eventstatus::findOrFail($id);
      $eventstatus->fill($request->all())->save();
      return redirect()->route('eventstatuses.index');
    }
}
