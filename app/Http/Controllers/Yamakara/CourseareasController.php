<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Field\Coursearea;
use Field\Course;
use Illuminate\Support\Facades\Mail;

use Field\Mail\TestMail;

class CourseareasController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index()
	{
		$courseareas = Coursearea::orderBy('order', 'asc')->get();
		return view('yamakara.courseareas.index')->with(compact('courseareas'));
	}

	public function create()
	{
		return view('yamakara.courseareas.create');
	}

	public function store(Request $request)
	{
		$coursearea = new Coursearea();
		$coursearea->fill($request->all())->save();
		return redirect()->route('yamakara.courseareas');
	}

	public function edit($id)
	{
		$coursearea = Coursearea::findOrFail($id);
		return view('yamakara.courseareas.edit')->with(compact('coursearea'));
	}

	public function update(Request $request, $id)
	{
		$coursearea = Coursearea::findOrFail($id);
		$coursearea->fill($request->all())->save();
		//Mail::to('yamada@field-mt.com')->send(new TestMail());
		return redirect()->route('courseareas.index');
	}
}
