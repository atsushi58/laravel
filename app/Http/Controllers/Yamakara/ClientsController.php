<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Field\Http\Controllers\Controller;

use Carbon\Carbon;

use Field\Balance;
use Field\Event;
use Field\Eventattendance;
use Field\Eventprice;
use Field\Eventattendancepaymentmethod;
use Field\Eventattendancepaymentstatus;
use Field\User;

class ClientsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        if ($search)
        {
            $clients = User::where('name', 'LIKE', "%$search%")->orwhere('email', 'LIKE', "%$search%")->orwhere('kana', 'LIKE', "%$search%")->orwhere('tel', 'LIKE', "%$search%")->paginate(10);
        }else
        {
            $clients = User::paginate(10);
        }
        return view('yamakara.clients.index')->with(compact('clients', 'search', 'sort'));
    }

    public function thisyearpaid($amount)
    {
        $clients = User::get()->where('Thisyearpaid', '>', $amount);
        return view('yamakara.clients.select')->with(compact('clients'));
    } 

    public function thisyearattend($times)
    {
        $clients = User::get()->where('Thisyearattendance', '>', $times);
        return view('yamakara.clients.select')->with(compact('clients'));
    } 

    public function show($id)
    {
        $client = User::findOrFail($id);
        $eventattendances = Eventattendance::select('eventattendances.*')->where('user_id', $id)->join('eventprices', 'eventprices.id', '=', 'eventattendances.eventprice_id')->join('events', 'eventprices.event_id', '=', 'events.id')->orderBy('events.depdate')->get();
        $balances = Balance::where('user_id', $id)->orderBy('day', 'ASC')->get();
        return view('yamakara.clients.show')->with(compact('client', 'eventattendances', 'balances'));
    }

    public function create(){
        $prefs = config('pref');
        return view('yamakara.clients.create')->with(compact('prefs'));
    } 

    public function store(Request $request)
    {
        $client = new User();
        $client->fill($request->all())->save();
        return redirect()->route('clients.show', $client->id);
    }

    public function edit($id)
    {
        $client = User::findOrFail($id);
        $prefs = config('pref');
        return view('yamakara.clients.edit')->with(compact('client', 'prefs'));

    }

    public function update(Request $request, $id)
    {
        $client = User::findOrFail($id);
        $client->fill($request->all())->save();
        return redirect()->route('clients.show', $client->id);
    }

    public function createeventattendance($id)
    {
        $alleventprices = Eventprice::select('eventprices.*')->join('events', 'events.id', '=', 'eventprices.event_id')->where('depdate', '>=', Carbon::today())->orderBy('depdate', 'ASC')->get();
        $eventprices = $alleventprices->pluck('Tourname', 'id');
        $eventattendancepaymentmethods = Eventattendancepaymentmethod::all()->pluck('name', 'id');
        $eventattendancepaymentstatuses = Eventattendancepaymentstatus::all()->pluck('name', 'id');
        return view('yamakara.clients.createeventattendance')->with(compact('event', 'eventattendancepaymentmethods', 'eventattendancepaymentstatuses', 'eventprices', 'id'));
    }

    public function storeeventattendance(Request $request)
    {
        if(Eventattendance::where('user_id', $request->user_id)->where('eventprice_id', $request->eventprice_id)->exists())
        {
            return redirect()->route('clients.show', $request->user_id);
        }
        $eventattendance = new Eventattendance();
        $eventattendance->fill($request->all())->save();
        return redirect()->route('clients.show', $eventattendance->user_id);
    }

    public function selectmerge()
    {
        
    }
}
 
