<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;

use Field\Eventattendancepaymentmethod;

class EventattendancepaymentmethodsController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth:admin');
  }
    public function index(){
    	$eventattendancepaymentmethods = Eventattendancepaymentmethod::all();

    	return view('yamakara.eventattendancepaymentmethods.index')->with(compact('eventattendancepaymentmethods'));
    }

    public function store(Request $request){
    	$eventattendancepaymentmethod = new Eventattendancepaymentmethod();
    	$eventattendancepaymentmethod->fill($request->all())->save();
    	return redirect()->route('yamakara.eventattendancepaymentmethods');
    }

    public function edit($id) {
      $eventattendancepaymentmethod = Eventattendancepaymentmethod::findOrFail($id);
      return view('yamakara.eventattendancepaymentmethods.edit')->with(compact('eventattendancepaymentmethod'));
    }

    public function update(Request $request, $id) {
      $eventattendancepaymentmethod = Eventattendancepaymentmethod::findOrFail($id);
      $eventattendancepaymentmethod->fill($request->all())->save();
      return redirect()->route('yamakara.eventattendancepaymentmethods');
    }

    public function destroy($id) {
      $eventattendancepaymentmethod = Eventattendancepaymentmethod::findOrFail($id);
      $eventattendancepaymentmethod->delete();
      return redirect()->route('yamakara.eventattendancepaymentmethods');
  }
}
