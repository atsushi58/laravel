<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Field\Courselength;
use Field\Course;

class CourselengthsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
    	$courselengths = Courselength::all();
    	return view('yamakara.courselengths.index')->with(compact('courselengths'));
    }

    public function create()
    {
        return view('yamakara.courselengths.create');
    }


    public function store(Request $request)
    {
    	$courselength = new Courselength();
    	$courselength->fill($request->all())->save();
    	return redirect()->route('courselengths.index');
    }

    public function edit($id)
    {
        $courselength = Courselength::findOrFail($id);
        return view('yamakara.courselengths.edit')->with(compact('courselength'));
    }

    public function update(Request $request, $id)
    {
        $courselength = Courselength::findOrFail($id);
        $courselength->fill($request->all())->save();
        return redirect()->route('courselengths.index');
    }
}
