<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Field\Course;
use Field\Schedule;

class SchedulesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function create(Request $request, $id)
    {
        $course = Course::findOrFail($id);
        return view('yamakara.schedules.create')->with(compact('course'));
    }

    public function store(Request $request)
    {
        $schedule = new Schedule();
        $schedule->fill($request->all())->save();
        return redirect()->route('courses.show', $schedule->course_id);
    }

    public function edit($id)
    {
        $schedule = Schedule::findOrFail($id);
        $coursename = $schedule->course->name;
        return view('yamakara.schedules.edit')->with(compact('schedule', 'coursename'));
    }

    public function update(Request $request, $id)
    {
        $schedule = Schedule::findOrFail($id);
        $schedule->fill($request->all())->save();
        return redirect()->route('courses.show', $schedule->course_id);
    }

    public function destroy($id)
    {
        $schedule = Schedule::findOrFail($id);
        $schedule->delete();
        return redirect()->route('courses.show', $schedule->course_id);
    }
}
