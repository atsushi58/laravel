<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Field\Tourmailcategory;

class TourmailcategoriesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}
		
	public function index(){
		$tourmailcategories = Tourmailcategory::all();
		return view('yamakara.tourmailcategories.index')->with(compact('tourmailcategories'));
	}

	public function create()
	{
		return view('yamakara.tourmailcategories.create');

	}

	public function store(Request $request)
	{
		$tourmailcategory = new Tourmailcategory();
		$tourmailcategory->fill($request->all())->save();
		return redirect()->route('tourmailcategories.index');
	}
	
	public function edit($id)
	{
		$tourmailcategory = Tourmailcategory::findOrFail($id);
		return view('yamakara.tourmailcategories.edit')->with(compact('tourmailcategory'));
	}

	public function update(Request $request, $id)
	{
		$tourmailcategory = Tourmailcategory::findOrFail($id);
		$tourmailcategory->fill($request->all())->save();
		return redirect()->route('tourmailcategories.index');
	}
}
