<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Field\Eventbus;

class EventbusesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
    	$eventbuses = Eventbus::all();
    	return view('yamakara.eventbuses.index')->with(compact('eventbuses'));
    }

    public function create()
    {
        return view('yamakara.eventbuses.create');
    }

    public function store(Request $request)
    {
    	$eventbus = new Eventbus();
    	$eventbus->fill($request->all())->save();
    	return redirect()->route('eventbuses.index');
    }

    public function edit($id)
    {
        $eventbus = Eventbus::findOrFail($id);
        return view('yamakara.eventbuses.edit')->with(compact('eventbus'));
    }

    public function update(Request $request, $id)
    {
      $eventbus = Eventbus::findOrFail($id);
      $eventbus->fill($request->all())->save();
      return redirect()->route('eventbuses.index');
    }
}
