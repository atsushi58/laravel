<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;

use Field\Eventattendancestatus;

class EventattendancestatusesController extends Controller
{
  public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
    	$eventattendancestatuses = Eventattendancestatus::all();

    	return view('yamakara.eventattendancestatuses.index')->with(compact('eventattendancestatuses'));
    }

    public function create()
    {
        return view('yamakara.eventattendancestatuses.create');
    }
    public function store(Request $request)
    {
    	$eventattendancestatus = new Eventattendancestatus();
    	$eventattendancestatus->fill($request->all())->save();
    	return redirect()->route('eventattendancestatuses.index');
    }

    public function edit($id) {
      $eventattendancestatus = Eventattendancestatus::findOrFail($id);
      return view('yamakara.eventattendancestatuses.edit')->with(compact('eventattendancestatus'));
    }

    public function update(Request $request, $id) {
      $eventattendancestatus = Eventattendancestatus::findOrFail($id);
      $eventattendancestatus->fill($request->all())->save();
      return redirect()->route('eventattendancestatuses.index');
    }

}
