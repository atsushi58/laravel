<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;

use Field\Event;
use Field\Eventcost;
use Field\Eventcostcategory;
use Field\Eventcosttype;
use Field\Eventcostpaymentstatus;

class EventcostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function create($id)
    {
        $event = Event::findOrFail($id);
        $eventcostcategories = Eventcostcategory::all()->pluck('name','id');
        $eventcosttypes = Eventcosttype::all()->pluck('name','id');
        $eventcostpaymentstatuses = Eventcostpaymentstatus::all()->pluck('name','id');
        return view('yamakara.eventcosts.create')->with(compact('event', 'eventcostcategories', 'eventcosttypes', 'eventcostpaymentstatuses'));
    }

    public function store(Request $request){
    	$eventcost = new Eventcost();
    	$eventcost->fill($request->all())->save();
    	return redirect()->route('events.show', $eventcost->event_id);
    }

    public function edit($id) {
        $eventcost = Eventcost::findOrFail($id);
        $eventcostcategories = Eventcostcategory::all()->pluck('name','id');
        $eventcosttypes = Eventcosttype::all()->pluck('name','id');
        $eventcostpaymentstatuses = Eventcostpaymentstatus::all()->pluck('name','id');
      return view('yamakara.eventcosts.edit')->with(compact('eventcost', 'eventcostcategories', 'eventcosttypes', 'eventcostpaymentstatuses'));
    }

    public function destroy($id)
    {
        $eventcost = Eventcost::findOrFail($id);
        $eventcost->delete();
        return redirect()->back();
    }

    public function update(Request $request, $id) {
      $eventcost = Eventcost::findOrFail($id);
      $eventcost->fill($request->all())->save();
        return redirect()->route('events.show', $eventcost->event_id);
    }

    public function unpaid()
    {
        $eventcosts = Eventcost::where('eventcostpaymentstatus_id', '3')->get();
        return view('yamakara.eventcosts.unpaid')->with(compact('eventcosts'));
    }

    public function pay($id)
    {
        $eventcost = Eventcost::findOrFail($id);
        $eventcost->eventcostpaymentstatus_id = 4;
        $eventcost->save();
        return redirect()->route('eventcosts.unpaid');
    }
}
