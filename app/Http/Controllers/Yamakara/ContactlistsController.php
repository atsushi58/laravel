<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Field\Contact;
use Field\Contactlist;

class ContactlistsController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index()
	{
		$contactlists = Contactlist::all();
		return view('yamakara.contactlists.index')->with(compact('contactlists'));
	}

	public function show($id)
	{
		$contactlist = Contactlist::findOrFail($id);
		return view('yamakara.contactlists.show')->with(compact('contactlist'));
	}

	public function create()
	{
		return view('yamakara.contactlists.create');
	}

	public function store(Request $request)
	{
		$contactlist = new Contactlist();
		$contactlist->fill($request->all())->save();
		return redirect()->route('contactlists.index');
	}

	public function edit($id)
	{
		$contactlist = Contactlist::findOrFail($id);
		return view('yamakara.contactlists.edit')->with(compact('contactlist'));
	}

	public function update(Request $request, $id)
	{
		$contactlist = Contactlist::findOrFail($id);
		$contactlist->fill($request->all())->save();
		return redirect()->route('contactlists.index');
	}

	public function createcontact($contactlist_id)
	{
		$contactlist = Contactlist::findOrFail($contactlist_id);
		return view('yamakara.contactlists.createcontact')->with(compact('contactlist'));
	}

	public function storecontact(Request $request)
	{
		$contact =  new Contact();
		$contact->fill($request->all())->save();
		return redirect()->route('contactlists.show', $request->contactlist_id);
	}

	public function editcontact($id)
	{
		$contact = Contact::findOrFail($id);
		return view('yamakara.contactlists.editcontact')->with(compact('contact'));
	}

	public function updatecontact(Request $request, $id)
	{
		$contact = Contact::findOrFail($id);
		$contact->fill($request->all())->save();
		return redirect()->route('contactlists.show', $contact->contactlist_id);
	}


}
