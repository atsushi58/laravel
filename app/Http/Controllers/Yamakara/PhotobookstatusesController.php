<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;

use Field\Photobookstatus;

class PhotobookstatusesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $photobookstatuses = Photobookstatus::all();
        return view('yamakara.photobookstatuses.index')->with(compact('photobookstatuses'));
    }

    public function create()
    {
        return view('yamakara.photobookstatuses.create');
    }


    public function store(Request $request)
    {
        $photobookstatus = new Photobookstatus();
        $photobookstatus->fill($request->all())->save();
        return redirect()->route('photobookstatuses.index');
    }

    public function edit($id)
    {
        $photobookstatus = Photobookstatuse::findOrFail($id);
        return view('yamakara.photobookstatuses.edit')->with(compact('photobookstatuse'));
    }

    public function update(Request $request, $id) {
      $photobookstatus = Photobookstatus::findOrFail($id);
      $photobookstatus->fill($request->all())->save();        
      return redirect()->route('photobookstatuses.index');
    }
}
