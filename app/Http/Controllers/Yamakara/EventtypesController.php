<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Field\Eventtype;

class EventtypesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}
		
	public function index(){
		$eventtypes = Eventtype::all();
		return view('yamakara.eventtypes.index')->with('eventtypes', $eventtypes);
	}

	public function create()
	{
		return view('yamakara.eventtypes.create');

	}

	public function store(Request $request)
	{
		$eventtype = new Eventtype();
		$eventtype->fill($request->all())->save();
		return redirect()->route('eventtypes.index');
	}
	
	public function edit($id)
	{
		$eventtype = Eventtype::findOrFail($id);
		return view('yamakara.eventtypes.edit')->with(compact('eventtype'));
	}

	public function update(Request $request, $id)
	{
		$eventtype = Eventtype::findOrFail($id);
		$eventtype->fill($request->all())->save();
		return redirect()->route('eventtypes.index');
	}
}
