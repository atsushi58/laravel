<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;

use Field\Eventcostcategory;

class EventcostcategoriesController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth:admin');
  }
    public function index(){
    	$eventcostcategories = Eventcostcategory::all();
    	return view('yamakara.eventcostcategories.index')->with(compact('eventcostcategories'));
    }

    public function create()
    {
        return view('yamakara.eventcostcategories.create');
    }

    public function store(Request $request){
    	$eventcostcategory = new Eventcostcategory();
    	$eventcostcategory->fill($request->all())->save();
    	return redirect()->route('eventcostcategories.index');
    }

    public function edit($id) {
      $eventcostcategory = Eventcostcategory::findOrFail($id);
      return view('yamakara.eventcostcategories.edit')->with(compact('eventcostcategory'));
    }

    public function update(Request $request, $id) {
      $eventcostcategory = Eventcostcategory::findOrFail($id);
      $eventcostcategory->fill($request->all())->save();
      return redirect()->route('eventcostcategories.index');
    }
}
