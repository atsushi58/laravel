<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Field\Topslider;
use Field\Course;

class TopslidersController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index(Request $request){
		$topsliders = Topslider::all();
		$courses = Course::all()->pluck('name', 'id');
		return view('yamakara.topsliders.index')->with(compact('topsliders', 'courses'));
	}

	public function store(Request $request){
    	$topslider = new Topslider();
    	$filename = $request->sliderimage->getClientOriginalName();
    	$image = Image::make($request->sliderimage->getRealPath());
    	$image->fit('970', '524')
    		  ->save(public_path() . '/topsliders/' . $filename)
    		  ->resize(200, null, function ($constraint) {$constraint->aspectRatio();})
      		  ->save(public_path() . '/topsliders/' . '/thumbnail/' . $filename);
    	$topslider->filename = $filename;
    	// $topslider->path = $request->sliderimage->getRealPath();
        $topslider->fill($request->all())->save();
    	return redirect()->route('yamakara.topsliders');
    }

    public function destroy($id){
        $topslider = Topslider::findOrFail($id);
        $topslider->delete();
        unlink(public_path() . '/topsliders/'.$topslider->filename);
        unlink(public_path() . '/topsliders/thumbnail/'.$topslider->filename);
        return redirect()->route('yamakara.topsliders');
    }
}

