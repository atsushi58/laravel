<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Field\Inquirystatus;

class InquirystatusesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

    public function index(){
        $inquirystatuses = Inquirystatus::all();
        return view('yamakara.inquirystatuses.index')->with(compact('inquirystatuses'));
    }

    public function create()
    {
        return view('yamakara.inquirystatuses.create');
    }

    public function store(Request $request){
        $inquirystatus = new Inquirystatus();
        $inquirystatus->fill($request->all())->save();
        return redirect()->route('inquirystatuses.index');
    }

    public function edit($id) {
        $inquirystatus = Inquirystatus::findOrFail($id);
        return view('yamakara.inquirystatuses.edit')->with(compact('inquirystatus'));
    }

    public function update(Request $request, $id) {
        $inquirystatus = Inquirystatus::findOrFail($id);
        $inquirystatus->fill($request->all())->save();
        return redirect()->route('inquirystatuses.index');
    }
}

