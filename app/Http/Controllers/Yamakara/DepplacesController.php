<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Field\Depplace;

class DepplacesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    //
    public function index()
    {
    	$depplaces = Depplace::all();
    	return view('yamakara.depplaces.index')->with(compact('depplaces'));
    }

    public function create()
    {
        return view('yamakara.depplaces.create');
    }

    public function edit($id){
        $depplace = Depplace::findOrFail($id);
        return view('yamakara.depplaces.edit')->with(compact('depplace'));
    }

    public function store(Request $request){
    	$depplace = new Depplace();
        $depplace->fill($request->all())->save();
    	return redirect()->route('depplaces.index');
    }

    public function update(Request $request, $id) {
        $depplace = Depplace::findOrFail($id);
        $depplace->fill($request->all())->save();
        return redirect()->route('depplaces.index');
    }

    public function destroy($id) {
      $depplace = Depplace::findOrFail($id);
      $depplace->delete();
      return redirect()->route('depplaces.index');
  }
}
