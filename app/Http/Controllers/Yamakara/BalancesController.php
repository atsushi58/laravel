<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;

use Illuminate\Support\Facades\Mail;
use Sichikawa\LaravelSendgridDriver\SendGrid;
use Field\Mail\Yamakara\Inbyuser;
use Field\Mail\Yamakara\Pay;

use Field\Balance;
use Field\Balancetype;
use Field\Eventattendance;
use Field\User;

class BalancesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index()
	{
		$balances = Balance::all();
		$description = '全て';
		return view('yamakara.balances.index')->with(compact('balances', 'description'));
	}

	public function bymonth($year, $month)
	{
		$balances = Balance::whereYear('day', '=', $year)->whereMonth('day', '=', $month)->get();
		$description = $year. '年'. $month. '月';
		return view('yamakara.balances.index')->with(compact('balances', 'description'));
	}

	public function create()
	{
		$allclients = User::orderBy('kana')->get();
        $clients = $allclients->pluck('namelist', 'id');
        $balancetypes = Balancetype::pluck('name', 'id');
		return view('yamakara.balances.create')->with(compact('clients', 'balancetypes'));
	}

	public function store(Request $request)
	{
		$balance = new Balance();
		$balance->fill($request->all())->save();
		return redirect()->route('balances.index');
	}

	public function inbyuser($user_id)
	{
		$client = User::findOrFail($user_id);
		$balancetypes = Balancetype::whereIn('id', [1,2,6,10])->pluck('name', 'id');
		return view('yamakara.balances.inbyuser')->with(compact('balancetypes', 'client'));
	}

	public function inbyuserstore(Request $request)
	{
		$balance = new Balance();
		$balance->fill($request->all())->save();
		return redirect()->route('clients.show', $balance->user_id);
	}

	public function inbyuserwm($user_id)
	{
		$client = User::findOrFail($user_id);
		$balancetypes = Balancetype::whereIn('id', [1,2,6,10])->pluck('name', 'id');
		return view('yamakara.balances.inbyuserwm')->with(compact('balancetypes', 'client'));
	}

	public function inbyuserstorewm(Request $request)
	{
		$balance = new Balance();
		$balance->fill($request->all())->save();
		//入金のメール送信
		Mail::to($balance->user->email)->cc('yamakara@field-mt.com')->send(new Inbyuser($balance));
		$user = User::findOrFail($request->user_id);
		$unpaideventattendances = Eventattendance::select('eventattendances.*')->where('user_id', $user->id)->where('eventattendancestatus_id', 2)->whereIn('eventattendancepaymentstatus_id', [1,3])->whereNotIn('eventattendancepaymentmethod_id', [5,6])->join('eventprices', 'eventprices.id', '=', 'eventattendances.eventprice_id')->join('events', 'eventprices.event_id', '=', 'events.id')->orderBy('events.depdate', 'ASC')->get();
		$totaldeposit = $user->Deposit;
		// 支払い待ち残高総額より高額なら支払い済みに

		foreach($unpaideventattendances as $eventattendance)
		{
			if($totaldeposit - $eventattendance->Sales >= 0 && $eventattendance->Sales != 0)
			{
				$balance = new Balance();
				$eventattendance = Eventattendance::findOrFail($eventattendance->id);
				$balance->user_id = $user->id;
				$balance->day = date('Y-m-d');
				$balance->balancetype_id = 3;
				$balance->eventattendance_id = $eventattendance->id;
				$balance->deposit = -$eventattendance->Sales;
				$balance->save();
				$eventattendance->eventattendancepaymentstatus_id = 2;
				$eventattendance->save();
				$totaldeposit = $totaldeposit - $eventattendance->Sales; 
				if($balance->user->email){
				Mail::to($balance->user->email)->cc('yamakara@field-mt.com')->send(new Pay($balance));	
				}
			}else
			{
				break;
			}
		}
		return redirect()->route('clients.show', $balance->user_id);
	}

	public function createpay($user_id)
	{
		$client = User::findOrFail($user_id);
		$unpaideventattendances = $client->eventattendances->whereIn('eventattendancepaymentstatus_id', [1,3])->where('eventattendancestatus_id', 2)->pluck('tourname', 'id');
		return view('yamakara.balances.createpay')->with(compact('client', 'unpaideventattendances'));
	}

	public function createpaywm($user_id)
	{
		$client = User::findOrFail($user_id);
		$unpaideventattendances = $client->eventattendances->whereIn('eventattendancepaymentstatus_id', [1,3])->where('eventattendancestatus_id', 2)->pluck('tourname', 'id');
		return view('yamakara.balances.createpaywm')->with(compact('client', 'unpaideventattendances'));
	}

	public function storepay(Request $request)
	{
		$balance = new Balance();
		$eventattendance = Eventattendance::findOrFail($request->eventattendance_id);
		$deposit = $eventattendance->Sales;
		$balance->deposit = -$deposit;
		$balance->fill($request->all())->save();
		$eventattendance->eventattendancepaymentstatus_id = 2;
		$eventattendance->save();
		return redirect()->route('clients.show', $request->user_id);
	}

	public function storepaywm(Request $request)
	{
		$balance = new Balance();
		$eventattendance = Eventattendance::findOrFail($request->eventattendance_id);
		$deposit = $eventattendance->Sales;
		$balance->deposit = -$deposit;
		$balance->fill($request->all())->save();
		$eventattendance->eventattendancepaymentstatus_id = 2;
		$eventattendance->save();
		Mail::to($balance->user->email)->cc('yamakara@field-mt.com')->send(new Pay($balance));
		return redirect()->route('clients.show', $request->user_id);
	}

	public function createcxl($balance_id)
	{
		$balance = Balance::findOrFail($balance_id);
		return view('yamakara.balances.createcxl')->with(compact('balance'));
	}

	public function storecxl(Request $request)
	{ 
		$balance = new Balance();
		$balance->fill($request->all());
		$balance->deposit = $balance->suspend - $request->cxlcharge;
		$balance->suspend = -$balance->suspend;
		$balance->save();
		return redirect()->route('clients.show', $balance->user_id);
	}

	public function edit($id)
	{
		$balance = Balance::findOrFail($id);
		$balancetypes = Balancetype::pluck('name', 'id');
		return view('yamakara.balances.edit')->with(compact('balance', 'balancetypes'));
	}

	public function update(Request $request, $id)
	{
		$balance = Balance::findOrFail($id);
		$balance->fill($request->all())->save();
		return redirect()->route('clients.show', $balance->user_id);
	}

	public function destroy($id)
	{
		$balance = Balance::findOrFail($id);
		$balance->delete();
		return redirect()->route('clients.show', $balance->user_id);

	}
}
