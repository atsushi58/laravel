<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;

use Field\Depplace;
use Field\Event;
use Field\Eventprice;

class EventpricesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $eventprices = Eventprice::all();
        return view('yamakara.eventprices.index')->with(compact('eventprices'));
    }

    public function create($id)
    {
        $event = Event::findOrFail($id);
        $depplaces = Depplace::all()->pluck('name', 'id');
        return view('yamakara.eventprices.create')->with(compact('event', 'depplaces'));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'price' => 'required|integer',
        ]);

        $eventprice = new Eventprice();
        $eventprice->fill($request->all())->save();
        return redirect()->route('events.show', $eventprice->event_id);
    }

    public function edit($id)
    {
        $eventprice = Eventprice::findOrFail($id);
        $depplaces = Depplace::all()->pluck('name', 'id');
        return view('yamakara.eventprices.edit')->with(compact('eventprice', 'depplaces'));
    }

    public function update(Request $request, $id) {
      $eventprice = Eventprice::findOrFail($id);
      $eventprice->fill($request->all())->save();
      return redirect()->route('events.show', $eventprice->event_id);
    }

    public function destroy($id) {
      $eventstatus = Eventstatus::findOrFail($id);
      $eventstatus->delete();
      return redirect()->route('yamakara.eventstatuses');
  }
}
