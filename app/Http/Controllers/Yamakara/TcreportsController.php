<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;

use Field\Tcreport;
use Field\Tctimereport;

class TcreportsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $tcreports = Tcreport::all();
        return view('yamakara.tcreports.index')->with(compact('tcreports'));
    }

    public function show($id)
    {
        $tcreport = Tcreport::findOrFail($id);
        return view('yamakara.tcreports.show')->with(compact('tcreport'));
    }

    public function create($event_id)
    {
        return view('yamakara.tcreports.create')->with(compact('event_id'));
    }


    public function store(Request $request)
    {
        $tcreport = new Tcreport();
        $tcreport->fill($request->all())->save();
        return redirect()->route('tcreports.show', $tcreport->id);
    }

    public function edit($id)
    {
        $tcreport = Tcreport::findOrFail($id);
        return view('yamakara.tcreports.edit')->with(compact('tcreport'));
    }

    public function update(Request $request, $id) {
      $tcreport = Tcreport::findOrFail($id);
      $tcreport->fill($request->all())->save();        
      return redirect()->route('tcreports.show', $tcreport->id);
    }

    public function createtimereport($tcreport_id)
    {
        return view('yamakara.tcreports.createtimereport')->with(compact('tcreport_id'));
    }

    public function storetimereport(Request $request)
    {
        $timereport = new Tctimereport();
        $request->uptime = $request->uptime_date.$request->uptime_time;
        $timereport->fill($request->all())->save();
        return redirect()->route('tcreports.show', $timereport->tcreport_id);
    }

    public function edittimereport($tctimereport_id)
    {
        $timereport = Tctimereport::findOrFail($tctimereport_id);
        return view('yamakara.tcreports.edittimereport')->with(compact('timereport'));
    }

    public function updatetimereport(Request $request, $tctimereport_id)
    {
        $timereport = Tctimereport::findOrFail($tctimereport_id);
        $timereport->fill($request->all())->save();
        return redirect()->route('tcreports.show', $timereport->tcreport_id);
    }
}
