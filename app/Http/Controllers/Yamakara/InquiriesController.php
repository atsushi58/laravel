<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Field\Inquiry;

class InquiriesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

    public function index(){
    	$inquiries = Inquiry::all();
        return view('yamakara.inquiries.index')->with(compact('inquiries'));
    }

    public function show($id){
    	$inquiry = Inquiry::findOrFail($id);
    	return view('yamakara.inquiries.show')->with(compact('inquiry'));
    }
}

