<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;

use Illuminate\Support\Facades\Mail;
use Sichikawa\LaravelSendgridDriver\SendGrid;
use Field\Mail\Yamakara\Cancel;
use Field\Mail\Yamakara\Attend;
use Field\Mail\Yamakara\Point;
use Field\Mail\Yamakara\Attendcheck;
use Field\Mail\Yamakara\Inbyuser;
use Field\Mail\Yamakara\Paytoday;

use Field\Balance;
use Field\Event;
use Field\Eventattendance;
use Field\Eventattendancepaymentmethod;
use Field\Eventattendancepaymentstatus;
use Field\Eventattendancestatus;
use Field\Eventprice;
use Field\Rentaldelivery;
use Field\User;
use Field\Tourmailtemplate;


class EventattendancesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index($year, $month)
    { 
        $events = Event::whereYear('depdate', $year)->whereMonth('depdate', $month)->get();
        $eventpriceids = [];
        foreach($events as $event){
            foreach($event->eventprices as $eventprice){
                $eventpriceids[] = $eventprice->id;
            }
        }
        $eventattendances = Eventattendance::whereIn('eventprice_id', $eventpriceids)->whereNotNull('eventattendancestatus_id')->get();
        $description = '('. $year. '年'. $month. '月)';
        return view('yamakara.eventattendances.index')->with(compact('eventattendances', 'description'));
    }

    public function bydate($date)
    {
        $eventattendances = Eventattendance::whereDate('created_at', '=', $date)->get();
        $description = '('. $date. ')';
        return view('yamakara.eventattendances.index')->with(compact('eventattendances', 'description'));
    }

    public function point($year, $month)
    {
        $events = Event::whereYear('depdate', $year)->whereMonth('depdate', $month)->get();
        $eventpriceids = [];
        foreach($events as $event){
            foreach($event->eventprices as $eventprice){
                $eventpriceids[] = $eventprice->id;
            }
        }
        $eventattendances = Eventattendance::whereIn('eventprice_id', $eventpriceids)->where('eventattendancepaymentmethod_id', 5)->get();
        $description = '('. $year. '年'. $month. '月)ポイント利用';
        return view('yamakara.eventattendances.index')->with(compact('eventattendances', 'description'));
    }

    public function multiple()
    {
        $events = Event::where('depdate', '>', date('Y-m-d'))->get();
        $eventpriceids = [];
        foreach($events as $event){
            foreach($event->eventprices as $eventprice){
                $eventpriceids[] = $eventprice->id;
            }

        }
        $eventattendances = Eventattendance::whereIn('eventprice_id', $eventpriceids)->where('numofpeople', '>', 1)->whereNotIn('eventattendancestatus_id', [6])->get();
        $description = '複数人参加';
        return view('yamakara.eventattendances.index')->with(compact('eventattendances', 'description'));
    }

    public function show($id)
    {
    	$eventattendance = Eventattendance::findOrFail($id);
    	return view('yamakara.eventattendances.show')->with(compact('eventattendance'));
    }

    public function edit($id)
    {
        $eventattendance = Eventattendance::findOrFail($id);
        $allclients = User::orderBy('kana')->get();
        $clients = $allclients->pluck('namelist', 'id');
        $event = $eventattendance->eventprice->event;
        $eventprices = $event->eventprices->pluck('name','id');
        $eventattendancestatuses = Eventattendancestatus::all()->pluck('name', 'id');
        $eventattendancepaymentmethods = Eventattendancepaymentmethod::all()->pluck('name', 'id');
        $eventattendancepaymentstatuses = Eventattendancepaymentstatus::all()->pluck('name', 'id');
        return view('yamakara.eventattendances.edit')->with(compact('clients', 'eventattendance', 'eventattendancepaymentmethods', 'eventattendancepaymentstatuses', 'eventattendancestatuses', 'eventprices'));
    }

    public function update(Request $request, $id) {
      $eventattendance = Eventattendance::findOrFail($id);
      $eventattendance->fill($request->all())->save();
      return redirect()->route('events.show', $eventattendance->eventprice->event->id);
    }

    public function unpaid($date)
    {
        $events = Event::where('depdate', $date)->get();
        $eventpriceids = [];
        foreach($events as $event){
            foreach($event->eventprices as $eventprice){
                $eventpriceids[] = $eventprice->id;
            }
        }
        $eventattendances = Eventattendance::whereIn('eventprice_id', $eventpriceids)->where('eventattendancestatus_id', 2)->where('eventattendancepaymentstatus_id', 1)->get();
        $description = '('. $date. ')';
        return view('yamakara.eventattendances.unpaid')->with(compact('eventattendances', 'description'));
    }

    public function unpack()
    {
        $events = Event::where('depdate', '<', date("Y-m-d", strtotime("+2 week")))->where('depdate', '>', date("Y-m-d"))->get();
        $eventpriceids = [];
        foreach($events as $event){
            foreach($event->eventprices as $eventprice){
                $eventpriceids[] = $eventprice->id;
            }
        }
        $description = '未梱包一覧('. date("Y-m-d"). 'の2週間後までの未梱包)';
        $eventattendancesa = Eventattendance::whereIn('eventprice_id', $eventpriceids)->whereNull('rentalpack')->where('eventattendancestatus_id', 2)->whereNotNull('rental')->get();
        $eventattendancesb = Eventattendance::select('eventattendances.*')->whereIn('eventprice_id', $eventpriceids)->whereNull('rentalpack')->where('eventattendancestatus_id', 2)->join('users', 'users.id', '=', 'eventattendances.user_id')->whereNotNull('defaultrental')->get();
        $eventattendances = $eventattendancesa->merge($eventattendancesb);
        return view('yamakara.eventattendances.unpack')->with(compact('eventattendances', 'description'));
    }

    public function undeliver()
    {
        $description = '梱包済未配送一覧';
        $eventattendancesa = Eventattendance::whereNotNull('rentalpack')->where('eventattendancestatus_id', 2)->where('rentaldelivery_id', 3)->wherenull('rentaldeliver')->whereNotNull('rental')->get();
        $eventattendancesb = Eventattendance::select('eventattendances.*')->whereNotNull('rentalpack')->where('eventattendancestatus_id', 2)->where('rentaldelivery_id', 3)->wherenull('rentaldeliver')->join('users', 'users.id', '=', 'eventattendances.user_id')->whereNotNull('defaultrental')->get();
        $eventattendances = $eventattendancesa->merge($eventattendancesb);
        return view('yamakara.eventattendances.undeliver')->with(compact('eventattendances', 'description'));

    }

    public function rentalall()
    {
        $events = Event::where('depdate', '<', date("Y-m-d", strtotime("+2 week")))->where('depdate', '>', date("Y-m-d"))->get();
        $eventpriceids = [];
        foreach($events as $event){
            foreach($event->eventprices as $eventprice){
                $eventpriceids[] = $eventprice->id;
            }
        }
        $description = 'レンタル全一覧('. date("Y-m-d"). 'の2週間後までの全一覧)';
        $eventattendances = Eventattendance::whereIn('eventprice_id', $eventpriceids)->where('eventattendancestatus_id', 2)->get();
        return view('yamakara.eventattendances.unpack')->with(compact('eventattendances', 'description'));
    }

    public function editrental($id)
    {
        $eventattendance = Eventattendance::findOrFail($id);
        $rentaldeliveries = Rentaldelivery::all()->pluck('name', 'id');
        return view('yamakara.eventattendances.editrental')->with(compact('eventattendance', 'rentaldeliveries'));
    }

    public function updaterental(Request $request, $id)
    {
        $eventattendance = Eventattendance::findOrFail($id);
        $eventattendance->fill($request->all())->save();
        return redirect()->route('eventattendances.booked', $request->bookdate);
    }

    public function edittourrental($id)
    {
        $eventattendance = Eventattendance::findOrFail($id);
        $rentaldeliveries = Rentaldelivery::all()->pluck('name', 'id');
        return view('yamakara.eventattendances.editrental')->with(compact('eventattendance', 'rentaldeliveries'));
    }

    public function updatetourrental(Request $request, $id)
    {
        $eventattendance = Eventattendance::findOrFail($id);
        $eventattendance->fill($request->all())->save();
        return redirect()->route('events.show', $eventattendance->eventprice->event_id);
    }

    public function cancel($id)
    {
        $eventattendance = Eventattendance::findOrFail($id);
        $eventattendance->eventattendancestatus_id = 6;
        $eventattendance->save();
        // 入金済みならキャンセル料計算待ちへ
        if($eventattendance->eventattendancepaymentstatus_id == 2)
        {
            $balance = new Balance();
            $balance->user_id = $eventattendance->user_id;
            $balance->eventattendance_id = $id;
            $balance->day = date("Y-m-d");
            $balance->balancetype_id = 4;
            $balance->suspend = $eventattendance->Sales;
            $balance->save();
        }
        //キャンセルメール送信
        if($eventattendance->user->email){
        Mail::to($eventattendance->user->email)->cc('yamakara@field-mt.com')->send(new Cancel($eventattendance));
        }
        //キャンセル待ちがいたら繰上
        $event = Event::findOrFail($eventattendance->eventprice->event_id);
        if($event->Waitingclients != "[]")
        {
            $numofpossible = $event->maxpax - $event->Activetotalclients;
            if($numofpossible > 0)
            {
                for($i = 0;$i < $numofpossible; $i++)
                {
                    $person = $event->Clients->where('eventattendancestatus_id', 4)->first();
                    $personattendance = Eventattendance::findOrFail($person->id);
                    $personattendance->eventattendancestatus_id = 5;
                    $personattendance->save();
                    Mail::to($person->user->email)->cc('yamakara@field-mt.com')->send(new Attendcheck($person));
                }
            }
        }else
        {
        // キャンセル待ちがいなければツアーステータス判定
            if($event->Totalpossibleclients < $event->maxpax){
            }
        }
        return redirect()->back();
    }

    public function attend($id)
    {
        $eventattendance = Eventattendance::findOrFail($id);
        $eventattendance->eventattendancestatus_id = 2;
        $eventattendance->save();
        $eventprice = Eventprice::findOrFail($eventattendance->eventprice_id);
        $event = Event::findOrFail($eventprice->event_id);
        $tourmailtemplate = Tourmailtemplate::findOrFail($event->tourmailtemplate_id);
        Mail::to($eventattendance->user->email)->cc('yamakara@field-mt.com')->send(new Attend($eventattendance, $tourmailtemplate));
        return redirect()->back();
    }

    public function attendcheck($id)
    {
        $eventattendance = Eventattendance::findOrFail($id);
        Mail::to($eventattendance->user->email)->cc('yamakara@field-mt.com')->send(new Attendcheck($eventattendance));
        return redirect()->back();
    }

    public function rentalpack($id)
    {
        $eventattendance = Eventattendance::findOrFail($id);
        $eventattendance->rentalpack = 1;
        $eventattendance->save();
        return redirect()->back();
    }

    public function rentaldeliver($id)
    {
        $eventattendance = Eventattendance::findOrFail($id);
        $eventattendance->rentaldeliver = 1;
        $eventattendance->save();
        return redirect()->back();
    }

    public function inpay($id)
    {
        $eventattendance = Eventattendance::findOrFail($id);
        if($eventattendance->eventattendancepaymentmethod_id == '5')
        {
            $eventattendance->eventattendancepaymentstatus_id = 2;
            $eventattendance->save();
            Mail::to($eventattendance->user->email)->cc('yamakara@field-mt.com')->send(new Point($eventattendance));
        }else
        {
            $balancein = new Balance();
            $balancein->user_id = $client = $eventattendance->user_id;
            $balancein->day = date('Y-m-d');
            $balancein->balancetype_id = 2;
            $balancein->deposit = $eventattendance->Sales;
            $balancein->save();
            Mail::to($balancein->user->email)->cc('yamakara@field-mt.com')->send(new Inbyuser($balancein));
            $balancepay = new Balance();
            $balancepay->user_id = $client = $eventattendance->user_id;
            $balancepay->balancetype_id = 3;
            $balancepay->day = date('Y-m-d');
            $balancepay->eventattendance_id = $eventattendance->id;
            $deposit = $eventattendance->Sales;
            $balancepay->deposit = -$deposit;
            $balancepay->save();
            $eventattendance->eventattendancepaymentstatus_id = 2;
            $eventattendance->save();
            Mail::to($balancepay->user->email)->cc('yamakara@field-mt.com')->send(new Paytoday($balancepay));
        }
        return redirect()->back();
    }

    public function zerotoone()
    {
        $eventattendances = Eventattendance::all();
        foreach($eventattendances as $eventattendance)
        {
            if(empty($eventattendance->numofpeople) || $eventattendance->numofpeople == 0)
            {
                $eventattendance->numofpeople =1;
                $eventattendance->save();
            }
        }

        return redirect()->route('eventattendances.show', ['2018','02']);
    }

    public function createrental($id)
    {
        $eventattendance = Eventattendance::findOrFail($id);
        return view('yamakara.eventattendances.creeaterental')->with(compact('eventattendance'));
    }

    public function storerental(Request $request, $id)
    {
        $eventattendance = new Eventattendance();
        $eventattendance->fill($request->all())->save();
        return redirect()->route('events.show', $eventattendance->eventprice->event->id);
    }

    public function booked($date)
    {
        $eventattendances = Eventattendance::whereDate('bookdate', '=', $date)->get();
        $description = '来店予約一覧('. date("Y-m-d"). ')';
        return view('yamakara.eventattendances.unpack')->with(compact('eventattendances', 'description'));

    }
}
