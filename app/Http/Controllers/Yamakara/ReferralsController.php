<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;

use Field\Eventattendance;
use Field\Referral;
use Field\User;

class ReferralsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $referrals = Referral::all();
        return view('yamakara.referrals.index')->with(compact('referrals'));
    }

    public function prepoint()
    {
        $referrals = Referral::where('stamp', '=', 0)->get();
        return view('yamakara.referrals.index')->with(compact('referrals'));
    }

    public function point($id)
    {
        $referral = Referral::findOrFail($id);
        $referral->stamp = 1;
        $referral->save();
        return redirect()->route('referrals.prepoint');
    }

    public function create($eventattendance_id)
    {
        $eventattendance = Eventattendance::findOrFail($eventattendance_id);
        $allclients = User::orderBy('kana')->get();
        $clients = $allclients->pluck('namelist', 'id');
        return view('yamakara.referrals.create')->with(compact('eventattendance', 'clients'));
    }

    public function store(Request $request)
    {
        $referral = new Referral();
        $referral->fill($request->all())->save();
        return redirect()->route('events.show', $referral->eventattendance->eventprice->event_id);
    }

    public function edit($id)
    {
        $referral = Referral::findOrFail($id);
        $allclients = User::orderBy('kana')->get();
        $clients = $allclients->pluck('namelist', 'id');
        return view('yamakara.referrals.edit')->with(compact('referral', 'clients'));
    }

    public function update(Request $request, $id) {
        $referral = Referral::findOrFail($id);
        $referral->fill($request->all())->save();
        return redirect()->route('referrals.prepoint');
    }
}
