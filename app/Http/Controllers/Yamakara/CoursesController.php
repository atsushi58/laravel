<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Sichikawa\LaravelSendgridDriver\SendGrid;
use Field\Yamakara\In;
use Field\Yamakara\Pay;
use Field\Admin;
use Field\Contactlist;
use Field\Course;
use Field\Coursearea;
use Field\Courselength;
use Field\Depplace;
use Field\Event;
use Field\Eventstatus;
use Field\Eventtype;
use Field\Schedule;
use Field\Tourimage;

class CoursesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
    	$courses = Course::all();
        $courseareas = Coursearea::all();
        $description = '(全て)';
    	return view('yamakara.courses.index')->with(compact('courses', 'courseareas', 'description'));
    }

    public function coursesbyarea($id)
    {
        $coursearea = Coursearea::findOrFail($id);
        $courses = $coursearea->courses;
        $courseareas = Coursearea::all();
        $description = '('. $coursearea->name .')';
        return view('yamakara.courses.index')->with(compact('courses', 'courseareas', 'description'));   
    }

    public function show($id)
    {
    	$course = Course::findOrFail($id);
        $events = $course->events->sortByDesc('depdate');
    	return view('yamakara.courses.show')->with(compact('course', 'events'));
    }

    public function create()
    {
    	$courseareas = Coursearea::all()->pluck('name', 'id');
        $courselengths = Courselength::all()->pluck('name', 'id');
        $depplaces = Depplace::all()->pluck('name', 'id');
        $eventtypes = Eventtype::all()->pluck('name', 'id');
    	return view('yamakara.courses.create')->with(compact('courseareas', 'courselengths', 'depplaces', 'eventtypes'));
    }

    public function store(Request $request)
    {
    	$course = new Course();
    	$course->fill($request->all())->save();
    	return redirect()->route('courses.index');
    }

    public function edit($id)
    {
        $course = Course::findOrFail($id);
        $courseareas = Coursearea::pluck('name', 'id');
        $courselengths = Courselength::pluck('name', 'id');
        $depplaces = Depplace::all()->pluck('name', 'id');
        $eventtypes = Eventtype::pluck('name', 'id');
        return view('yamakara.courses.edit')->with(compact('course', 'courseareas', 'depplaces', 'courselengths', 'eventtypes'));
    }

    public function update(Request $request, $id)
    {
        $course = Course::findOrFail($id);
        $course->fill($request->all())->save();
        return redirect()->route('courses.index');
    }

    public function destroy($id)
    {
      $course = Course::findOrFail($id);
      $course->delete();
      return redirect()->route('courses.index');
    }

    public function createschedule(Request $request, $id)
    {
        $course = Course::findOrFail($id);
        return view('yamakara.courses.createschedule')->with(compact('course'));
    }

    public function storeschedule(Request $request, $id)
    {
        $course = Course::findOrFail($id);
        $schedule = new Schedule();
        $schedule->fill($request->all())->save();
        return redirect()->route('courses.show', $course->id);
    }

    public function editschedule($id)
    {
        $schedule = Schedule::findOrFail($id);
        $coursename = $schedule->course->name;
        return view('yamakara.courses.editschedule')->with(compact('schedule', 'coursename'));
    }

    public function updateschedule(Request $request, $id)
    {
        $schedule = Schedule::findOrFail($id);
        $schedule->fill($request->all())->save();
        return redirect()->route('courses.show', $schedule->course_id);
    }

    public function destroyschedule($id)
    {
        $schedule = Schedule::findOrFail($id);
        $schedule->delete();
        return redirect()->route('courses.show', $schedule->course_id);
    }

    public function storetourimage(Request $request, $id)
    {
        $course = Course::findOrFail($id);
        $tourimage = new Tourimage();
        $filename = $request->tourimage->getClientOriginalName();
        $image = Image::make($request->tourimage->getRealPath());
        $image->fit('970', '524')
              ->save(public_path() . '/tourimages/' . $filename)
              ->resize(200, 200, function ($constraint) {$constraint->aspectRatio();})
              ->save(public_path() . '/tourimages/' . 'thumbnail/' . $filename);
        $tourimage->filename = $filename;
        $tourimage->fill($request->all())->save();
        return redirect()->route('courses.show', $id);
    }

    public function destroytourimage($id)
    {
        $tourimage = Tourimage::findOrFail($id);
        $tourimage->delete();
        unlink(public_path() . '/tourimages/'.$tourimage->filename);
        unlink(public_path() . '/tourimages/thumbnail/'.$tourimage->filename);
        return redirect()->route('courses.show', $tourimage->course_id);
    }

    public function createevent($id)
    {
        $course = Course::findOrFail($id);
        $admins = Admin::all()->pluck('name', 'id');
        $eventstatuses = Eventstatus::all()->pluck('name', 'id');
        return view('yamakara.courses.createevent')->with(compact('course', 'admins', 'eventstatuses'));
    }

    public function storeevent(Request $request)
    {
        $event = new Event();
        $event->fill($request->all())->save();
        return redirect()->route('courses.show', $event->course_id);
    }

    public function createcontacts($course_id)
    {
        $course = Course::findOrFail($course_id);
        $contactlists = Contactlist::all()->pluck('name', 'id');
        return view('yamakara.courses.createcontacts')->with(compact('course', 'contactlists'));
    }

    public function storecontacts(Request $request, $course_id)
    {
        $course = Course::findOrFail($course_id);
        $course->contactlists()->attach($request->input('contactlist'));
        return redirect()->route('courses.show', $course->id);
    }

    public function destroycontacts($course_id)
    {
        $course = Course::findOrFail($course_id);
        $contactlists = $course->contactlists->pluck('name', 'id');
        return view('yamakara.courses.destroycontacts')->with(compact('course', 'contactlists'));
    }

    public function updatecontacts(Request $request, $course_id)
    {
        $course = Course::findOrFail($course_id);
        $course->contactlists()->detach($request->input('contactlist'));
        return redirect()->route('courses.show', $course->id);
    }
}
