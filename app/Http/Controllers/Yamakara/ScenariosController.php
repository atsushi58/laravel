<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Field\Eventcosttype;
use Field\Eventcostcategory;
use Field\Scenario;
use Field\Scenariocost;

class ScenariosController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }

    public function show($id){
    	$scenario = Scenario::findOrFail($id);
    	return view('yamakara.scenarios.show')->with(compact('scenario'));
    }

    public function edit($id){
        $scenario = Scenario::findOrFail($id);
        return view('yamakara.scenarios.edit')->with(compact('scenario'));

    }

    public function create($course_id){
    	return view('yamakara.scenarios.create')->with(compact('course_id'));
    }

    public function store(Request $request){
    	$scenario = new Scenario();
        $scenario->fill($request->all())->save();
    	return redirect()->route('scenarios.show', $scenario->id);
    }

    public function update(Request $request, $id) {
        $scenario = Scenario::findOrFail($id);
        $scenario->fill($request->all())->save();
        return redirect()->route('scenarios.show', $scenario->id);
    }

    public function createcost($id)
    {
        $eventcostcategories = Eventcostcategory::all()->pluck('name', 'id');
        $eventcosttypes = Eventcosttype::all()->pluck('name', 'id');
        return view('yamakara.scenarios.createcost')->with(compact('id', 'eventcostcategories', 'eventcosttypes'));
    }

    public function storecost(Request $request)
    {
        $scenariocost = new Scenariocost();
        if($request->limit == null){
            $scenariocost->limit = 1;
        }
        $scenariocost->fill($request->all())->save();
        return redirect()->route('scenarios.show', $scenariocost->scenario_id);
    }

    public function editcost($scenariocost_id)
    {
        $cost = Scenariocost::findOrFail($scenariocost_id);
        $eventcostcategories = Eventcostcategory::all()->pluck('name', 'id');
        $eventcosttypes = Eventcosttype::all()->pluck('name', 'id');
        return view('yamakara.scenarios.editcost')->with(compact('cost', 'eventcostcategories', 'eventcosttypes'));
    }

    public function updatecost(Request $request, $id)
    {
        $cost = Scenariocost::findOrFail($id);
        if($request->limit == null){
            $cost->limit = 1;
        }
        $cost->fill($request->all())->save();
        return redirect()->route('scenarios.show', $cost->scenario_id);
    }

    public function destroycost($scenariocost_id)
    {
        $cost = Scenariocost::findOrFail($scenariocost_id);
        $cost->delete();
        return redirect()->back();
    }

}
