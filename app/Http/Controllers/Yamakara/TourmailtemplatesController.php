<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Field\Tourmailtemplate;
use Field\Tourmailcategory;

class TourmailtemplatesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}
		
	public function index(){
		$tourmailtemplates = Tourmailtemplate::all();
		return view('yamakara.tourmailtemplates.index')->with(compact('tourmailtemplates'));
	}

	public function create()
	{
		$tourmailcategories = Tourmailcategory::all()->pluck('name', 'id');
		return view('yamakara.tourmailtemplates.create')->with(compact('tourmailcategories'));

	}

	public function store(Request $request)
	{
		$tourmailtemplate = new Tourmailtemplate();
		$tourmailtemplate->fill($request->all())->save();
		return redirect()->route('tourmailtemplates.index');
	}
	
	public function edit($id)
	{
		$tourmailtemplate = Tourmailtemplate::findOrFail($id);
		$tourmailcategories = Tourmailcategory::all()->pluck('name', 'id');
		return view('yamakara.tourmailtemplates.edit')->with(compact('tourmailtemplate', 'tourmailcategories'));
	}

	public function update(Request $request, $id)
	{
		$tourmailtemplate = Tourmailtemplate::findOrFail($id);
		$tourmailtemplate->fill($request->all())->save();
		return redirect()->route('tourmailtemplates.index');
	}
}
