<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;

use Field\Rentaldelivery;

class RentaldeliveriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $rentaldeliveries = Rentaldelivery::all();
        return view('yamakara.rentaldeliveries.index')->with(compact('rentaldeliveries'));
    }

    public function create()
    {
        return view('yamakara.rentaldeliveries.create');
    }


    public function store(Request $request)
    {
        $rentaldelivery = new Rentaldelivery();
        $rentaldelivery->fill($request->all())->save();
        return redirect()->route('rentaldeliveries.index');
    }

    public function edit($id)
    {
        $rentaldelivery = Rentaldelivery::findOrFail($id);
        return view('yamakara.rentaldeliveries.edit')->with(compact('Rentaldelivery'));
    }

    public function update(Request $request, $id) {
      $rentaldelivery = Rentaldelivery::findOrFail($id);
      $rentaldelivery->fill($request->all())->save();        
      return redirect()->route('rentaldeliveries.index');
    }
}
