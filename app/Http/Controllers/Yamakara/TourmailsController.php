<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;

use Illuminate\Support\Facades\Mail;
use Field\Mail\Yamakara\Customermail;

use Field\Event;
use Field\Tourmail;
use Field\Tourmailcategory;
use Field\User;

class TourmailsController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}
		
	public function index(){
		$eventtypes = Eventtype::all();
		return view('yamakara.tourmails.index')->with('eventtypes', $eventtypes);
	}

	public function create($event_id)
	{
		$event = Event::findOrFail($event_id);
		$tourmailcategories = Tourmailcategory::pluck('name', 'id');
		$users = $event->Clients->pluck('Clientname', 'Clientid');
		return view('yamakara.tourmails.create')->with(compact('tourmailcategories', 'users', 'event_id', 'event'));

	}

	public function store(Request $request)
	{
		$tourmail = new Tourmail();
		$user = User::findOrFail($request->user_id);
		$tourmail->to = $user->name;
		$email = $user->email;
		$tourmail->fill($request->all())->save();
		Mail::to($email)->cc('yamakara@field-mt.com')->send(new Customermail($tourmail, $user));
		return redirect()->route('events.show', $tourmail->event_id);
	}

	public function createallmail($event_id)
	{
		$event = Event::findOrFail($event_id);
		$pagetitle = '参加予定者一斉メール(申込完了 or 出発済み)';
		$tourmailcategories = Tourmailcategory::pluck('name', 'id');
		return view('yamakara.tourmails.createallmail')->with(compact('pagetitle', 'tourmailcategories', 'event_id', 'event'));
	}

	public function storeallmail(Request $request)
	{		
		$event = Event::findOrFail($request->event_id);
		$allclients = $event->Activeclientinfo;
		$tourmail = new Tourmail();
		$tourmail->to = '参加者/参加予定者一斉';
		$tourmail->fill($request->all())->save();
		foreach($allclients as $client)
		{
			if($client->email)
			{
			Mail::to($client->email)->cc('yamakara@field-mt.com')->send(new Customermail($tourmail, $client));
			}
		}
		return redirect()->route('events.show', $tourmail->event_id);
	}

	public function createfullmail($event_id)
	{
		$event = Event::findOrFail($event_id);
		$pagetitle = '申込者(含むキャンセル待ち/意向確認中)一斉メール';
		$tourmailcategories = Tourmailcategory::pluck('name', 'id');
		return view('yamakara.tourmails.createfullmail')->with(compact('pagetitle', 'tourmailcategories', 'event_id', 'event'));
	}

	public function storefullmail(Request $request)
	{		
		$event = Event::findOrFail($request->event_id);
		$fullclients = $event->Clientinfo;
		$tourmail = new Tourmail();
		$tourmail->to = '申込者(含むキャンセル待ち/意向確認中)一斉';
		$tourmail->fill($request->all())->save();
		foreach($fullclients as $client)
		{
			if($client->email)
			{
			Mail::to($client->email)->cc('yamakara@field-mt.com')->send(new Customermail($tourmail, $client));
			}
		}
		return redirect()->route('events.show', $tourmail->event_id);
	}
	
	public function edit($id)
	{
		$eventtype = Eventtype::findOrFail($id);
		return view('yamakara.tourmails.edit')->with(compact('eventtype'));
	}

	public function update(Request $request, $id)
	{
		$eventtype = Eventtype::findOrFail($id);
		$eventtype->fill($request->all())->save();
		return redirect()->route('eventtypes.index');
	}
}
