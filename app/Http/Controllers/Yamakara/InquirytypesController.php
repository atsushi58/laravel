<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Field\Inquirytype;

class InquirytypesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

    public function index()
    {
        $inquirytypes = Inquirytype::all();
        return view('yamakara.inquirytypes.index')->with(compact('inquirytypes'));
    }

    public function create()
    {
        return view('yamakara.inquirytypes.create');

    }

    public function store(Request $request)
    {
        $inquirytype = new Inquirytype();
        $inquirytype->fill($request->all())->save();
        return redirect()->route('inquirytypes.index');
    }

    public function edit($id)
    {
        $inquirytype = Inquirytype::findOrFail($id);
        return view('yamakara.inquirytypes.edit')->with(compact('inquirytype'));
    }

    public function update(Request $request, $id)
    {
        $inquirytype = Inquirytype::findOrFail($id);
        $inquirytype->fill($request->all())->save();
        return redirect()->route('inquirytypes.index');
    }
}

