<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Field\Mail\YamakaraMail\Admin\Admincheck;
use Sichikawa\LaravelSendgridDriver\SendGrid;
use Field\Mail\Yamakara\Waiting;
use Field\Mail\Yamakara\Attend;

use Field\Admin;
use Field\Balance;
use Field\Course;
use Field\Contact;
use Field\Contactlist;
use Field\Depplace;
use Field\Event;
use Field\Eventattendance;
use Field\Eventattendancestatus;
use Field\Eventbus;
use Field\Eventstatus;
use Field\Eventprice;
use Field\Eventattendancepaymentmethod;
use Field\Eventattendancepaymentstatus;
use Field\Photobookstatus;
use Field\Shift;
use Field\Task;
use Field\Tcreport;
use Field\Tctimereport;
use Field\Tctel;
use Field\Tourmailtemplate;
use Field\User;

class EventsController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }

    public function index(){
    	$events = Event::where('depdate', '>', date('Y-m-d'))->orderBy('depdate', 'ASC')->get();
        $description = '全て';
    	return view('yamakara.events.index')->with(compact('events', 'description'));
    }

    public function admincheck($id)
    {
        $event = Event::findOrFail($id);
        $event->admincheck = 1;
        $event->save();
        //Mail::send(new Admincheck($event));
        return redirect()->back();
        
    }

    public function eventsbyadmin($admin_id)
    {
        $admin = Admin::find($admin_id);
        $events = Event::whereYear('depdate', '=', '2018')->where('admin_id', '=', $admin_id)->orderBy('depdate', 'ASC')->get();
        $description = $admin->name. '担当ツアー';
        return view('yamakara.events.index')->with(compact('events', 'description'));

    }

    public function eventsbystatus($id)
    {
        $events = Event::where('eventstatus_id', $id)->orderBy('depdate', 'ASC')->get();
        $description = $events->first()->eventstatus->name;
        return view('yamakara.events.index')->with(compact('events', 'description'));
    }

    public function eventsbymonth($year, $month)
    {
        $events = Event::whereYear('depdate', '=', $year)->whereMonth('depdate', '=', $month)->whereIn('eventstatus_id', [4,5,6,11])->orderBy('depdate', 'ASC')->get();
        $description = $year. '年'. $month. '月';
        $sales = $events->sum('Paidsales');
        return view('yamakara.events.index')->with(compact('events', 'description', 'sales'));
    }

    public function eventsbyyear($year)
    {
        $events = Event::whereYear('depdate', '=', $year)->orderBy('depdate', 'ASC')->get();
        $description = $year. '年';
        $sales = $events->sum('Paidsales');
        return view('yamakara.events.index')->with(compact('events', 'description', 'sales'));

    }

    public function show($id){
    	$event = Event::findOrFail($id);
        $advancepayments = $event->eventcosts->where('eventcostpaymentstatus_id', '2');
        $courseconactlist = $event->course->contactlists;
        $tmpcontactlist = Contactlist::where('event_id', '=', $id);
        $contactlists = $courseconactlist->merge($tmpcontactlist);
        $contactlistid = $contactlists->pluck('id');
        $contacts = Contact::whereIn('contactlist_id', $contactlistid)->get();
    	return view('yamakara.events.show')->with(compact('event', 'advancepayments', 'contactlists', 'contacts'));
    }

    public function create(){
        $admins = Admin::where('active', 1)->pluck('name', 'id');
        $courses = Course::all()->pluck('name', 'id');
        $eventstatuses = Eventstatus::all()->pluck('name', 'id');
        $photobookstatuses = Photobookstatus::all()->pluck('name', 'id');
        $tctels = Tctel::all()->pluck('num', 'id');
        $tourmailtemplates = Tourmailtemplate::where('tourmailcategory_id', '1')->pluck('name', 'id');
    	return view('yamakara.events.create')->with(compact('admins', 'event', 'courses', 'eventstatuses', 'photobookstatuses', 'tctels', 'tourmailtemplates'));
    }

    public function store(Request $request){
        $validatedData = $request->validate([
            'maxpax' => 'required'
        ]);
    	$event = new Event();
        $event->fill($request->all())->save();
    	return redirect()->route('events.show', $event->id);
    }

    public function edit($id){
        $event = Event::findOrFail($id);
        $admins = Admin::where('active', 1)->pluck('name', 'id');
        $courses = Course::all()->pluck('name', 'id');
        $eventstatuses = Eventstatus::all()->pluck('name', 'id');
        $eventbuses = Eventbus::all()->pluck('name', 'id');
        $photobookstatuses = Photobookstatus::all()->pluck('name', 'id');
        $tctels = Tctel::all()->pluck('num', 'id');
        $tourmailtemplates = Tourmailtemplate::where('tourmailcategory_id', '1')->pluck('name', 'id');
        return view('yamakara.events.edit')->with(compact('admins', 'event', 'courses', 'eventstatuses', 'photobookstatuses', 'tctels', 'tourmailtemplates', 'eventbuses'));
    }

    public function update(Request $request, $id) {
        $event = Event::findOrFail($id);
        $event->fill($request->all())->save();
        return redirect()->route('events.show', $event->id);
    }

    public function createeventattendance($id)
    {
        $event = Event::findOrFail($id);
        $allclients = User::orderBy('kana')->get();
        $clients = $allclients->pluck('namelist', 'id');
        $eventprices = $event->eventprices->pluck('name', 'id');
        $eventattendancestatuses = Eventattendancestatus::all()->pluck('name', 'id');
        $eventattendancepaymentmethods = Eventattendancepaymentmethod::all()->pluck('name', 'id');
        $eventattendancepaymentstatuses = Eventattendancepaymentstatus::all()->pluck('name', 'id');
        return view('yamakara.events.createeventattendance')->with(compact('clients', 'event', 'eventattendancestatuses', 'eventattendancepaymentmethods', 'eventattendancepaymentstatuses', 'eventprices'));
    }

    public function storeeventattendance(Request $request, $id)
    {
        if(Eventattendance::where('user_id', $request->user_id)->where('eventprice_id', $request->eventprice_id)->exists())
        {
            $eventprice = Eventprice::findOrFail($request->eventprice_id);
            $eventid = $eventprice->event_id;
            return redirect()->route('events.show', $eventid);
        }
        $eventattendance = new Eventattendance();
        $eventattendance->fill($request->all())->save();
        return redirect()->route('events.show', $eventattendance->eventprice->event_id);
    }

    public function createeventattendancewm($id)
    {
        $event = Event::findOrFail($id);
        $allclients = User::orderBy('kana')->get();
        $clients = $allclients->pluck('namelist', 'id');
        $eventprices = $event->eventprices->pluck('name', 'id');
        $eventattendancestatuses = Eventattendancestatus::all()->pluck('name', 'id');
        $eventattendancepaymentmethods = Eventattendancepaymentmethod::all()->pluck('name', 'id');
        $eventattendancepaymentstatuses = Eventattendancepaymentstatus::all()->pluck('name', 'id');
        return view('yamakara.events.createeventattendancewm')->with(compact('clients', 'event', 'eventattendancestatuses', 'eventattendancepaymentmethods', 'eventattendancepaymentstatuses', 'eventprices'));
    }

    public function storeeventattendancewm(Request $request, $id)
    {
        if(Eventattendance::where('user_id', $request->user_id)->where('eventprice_id', $request->eventprice_id)->exists())
        {
            $eventprice = Eventprice::findOrFail($request->eventprice_id);
            $eventid = $eventprice->event_id;
            return redirect()->route('events.show', $eventid);
        }
        $eventattendance = new Eventattendance();
        $eventattendance->fill($request->all())->save();
        //申込完了、キャンセル待ちに応じてメール送信
        $user = User::findOrFail($eventattendance->user_id);
        if($eventattendance->eventattendancestatus_id == 2)
        {
            $eventprice = Eventprice::findOrFail($request->eventprice_id);
            $event = Event::findOrFail($eventprice->event_id);
            $tourmailtemplate = Tourmailtemplate::findOrFail($event->tourmailtemplate_id);
            Mail::to($user->email)->cc('yamakara@field-mt.com')->send(new Attend($eventattendance, $tourmailtemplate));
        }elseif($eventattendance->eventattendancestatus_id == 4)
        {
            Mail::to($user->email)->cc('yamakara@field-mt.com')->send(new Waiting($eventattendance));
        }
        return redirect()->route('events.show', $eventattendance->eventprice->event_id);
    }
    public function tcset($id)
    {
        $event = Event::findOrFail($id);
        $tcs = Admin::where('active', 1)->pluck('name', 'id');
        $tasks = Task::whereIn('id', [13,21,22])->pluck('name', 'id');
        return view('yamakara.events.tcset')->with(compact('event', 'tcs', 'tasks'));
    }

    public function tcstore(Request $request)
    {
        $shift = new Shift();
        $admin = Admin::findOrFail($request->admin_id);
        if($admin->role_id != 6)
        {
        $shift->transportation = $admin->payments->where('workplace_id', 2)->first()->transportation;
        }
        $shift->fill($request->all())->save();
        return redirect()->route('events.index');
    }

    public function createcontacts($event_id)
    {
        $event = Event::findOrFail($event_id);
        return view('yamakara.events.createcontacts')->with(compact('event'));
    }

    public function storecontacts(Request $request)
    {
        $contactlist = new Contactlist();
        $contactlist->fill($request->all())->save();
        return redirect()->route('events.show', $contactlist->event_id);
    }

    public function createcontactmemo($event_id, $contactlist_id)
    {
        $contactlist = Contactlist::findOrFail($contactlist_id);
        $event = Event::findOrFail($event_id);
        return view('yamakara.events.createcontactmemo')->with(compact('event_id', 'contactlist', 'event'));
    }

    public function storecontactmemo(Request $request, $event_id)
    {
        $contact = new Contact();
        $contact->fill($request->all())->save();
        return redirect()->route('events.show', $event_id);
    }

    public function depevents($id)
    {
        $event = Event::findOrFail($id);
        $event->eventstatus_id = 11;
        $event->save();
        foreach($event->Clients as $eventattendance){
            if($eventattendance->eventattendancestatus_id == 2)
            {
                $eventattendance->eventattendancestatus_id = 8;
                $eventattendance->save();
            }
        }
        return redirect()->back();
    }

    public function cancelevents($id)
    {
        $event = Event::findOrFail($id);
        foreach($event->Clients as $eventattendance){
            if($eventattendance->eventattendancestatus_id == 2 || $eventattendance->eventattendancestatus_id == 8)
            {
                $eventattendance->eventattendancestatus_id = 7;
                $eventattendance->save();
            }
            if($eventattendance->eventattendancepaymentstatus_id ==2)
            {
                $balance = new Balance();
                $balance->user_id = $eventattendance->user_id;
                $balance->eventattendance_id = $eventattendance->id;
                $balance->day = date("Y-m-d");
                $balance->balancetype_id = 4;
                $balance->suspend = $eventattendance->Sales;
                $balance->save();
            }
        }
        return redirect()->back();
    }
}
