<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;

use Field\Tctel;

class TctelsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $tctels = Tctel::all();
        return view('yamakara.tctels.index')->with(compact('tctels'));
    }

    public function create()
    {
        return view('yamakara.tctels.create');
    }


    public function store(Request $request)
    {
        $tctel = new Tctel();
        $tctel->fill($request->all())->save();
        return redirect()->route('tctels.index');
    }

    public function edit($id)
    {
        $tctel = Tctel::findOrFail($id);
        return view('yamakara.tctels.edit')->with(compact('tctel'));
    }

    public function update(Request $request, $id) {
      $tctel = Tctel::findOrFail($id);
      $tctel->fill($request->all())->save();        
      return redirect()->route('tctels.index');
    }
}
