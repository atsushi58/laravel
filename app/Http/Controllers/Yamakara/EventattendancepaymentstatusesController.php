<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;

use Field\Eventattendancepaymentstatus;

class EventattendancepaymentstatusesController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth:admin');
  }
    public function index(){
    	$eventattendancepaymentstatuses = Eventattendancepaymentstatus::all();

    	return view('yamakara.eventattendancepaymentstatuses.index')->with(compact('eventattendancepaymentstatuses'));
    }

    public function store(Request $request){
    	$eventattendancepaymentstatus = new Eventattendancepaymentstatus();
    	$eventattendancepaymentstatus->fill($request->all())->save();
    	return redirect()->route('yamakara.eventattendancepaymentstatuses');
    }

    public function edit($id) {
      $eventattendancepaymentstatus = Eventattendancepaymentstatus::findOrFail($id);
      return view('yamakara.eventattendancepaymentstatuses.edit')->with(compact('Eventattendancepaymentstatus'));
    }

    public function update(Request $request, $id) {
      $eventattendancepaymentstatus = Eventattendancepaymentstatus::findOrFail($id);
      $eventattendancepaymentstatus->fill($request->all())->save();
      return redirect()->route('yamakara.eventattendancepaymentstatuses');
    }

    public function destroy($id) {
      $eventattendancepaymentstatus = Eventattendancepaymentstatus::findOrFail($id);
      $eventattendancepaymentstatus->delete();
      return redirect()->route('yamakara.eventattendancepaymentstatuses');
  }
}
