<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;

use Field\Eventcostpaymentstatus;

class EventcostpaymentstatusesController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth:admin');
  }
    public function index(){
    	$eventcostpaymentstatuses = Eventcostpaymentstatus::all();
    	return view('yamakara.eventcostpaymentstatuses.index')->with(compact('eventcostpaymentstatuses'));
    }

    public function create()
    {
        return view('yamakara.eventcostpaymentstatuses.create');
    }

    public function store(Request $request){
    	$eventcostpaymentstatus = new Eventcostpaymentstatus();
    	$eventcostpaymentstatus->fill($request->all())->save();
    	return redirect()->route('eventcostpaymentstatuses.index');
    }

    public function edit($id) {
      $eventcostpaymentstatus = Eventcostpaymentstatus::findOrFail($id);
      return view('yamakara.eventcostpaymentstatuses.edit')->with(compact('eventcostpaymentstatus'));
    }

    public function update(Request $request, $id) {
      $eventcostpaymentstatus = Eventcostpaymentstatus::findOrFail($id);
      $eventcostpaymentstatus->fill($request->all())->save();
      return redirect()->route('eventcostpaymentstatuses.index');
    }
}
