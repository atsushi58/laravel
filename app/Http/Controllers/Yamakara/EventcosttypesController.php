<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;

use Field\Eventcosttype;

class EventcosttypesController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth:admin');
  }
    public function index(){
    	$eventcosttypes = Eventcosttype::all();
    	return view('yamakara.eventcosttypes.index')->with(compact('eventcosttypes'));
    }

    public function create()
    {
        return view('yamakara.eventcosttypes.create');
    }

    public function store(Request $request){
    	$eventcosttype = new Eventcosttype();
    	$eventcosttype->fill($request->all())->save();
    	return redirect()->route('eventcosttypes.index');
    }

    public function edit($id) {
      $eventcosttype = Eventcosttype::findOrFail($id);
      return view('yamakara.eventcosttypes.edit')->with(compact('eventcosttype'));
    }

    public function update(Request $request, $id) {
      $eventcosttype = Eventcosttype::findOrFail($id);
      $eventcosttype->fill($request->all())->save();
      return redirect()->route('eventcosttypes.index');
    }
}
