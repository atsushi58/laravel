<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use Field\Mailmagazine;
use Field\Mail\YamakaraMailmagazineMail;

class MailmagazinesController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }

    public function index(){
    	$mailmagazines = Mailmagazine::all();
    	return view('yamakara.mailmagazines.index')->with(compact('mailmagazines'));
    }

    public function create(){
        return view('yamakara.mailmagazines.create');
    }

    public function send(){

    }
}
