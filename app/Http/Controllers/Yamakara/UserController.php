<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Field\Http\Controllers\Controller;
use Field\Payment;
use Field\Role;
use Field\Bankaccount;
use Field\Bank;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admin =  Auth::user();
        return view('admin.admins.show')->with(compact('admin'));
    }

    public function edit(){
        $admin = Auth::user();
        $roles = Role::all()->pluck('name', 'id');
        return view('admin.admins.edit')->with(compact('admin', 'roles'));
    }

    public function update(Request $request){
        $admin = Auth::user();
        $admin->fill($request->all())->save();
        return redirect()->route('staff.user');
    }

    public function createbankaccount(){
        $id = Auth::user()->id;
        $banks = Bank::all()->pluck('name', 'id');
        return view('admin.admins.createbankaccount')->with(compact('banks', 'id'));
    }

    public function storebankaccount(Request $request){
        $bankaccount = new Bankaccount();
        $bankaccount->fill($request->all())->save();
        return redirect()->route('staff.user');
    }

    public function editbankaccount(){
        $bankaccount = Auth::user()->bankaccount;
        $banks = Bank::all()->pluck('name', 'id');
        return view('admin.admins.editbankaccount')->with(compact('bankaccount', 'banks'));
    }

    public function updatebankaccount(Request $request){
        $bankaccount = Auth::user()->bankaccount;
        $bankaccount->fill($request->all())->save();
        return redirect()->route('staff.user');
    }
}
 
