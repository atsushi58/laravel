<?php

namespace Field\Http\Controllers\CommonClass;

use Field\Admin;
use Field\Shift;
use Field\Salary;
use Field\Tax;

class StatementsClass
{
    public function amounts($id, $year, $month){
    	$salary = Salary::where('admin_id', $id)->where('year', $year)->where('month', $month)->first();
    	$user = Admin::where('id', $id)->first();
        $shifts = Shift::where('admin_id','=', $id)->whereYear('workday', '=', $year)->whereMonth('workday', $month)->whereNotIn('confirm', ['2'])->get();
    	$amounts = [];
        $amounts['name'] = $user->name;
        $amounts['admin_id'] = $user->id;
    	if(isset($salary) && $salary->accountingconfirm == 1)
        {
            $amounts['amounta'] = $salary->basesalary;
            $amounts['amountb'] = $salary->balancesalary;
            $amounts['amountc'] = $salary->overtime;
            $amounts['amountd'] = $salary->summerspecial;
            $amounts['amounte'] = $salary->basetransportation;
            $amounts['amountf'] = $salary->transportationbalance;
            $amounts['amountg'] = $salary->biztransportation;
            $amounts['amounth'] = $salary->reimbursement;
            $amounts['amounti'] = $salary->biztrip;
            $amounts['amountj'] = $salary->biztripallowance;
            $amounts['amounto'] = $salary->healthinsurance;
            $amounts['amountp'] = $salary->nursinginsurance;
            $amounts['amountq'] = $salary->pension;
            $amounts['amountr'] = $salary->Employmentinsurance;
            $amounts['amountk'] = $amounts['amounta'] + $amounts['amountb'] + $amounts['amountc'] + $amounts['amountd']-$amounts['amounto']-$amounts['amountp']-$amounts['amountq']-$amounts['amountr'];
            $amounts['amounts'] = $salary->incometax;
            $amounts['amountt'] = $salary->inhabitanttax;
            $amounts['amountu'] = $salary->deducationbalance;
            $amounts['amountw'] = $salary->adjustment;
            $amounts['amountx'] = $salary->transferamount;
            $amounts['workingdays'] = $salary->workingdays;
            $amounts['workingminutes'] = $salary->workingminutes;
            $amounts['accountingconfirm'] = 1;
        }elseif(isset($salary) && $salary->accountingconfirm == 0)
        {
            if(isset($user->basesalary))
            {
                $amounts['amounta'] = $user->basesalary;
            }else
            {
                $amounts['amounta'] = $shifts->sum('basepay');
            }
            $amounts['amountb'] = $salary->balancesalary;
            $amounts['amountc'] = $shifts->sum('Overtimepay');
            $amounts['amountd'] = $shifts->sum('summerspecial');
            $amounts['amounte'] = $salary->basetransportation;
            $amounts['amountf'] = $shifts->sum('transportationbalance');
            $amounts['amountg'] = $salary->biztransportation;
            $amounts['amounth'] = $salary->reimbursement;
            $amounts['amounti'] = $salary->biztrip;
            $amounts['amountj'] = $salary->biztripallowance;
            $amounts['amounto'] = $user->healthinsuranceamount;
            $amounts['amountp'] = $user->nursinginsuranceamount;
            $amounts['amountq'] = $user->pensionamount;
            $amounts['amountr'] = $salary->Employmentinsurance;
            $amounts['amountk'] = $amounts['amounta'] + $amounts['amountb'] + $amounts['amountc'] + $amounts['amountd']-$amounts['amounto']-$amounts['amountp']-$amounts['amountq']-$amounts['amountr'];
            if($amounts['amountk'] <88000 && $user->taxsection == 0)
            {   
                switch($user->taxsection)
                {
                    case 0:$amounts['amounts'] = floor($amounts['amountk'] * 3.063 / 100);break;
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    $amounts['amounts'] = 0;break;
                }
            }else
            {
               $tax = Tax::where('kara', '<=', $amounts['amountk'])->where('made', '>', $amounts['amountk'])->first();
               if(isset($tax)){
                switch($user->taxsection)
                {
                    case 0:$amounts['amounts'] = $tax->otsu;break;
                    case 1:$amounts['amounts'] = $tax->zerodependants;break;
                    case 2:$amounts['amounts'] = $tax->onedependants;break;
                    case 3:$amounts['amounts'] = $tax->twodependants;break;
                    case 4:$amounts['amounts'] = $tax->threedependants;break;
                    case 5:$amounts['amounts'] = $tax->fourdependants;break;
                    case 6:$amounts['amounts'] = $tax->fivedependants;break;
                    case 7:$amounts['amounts'] = $tax->sixdependants;break;
                    case 8:$amounts['amounts'] = $tax->sevendependants;break;
                }}else{$amounts['amounts'] = 0;}
            }
            if(isset($user->inhabitanttax))
            {
                $amounts['amountt'] = $user->inhabitanttax;}else{$amounts['amountt'] = 0;
            }
            $amounts['amountu'] = $salary->deducationbalance;
            $amounts['amountw'] = $salary->adjustment;
            $amounts['workingdays'] = $shifts->count();
            $amounts['amountx'] = $amounts['amountk'] + $amounts['amounte'] + $amounts['amountf'] + $amounts['amountg'] + $amounts['amounth'] + $amounts['amounti'] + $amounts['amountj'] - ($amounts['amounts'] + $amounts['amountt'] + $amounts['amountu']) + $amounts['amountw'];
            $amounts['workingdays'] = $shifts->count();
            $amounts['workingminutes'] = $shifts->sum('worktime');
            $amounts['accountingconfirm'] = 0;
        }else
        {
        	if(isset($user->basesalary))
                {
                    $amounts['amounta'] = $user->basesalary;
                }else
                {
                    $amounts['amounta'] = $shifts->sum('Basepay');
                }
        	$amounts['amountb'] = $shifts->sum('paybalance');
        	$amounts['amountc'] = $shifts->sum('Overtimepay');
        	$amounts['amountd'] = $shifts->sum('summerspecial');
        	if(isset($user->basetransportation)){$amounts['amounte'] = $user->basetransportation;}else{$amounts['amounte'] = $shifts->sum('transportation');}
        	$amounts['amountf'] = $shifts->sum('transportationbalance');
        	$amounts['amountg'] = 0;
        	$amounts['amounth'] = 0;
        	$amounts['amounti'] = 0;
        	$amounts['amountj'] = 0;
        	$amounts['amounto'] = $user->healthinsuranceamount;
        	$amounts['amountp'] = $user->nursinginsuranceamount;
        	$amounts['amountq'] = $user->pensionamount;
            if($user->taxsection ==0)
                {
                    $amounts['amountr'] = 0;
                }else
                {
                    $amounts['amountr'] = round(($amounts['amounta'] + $amounts['amountb'] + $amounts['amountc'] + $amounts['amountd'] + $amounts['amounte'] + $amounts['amountf'])*3/1000);
                }
            $amounts['amountk'] = $amounts['amounta'] + $amounts['amountb'] + $amounts['amountc'] + $amounts['amountd']-$amounts['amounto']-$amounts['amountp']-$amounts['amountq']-$amounts['amountr'];
            if($amounts['amountk'] > 860000)
            {
                $amounts['amounts'] = floor($amounts['amountk'] * 23.483 /100);
            }elseif($amounts['amountk'] < 88000)
            {   
                switch($user->taxsection)
                {
                    case 0:$amounts['amounts'] = floor($amounts['amountk'] * 3.063 / 100);break;
                    case 1:$amounts['amounts'] = 0;break;
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    $amounts['amounts'] = 0;break;
                }
            }else
            {
        	   $tax = Tax::where('kara', '<=', $amounts['amountk'])->where('made', '>', $amounts['amountk'])->first();
               if(isset($tax)){
            	switch($user->taxsection)
                {
            		case 0:$amounts['amounts'] = $tax->otsu;break;
            		case 1:$amounts['amounts'] = $tax->zerodependants;break;
            		case 2:$amounts['amounts'] = $tax->onedependants;break;
            		case 3:$amounts['amounts'] = $tax->twodependants;break;
            		case 4:$amounts['amounts'] = $tax->threedependants;break;
            		case 5:$amounts['amounts'] = $tax->fourdependants;break;
            		case 6:$amounts['amounts'] = $tax->fivedependants;break;
            		case 7:$amounts['amounts'] = $tax->sixdependants;break;
            		case 8:$amounts['amounts'] = $tax->sevendependants;break;
            	}}else{$amounts['amounts'] = 0;}
            }
        	if(isset($user->inhabitanttax)){$amounts['amountt'] = $user->inhabitanttax;}else{$amounts['amountt'] = 0;}
        	$amounts['amountu'] = 0;
        	$amounts['amountw'] = 0;
            $amounts['workingdays'] = $shifts->count();
            $amounts['workingminutes'] = $shifts->sum('worktime');
            $amounts['accountingconfirm'] = 0;
        }
        $amounts['amountl'] = $amounts['amounte'] + $amounts['amountf'] + $amounts['amountg'] + $amounts['amounth'] + $amounts['amounti'] + $amounts['amountj'];
        $amounts['amountm'] = $amounts['amounta'] + $amounts['amountb'] + $amounts['amountc'] + $amounts['amountd'] + $amounts['amounte'] + $amounts['amountf'];
        $amounts['amountn'] = $amounts['amountk'] + $amounts['amountl'];
        $amounts['amountv'] = $amounts['amounto'] + $amounts['amountp'] + $amounts['amountq'] + $amounts['amountr'] + $amounts['amounts'] + $amounts['amountt'] + $amounts['amountu'];  
        if(!isset($amounts['amountx'])){$amounts['amountx'] = $amounts['amountn'] - ($amounts['amounts'] + $amounts['amountt'] + $amounts['amountu']) + $amounts['amountw'];}
       	return $amounts;
    }

    public function amountlist($year, $month)
    {
        $admins = Shift::whereYear('workday', '=', $year)->whereMonth('workday', $month)->pluck('admin_id');
    }
}
