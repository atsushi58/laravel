<?php

namespace Field\Http\Controllers\Yamakaraweb;

use Illuminate\Http\Request;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;

use Field\User;

use Field\Yakushima\Yakuactivity;
use Field\Yakushima\Yakuaccompany;
use Field\Yakushima\Yakucourse;
use Field\Yakushima\Yakucourseprice;
use Field\Yakushima\Yakudefaultactivity;
use Field\Yakushima\Yakueventattendance;
use Field\Yakushima\Yakuhostel;


class YakupagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function yakuattendanceform($course_id, $date)
    {
        $date = new Carbon($date);
        $year = $date->copy()->year;
        $month = $date->copy()->month;
        $day = $date->copy()->day;
        $enddate = $date->copy()->endOfMonth()->day; 
        $yakucourse = Yakucourse::findOrFail($course_id);
        $today = Carbon::today();
        $metatitle = $yakucourse->name. $yakucourse->yakudepplace->name. '発お申込みフォーム';

        $yakuhostels = Yakuhostel::all()->pluck('name', 'id');

        // 価格部分
        if(Yakucourseprice::where('yakucourse_id', '=', $course_id)->where('depdate', '=', $date->format("Y-m-d"))->exists())
        {
            $yakucourseprice = Yakucourseprice::where('yakucourse_id', '=', $course_id)->where('depdate', '=', $date->format("Y-m-d"))->first();
        }elseif(Yakucourseprice::where('yakucourse_id', '=', $course_id)->where('yakucoursepricetype_id', '=', 4)->exists() && $date > $today->copy()->addDay(60))
        {
            $yakucourseprice = Yakucourseprice::where('yakucourse_id', '=', $course_id)->where('yakucoursepricetype_id', '=', 4)->first();
        }elseif(Yakucourseprice::where('yakucourse_id', '=', $course_id)->where('yakucoursepricetype_id', '=', 3)->exists() && $date > $today->copy()->addDay(30))
        {
            $yakucourseprice = Yakucourseprice::where('yakucourse_id', '=', $course_id)->where('yakucoursepricetype_id', '=', 3)->first();
        }elseif(Yakucourseprice::where('yakucourse_id', '=', $course_id)->where('yakucoursepricetype_id', '=', 1)->exists())
        {
            $yakucourseprice = Yakucourseprice::where('yakucourse_id', '=', $course_id)->where('yakucoursepricetype_id', '=', 1)->first();
        }

        return view('yamakaraweb.yakuattendanceform')->with(compact('yakucourse', 'year', 'month', 'day', 'enddate', 'date', 'yakucourseprice', 'yakuhostels', 'metatitle'));
    }

    public function storeyakueventattendance(Request $request)
    {
        //新規ユーザーを作成
        $user = new User();
        $user->name = $request->name;
        $user->kana = $request->kana;
        $user->email = $request->email;
        $user->tel = $request->tel;
        $user->agebracket = $request->agebracket;
        $user->sex = $request->sex;
        $user->save();

        //イベント参加を追加
        $yakueventattendance = new Yakueventattendance();
        $yakueventattendance->user_id = $user->id;
        $yakueventattendance->depdate = $request->depdate;
        $yakueventattendance->yakucourse_id = $request->yakucourse_id;
        $yakueventattendance->numofpeople = $request->numofpeople;
        $yakueventattendance->yakuhostel_id = $request->yakuhostel_id;
        $yakueventattendance->eventattendancestatus_id = 2;
        $yakueventattendance->eventattendancepaymentstatus_id = 1;
        $yakueventattendance->save();

        //同行者1を登録
        if($yakueventattendance->numofpeople > 1)
        {
            $yakuaccompany = new Yakuaccompany();
            $yakuaccompany->name = $request->name1;
            $yakuaccompany->sex = $request->sex1;
            $yakuaccompany->agebracket = $request->agebracket1;
            $yakuaccompany->yakueventattendance_id = $yakueventattendance->id;
            $yakuaccompany->save();
        }
        //同行者2を登録
        if($yakueventattendance->numofpeople > 2)
        {
            $yakuaccompany = new Yakuaccompany();
            $yakuaccompany->name = $request->name2;
            $yakuaccompany->sex = $request->sex2;
            $yakuaccompany->agebracket = $request->agebracket2;
            $yakuaccompany->yakueventattendance_id = $yakueventattendance->id;
            $yakuaccompany->save();
        }
        //同行者3を登録
        if($yakueventattendance->numofpeople > 3)
        {
            $yakuaccompany = new Yakuaccompany();
            $yakuaccompany->name = $request->name3;
            $yakuaccompany->sex = $request->sex3;
            $yakuaccompany->agebracket = $request->agebracket3;
            $yakuaccompany->yakueventattendance_id = $yakueventattendance->id;
            $yakuaccompany->save();
        }
        //同行者4を登録
        if($yakueventattendance->numofpeople > 4)
        {
            $yakuaccompany = new Yakuaccompany();
            $yakuaccompany->name = $request->name4;
            $yakuaccompany->sex = $request->sex4;
            $yakuaccompany->agebracket = $request->agebracket4;
            $yakuaccompany->yakueventattendance_id = $yakueventattendance->id;
            $yakuaccompany->save();
        }
        //同行者5を登録
        if($yakueventattendance->numofpeople > 5)
        {
            $yakuaccompany = new Yakuaccompany();
            $yakuaccompany->name = $request->name5;
            $yakuaccompany->sex = $request->sex5;
            $yakuaccompany->agebracket = $request->agebracket5;
            $yakuaccompany->yakueventattendance_id = $yakueventattendance->id;
            $yakuaccompany->save();
        }
        //同行者6を登録
        if($yakueventattendance->numofpeople > 6)
        {
            $yakuaccompany = new Yakuaccompany();
            $yakuaccompany->name = $request->name6;
            $yakuaccompany->sex = $request->sex6;
            $yakuaccompany->agebracket = $request->agebracket6;
            $yakuaccompany->yakueventattendance_id = $yakueventattendance->id;
            $yakuaccompany->save();
        }

        $yakucourse = Yakucourse::findOrFail($yakueventattendance->yakucourse_id);
        if(Yakudefaultactivity::where('yakucourse_id', '=', $yakucourse->id)->exists())
        {
            foreach($yakucourse->yakudefaultactivities as $yakudefaultactivity)
            {
                $depdate = new Carbon($yakueventattendance->depdate);
                $activitydate = $depdate->addDay($yakudefaultactivity->day-1);
                if(Yakuactivity::where('depdate', '=', $activitydate)->where('yakuactivitytype_id', '=', $yakudefaultactivity->yakuactivitytype_id)->where('full', '<', 1)->exists())
                {
                    $yakuactivity = Yakuactivity::where('depdate', '=', $activitydate)->where('yakuactivitytype_id', '=', $yakudefaultactivity->yakuactivitytype_id)->where('full', '<', 1)->first();

                }else{
                    $yakuactivity = new Yakuactivity();
                    $yakuactivity->yakuactivitytype_id = $yakudefaultactivity->yakuactivitytype_id;
                    $yakuactivity->depdate = $activitydate;
                    $yakuactivity->full = 0;
                    $yakuactivity->save();
                }
                $yakueventattendance->yakuactivities()->attach($yakuactivity);
                if($yakuactivity->yakueventattendances->count('id') == $yakuactivity->yakuactivitytype->limit)
                {
                    $yakuactivity->full = 1;
                    $yakuactivity->save();
                }
            }
        }

        if($request->yakuoptionactivity)
        {
            $
            
        }
    }

}
