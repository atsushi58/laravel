<?php

namespace Field\Http\Controllers\Yamakaraweb;

use Illuminate\Http\Request;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use Field\Topslider;
use Field\Inquirytype;
use Field\Inquiry;
use Illuminate\Support\Facades\Mail;
use Sichikawa\LaravelSendgridDriver\SendGrid;
use Field\Mail\Yamakara\Cancel;
use Field\Mail\Yamakara\Waiting;
use Field\Mail\Yamakara\Attend;
use Field\Mail\Yamakara\Adminwaitingalert;
use Field\Mail\Yamakara\Rentalconfirm;
use Field\Mail\InquiryMail\CustomerMail;
use Field\Mail\Staff;

use Field\Balance;
use Field\Course;
use Field\Event;
use Field\Eventattendance;
use Field\Eventprice;
use Field\Eventattendancepaymentmethod;
use Field\Eventattendancepaymentstatus;
use Field\Eventattendancestatus;
use Field\Rentaldelivery;
use Field\User;
use Field\Tourmail;
use Field\Tourimage;
use Field\Staffmail;
use Field\Tourmailtemplate;

class PagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topsliders = Topslider::all();
        return view('yamakaraweb.index')->with(compact('topsliders'));
    }

    public function mypage()
    {
        $user = Auth::user();
        return view('yamakaraweb.mypage')->with(compact('user'));
    }

    public function tourpage($course_slug)
    {
        $course = Course::where('slug', $course_slug)->first();
        $bigimage = Tourimage::where('bigimage', '=', '1')->first();
        $metatitle = $course->metatitle;
        $metakeywords = $course->metakeywords;
        $metadescription = $course->metadescription;
        return view('yamakaraweb.course')->with(compact('course', 'metatitle', 'metakeywords', 'metadescription', 'bigimage'));
    }

    public function tourlist()
    {
        $events = Event::where('depdate', '>', date('Y-m-d'))->whereIn('eventstatus_id', ['4','5','6','7'])->orderBy('depdate', 'ASC')->get();
        $metatitle = '登山ツアー一覧';
        $metadescription = '登山ツアー装備無料レンタル付ヤマカラの登山ツアー一覧です。各日程の参加者数はリアルタイムで反映しております。キャンセル待ちになっているツアーも順次日程追加していきます。キャンセル待ちでも入れておいていただければ、日程追加の際、キャンセル待ちの方に先に追加日程をご案内します。';
        return view('yamakaraweb.tourlist')->with(compact('events', 'metatitle', 'metadescription'));
    }

    public function inquiry()
    {
        return redirect()->away('http://www.yamakara.com');
        $inquirytypes = Inquirytype::all()->pluck('name', 'id');
        return view('yamakaraweb.inquiry')->with(compact('inquirytypes'));
    }

    public function storeinquiry(Request $request){
        $inquiry = new Inquiry();
        $inquiry->fill($request->all())->save();
        Mail::to($inquiry->email)->send(new CustomerMail())->with(compact('inquiry'));
        Mail::to('customer@field-mt.com')->send(new AdminMail())->with(compact('inquiry'));
        return redirect()->route('yamakaraweb.toppage');
    }

    public function attendanceform($id)
    {
        $course = Course::findOrFail($id);
        $rentaldeliveries = Rentaldelivery::all()->pluck('name', 'id');
        $events = Event::where('course_id', $course->id)->where('depdate', '>', date('Y-m-d'))->whereIn('eventstatus_id', ['4', '5', '6', '12'])->get()->pluck('id');
        $eventprices = Eventprice::whereIn('event_id', $events)->where('hpactive', '1')->get()->pluck('Tourname', 'id');
        $metatitle = $course->name. 'お申込みフォーム';
        if($course->eventtype_id == 1)
        {
            $paymentmethods = Eventattendancepaymentmethod::where('id', 6)->pluck('name', 'id');
        }elseif($course->depplace_id == 2 && $course->eventtype_id == 2)
        {
            $paymentmethods = Eventattendancepaymentmethod::whereIn('id', [1,2,5])->pluck('name', 'id');
        }elseif($course->depplace_id == 2 && $course->eventtype_id != 2)
        {
            $paymentmethods = Eventattendancepaymentmethod::whereIn('id', [1,2])->pluck('name', 'id');
        }elseif($course->depplace_id != 2 && $course->eventtype_id == 2)
        {
            $paymentmethods = Eventattendancepaymentmethod::whereIn('id', [1,2,5])->pluck('name', 'id');
        }else
        {
            $paymentmethods = Eventattendancepaymentmethod::whereIn('id', [1,2])->pluck('name', 'id');
        }
        return view('yamakaraweb.attendanceform')->with(compact('events', 'eventprices', 'course', 'paymentmethods', 'rentaldeliveries', 'metatitle'));
    }

    public function storeattendance(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'kana' => 'required',
            'email' => 'required|email|confirmed',
            'eventprice_id' => 'required',
            'sex' => 'required',
            'tel' => 'required'
        ]);
        $request->name = str_replace(array(" ", "　"), "", $request->name);

        $originalname = $request->name;

        for ($i = 0; $i < $request->numofpeople; $i++)
        {
		    // メール、名前で該当者がいればそちらで登録、いなければ新規ユーザー登録
			if($i == 0 && User::where('email', $request->email)->where('name', $request->name)->exists())
		    {
		        $user = User::where('email', $request->email)->where('name', $request->name)->first();
		    }
		    elseif($i == 0)
		    {
		        $user = new User();
		        $user->name = $request->name;
		        $user->kana = $request->kana;
		        $user->sex = $request->sex;
		        $user->birthday = $request->year.'-'.$request->month.'-'.$request->day;
		        $user->email = $request->email;
		        $user->tel = $request->tel;
		        $user->zipcode = $request->zipcode;
		        $user->address = $request->address;
		        $user->save();
		    }
		    else
		    {
		    	$user = new User();
		        $user->name = $request->name;
		        $user->email = $request->email;
		        $user->save();
		    }
        	$eventprice = Eventprice::findOrFail($request->eventprice_id);
        	$event = Event::findOrFail($eventprice->event_id);
        	$tourmailtemplate = Tourmailtemplate::findOrFail($event->tourmailtemplate_id);
        	$eventattendance = new Eventattendance();
        	$eventattendance->user_id = $user->id;  
        	$eventattendance->eventprice_id = $request->eventprice_id;
        	$eventattendance->eventattendancepaymentmethod_id = $request->eventattendancepaymentmethod_id;
        	$eventattendance->numofpeople = 1;
        	$eventattendance->friendinfo = $request->friendinfo;
        	$eventattendance->rental = $request->rental;
        	$eventattendance->memo = $request->memo;

        	//イベントがキャンセル待ちならキャンセル待ちで登録
        	if($event->eventstatus_id == 6)
        	{
            	$eventattendance->eventattendancestatus_id = 4;
            	$eventattendance->eventattendancepaymentstatus_id = 1;
            	Mail::to($user->email)->cc('yamakara@field-mt.com')->send(new Waiting($eventattendance));
        	}
        	//イベント参加人数が定員越えたら、イベントをキャンセル待ちにして、キャンセル待ちで登録
        	elseif($request->numofpeople + $event->Activetotalclients > $event->maxpax)
        	{
            	$event->eventstatus_id = 6;
            	$event->save();
            	Mail::to("yamakara@field-mt.com")->send(new Adminwaitingalert($event));
            	$eventattendance->eventattendancestatus_id = 4;
            	$eventattendance->eventattendancepaymentstatus_id = 1;
            	Mail::to($user->email)->cc('yamakara@field-mt.com')->send(new Waiting($eventattendance));
        	}       
         	//イベント参加人数が定員と同数なら、イベントをキャンセル待ちにして、申込完了で登録
        	elseif($request->numofpeople + $event->Activetotalclients == $event->maxpax)
        	{
            	$event->eventstatus_id = 6;
            	$event->save();
            	Mail::to("customer@field-mt.com")->send(new Adminwaitingalert($event));
            	$eventattendance->eventattendancestatus_id = 2;
            	$eventattendance->eventattendancepaymentstatus_id = 1;
            	$eventattendances = $user->eventattendances;
            	Mail::to($user->email)->cc('yamakara@field-mt.com')->send(new Attend($eventattendance, $tourmailtemplate));
        	}
        	else
        	{
            	$eventattendance->eventattendancestatus_id = 2;
            	$eventattendance->eventattendancepaymentstatus_id = 1; 
            	$eventattendances = $user->eventattendances;
            	Mail::to($user->email)->cc('yamakara@field-mt.com')->send(new Attend($eventattendance, $tourmailtemplate));
        	}

        	//ツアー参加の登録
        	$eventattendance->save();
        	$num = $i +1;
        	$request->name = $originalname. '同行者'. $num;
    	}

        for ($j = 0; $j < $request->numofchildren; $j++)
        {
        	$numchildren = $j +1;
        // メール、名前で該当者がいればそちらで登録、いなければ新規ユーザー登録
            $user = new User();
            $user->name = $originalname. '同行者(子供)'. $numchildren;
            $user->email = $request->email;
            $user->tel = $request->tel;
            $user->zipcode = $request->zipcode;
            $user->address = $request->address;
            $user->save();
        
        $eventprice = Eventprice::findOrFail($request->eventprice_id);
        $event = Event::findOrFail($eventprice->event_id);
        $tourmailtemplate = Tourmailtemplate::findOrFail($event->tourmailtemplate_id);
        $eventattendance = new Eventattendance();
        $eventattendance->user_id = $user->id;  
        $eventattendance->eventprice_id = $request->eventprice_id;
        $eventattendance->eventattendancepaymentmethod_id = $request->eventattendancepaymentmethod_id;
        $eventattendance->numofchildren = 1;
        $eventattendance->friendinfo = $request->friendinfo;
        $eventattendance->rental = $request->rental;
        $eventattendance->memo = $request->memo;

        //イベントがキャンセル待ちならキャンセル待ちで登録
        if($event->eventstatus_id == 6)
        {
            $eventattendance->eventattendancestatus_id = 4;
            $eventattendance->eventattendancepaymentstatus_id = 1;
            Mail::to($user->email)->cc('yamakara@field-mt.com')->send(new Waiting($eventattendance));
        }
        //イベント参加人数が定員越えたら、イベントをキャンセル待ちにして、キャンセル待ちで登録
        elseif($request->numofpeople + $event->Activetotalclients > $event->maxpax)
        {
            $event->eventstatus_id = 6;
            $event->save();
            Mail::to("yamakara@field-mt.com")->send(new Adminwaitingalert($event));
            $eventattendance->eventattendancestatus_id = 4;
            $eventattendance->eventattendancepaymentstatus_id = 1;
            Mail::to($user->email)->cc('yamakara@field-mt.com')->send(new Waiting($eventattendance));
        }       
         //イベント参加人数が定員と同数なら、イベントをキャンセル待ちにして、申込完了で登録
        elseif($request->numofpeople + $event->Activetotalclients == $event->maxpax)
        {
            $event->eventstatus_id = 6;
            $event->save();
            Mail::to("customer@field-mt.com")->send(new Adminwaitingalert($event));
            $eventattendance->eventattendancestatus_id = 2;
            $eventattendance->eventattendancepaymentstatus_id = 1;
            $eventattendances = $user->eventattendances;
            Mail::to($user->email)->cc('yamakara@field-mt.com')->send(new Attend($eventattendance, $tourmailtemplate));
        }
        else
        {
            $eventattendance->eventattendancestatus_id = 2;
            $eventattendance->eventattendancepaymentstatus_id = 1; 
            $eventattendances = $user->eventattendances;
            Mail::to($user->email)->cc('yamakara@field-mt.com')->send(new Attend($eventattendance, $tourmailtemplate));
        }

        //ツアー参加の登録
        $eventattendance->save();
    	}

        return redirect()->away('http://www.yamakara.com');
    }

    public function createrental($user_id, $eventprice_id)
    {
        $rentaldeliveries = Rentaldelivery::all()->pluck('name', 'id');
        $eventattendance = Eventattendance::where('user_id', $user_id)->where('eventprice_id', $eventprice_id)->first();
        if(!$eventattendance)
        {
            return redirect()->away('http://www.yamakara.com');
        }else
        {
            return view('yamakaraweb.rentalform')->with(compact('eventattendance', 'rentaldeliveries'));
        }
    }

    public function updaterental(Request $request, $id)
    {
        $eventattendance = Eventattendance::findOrFail($id);
        $eventattendance->fill($request->all())->save();
        Mail::to($eventattendance->user->email)->cc('yamakara@field-mt.com')->send(new Rentalconfirm($eventattendance));
        return redirect()->away('http://www.yamakara.com');
    }

    public function attendcheck($user_id, $eventprice_id)
    {
        $eventattendance = Eventattendance::where('user_id', $user_id)->where('eventprice_id', $eventprice_id)->where('eventattendancestatus_id', 5)->first();
        if(!$eventattendance)
        {
            return redirect()->away('http://www.yamakara.com');
        }else
        {
            return view('yamakaraweb.attendcheck')->with(compact('eventattendance'));
        }
    }

    public function updateattend(Request $request, $id)
    {
        $eventattendance = Eventattendance::findOrFail($id);
        if($request->check == 1)
        {
            $eventprice = Eventprice::findOrFail($eventattendance->eventprice_id);
            $event = Event::findOrFail($eventprice->event_id);
            $tourmailtemplate = Tourmailtemplate::findOrFail($event->tourmailtemplate_id);
            $eventattendance->eventattendancestatus_id = 2;
            $eventattendance->save();
            Mail::to($eventattendance->user->email)->cc('yamakara@field-mt.com')->send(new Attend($eventattendance, $tourmailtemplate));
        }elseif($request->check == 2)
        {
            $eventattendance->eventattendancestatus_id = 6;
            $eventattendance->save();
            // 入金済みならキャンセル料計算待ちへ
            if($eventattendance->eventattendancepaymentstatus_id == 2)
            {
                $balance = new Balance();
                $balance->user_id = $eventattendance->user_id;
                $balance->eventattendance_id = $id;
                $balance->day = date("Y-m-d");
                $balance->balancetype_id = 4;
                $balance->suspend = $eventattendance->Sales;
                $balance->save();
            }
            //キャンセルメール送信
            Mail::to($eventattendance->user->email)->cc('yamakara@field-mt.com')->send(new Cancel($eventattendance));
            //キャンセル待ちがいたら繰上
            $event = Event::findOrFail($eventattendance->eventprice->event_id);
            if($event->Waitingclients != "[]")
            {
                $numofpossible = $event->maxpax - $event->Activetotalclients;
                if($numofpossible > 0)
                {
                    for($i = 0;$i < $numofpossible; $i++)
                    {
                        $person = $event->Clients->where('eventattendancestatus_id', 4)->first();
                        $personattendance = Eventattendance::findOrFail($person->id);
                        $personattendance->eventattendancestatus_id = 5;
                        $personattendance->save();
                        Mail::to($person->user->email)->cc('yamakara@field-mt.com')->send(new Attendcheck($person));
                    }
                }
                }else
                {
                // キャンセル待ちがいなければツアーステータス判定
                    if($event->Totalpossibleclients < $event->maxpax)
                    {
                        $event->eventstatus_id =5;
                        $event->save();
                    }
                }
        }
    return redirect()->away('http://www.yamakara.com');
    }

    public function cancelform($user_id, $eventprice_id)
    {
        $eventattendance = Eventattendance::where('user_id', $user_id)->where('eventprice_id', $eventprice_id)->where('eventattendancestatus_id', 2)->first();
        if(!$eventattendance)
        {
            return redirect()->away('http://www.yamakara.com');
        }else
        {
            return view('yamakaraweb.cancelform')->with(compact('eventattendance'));
        }
    }

    public function confirmcancel(Request $request, $id)
    {
        $eventattendance = Eventattendance::findOrFail($id);
        if($request->check == 1)
        {
            $eventattendance->eventattendancestatus_id = 6;
            $eventattendance->save();
            // 入金済みならキャンセル料計算待ちへ
            if($eventattendance->eventattendancepaymentstatus_id == 2)
            {
                $balance = new Balance();
                $balance->user_id = $eventattendance->user_id;
                $balance->eventattendance_id = $id;
                $balance->day = date("Y-m-d");
                $balance->balancetype_id = 4;
                $balance->suspend = $eventattendance->Sales;
                $balance->save();
            }
            //キャンセルメール送信
            Mail::to($eventattendance->user->email)->cc('yamakara@field-mt.com')->send(new Cancel($eventattendance));
            //キャンセル待ちがいたら繰上
            $event = Event::findOrFail($eventattendance->eventprice->event_id);
            if($event->Waitingclients != "[]")
            {
                $numofpossible = $event->maxpax - $event->Activetotalclients;
                if($numofpossible > 0)
                {
                    for($i = 0;$i < $numofpossible; $i++)
                    {
                        $person = $event->Clients->where('eventattendancestatus_id', 4)->first();
                        $personattendance = Eventattendance::findOrFail($person->id);
                        $personattendance->eventattendancestatus_id = 5;
                        $personattendance->save();
                        Mail::to($person->user->email)->cc('yamakara@field-mt.com')->send(new Attendcheck($person));
                    }
                }
                }else
                {
                // キャンセル待ちがいなければツアーステータス判定
                    if($event->Totalpossibleclients < $event->maxpax)
                    {
                        $event->eventstatus_id =5;
                        $event->save();
                    }
                }
        }
    return redirect()->away('http://www.yamakara.com');

    }
}
