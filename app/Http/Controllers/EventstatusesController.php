<?php

namespace Field\Http\Controllers;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Field\Eventstatus;

class EventstatusesController extends Controller
{
    //
    public function index(){
    	$eventstatuses = Eventstatus::all();

    	return view('eventstatuses.index')->with('eventstatuses', $eventstatuses);
    }

    public function show($id){
    	$eventstatus = Eventstatus::findOrFail($id);

    	return view('eventstatuses.show')->with('eventstatus', $eventstatus);
    }

    public function create(){
    	return view('eventstatuses.create');
    }

    public function store(Request $request){
    	$eventstatus = new Eventstatus();
    	$eventstatus->name = $request->name;
    	$eventstatus->save();
    	return redirect('/eventstatuses');
    }

    public function edit($id) {
      $eventstatus = Eventstatus::findOrFail($id);
      return view('eventstatuses.edit')->with('eventstatus', $eventstatus);
    }

    public function update(Request $request, $id) {
      $eventstatus = Eventstatus::findOrFail($id);
      $eventstatus->name = $request->name;
      $eventstatus->save();
      return redirect('/eventstatuses')->with('flash_message', 'Eventstatus Updated!');
    }

    public function destroy($id) {
      $eventstatus = Eventstatus::findOrFail($id);
      $eventstatus->delete();
      return redirect('/eventstatuses')->with('flash_message', 'Deleted!');
  }
}
