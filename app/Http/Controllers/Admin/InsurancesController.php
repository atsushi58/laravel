<?php

namespace Field\Http\Controllers\Admin;

use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Field\Http\Controllers\Controller;
use Field\Admin;
use Field\Insurance;

class InsurancesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(){
    	$insurances = Insurance::all();
    	return view('admin.insurances.index')->with(compact('insurances'));
    }

    public function store(Request $request){
    	$insurance = new Insurance();
        $insurance->fill($request->all())->save();
    	return redirect()->route('admin.insurances');
    }


    public function edit($id){
        $insurance = Insurance::findOrFail($id);
        return view('admin.insurances.edit')->with(compact('insurance'));
    }

	public function update(Request $request, $id) {
        $insurance = Insurance::findOrFail($id);
        $insurance->fill($request->all())->save();
        return redirect()->route('admin.insurances');
    }

	public function destroy($id) {
    	$insurance = Insurance::findOrFail($id);
    	$insurance->delete();
      	return redirect()->route('admin.insurances');
  }
}
