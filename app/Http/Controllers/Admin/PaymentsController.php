<?php

namespace Field\Http\Controllers\Admin;

use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Field\Http\Controllers\Controller;
use Field\Admin;
use Field\Payment;
use Field\Workplace;

class PaymentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $payments = Payment::select('payments.*')->join('admins', 'admins.id', '=', 'payments.admin_id')->where('active', 1)->get();
        return view('admin.payments.index')->with(compact('payments'));
    }

    public function create(){
        $admins = Admin::where('active', 1)->pluck('name', 'id');
        $workplaces = Workplace::where('id', '>', 1)->pluck('name', 'id');
        return view('admin.payments.create')->with(compact('admins', 'workplaces'));
    }

    public function store(Request $request){
    	$payment = new Payment();
        $payment->fill($request->all())->save();
        return redirect()->route('payments.index');
    }

    public function edit($id){
        $payment = Payment::findOrFail($id);
        $admins = Admin::where('active', 1)->pluck('name', 'id');
        $workplaces = Workplace::where('id', '>', 1)->pluck('name', 'id');
        return view('admin.payments.edit')->with(compact('payment', 'admins', 'workplaces'));
    }

    public function update(Request $request, $id) {
        $payment = payment::findOrFail($id);
        $payment->fill($request->all())->save();
        return redirect()->route('payments.index');
    }

    public function destroy($id) {
        $payment = Payment::findOrFail($id);
        $payment->delete();
        return redirect()->route('payments.index');
  }
}
