<?php

namespace Field\Http\Controllers\Admin;

use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Field\Http\Controllers\Controller;
use Field\Admin;
use Field\Role;

class RolesController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(){
    	$roles = Role::all();

    	return view('admin.roles.index')->with(compact('roles'));
    }

    public function store(Request $request){
    	$role = new Role();
        $role->name = $request->name;
    	$role->save();
    	return redirect()->route('admin.roles');
    }

    public function edit($id){
        $role = Role::findOrFail($id);
        return view('admin.roles.edit')->with(compact('role'));
    }

	public function update(Request $request, $id) {
        $role = Role::findOrFail($id);
        $role->name = $request->name;
    	$role->save();
        return redirect()->route('admin.roles');
    }

	public function destroy($id) {
    	$role = role::findOrFail($id);
    	$role->delete();
      	return redirect()->route('admin.roles');
  }
}
