<?php

namespace Field\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Field\Http\Controllers\Controller;
use Field\Admin;
use Field\Role;
use Field\Bankaccount;
use Field\Bank;

class AdminsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = Admin::where('active', '1')->get();
        return view('admin.admins.index')->with(compact('admins'));
    }

    public function all()
    {
        $admins = Admin::all();
        return view('admin.admins.index')->with(compact('admins'));
    }

    public function show ($id){
        $admin = Admin::findOrFail($id);
        return view('admin.admins.show')->with(compact('admin'));
    }

    public function create(){
        $roles = Role::all()->where('id', '>', 2)->pluck('name', 'id');
        return view('admin.admins.create')->with(compact('roles'));
    }

    public function createbankaccount($id){
        $banks = Bank::all()->pluck('name', 'id');
        return view('admin.admins.createbankaccount')->with(compact('banks', 'id'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);
        $admin = new Admin();
        $admin->fill($request->all())->save();
        return redirect()->route('admin.admins');
    }

    public function storebankaccount(Request $request){
        $bankaccount = new Bankaccount();
        $bankaccount->fill($request->all())->save();
        return redirect()->route('admin.admin.show', $bankaccount->admin_id);
    }

    public function edit($id){
        $admin = Admin::findOrFail($id);
        $roles = Role::all()->where('id', '!=', 1)->pluck('name', 'id');
        return view('admin.admins.edit')->with(compact('admin', 'roles'));
    }

    public function edittax($id){
        $admin = Admin::findOrFail($id);
        return view('admin.admins.edittax')->with(compact('admin'));
    }

    public function editbankaccount($id){
        $admin = Admin::findOrFail($id);
        $bankaccount = $admin->bankaccount;
        $banks = Bank::all()->pluck('name', 'id');
        return view('admin.admins.editbankaccount')->with(compact('bankaccount', 'banks'));
    }

    public function update(Request $request, $id){
        $admin = Admin::findOrFail($id);
        $admin->fill($request->all())->save();
        return redirect()->route('admin.admin.show', $id);
    }

    public function updatebankaccount(Request $request, $id){
        $bankaccount = Bankaccount::findOrFail($id);
        $bankaccount->fill($request->all())->save();
        return redirect()->route('admin.admin.show', $bankaccount->admin_id);
    }
    
    public function destroy($id){
        $admin = Admin::FindOrFail($id);
        $admin->active = 0;
        $admin->save();
        return redirect()->route('admin.admins');
    }
}
