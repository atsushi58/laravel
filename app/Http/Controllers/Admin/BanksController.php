<?php

namespace Field\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Field\Http\Controllers\Controller;
use Field\Bank;

class BanksController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(){
    	$banks = Bank::orderBy('bankcode')->get();
    	return view('admin.banks.index')->with(compact('banks'));
    }

    public function create()
    {
        return view('admin.banks.create');
    }

    public function store(Request $request){
    	$bank = new Bank();
        $bank->fill($request->all())->save();
    	return redirect()->route('banks.index');
    }

    public function edit($id){
        $bank = Bank::findOrFail($id);
        return view('admin.banks.edit')->with(compact('bank'));
    }

	public function update(Request $request, $id) {
        $bank = Bank::findOrFail($id);
        $bank->fill($request->all())->save();
        return redirect()->route('banks.index');
    }
}
