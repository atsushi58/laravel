<?php

namespace Field\Http\Controllers\Admin;

use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Field\Http\Controllers\Controller;
use Field\Admin;
use Field\Tax;

class TaxesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(){
    	$taxes = Tax::all();
    	return view('admin.taxes.index')->with(compact('taxes'));
    }

    public function store(Request $request){
    	$tax = new Tax();
        $tax->fill($request->all())->save();
    	return redirect()->route('admin.taxes');
    }

    public function edit($id){
        $tax = Tax::findOrFail($id);
        return view('admin.taxes.edit')->with(compact('tax'));
    }

	public function update(Request $request, $id) {
        $tax = tax::findOrFail($id);
        $tax->fill($request->all())->save();
        return redirect()->route('admin.taxes');
    }

	public function destroy($id) {
    	$tax = tax::findOrFail($id);
    	$tax->delete();
      	return redirect()->route('admin.taxes');
  }
}
