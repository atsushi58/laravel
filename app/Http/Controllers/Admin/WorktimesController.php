<?php

namespace Field\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Field\Http\Controllers\Controller;
use Field\Worktime;

class WorktimesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(){
    	$worktimes = Worktime::all();

    	return view('admin.worktimes.index')->with(compact('worktimes'));
    }

    public function store(Request $request){
    	$worktime = new Worktime();
        $worktime->fill($request->all())->save();
    	return redirect()->route('admin.worktimes');
    }

    public function edit($id){
        $worktime = worktime::findOrFail($id);
        return view('admin.worktimes.edit')->with(compact('worktime'));
    }

	public function update(Request $request, $id) {
        $worktime = worktime::findOrFail($id);
        $worktime->fill($request->all())->save();
        return redirect()->route('admin.worktimes');
    }

	public function destroy($id) {
    	$worktime = worktime::findOrFail($id);
    	$worktime->delete();
      	return redirect()->route('admin.worktimes');
  }
}