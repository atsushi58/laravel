<?php

namespace Field\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Field\Http\Controllers\Controller;
use Field\Task;
use Field\Admin\Division;

class TasksController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(){
    	$tasks = Task::all();
    	return view('admin.tasks.index')->with(compact('tasks'));
    }

    public function create()
    {
        $divisions = Division::all()->pluck('name', 'id');
        return view('admin.tasks.create')->with(compact('divisions'));
    }

    public function store(Request $request){
    	$task = new Task();
        $task->fill($request->all())->save();
    	return redirect()->route('tasks.index');
    }

    public function edit($id){
        $task = Task::findOrFail($id);
        $divisions = Division::all()->pluck('name', 'id');
        return view('admin.tasks.edit')->with(compact('task', 'divisions'));
    }

	public function update(Request $request, $id) {
        $task = Task::findOrFail($id);
        $task->fill($request->all())->save();
        return redirect()->route('tasks.index');
    }

	public function destroy($id) {
    	$task = Task::findOrFail($id);
    	$task->delete();
        return redirect()->route('admin.tasks');
  }
}
