<?php

namespace Field\Http\Controllers\Admin;

use Carbon\Carbon;

use Illuminate\Http\Request;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Field\Shift;
use Field\Workplace;
use Field\Worktime;
use Field\Admin;
use Field\Task;
use Field\Payment;

class ShiftsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index($workplace_id, $date){
        $date = Carbon::parse($date);
    	$shifts = Shift::where('workplace_id', $workplace_id)->where('workday', $date)->orderBy('confirm', 'ASC')->get();
        $workplaces = Workplace::where('id', '<>', '1')->pluck('name', 'id');
        $title = $date->copy()->format('Y年m月d日'). '/' . $workplaces[$workplace_id];
        $thisDay = Carbon::today()->startofWeek();
        $twoweeks = array();
        for ($i = 0; $i <14; $i++){
            $twoweeks[] = $thisDay->toDateString("m-d");
            $thisDay->addDay(1);
        };
    	return view('admin.shifts.index')->with(compact('shifts', 'workplace_id', 'workplaces', 'date', 'twoweeks', 'title'));
    }

    public function shiftsbymonth($year, $month)
    {
        $shifts = Shift::whereYear('workday', $year)->whereMonth('workday', $month)->get();
        $title = $year. '年'. $month. '月';
        $thisDay = Carbon::today()->startofWeek();
        $twoweeks = array();
        for ($i = 0; $i <14; $i++){
            $twoweeks[] = $thisDay->toDateString("m-d");
            $thisDay->addDay(1);
        };

        //総労働時間
        $totalworktime = floor($shifts->sum('worktime')/60). '時間'. ($shifts->sum('worktime')%60) . '分';
        $totalpay = $shifts->sum('basepay');

        // レンタルのタスク
        $rentaltask = Task::where('division_id', '=', '2')->pluck('id');
        $rentaltasks = json_decode(json_encode($rentaltask), true);
        $totalrentalworktime = $shifts->where('secondarytask_id', '1')->whereIn('primarytask_id', $rentaltasks)->sum('worktime') + $shifts->whereIn('secondarytask_id', $rentaltasks)->sum('worktime')/2;
        return view('admin.shifts.index')->with(compact('shifts', 'twoweeks', 'title', 'year', 'month', 'totalworktime', 'totalpay', 'totalrentalworktime', 'rentaltasks'));
    }

    public function inserthourlypay($year, $month)
    {
        $shifts = Shift::whereYear('workday', $year)->whereMonth('workday', $month)->get();
        foreach($shifts as $shift){
            if(empty($shift->hourlypay) || empty($shift->transportation))
            {
                if(isset($shift->actual_end))
                {
                    if(null !== Payment::where('admin_id', $shift->admin_id)->where('workplace_id', $shift->workplace_id))
                    {
                        $paydata = Payment::where('admin_id', $shift->admin_id)->where('workplace_id', $shift->workplace_id)->first();
                        $shift->hourlypay = $paydata->payment;
                        $shift->transportation = $paydata->transportation;
                        $shift->save();
                    }
                }
            }
        }
        return redirect()->route('admin.shifts.bymonth', [$year, $month]);
    }

    public function finishedshifts($workplace_id, $date){
        $date = Carbon::parse($date);
        $shifts = Shift::where('workplace_id', $workplace_id)->where('workday', $date);
        $workplaces = Workplace::where('id', '<>', '1')->pluck('name', 'id');
        $title = $date->copy()->format('Y年m月d日'). '/' . $workplaces[$workplace_id];
        $thisDay = Carbon::today()->startofWeek();
        $twoweeks = array();
        for ($i = 0; $i <14; $i++){
            $twoweeks[] = $thisDay->toDateString("m-d");
            $thisDay->addDay(1);
        };
        return view('admin.shifts.finishedshifts')->with(compact('shifts', 'workplace_id', 'workplaces', 'date', 'twoweeks', 'title'));
    }

    public function confirming($id){
      $shift = Shift::findOrFail($id);
      $shift->confirm = 1;
      $shift->save();
      return redirect()->back();
    }

    public function declining($id)
    {
        $shift = Shift::findOrFail($id);
        $shift->confirm = 2;
        $shift->save();
        return redirect()->back();

    }

    public function show($id){
        $shift = Shift::findOrFail($id);
        return view('admin.shifts.show')->with(compact('shift'));
    }

    public function edit($id){
        $shift = Shift::findOrFail($id);
        $admins = Admin::all()->pluck('name', 'id');
        $workplaces = Workplace::all()->pluck('name', 'id');
        $worktimes = Worktime::all()->pluck('worktime', 'id');
        $tasks = Task::all()->pluck('name', 'id');
        return view('admin.shifts.edit')->with(compact('shift', 'admins', 'workplaces', 'worktimes', 'tasks'));
    }

    public function finishededit($id){
        $shift = Shift::findOrFail($id);
        $admins = Admin::all()->pluck('name', 'id');
        $workplaces = Workplace::all()->pluck('name', 'id');
        $worktimes = Worktime::all()->pluck('worktime', 'id');
        $tasks = Task::all()->pluck('name', 'id');
        return view('admin.shifts.finishededit')->with(compact('shift', 'admins', 'workplaces', 'worktimes', 'tasks'));
    }

    public function finishedupdate(Request $request, $id) {
        $shift = Shift::findOrFail($id);
        $shift->fill($request->all())->save();
        return redirect()->route('admin.finishedshifts', [$shift->workplace_id, date('Y-m-d')] );
    }

    public function create(){
        $worktimes = Worktime::all()->pluck('worktime', 'id');
        $workplaces = Workplace::whereNotIn('id', ['1', '2'])->pluck('name', 'id');
        $tasks = Task::all()->pluck('name', 'id');
        $admins = Admin::where('active', '1')->pluck('name', 'id');
    	return view('admin.shifts.create')->with(compact('worktimes', 'workplaces', 'tasks', 'admins'));
    }

    public function store(Request $request){
        if(Shift::where('admin_id', $request->admin_id)->where('workday', $request->workday)->exists())
        {
            return redirect()->route('admin.shifts', [$shift->workplace_id, date('Y-m-d')]);
        }else
        {
        $shift = new Shift();
        $shift->fill($request->all())->save();
        $shift->save();
        return redirect()->route('admin.shifts', [$shift->workplace_id, date('Y-m-d')]);
        }
    }

    public function update(Request $request, $id) {
        $shift = Shift::findOrFail($id);
        $shift->fill($request->all())->save();
        return redirect()->route('admin.shifts', [$shift->workplace_id, $shift->workday]);
    }

    public function destroy($id) {
      $shift = Shift::findOrFail($id);
      $shift->delete();
      return redirect()->back();
    }
}
