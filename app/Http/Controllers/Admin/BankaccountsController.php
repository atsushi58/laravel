<?php

namespace Field\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Field\Http\Controllers\Controller;
use Field\Bankaccount;
use Field\Admin;
use Field\Bank;

class bankaccountsController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(){
    	$bankaccounts = Bankaccount::select('bankaccounts.*')->join('admins', 'admins.id', '=', 'bankaccounts.admin_id')->where('admins.active', 1)->get();
    	return view('admin.bankaccounts.index')->with(compact('bankaccounts'));
    }

    public function create()
    {
        $admins = Admin::where('active', 1)->pluck('name', 'id');
        $banks = Bank::all()->pluck('name', 'id');
        return view('admin.bankaccounts.create')->with(compact('admins', 'banks'));
    }

    public function store(Request $request){
    	$bankaccount = new Bankaccount();
        $bankaccount->fill($request->all())->save();
    	return redirect()->route('bankaccounts.index');
    }

    public function edit($id){
        $bankaccount = Bankaccount::findOrFail($id);
        $admins = Admin::where('active', 1)->pluck('name', 'id');
        $banks = Bank::all()->pluck('name', 'id');
        return view('admin.bankaccounts.edit')->with(compact('bankaccount', 'admins', 'banks'));
    }

	public function update(Request $request, $id) {
        $bankaccount = Bankaccount::findOrFail($id);
        $bankaccount->fill($request->all())->save();
        return redirect()->route('bankaccounts.index');
    }
}
