<?php
namespace Field\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Field\Http\Controllers\Controller;
use Field\Http\Controllers\CommonClass\StatementsClass;
use Field\Shift;
use Field\Salary;
use Field\Admin;
use Field\Payment;

class SalariesController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index($id, $year, $month){
        $user = Admin::where('id', $id)->first();
    	$shifts = Shift::where('admin_id', $id)->whereYear('workday', '=', $year)->whereMonth('workday', '=', $month)->whereNotIn('confirm', ['2'])->get();
        $admins = Admin::where('active', '1')->pluck('name', 'id');
        $salary = Salary::where('admin_id', $id)->where('year', $year)->where('month', $month)->first();
        // 前月の設定
        if($month == 1){
            $prevyear = $year-1;
            $prevmonth = 12;
        }else{
            $prevyear = $year;
            $prevmonth = $month - 1;
        };
        //翌月の設定
        if($month == 12){
            $nextyear = $year + 1;
            $nextmonth = 1;
        }else{
            $nextyear = $year;
            $nextmonth = $month +1;
        }

        $statement = new StatementsClass();
        $amounts = $statement->amounts($id, $year, $month);
        $totalworkinghours = $shifts->sum('worktime');
        
        return view('admin.salaries.index')->with(compact('shifts', 'user', 'admins', 'year', 'month', 'prevyear', 'prevmonth', 'nextyear', 'nextmonth', 'salary', 'amounts', 'totalworkinghours'));
    }

    public function renew($id, $year, $month)
    {
        $user = Admin::where('id', $id)->first();
        $shifts = Shift::where('admin_id', $id)->whereYear('workday', '=', $year)->whereMonth('workday', '=', $month)->whereNotIn('confirm', ['2'])->get();
        $admins = Admin::where('active', '1')->pluck('name', 'id');

        foreach($shifts as $shift)
        {
            $payment = Payment::where('admin_id', '=', $id)->where('workplace_id', '=', $shift->workplace_id)->first();
            if(isset($payment->payment))
                {
                    $hourlypayment = $payment->payment;
                }else
                {
                    $hourlypayment = 0;
                }
            $shift->hourlypay = $hourlypayment;
            $workinghours = (strtotime($shift->actual_end)-strtotime($shift->actual_start))/3600;
                    $totalpayment = $workinghours * $hourlypayment;
        if($totalpayment > 0){
            $shift->basepayment = $totalpayment;
        }else{
            $shift->basepayment = 0;
        }
        $shift->save();
        
        }
        return redirect()->route('admin.salary', [$user->id, $year, $month]);
    }

    public function salarylist($year, $month){
        $salaries = Salary::select(['salaries.*'])->where('year', $year)->where('month', $month)->orderBy('salaries.basesalary', 'desc')->join('admins', 'admins.id', '=', 'salaries.admin_id')->whereIn('admins.role_id', [4,5])->get();
        $totalamount = $salaries->sum('Total');
        $workingpeople = $salaries->count();
        return view('admin.salaries.salarylist')->with(compact('salaries', 'workingpeople', 'totalamount', 'year', 'month'));
    }

    public function unconfirmedsalarylist($year, $month)
    {
        $activeadmins = Shift::whereYear('workday', '=', $year)->whereMonth('workday', '=', $month)->pluck('admin_id')->toArray();
        $activeadmins_unique_key = array_unique($activeadmins);
        $activeadmins_unique = array_values($activeadmins_unique_key);
        $statementlist =[];
        foreach($activeadmins as $key => $value)
        {
            $statement = new StatementsClass();
            $statementlist += [$statementlist[$value] = $statement->amounts($value, $year, $month)];
        }
        unset($statementlist[0]);
        return view('admin.salaries.unconfirmedsalarylist')->with(compact('year', 'month', 'activeadmins_unique', 'statementlist'));

    }

    public function create($user_id, $year, $month){
        $statement = new StatementsClass();
        $amounts = $statement->amounts($user_id, $year, $month);
        return view('admin.salaries.create')->with(compact('user_id', 'year', 'month', 'amounts'));
    }

    public function createfulltime(){
        $admins = Admin::where('role_id', ['1', '2'])->pluck('name', 'id');
        return view('admin.salaries.createfulltime')->with(compact('admins'));   
    }

    public function store(Request $request){
        $salary = new Salary();
        $salary->fill($request->all())->save();
        $salary->save();
        return redirect()->route('admin.salary', [$salary->admin_id, $salary->year, $salary->month]);
    }

    public function edit($id){
        $salary = Salary::findOrFail($id);
        return view('admin.salaries.edit')->with(compact('salary'));
    }

    public function update(Request $request, $id){
        $salary = Salary::findOrFail($id);
        $salary->fill($request->all())->save();
        return redirect()->route('admin.salary', [$salary->admin_id, $salary->year, $salary->month]);
    }

    public function releaseaccountingconfirm($id)
    {
        $salary = Salary::findOrFail($id);
        $salary->accountingconfirm = 0;
        $salary->save();
        return redirect()->back();

    }
}
