<?php

namespace Field\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Field\Http\Controllers\Controller;
use Field\Workplace;


class WorkplacesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(){
    	$workplaces = Workplace::all();

    	return view('admin.workplaces.index')->with(compact('workplaces'));
    }

    public function store(Request $request){
    	$workplace = new Workplace();
        $workplace->fill($request->all())->save();
    	return redirect()->route('admin.workplaces');
    }

    public function edit($id){
        $workplace = Workplace::findOrFail($id);
        return view('admin.workplaces.edit')->with(compact('workplace'));
    }

	public function update(Request $request, $id) {
        $workplace = Workplace::findOrFail($id);
        $workplace->fill($request->all())->save();
        return redirect()->route('admin.workplaces');
    }

	public function destroy($id) {
    	$workplace = Workplace::findOrFail($id);
    	$workplace->delete();
      	return redirect()->route('admin.workplaces');
  }
}
