<?php

namespace Field\Http\Controllers;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Field\Courselength;
use Field\Course;

class CourselengthsController extends Controller
{
    //

    public function index(){
    	$courselengths = Courselength::all();
        $courselengths = Courselength::Paginate(10);

    	return view('courselengths.index')->with('courselengths', $courselengths);
    }

    public function show($id){
    	$courselength = Courselength::findOrFail($id);

    	return view('courselengths.show')->with('courselength', $courselength);
    }

    public function create(){
    	return view('courselengths.create');
    }

    public function store(Request $request){
    	$courselength = new Courselength();
    	$courselength->name = $request->name;
    	$courselength->save();
    	return redirect('/courselengths');
    }

    public function edit($id) {
      $courselength = Courselength::findOrFail($id);
      return view('courselengths.edit')->with('courselength', $courselength);
    }

    public function update(Request $request, $id) {
      $courselength = Courselength::findOrFail($id);
      $courselength->name = $request->name;
      $courselength->save();
      return redirect('/courselengths')->with('flash_message', 'Courselength Updated!');
    }

    public function destroy($id) {
      $courselength = Courselength::findOrFail($id);
      $courselength->delete();
      return redirect('/courselengths')->with('flash_message', 'Deleted!');
  }
}
