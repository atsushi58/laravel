<?php

namespace Field\Http\Controllers;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Field\Course;
use Field\Coursearea;
use Field\Courselength;
use Field\Depplace;

class CoursesController extends Controller
{
    //
    public function index(){
    	$courses = Course::all();
    	return view('courses.index')->with(compact('courses'));
    }

    public function show($id){
    	$course = Course::findOrFail($id);
    	return view('courses.show')->with(compact('course'));
    }

    public function create(){
    	$courseareas = Coursearea::all()->pluck('name', 'id');
        $courselengths = Courselength::all()->pluck('name', 'id');
        $depplaces = Depplace::all()->pluck('name', 'id');
    	return view('courses.create')->with(compact('courseareas', 'courselengths', 'depplaces'));
    }

    public function store(Request $request){
    	$course = new Course();
    	$course->coursearea_id = $request->coursearea_id;
    	$course->name = $request->name;
    	$course->memo = $request->memo;
    	$course->save();
    	return redirect('/courses');
    }

    public function edit($id) {
        $course = Course::findOrFail($id);
        $courseareas = Coursearea::pluck('name', 'id');
        $courselengths = Courselength::puck('name', 'id');
        return view('courses.edit')->with(compact('course', 'courseareas', 'courselengths'));
    }

    public function update(Request $request, $id) {
        $course = Course::findOrFail($id);
        $course->coursearea_id = $request->coursearea_id;
        $course->name = $request->name;
        $course->memo = $request->memo;
        $course->save();
        return redirect('/courses')->with('flash_message', 'course Updated!');
    }

    public function destroy($id) {
      $course = Course::findOrFail($id);
      $course->delete();
      return redirect('/courses')->with('flash_message', 'Deleted!');
  }

}
