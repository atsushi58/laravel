<?php

namespace Field\Http\Controllers;

use Illuminate\Support\Facades\Route;

use Carbon\Carbon;

use Illuminate\Http\Request;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Field\Shift;
use Field\Workplace;
use Field\Payment;

class TimecardsController extends Controller
{
    public function index($id)
    {
        $workplace = Workplace::findOrFail($id);
        if($id == 2)
        {
            $workplaces = Workplace::where('id', '<>', '1')->get();
            $shiftToday = Shift::where('workday', '>', Carbon::today()->startofWeek())->where('workday', '<', Carbon::today()->startofWeek()->addDay(14))->where('workplace_id', '=', $id)
                  ->get();
            return view('timecards.tc')->with(compact('shiftToday', 'workplaces'));
        }else
        {
        $workplaces = Workplace::where('id', '<>', '1')->get();
    	$shiftToday = Shift::where('workday', '=', Carbon::today())->where('workplace_id', '=', $id)->whereIn('confirm', ['0', '1'])->get();
    	return view('timecards.index')->with(compact('shiftToday', 'workplace', 'workplaces', 'id', 'payment'));
        }
    }

    public function updatestart(Request $request, $id){
    	$shift = Shift::findOrFail($id);
        $shift->actual_start = date("Y-m-d H:i:s");
        $shift->fill($request->all())->save();
        return redirect()->route('timecards', $shift->workplace_id);
    }

    public function updateend(Request $request, $id){
        $shift = Shift::findOrFail($id);
        
        //時給・交通費を算出
        $payment = Payment::where('admin_id', '=', $shift->admin_id)->where('workplace_id', '=', $shift->workplace_id)->first();
        if(isset($payment->payment))
        {
            $hourlypayment = $payment->payment;
        }else{
            $hourlypayment = 0;
        }
        $shift->hourlypay = $hourlypayment;
        $shift->transportation = $payment->transportation;
        $shift->actual_end = date("Y-m-d H:i:s");

        //休憩の有り無しの判別
        $workinghours = (strtotime($shift->actual_end)-strtotime($shift->actual_start))/3600;
        if(isset($shift->workplace->lunchbreaksetting) && $workinghours > $shift->workplace->lunchbreaksetting){
                $break = 60;
            }else{
                $break = 0;
        }
        
        $shift->break = $break;

        //合計日当
        $totalpayment = $workinghours * $hourlypayment;
        if($totalpayment > 0){
            $shift->basepayment = $totalpayment;
        }else{
            $shift->basepayment = 0;
        }

        $shift->fill($request->all())->save();
        return redirect()->route('timecards', $shift->workplace_id);
    }
}
