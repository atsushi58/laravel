<?php

namespace Field\Http\Controllers\Staff;

use Illuminate\Http\Request;
use Field\Http\Controllers\Controller;
use Field\Staffmailcategory;


class StaffmailcategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(){
    	$staffmailcategories = Staffmailcategory::all();
    	return view('staff.staffmailcategories.index')->with(compact('staffmailcategories'));
    }

    public function store(Request $request){
    	$staffmailcategory = new Staffmailcategory();
        $staffmailcategory->fill($request->all())->save();
    	return redirect()->route('staff.staffmailcategories');
    }

    public function edit($id){
        $staffmailcategory = Staffmailcategory::findOrFail($id);
        return view('staff.staffmailcategories.edit')->with(compact('staffmailcategory'));
    }


	public function update(Request $request, $id) {
        $staffmailcategory = Staffmailcategory::findOrFail($id);
        $staffmailcategory->fill($request->all())->save();
        return redirect()->route('staff.staffmailcategories');
    }

	public function destroy($id) {
    	$staffmailcategory = Staffmailcategory::findOrFail($id);
    	$staffmailcategory->delete();
      	return redirect()->route('staff.staffmailcategories');
  }
}
