<?php

namespace Field\Http\Controllers\Staff;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Field\Http\Controllers\Controller;
use Field\Admin;
use Field\Bank;
use Field\Bankaccount;

class BankaccountsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function create(){
        $banks = Bank::all()->pluck('name', 'id');
        return view('staff.bankaccount.create')->with(compact('banks'));
    }

    public function store(Request $request){
        $bankaccount = new Bankaccount();
    	$bankaccount->fill($request->all())->save();
        return redirect()->route('staff.user');
    }

    public function edit($id){
        $bankaccount = Bankaccount::findOrFail($id);
        $banks = Bank::all()->pluck('name', 'id');
        return view('staff.bankaccount.edit')->with(compact('bankaccount', 'banks'));
    }

    public function update(Request $request, $id) {
        $bankaccount = Bankaccount::findOrFail($id);
        $bankaccount->bank_id = $request->bank_id;
        $bankaccount->branch = $request->branch;
        $bankaccount->accountno = $request->accountno;
    	$bankaccount->save();
    }
}
