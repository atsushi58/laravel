<?php

namespace Field\Http\Controllers\Staff;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Field\Http\Controllers\Controller;

class PasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function changepassword()
    {
        $admin = Auth::user();
        return view('staff.password.changepassword')->with(compact('admin'));
    }

    public function updatepassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|string|min:6',
            'password' => 'confirmed',
        ], [
            'パスワードは6文字以上で設定してください',
            '2つの入力されたパスワードが異なります']);
    	$admin = Auth::user();
    	$admin->password = bcrypt($request->password);
    	$admin->save();
        return redirect()->route('staff.user');

    }
}