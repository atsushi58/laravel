<?php

namespace Field\Http\Controllers\Staff;

use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Field\Http\Controllers\Controller;
use Field\Event;
use Field\Shift;
use Field\Workplace;
use Field\Worktime;
use Field\Admin;
use Field\Task;

class ShiftsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function thisweek(){
        $thisMon = Carbon::today()->startofWeek();
        $thisDay = Carbon::today()->startofWeek();
        $twoweeks = array();
        for ($i = 0; $i <14; $i++){
            $twoweeks[] = $thisDay->toDateString("Y-m-d");
            $thisDay->addDay(1);
        };

        $nextSun = Carbon::today()->startofWeek()->addDay(14);
        
        $user = Auth::guard('admin')->user();
        
        $shifts = $user->shifts->where('workday', '>=', $thisMon->subDay(7))->where('workday', '<', $nextSun);
        
        return view('staff.shifts.thisweek')->with(compact('thisMon', 'shifts', 'twoweeks'));
    }


    public function nextweek(){
        if(Carbon::today()->diffInDays(Carbon::today()->startofWeek()) < 3){
            $startOfDate = Carbon::today()->startofWeek()->addDay(7)
            ;}
        else{
            $startOfDate = Carbon::today()->startofWeek()->addDay(14);
        }
        $user = Auth::guard('admin')->user();
        $shifts = $user->shifts->where('workday', '>=', $startOfDate->copy()->subDay(1))->where('workday', '<', $startOfDate->copy()->addDay(7));
        $nextweek = array();
        for ($i =0; $i <7; $i++){
            $nextweek[]= $startOfDate->toDateString("Y-m-d");
            $startOfDate->addDay(1);
        }
        return view('staff.shifts.nextweek')->with(compact('nextweek', 'shifts', 'startOfDate', 'user'));
    }

    public function confirmed($workplace, $date){
        $shifts = Shift::where('workplace_id', 'workplace')->where('workday', $date);
        return view('staff.shifts.confirmed')->with(compact('shifts'));
    }


    public function nextweekedit($id){
        $shift = Shift::findOrFail($id);
        $worktimes = Worktime::all()->pluck('worktime', 'id');
        $workplaces = Workplace::where('id', '<>', '1')->pluck('name', 'id');
        return view('staff.shifts.nextweekedit')->with(compact('shift', 'worktimes', 'workplaces'));
    }


    public function nextweekcreate($date){
        $worktimes = Worktime::all()->pluck('worktime', 'id');
        $workplaces = Workplace::where('id', '<>', '1')->pluck('name', 'id');
        $tasks = Task::all()->pluck('name', 'id');
        $admins = Admin::all()->pluck('name', 'id');
        return view('staff.shifts.nextweekcreate')->with(compact('worktimes', 'workplaces', 'tasks', 'admins', 'date'));
    }

    public function nextweekstore(Request $request){
        if(Shift::where('admin_id', $request->admin_id)->where('workday', $request->workday)->exists())
        {
            return redirect()->route('staff.shifts.nextweek');
        }else
        {
            $shift = new Shift();
            $shift->fill($request->all())->save();
            $shift->save();
            return redirect()->route('staff.shifts.nextweek');
        }
    }

    public function nextweekupdate(Request $request, $id) {
        $shift = Shift::findOrFail($id);
        $shift->fill($request->all())->save();      
        $shift->save();
        return redirect()->route('staff.shifts.nextweek');
    }

    public function nextweekdestroy($id) {
      $shift = Shift::findOrFail($id);
      $shift->delete();
      return redirect()->route('staff.shifts.nextweek');
    }
}
