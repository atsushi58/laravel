<?php

namespace Field\Http\Controllers\Staff;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Field\Http\Controllers\Controller;
use Field\Http\Controllers\CommonClass\StatementsClass;

use Field\Admin;
use Field\Shift;

class SalariesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index($year, $month)
    {
        $user = Auth::user();
    	$shifts = Shift::where('admin_id', Auth::user()->id)->whereYear('workday', '=', $year)->whereMonth('workday', '=', $month)->get();
        $admins = Admin::where('active', '1')->pluck('name', 'id');
        $salary = Auth::user()->salaries->where('year', $year)->where('month', $month)->first();
        // 前月の設定
        if($month == 1){
            $prevyear = $year-1;
            $prevmonth = 12;
        }else{
            $prevyear = $year;
            $prevmonth = $month - 1;
        };
        //翌月の設定
        if($month == 12){
            $nextyear = $year + 1;
            $nextmonth = 1;
        }else{
            $nextyear = $year;
            $nextmonth = $month +1;
        }

        $statement = new StatementsClass();
        $amounts = $statement->amounts(Auth::id(), $year, $month);
        $totalworkinghours = $shifts->sum('worktime');
        
        return view('admin.salaries.index')->with(compact('shifts', 'user', 'admins', 'year', 'month', 'prevyear', 'prevmonth', 'nextyear', 'nextmonth', 'salary', 'amounts', 'totalworkinghours'));
    }
}