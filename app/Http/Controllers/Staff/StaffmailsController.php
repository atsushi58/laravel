<?php

namespace Field\Http\Controllers\Staff;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Sichikawa\LaravelSendgridDriver\SendGrid;
use Field\Mail\StaffMailable;

use Field\Staffmail;
use Field\Staffmailcategory;
use Field\Admin;

class StaffmailsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
    	$staffmails = Staffmail::all();
    	return view('staff.staffmails.index')->with(compact('staffmails'));
    }

    public function create()
    {
        $staffmailcategories = Staffmailcategory::all()->pluck('name', 'id');
    	return view('staff.staffmails.create')->with(compact('staffmailcategories'));
    }

    /*
    public function store(Request $request)
    {
   		$staffmail = new Staffmail();
    	$staffmail->fill($request->all())->save();
        if($staffmail->staffmailcategory->id == 1){
            $tos = Admin::whereIn('role_id', [1,2,3])->where('active',1)->pluck('email')->toArray();
        }elseif($staffmail->staffmailcategory->id == 2){
            $tos = Admin::where('active', 1)->get();
        }
        foreach($tos as $to){
        Mail::to($to)->send(new Staff($staffmail));
        }
        //Mail::send(new Staff($staffmail));
        return redirect()->route('staff.staffmails');
    }

    */

    public function store(Request $request)
    {
        $staffmail = new Staffmail();
        $staffmail->fill($request->all())->save();
        if($staffmail->staffmailcategory->id == 1){
            Admin::whereIn('role_id', [1,2,3])->where('active',1)->chunk(1000,  function($admins) {
                Mail::send(new StaffMailable($admins));
            });
        }elseif($staffmail->staffmailcategory->id == 2){
            Admin::where('active', 1)->chunk(1000, function($admins) {
                Mail::send(new StaffMailable($admins));
            });
        }
        return redirect()->route('staff.staffmails');
    }
}