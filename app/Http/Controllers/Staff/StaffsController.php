<?php

namespace Field\Http\Controllers\Staff;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Field\Http\Controllers\Controller;

use Field\Admin;

class StaffsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = Admin::where('active', '1')->get();
        return view('admin.admins.index')->with(compact('admins'));
    }

    public function show($id){
        $admin = Admin::findOrFail($id);
        return view('admin.admins.show')->with(compact('admin'));
    }
}
 
