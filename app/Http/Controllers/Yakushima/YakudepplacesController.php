<?php

namespace Field\Http\Controllers\Yakushima;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use Field\Yakushima\Yakudepplace;

class YakudepplacesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index()
	{
		$yakudepplaces = Yakudepplace::all();
		return view('yakushima.yakudepplaces.index')->with(compact('yakudepplaces'));
	}

	public function create()
	{
		return view('yakushima.yakudepplaces.create');
	}

	public function store(Request $request)
	{
		$yakudepplace = new Yakudepplace();
		$yakudepplace->fill($request->all())->save();
		return redirect()->route('yakudepplaces.index');
		
	}

	public function edit($id)
	{
		$yakudepplace = Yakudepplace::findOrFail($id);
		return view('yakushima.yakudepplaces.edit')->with(compact('yakudepplace'));
	}

	public function update(Request $request, $id)
	{
		$yakudepplace = Yakudepplace::findOrFail($id);
		$yakudepplace->fill($request->all())->save();
		return redirect()->route('yakudepplaces.index');
	}
}
