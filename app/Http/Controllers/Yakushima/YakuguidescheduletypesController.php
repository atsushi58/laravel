<?php

namespace Field\Http\Controllers\Yakushima;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use Field\Yakushima\Yakuguidescheduletype;

class YakuguidescheduletypesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index()
	{
		$yakuguidescheduletypes = Yakuguidescheduletype::all();
		return view('yakushima.yakuguidescheduletypes.index')->with(compact('yakuguidescheduletypes'));
	}

	public function create()
	{
		return view('yakushima.yakuguidescheduletypes.create');
	}

	public function store(Request $request)
	{
		$yakuguidescheduletype = new Yakuguidescheduletype();
		$yakuguidescheduletype->fill($request->all())->save();
		return redirect()->route('yakuguidescheduletypes.index');
	}

	public function edit($id)
	{
		$yakuguidescheduletype = Yakuguidescheduletype::findOrFail($id);
		return view('yamakara.yakuguidescheduletypes.edit')->with(compact('Yakuguidescheduletype'));
	}

	public function update(Request $request, $id)
	{
		$yakuguidescheduletype = Yakuguidescheduletype::findOrFail($id);
		$yakuguidescheduletype->fill($request->all())->save();
		return redirect()->route('yakuguidescheduletypes.index');
	}
}
