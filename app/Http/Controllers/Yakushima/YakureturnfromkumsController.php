<?php

namespace Field\Http\Controllers\Yakushima;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use Field\Yakureturnfromkum;

class YakureturnfromkumsController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index()
	{
		$yakureturnfromkums = Yakureturnfromkum::all();
		return view('yakushima.yakureturnfromkums.index')->with(compact('yakureturnfromkums'));
	}

	public function create()
	{
		return view('yakushima.yakureturnfromkums.create');
	}

	public function store(Request $request)
	{
		$yakureturnfromkum = new Yakureturnfromkum();
		$yakureturnfromkum->fill($request->all())->save();
		return redirect()->route('yakureturnfromkums.index');
	}

	public function edit($id)
	{
		$yakureturnfromkum = Yakureturnfromkum::findOrFail($id);
		return view('yakushima.yakureturnfromkums.edit')->with(compact('yakureturnfromkum'));
	}

	public function update(Request $request, $id)
	{
		$yakureturnfromkum = Yakureturnfromkum::findOrFail($id);
		$yakureturnfromkum->fill($request->all())->save();
		return redirect()->route('yakureturnfromkums.index');
	}
}
