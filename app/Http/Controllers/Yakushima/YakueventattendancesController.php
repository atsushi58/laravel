<?php

namespace Field\Http\Controllers\Yakushima;

use Illuminate\Http\Request;

use Carbon\Carbon;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;

use Field\User;
use Field\Yakushima\Yakuaccompany;
use Field\Yakushima\Yakuactivity;
use Field\Yakushima\Yakudefaultactivity;
use Field\Yakushima\Yakueventattendance;
use Field\Yakushima\Yakucourse;



class YakueventattendancesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
    	$eventattendances = Yakueventattendance::all();
    	return view('yakushima.yakueventattendances.index')->with(compact('eventattendances'));
    }

    public function show($id)
    {
        $yakueventattendance = Yakueventattendance::findOrFail($id);
        return view('yakushima.yakueventattendances.show')->with(compact('yakueventattendance'));
    }

    public function create($id)
    {
    	$yakucourse = Yakucourse::findOrFail($id);
        $allclients = User::orderBy('kana')->get();
        $clients = $allclients->pluck('namelist', 'id');
    	return view('yakushima.yakueventattendances.create')->with(compact('yakucourse', 'clients'));
    }

    public function store(Request $request)
    {
        $yakueventattendance = new Yakueventattendance();
    	$yakueventattendance->fill($request->all())->save();
    	$yakucourse = Yakucourse::findOrFail($yakueventattendance->yakucourse_id);
    	if(Yakudefaultactivity::where('yakucourse_id', '=', $yakucourse->id)->exists())
    	{
    		foreach($yakucourse->yakudefaultactivities as $yakudefaultactivity)
    		{
                $depdate = new Carbon($yakueventattendance->depdate);
                $activitydate = $depdate->addDay($yakudefaultactivity->day-1);
                if(Yakuactivity::where('depdate', '=', $activitydate)->where('yakuactivitytype_id', '=', $yakudefaultactivity->yakuactivitytype_id)->where('full', '<', 1)->exists())
                {
                    $yakuactivity = Yakuactivity::where('depdate', '=', $activitydate)->where('yakuactivitytype_id', '=', $yakudefaultactivity->yakuactivitytype_id)->where('full', '<', 1)->first();

                }else{
                    $yakuactivity = new Yakuactivity();
                    $yakuactivity->yakuactivitytype_id = $yakudefaultactivity->yakuactivitytype_id;
                    $yakuactivity->depdate = $activitydate;
                    $yakuactivity->full = 0;
                    $yakuactivity->save();
                }
                $yakueventattendance->yakuactivities()->attach($yakuactivity);
                if($yakuactivity->yakueventattendances->count('id') == $yakuactivity->yakuactivitytype->limit)
                {
                    $yakuactivity->full = 1;
                    $yakuactivity->save();
                }
            }
    	}
        return redirect()->route('yakucourses.show', $yakucourse->id);
    }

    public function editaccompany($id)
    {
        $accompany = Yakuaccompany::findOrFail($id);
        return view('yakushima.yakueventattendances.editaccompany')->with(compact('accompany'));
    }

    public function updateaccompany(Request $request, $id)
    {
        $accompany = Yakuaccompany::findOrFail($id);
        $accompany->fill($request->all())->save();
        return redirect()->route('yakueventattendances.show', $accompany->yakueventattendance_id);
    }



}
