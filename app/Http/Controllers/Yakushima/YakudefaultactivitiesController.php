<?php

namespace Field\Http\Controllers\Yakushima;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use Field\Yakushima\Yakuactivity;
use Field\Yakushima\Yakuactivitytype;
use Field\Yakushima\Yakucourse;
use Field\Yakushima\Yakudefaultactivity;
use Field\Yakushima\Yakuguide;

class YakudefaultactivitiesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function create($yakucourse_id)
	{
		$yakucourse = Yakucourse::findOrFail($yakucourse_id);
		$yakuactivitytypes = Yakuactivitytype::all()->pluck('name', 'id'); 
		return view('yakushima.yakudefaultactivities.create')->with(compact('yakuactivitytypes', 'yakucourse'));
	}

	public function store(Request $request)
	{
		$yakudefaultactivity = new Yakudefaultactivity();
		$yakudefaultactivity->fill($request->all())->save();
		return redirect()->route('yakucourses.show', $yakudefaultactivity->yakucourse_id);
	}

	public function edit($id)
	{
		$yakudefaultactivity = Yakudefaultactivity::findOrFail($id);
		$yakuactivitytypes = Yakuactivitytype::all()->pluck('name', 'id'); 
		return view('yakushima.yakudefaultactivities.edit')->with(compact('yakudefaultactivity', 'yakuactivitytypes'));
	}

	public function update(Request $request, $id)
	{
		$yakudefaultactivity = Yakudefaultactivity::findOrFail($id);
		$yakudefaultactivity->fill($request->all())->save();
		return redirect()->route('yakucourses.show', $yakudefaultactivity->yakucourse_id);
	}

	public function destroy($id)
	{
		$yakudefaultactivity = Yakudefaultactivity::findOrFail($id);
    	$yakudefaultactivity->delete();
      	return redirect()->back();
	}
}
