<?php

namespace Field\Http\Controllers\Yakushima;

use Illuminate\Http\Request;

use Carbon\Carbon;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use Field\Yakushima\Yakuactivity;
use Field\Yakushima\Yakuactivitytype;
use Field\Yakushima\Yakuguide;
use Field\Yakushima\Yakuguideschedule;

class YakuactivitiesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index()
	{
		$yakuactivities = Yakuactivity::all();
		return view('yakushima.yakuactivities.index')->with(compact('yakuactivities'));
	}

	public function show($id)
	{
		$yakuactivity = Yakuactivity::findOrFail($id);
		return view('yakushima.yakuactivities.show')->with(compact('yakuactivity'));
	}

	public function create()
	{
		$yakuactivitytypes = Yakuactivitytype::all()->pluck('name', 'id'); 
		$yakuguides = Yakuguide::all()->pluck('name', 'id');
		return view('yakushima.yakuactivities.create')->with(compact('yakuactivitytypes', 'yakuguides'));
	}

	public function store(Request $request)
	{
		$yakuactivity = new Yakuactivity();
		$yakuactivity->fill($request->all())->save();
		$yakuactivity->yakuguides()->attach($request->input('yakuguidelist'));
		return redirect()->route('yakuactivities.index');
	}

	public function edit($id)
	{
		return view('yakushima.yakuactivities.edit');
	}

	public function update(Request $request, $id)
	{
		$yakuactivity = Yakuactivity::findOrFail($id);
		$yakuactivity->fill($request->all())->save();
		return redirect()->route('yakuactivities.index');
	}

	public function guidefix($id)
	{
		$yakuguides = Yakuguide::all()->pluck('name', 'id');
		$yakuactivity = Yakuactivity::findOrFail($id);
		return view('yakushima.yakuactivities.guidefix')->with(compact('yakuactivity', 'yakuguides'));
	}

	public function guidestore(Request $request)
	{
		$yakuactivity = Yakuactivity::findOrFail($request->yakuactivity_id);
		$yakuactivity->yakuguides()->attach($request->yakuguides_id);
		
		//ガイドスケジュールの作成
		$date = new Carbon($yakuactivity->depdate);
		$yakuguideschedule = new Yakuguideschedule();
		$yakuguideschedule->yakuguide_id = $request->yakuguides_id;
		$yakuguideschedule->day = $date->copy()->format('Y-m-d');
		$yakuguideschedule->yakuguidescheduletype_id =1;
		$yakuguideschedule->memo = $yakuactivity->yakuactivitytype->name;
		$yakuguideschedule->save();

		if($yakuactivity->yakuactivitytype->length == 2)
		{
			$yakuguideschedule = new Yakuguideschedule();
			$yakuguideschedule->yakuguide_id = $request->yakuguides_id;
			$yakuguideschedule->day = $date->addDay()->format('Y-m-d');
			$yakuguideschedule->yakuguidescheduletype_id =1;
			$yakuguideschedule->memo = $yakuactivity->yakuactivitytype->name;
			$yakuguideschedule->save();
		}
		return redirect()->route('yakuactivities.index');
	}
}
