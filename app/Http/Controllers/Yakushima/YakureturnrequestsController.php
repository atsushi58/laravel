<?php

namespace Field\Http\Controllers\Yakushima;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use Field\Yakureturnfromkoj;
use Field\Yakureturnfromkum;
use Field\Yakureturnrequest;

class YakureturnrequestsController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index()
	{
		$yakureturnrequests = Yakureturnrequest::all();
		return view('yakushima.yakureturnrequests.index')->with(compact('yakureturnrequests'));
	}

	public function create()
	{
		$fromkojs = Yakureturnfromkoj::all()->pluck('name', 'id');
		$fromkums = Yakureturnfromkum::all()->pluck('name', 'id');
		return view('yakushima.yakureturnrequests.create')->with(compact('fromkojs', 'fromkums'));
	}

	public function store(Request $request)
	{
		$yakureturnrequest = new Yakureturnrequest();
		$yakureturnrequest->fill($request->all())->save();
		return redirect()->route('yakureturnrequests.index');
	}

	public function edit($id)
	{
		$yakureturnrequest = Yakureturnrequest::findOrFail($id);
		$fromkojs = Yakureturnfromkoj::all()->pluck('name', 'id');
		$fromkums = Yakureturnfromkum::all()->pluck('name', 'id');
		return view('yakushima.yakureturnrequests.edit')->with(compact('yakureturnrequest', 'fromkojs', 'fromkums'));
	}

	public function update(Request $request, $id)
	{
		$yakureturnrequest = Yakureturnrequest::findOrFail($id);
		$yakureturnrequest->fill($request->all())->save();
		return redirect()->route('yakureturnrequests.index');
	}
}
