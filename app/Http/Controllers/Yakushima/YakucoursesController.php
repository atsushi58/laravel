<?php

namespace Field\Http\Controllers\Yakushima;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Field\Admin;
use Field\Courselength;
use Field\Eventstatus;
use Field\Yakushima\Yakuactivitytype;
use Field\Yakushima\Yakuschedule;
use Field\Yakushima\Yakutourimage;
use Field\Yakushima\Yakucourse;
use Field\Yakushima\Yakucoursetype;
use Field\Yakushima\Yakudepplace;
use Field\Yakushima\Yakueventattendance;

class YakucoursesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
    	$courses = Yakucourse::all();
    	return view('yakushima.yakucourses.index')->with(compact('courses', 'courseareas', 'description'));
    }

    public function show($id)
    {
    	$yakucourse = Yakucourse::findOrFail($id);
        $yakueventattendances = Yakueventattendance::orderBy('depdate')->where('yakucourse_id', $yakucourse->id)->get();
    	return view('yakushima.yakucourses.show')->with(compact('yakucourse', 'yakueventattendances'));
    }

    public function create()
    {
        $yakucoursetypes = Yakucoursetype::all()->pluck('name', 'id');
        $courselengths = Courselength::all()->pluck('name', 'id');
        $yakudepplaces = Yakudepplace::all()->pluck('name', 'id');
    	return view('yakushima.yakucourses.create')->with(compact('yakucoursetypes', 'courselengths', 'yakudepplaces'));
    }

    public function store(Request $request)
    {
    	$course = new Yakucourse();
    	$course->fill($request->all())->save();
    	return redirect()->route('yakucourses.index');
    }

    public function edit($id)
    {
        $yakucourse = Yakucourse::findOrFail($id);
        $yakucoursetypes = Yakucoursetype::all()->pluck('name', 'id');
        $courselengths = Courselength::all()->pluck('name', 'id');
        $depplaces = Yakudepplace::all()->pluck('name', 'id');
        return view('yakushima.yakucourses.edit')->with(compact('yakucourse', 'yakucoursetypes', 'courselengths', 'depplaces'));
    }

    public function update(Request $request, $id)
    {
        $yakucourse = Yakucourse::findOrFail($id);
        $yakucourse->fill($request->all())->save();
        return redirect()->route('yakucourses.index');
    }

    public function createschedule(Request $request, $id)
    {
        $course = Course::findOrFail($id);
        return view('yakushima.courses.createschedule')->with(compact('course'));
    }

    public function storeschedule(Request $request, $id)
    {
        $course = Course::findOrFail($id);
        $schedule = new Schedule();
        $schedule->fill($request->all())->save();
        return redirect()->route('yakushima.course.show', $course->id);
    }

    public function editschedule($id)
    {
        $schedule = Schedule::findOrFail($id);
        $coursename = $schedule->course->name;
        return view('yakushima.courses.editschedule')->with(compact('schedule', 'coursename'));
    }

    public function updateschedule(Request $request, $id)
    {
        $schedule = Schedule::findOrFail($id);
        $schedule->fill($request->all())->save();
        return redirect()->route('yakushima.course.show', $schedule->course_id);
    }

    public function destroyschedule($id)
    {
        $schedule = Schedule::findOrFail($id);
        $schedule->delete();
        return redirect()->route('yakushima.course.show', $schedule->course_id);
    }

    public function storetourimage(Request $request, $id)
    {
        $course = Course::findOrFail($id);
        $tourimage = new Tourimage();
        $filename = $request->tourimage->getClientOriginalName();
        $image = Image::make($request->tourimage->getRealPath());
        $image->fit('970', '524')
              ->save(public_path() . '/yakutourimages/' . $filename)
              ->resize(200, 200, function ($constraint) {$constraint->aspectRatio();})
              ->save(public_path() . '/yakutourimages/' . 'thumbnail/' . $filename);
        $tourimage->filename = $filename;
        $tourimage->fill($request->all())->save();
        return redirect()->route('yakushima.course.show', $id);
    }

    public function destroytourimage($id)
    {
        $tourimage = Tourimage::findOrFail($id);
        $tourimage->delete();
        unlink(public_path() . '/yakutourimages/'.$tourimage->filename);
        unlink(public_path() . '/yakutourimages/thumbnail/'.$tourimage->filename);
        return redirect()->route('yakushima.course.show', $tourimage->course_id);
    }

    public function createevent($id)
    {
        $course = Yakucourse::findOrFail($id);
        $admins = Admin::all()->pluck('name', 'id');
        $eventstatuses = Eventstatus::all()->pluck('name', 'id');
        return view('yakushima.courses.createevent')->with(compact('course', 'admins', 'eventstatuses'));
    }

    public function storeevent(Request $request)
    {
        $event = new Event();
        $event->fill($request->all())->save();
        return redirect()->route('yakushima.course.show', $event->course_id);
    }

    public function createoptionactivity($id)
    {
        $yakucourse = Yakucourse::findOrFail($id);
        $yakuactivitytypes = Yakuactivitytype::all()->pluck('name', 'id');
        return view('yakushima.yakucourses.createoptionactivity')->with(compact('yakucourse', 'yakuactivitytypes'));
    }

    public function storeoptionactivity(Request $request)
    {
        $yakucourse = Yakucourse::findOrFail($request->yakucourse_id);
        $yakucourse->yakuactivitytypes()->attach($request->yakuactivitytype_id);
        return redirect()->route('yakucourses.show', $request->yakucourse_id);
    }

    public function detachoptionactivity($course_id, $activitytype_id)
    {
        $yakucourse = Yakucourse::findOrFail($course_id);
        $yakucourse->yakuactivitytypes()->detach($activitytype_id);
        return redirect()->route('yakucourses.show', $course_id);
    }
}
