<?php

namespace Field\Http\Controllers\Yakushima;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use Field\Yakushima\Yakuhosteltype;

class YakuhosteltypesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index()
	{
		$yakuhosteltypes = Yakuhosteltype::all();
		return view('yakushima.yakuhosteltypes.index')->with(compact('yakuhosteltypes'));
	}

	public function create()
	{
		return view('yakushima.yakuhosteltypes.create');
	}

	public function store(Request $request)
	{
		$yakuhosteltype = new Yakuhosteltype();
		$yakuhosteltype->fill($request->all())->save();
		return redirect()->route('yakuhosteltypes.index');
		
	}

	public function edit($id)
	{
		$yakuhosteltype = Yakuhosteltype::findOrFail($id);
		return view('yakushima.yakuhosteltypes.edit')->with(compact('yakuhosteltype'));
	}

	public function update(Request $request, $id)
	{
		$yakuhosteltype = Yakuhosteltype::findOrFail($id);
		$yakuhosteltype->fill($request->all())->save();
		return redirect()->route('yakuhosteltypes.index');
	}
}
