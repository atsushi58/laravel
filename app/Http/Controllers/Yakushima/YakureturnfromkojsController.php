<?php

namespace Field\Http\Controllers\Yakushima;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use Field\Yakureturnfromkoj;

class YakureturnfromkojsController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index()
	{
		$yakureturnfromkojs = Yakureturnfromkoj::all();
		return view('yakushima.yakureturnfromkojs.index')->with(compact('yakureturnfromkojs'));
	}

	public function create()
	{
		return view('yakushima.yakureturnfromkojs.create');
	}

	public function store(Request $request)
	{
		$yakureturnfromkoj = new Yakureturnfromkoj();
		$yakureturnfromkoj->fill($request->all())->save();
		return redirect()->route('yakureturnfromkojs.index');
	}

	public function edit($id)
	{
		$yakureturnfromkoj = Yakureturnfromkoj::findOrFail($id);
		return view('yakushima.yakureturnfromkojs.edit')->with(compact('yakureturnfromkoj'));
	}

	public function update(Request $request, $id)
	{
		$yakureturnfromkoj = Yakureturnfromkoj::findOrFail($id);
		$yakureturnfromkoj->fill($request->all())->save();
		return redirect()->route('yakureturnfromkojs.index');
	}
}
