<?php

namespace Field\Http\Controllers\Yakushima;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use Field\Yakuhostelarrangestatus;

class YakuhostelarrangestatusesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index()
	{
		$yakuhostelarrangestatuses = Yakuhostelarrangestatus::all();
		return view('yakushima.yakuhostelarrangestatuses.index')->with(compact('yakuhostelarrangestatuses'));
	}

	public function create()
	{
		return view('yakushima.yakuhostelarrangestatuses.create');
	}

	public function store(Request $request)
	{
		$yakuhostelarrangestatus = new Yakuhostelarrangestatus();
		$yakuhostelarrangestatus->fill($request->all())->save();
		return redirect()->route('yakuhostelarrangestatuses.index');
	}

	public function edit($id)
	{
		$yakuhostelarrangestatus = Yakuhostelarrangestatus::findOrFail($id);
		return view('yamakara.yakuhostelarrangestatuses.edit')->with(compact('Yakuhostelarrangestatus'));
	}

	public function update(Request $request, $id)
	{
		$yakuhostelarrangestatus = Yakuhostelarrangestatus::findOrFail($id);
		$yakuhostelarrangestatus->fill($request->all())->save();
		return redirect()->route('yakuhostelarrangestatuses.index');
	}
}
