<?php

namespace Field\Http\Controllers\Yakushima;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use Field\Yakushima\Yakucourse;
use Field\Yakushima\Yakucourseprice;


class YakucoursepricesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index($course_id, $year, $month)
	{
		$yakucourse = Yakucourse::findOrFail($course_id);
		$yakucourseprices = Yakucourseprice::where('yakucourse_id', $course_id)->whereYear('depdate', $year)->whereMonth('depdate', $month);
		return view('yakushima.yakucourseprices.index')->with(compact('yakucourse', 'yakucourseprices', 'year', 'month'));
	}

	public function create($course_id, $year, $month)
	{
		
	}

	public function createdefaultprice($course_id)
	{
		$yakucourse = Yakucourse::findOrFail($course_id);
		return view('yakushima.yakucourseprices.createdefaultprice')->with(compact('yakucourse'));
	}

	public function create30advanceprice($course_id)
	{
		$yakucourse = Yakucourse::findOrFail($course_id);
		return view('yakushima.yakucourseprices.create30advanceprice')->with(compact('yakucourse'));
	}

	public function create60advanceprice($course_id)
	{
		$yakucourse = Yakucourse::findOrFail($course_id);
		return view('yakushima.yakucourseprices.create60advanceprice')->with(compact('yakucourse'));
	}

	public function createdateprice($course_id)
	{
		$yakucourse = Yakucourse::findOrFail($course_id);
		return view('yakushima.yakucourseprices.createdateprice')->with(compact('yakucourse'));
	}

	public function store(Request $request)
	{
		$yakucourseprice = new Yakucourseprice();
		$yakucourseprice->fill($request->all())->save();
		return redirect()->route('yakucourses.show', $yakucourseprice->yakucourse_id);
	}


}
