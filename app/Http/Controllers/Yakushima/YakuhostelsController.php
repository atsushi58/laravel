<?php

namespace Field\Http\Controllers\Yakushima;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use Field\Yakushima\Yakuhostel;

class YakuhostelsController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index()
	{
		$yakuhostels = Yakuhostel::all();
		return view('yakushima.yakuhostels.index')->with(compact('yakuhostels'));
	}

	public function create()
	{
		return view('yakushima.yakuhostels.create');
	}

	public function store(Request $request)
	{
		$yakuhostel = new Yakuhostel();
		$yakuhostel->fill($request->all())->save();
		return redirect()->route('yakuhostels.index');
		
	}

	public function edit($id)
	{
		$yakuhostel = Yakuhostel::findOrFail($id);
		return view('yakushima.yakuhostels.edit')->with(compact('yakuhostel'));
	}

	public function update(Request $request, $id)
	{
		$yakuhostel = Yakuhostel::findOrFail($id);
		$yakuhostel->fill($request->all())->save();
		return redirect()->route('yakuhostels.index');
	}
}
