<?php

namespace Field\Http\Controllers\Yakushima;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use Field\Yakushiparrangestatus;

class YakushiparrangestatusesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index()
	{
		$yakushiparrangestatuses = Yakushiparrangestatus::all();
		return view('yakushima.yakushiparrangestatuses.index')->with(compact('yakushiparrangestatuses'));
	}

	public function create()
	{
		return view('yakushima.yakushiparrangestatuses.create');
	}

	public function store(Request $request)
	{
		$yakushiparrangestatus = new Yakushiparrangestatus();
		$yakushiparrangestatus->fill($request->all())->save();
		return redirect()->route('yakushiparrangestatuses.index');
	}

	public function edit($id)
	{
		$yakushiparrangestatus = Yakushiparrangestatus::findOrFail($id);
		return view('yamakara.yakushiparrangestatuses.edit')->with(compact('Yakushiparrangestatus'));
	}

	public function update(Request $request, $id)
	{
		$yakushiparrangestatus = Yakushiparrangestatus::findOrFail($id);
		$yakushiparrangestatus->fill($request->all())->save();
		return redirect()->route('yakushiparrangestatuses.index');
	}
}
