<?php

namespace Field\Http\Controllers\Yakushima;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use Field\Yakushima\Yakucourse;
use Field\Yakushima\Yakudefaulthostelarrange;
use Field\Yakushima\Yakuhosteltype;

class YakudefaulthostelarrangesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function create($yakucourse_id)
	{
		$yakucourse = Yakucourse::findOrFail($yakucourse_id);
		$yakuhosteltypes = Yakuhosteltype::all()->pluck('name', 'id'); 
		return view('yakushima.yakudefaulthostelarranges.create')->with(compact('yakuhosteltypes', 'yakucourse'));
	}

	public function store(Request $request)
	{
		$yakudefaulthostelarrange = new Yakudefaulthostelarrange();
		$yakudefaulthostelarrange->fill($request->all())->save();
		return redirect()->route('yakucourses.show', $yakudefaulthostelarrange->yakucourse_id);
	}

	public function edit($id)
	{
		$yakudefaulthostelarrange = Yakudefaulthostelarrange::findOrFail($id);

		$yakuhosteltypes = Yakuhosteltype::all()->pluck('name', 'id'); 
		return view('yakushima.yakudefaulthostelarranges.edit')->with(compact('yakudefaulthostelarrange', 'yakuhosteltypes'));
	}

	public function update(Request $request, $id)
	{
		$yakudefaulthostelarrange = Yakudefaulthostelarrange::findOrFail($id);
		$yakudefaulthostelarrange->fill($request->all())->save();
		return redirect()->route('yakucourses.show', $yakudefaulthostelarrange->yakucourse_id);
	}

	public function destroy($id)
	{
		$yakudefaulthostelarrange = Yakudefaulthostelarrange::findOrFail($id);
    	$yakudefaulthostelarrange->delete();
      	return redirect()->back();
	}
}
