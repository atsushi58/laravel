<?php

namespace Field\Http\Controllers\Yakushima;

use Illuminate\Http\Request;

use Carbon\Carbon;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use Field\Yakushima\Yakuguide;
use Field\Yakushima\Yakuguideschedule;
use Field\Yakushima\Yakuguidescheduletype;

class YakuguidesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index()
	{
		$yakuguides = Yakuguide::all();
		return view('yakushima.yakuguides.index')->with(compact('yakuguides'));
	}

	public function create()
	{
		return view('yakushima.yakuguides.create');
	}

	public function store(Request $request)
	{
		$yakuguide = new Yakuguide();
		$yakuguide->fill($request->all())->save();
		return redirect()->route('yakuguides.index');
	}

	public function edit($id)
	{
		$yakuguide = Yakuguide::findOrFail($id);
		return view('yakushima.yakuguides.edit')->with(compact('yakuguide'));
	}

	public function update(Request $request, $id)
	{
		$yakuguide = Yakuguide::findOrFail($id);
		$yakuguide->fill($request->all())->save();
		return redirect()->route('yakuguides.index');
	}

	public function show($id)
	{

	}

	public function worklist($date)
	{
		$date = new Carbon($date);
		$days = [];
		$days[1] = $date->copy()->subDay(3)->format("Y-m-d");
		$days[2] = $date->copy()->subDay(2)->format("Y-m-d");
		$days[3] = $date->copy()->subDay(1)->format("Y-m-d");
		$days[4] = $date->copy()->format("Y-m-d");
		$days[5] = $date->copy()->addDay(1)->format("Y-m-d");
		$days[6] = $date->copy()->addDay(2)->format("Y-m-d");
		$days[7] = $date->copy()->addDay(3)->format("Y-m-d");
		$yakuguides = Yakuguide::orderBy('orderid')->get();
		$year = $date->copy()->format("Y");
		$month = $date->copy()->format("m");
		return view('yakushima.yakuguides.worklist')->with(compact('yakuguides', 'days', 'year', 'month'));
	}

	public function personworklist($id, $year, $month)
	{
		$yakuguide = Yakuguide::findOrFail($id);
		$yakuguideschedules = Yakuguideschedule::where('yakuguide_id', '=', $id)->whereYear('day', '=', $year)->whereMonth('day', '=', $month)->get();
		return view('yakushima.yakuguides.personworklist')->with(compact('yakuguideschedules', 'yakuguide', 'year', 'month'));
	}

	public function worklistup($id)
	{
		$guide = Yakuguide::findOrFail($id);
		$guide->orderid = $guide->orderid -1;
		$guide->save();
		$prevguide = Yakuguide::where('orderid', '=', $guide->orderid)->first();
		$prevguide->orderid += 1;
		$prevguide->save();
		return redirect()->back();
	}

	public function createschedule($id)
	{
		$yakuguide = Yakuguide::findOrFail($id);
		$yakuguidescheduletypes = Yakuguidescheduletype::all()->pluck('name', 'id');
		return view('yakushima.yakuguides.createschedule')->with(compact('yakuguide', 'yakuguidescheduletypes'));
	}

	public function createscheduledate($id, $date)
	{
		$yakuguide = Yakuguide::findOrFail($id);
		$yakuguidescheduletypes = Yakuguidescheduletype::all()->pluck('name', 'id');
		return view('yakushima.yakuguides.createscheduledate')->with(compact('yakuguide', 'yakuguidescheduletypes', 'date'));
	}

	public function storeschedule(Request $request)
	{
		$yakuguideschedule = new Yakuguideschedule();
		$yakuguideschedule->fill($request->all())->save();
		return redirect()->route('yakuguides.worklist', $yakuguideschedule->day);
	}

	public function editschedule($id)
	{
		$yakuguideschedule = Yakuguideschedule::findOrFail($id);
		$yakuguidescheduletypes = Yakuguidescheduletype::all()->pluck('name', 'id');

		return view('yakushima.yakuguides.editschedule')->with(compact('yakuguideschedule', 'yakuguidescheduletypes'));
	}

	public function updateschedule(Request $request, $id)
	{
		
	}
}
