<?php

namespace Field\Http\Controllers\Yakushima;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use Field\Yakuattendancetype;
use Field\Yakuflightarrangestatus;

class YakuattendancetypesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index()
	{
		$yakuattendancetypes = Yakuattendancetype::all();
		return view('yakushima.yakuattendancetypes.index')->with(compact('yakuattendancetypes'));
	}

	public function create()
	{
		$yakuflightarrangestatuses = Yakuflightarrangestatus::all()->pluck('name', 'id');
		return view('yakushima.yakuattendancetypes.create')->with(compact('yakuflightarrangestatuses'));
	}

	public function store(Request $request)
	{
		$yakuattendancetype = new Yakuattendancetype();
		$yakuattendancetype->fill($request->all())->save();
		return redirect()->route('yakuattendancetypes.index');
	}

	public function edit($id)
	{
		$yakuattendancetype = Yakuattendancetype::findOrFail($id);
		return view('yakushima.yakuattendancetypes.edit')->with(compact('yakuattendancetype'));
	}

	public function update(Request $request, $id)
	{
		$yakuattendancetype = Yakuattendancetype::findOrFail($id);
		$yakuattendancetype->fill($request->all())->save();
		return redirect()->route('yakuattendancetypes.index');
	}
}
