<?php

namespace Field\Http\Controllers\Yamakara;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Field\Course;
use Field\Yakutourimage;

class YakutourimagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function create($id)
    {
        $course = Course::findOrFail($id);
        return view('yamakara.tourimages.create')->with(compact('course'));
    }

    public function store(Request $request)
    {
        $tourimage = new Tourimage();
        $filename = $request->tourimage->getClientOriginalName();
        $image = Image::make($request->tourimage->getRealPath());
        $image->fit('970', '524')
              ->save(public_path() . '/tourimages/' . $filename)
              ->resize(200, 200, function ($constraint) {$constraint->aspectRatio();})
              ->save(public_path() . '/tourimages/' . 'thumbnail/' . $filename);
        $tourimage->filename = $filename;
        $tourimage->fill($request->all())->save();
        return redirect()->route('yamakara.course.show', $id);
    }

    public function destroy($id)
    {
        $tourimage = Tourimage::findOrFail($id);
        $tourimage->delete();
        unlink(public_path() . '/tourimages/'.$tourimage->filename);
        unlink(public_path() . '/tourimages/thumbnail/'.$tourimage->filename);
        return redirect()->route('yamakara.course.show', $tourimage->course_id);
    }
}
