<?php

namespace Field\Http\Controllers\Yakushima;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use Field\Yakushima\Yakucoursetype;

class YakucoursetypesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index()
	{
		$yakucoursetypes = Yakucoursetype::all();
		return view('yakushima.yakucoursetypes.index')->with(compact('yakucoursetypes'));
	}

	public function create()
	{
		return view('yakushima.yakucoursetypes.create');
	}

	public function store(Request $request)
	{
		$yakucoursetype = new Yakucoursetype();
		$yakucoursetype->fill($request->all())->save();
		return redirect()->route('yakucoursetypes.index');
	}

	public function edit($id)
	{
		$yakucoursetype = Yakucoursetype::findOrFail($id);
		return view('yakushima.yakucoursetypes.edit')->with(compact('yakucoursetype'));
	}

	public function update(Request $request, $id)
	{
		$yakucoursetype = Yakucoursetype::findOrFail($id);
		$yakucoursetype->fill($request->all())->save();
		return redirect()->route('yakucoursetypes.index');
	}
}
