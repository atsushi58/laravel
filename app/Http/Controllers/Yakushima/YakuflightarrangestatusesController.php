<?php

namespace Field\Http\Controllers\Yakushima;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use Field\Yakuflightarrangestatus;

class YakuflightarrangestatusesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index()
	{
		$yakuflightarrangestatuses = Yakuflightarrangestatus::all();
		return view('yakushima.yakuflightarrangestatuses.index')->with(compact('yakuflightarrangestatuses'));
	}

	public function create()
	{
		return view('yakushima.yakuflightarrangestatuses.create');
	}

	public function store(Request $request)
	{
		$yakuflightarrangestatus = new Yakuflightarrangestatus();
		$yakuflightarrangestatus->fill($request->all())->save();
		return redirect()->route('yakuflightarrangestatuses.index');
	}

	public function edit($id)
	{
		$yakuflightarrangestatus = Yakuflightarrangestatus::findOrFail($id);
		return view('yamakara.yakuflightarrangestatuses.edit')->with(compact('Yakuflightarrangestatus'));
	}

	public function update(Request $request, $id)
	{
		$yakuflightarrangestatus = Yakuflightarrangestatus::findOrFail($id);
		$yakuflightarrangestatus->fill($request->all())->save();
		return redirect()->route('yakuflightarrangestatuses.index');
	}
}
