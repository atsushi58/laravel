<?php

namespace Field\Http\Controllers\Yakushima;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use Field\Yakucometokoj;

class YakucometokojsController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index()
	{
		$yakucometokojs = Yakucometokoj::all();
		return view('yakushima.yakucometokojs.index')->with(compact('yakucometokojs'));
	}

	public function create()
	{
		return view('yakushima.yakucometokojs.create');
	}

	public function store(Request $request)
	{
		$yakucometokoj = new Yakucometokoj();
		$yakucometokoj->fill($request->all())->save();
		return redirect()->route('yakucometokojs.index');
	}

	public function edit($id)
	{
		$yakucometokoj = Yakucometokoj::findOrFail($id);
		return view('yakushima.yakucometokojs.edit')->with(compact('yakucometokoj'));
	}

	public function update(Request $request, $id)
	{
		$yakucometokoj = Yakucometokoj::findOrFail($id);
		$yakucometokoj->fill($request->all())->save();
		return redirect()->route('yakucometokojs.index');
	}
}
