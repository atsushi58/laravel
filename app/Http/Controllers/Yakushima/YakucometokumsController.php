<?php

namespace Field\Http\Controllers\Yakushima;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use Field\Yakucometokum;

class YakucometokumsController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index()
	{
		$yakucometokums = Yakucometokum::all();
		return view('yakushima.yakucometokums.index')->with(compact('yakucometokums'));
	}

	public function create()
	{
		return view('yakushima.yakucometokums.create');
	}

	public function store(Request $request)
	{
		$yakucometokum = new Yakucometokum();
		$yakucometokum->fill($request->all())->save();
		return redirect()->route('yakucometokums.index');
	}

	public function edit($id)
	{
		$yakucometokum = Yakucometokum::findOrFail($id);
		return view('yakushima.yakucometokums.edit')->with(compact('yakucometokum'));
	}

	public function update(Request $request, $id)
	{
		$yakucometokum = Yakucometokum::findOrFail($id);
		$yakucometokum->fill($request->all())->save();
		return redirect()->route('yakucometokums.index');
	}
}
