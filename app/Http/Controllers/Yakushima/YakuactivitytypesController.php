<?php

namespace Field\Http\Controllers\Yakushima;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use Field\Yakushima\Yakuactivity;
use Field\Yakushima\Yakuactivitytype;
use Field\Yakushima\Yakudefaultactivity;

class YakuactivitytypesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index()
	{
		$yakuactivitytypes = Yakuactivitytype::all();
		return view('yakushima.yakuactivitytypes.index')->with(compact('yakuactivitytypes'));
	}

	public function create()
	{
		return view('yakushima.yakuactivitytypes.create');
	}

	public function store(Request $request)
	{
		$yakuactivitytype = new Yakuactivitytype();
		$yakuactivitytype->fill($request->all())->save();
		return redirect()->route('yakuactivitytypes.index');
	}

	public function edit($id)
	{
		$yakuactivitytype = Yakuactivitytype::findOrFail($id);
		return view('yakushima.yakuactivitytypes.edit')->with(compact('yakuactivitytype'));
	}

	public function update(Request $request, $id)
	{
		$yakuactivitytype = Yakuactivitytype::findOrFail($id);
		$yakuactivitytype->fill($request->all())->save();
		return redirect()->route('yakuactivitytypes.index');
	}
}
