<?php

namespace Field\Http\Controllers\Yakushima;

use Illuminate\Http\Request;

use Carbon\Carbon;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use Field\Yakushima\Yakuguide;
use Field\Yakushima\Yakuguideschedule;
use Field\Yakushima\Yakuguidescheduletype;

class YakuguideschedulesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function edit($id)
	{
		$yakuguideschedule = Yakuguideschedule::findOrFail($id);
		$yakuguidescheduletypes = Yakuguidescheduletype::all()->pluck('name', 'id');
		return view('yakushima.yakuguideschedules.edit')->with(compact('yakuguideschedule', 'yakuguidescheduletypes'));
	}

	public function update(Request $request, $id)
	{
		$yakuguideschedule = Yakuguideschedule::findOrFail($id);
		$yakuguideschedule->fill($request->all())->save();
		return redirect()->route('yakuguideschedules.index');
	}


}
