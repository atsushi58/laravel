<?php

namespace Field\Http\Controllers\Yakushima;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

use Field\Yakucometokoj;
use Field\Yakucometokum;
use Field\Yakucomerequest;

class YakucomerequestsController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index()
	{
		$yakucomerequests = Yakucomerequest::all();
		return view('yakushima.yakucomerequests.index')->with(compact('yakucomerequests'));
	}

	public function create()
	{
		$tokojs = Yakucometokoj::all()->pluck('name', 'id');
		$tokums = Yakucometokum::all()->pluck('name', 'id');
		return view('yakushima.yakucomerequests.create')->with(compact('tokojs', 'tokums'));
	}

	public function store(Request $request)
	{
		$yakucomerequest = new Yakucomerequest();
		$yakucomerequest->fill($request->all())->save();
		return redirect()->route('yakucomerequests.index');
	}

	public function edit($id)
	{
		$yakucomerequest = Yakucomerequest::findOrFail($id);
		$tokojs = Yakucometokoj::all()->pluck('name', 'id');
		$tokums = Yakucometokum::all()->pluck('name', 'id');
		return view('yakushima.yakucomerequests.edit')->with(compact('yakucomerequest', 'tokojs', 'tokums'));
	}

	public function update(Request $request, $id)
	{
		$yakucomerequest = Yakucomerequest::findOrFail($id);
		$yakucomerequest->fill($request->all())->save();
		return redirect()->route('yakucomerequests.index');
	}
}
