<?php

namespace Field\Http\Controllers;

use Illuminate\Http\Request;

class SchedulesController extends Controller
{
    //
    public function store(Request $request, $courseId){
    	$schedule = new Schedule([
    		'date' => $request->date,
    		'coursetime' => $request->coursetime,
    		'distance' => $request->distance,
    		'ascend' => $request->ascend,
    		'descend' => $request->descend,
    		'schedule' => $request->schedule
    		]);
    	$return redirect()->action('CoursesController@show', $course->id);
    }
}
