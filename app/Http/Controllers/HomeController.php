<?php

namespace Field\Http\Controllers;

use Illuminate\Http\Request;

use Field\Topslider;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topsliders = Topslider::all();
        return view('yamakaraweb.index')->with(compact('topsliders'));
    }
}
