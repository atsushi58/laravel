<?php

namespace Field\Http\Controllers;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Field\Coursearea;
use Field\Course;

class CourseareasController extends Controller
{
    //
    public function index(){
        $courseareas = Coursearea::all();
        $courseareas = Coursearea::Paginate(10);
    	return view('courseareas.index')->with('courseareas', $courseareas);
    }

    public function show($id){
    	$coursearea = Coursearea::findOrFail($id);
    	return view('courseareas.show')->with('coursearea', $coursearea);
    }

    public function create(){
    	return view('courseareas.create');
    }

    public function store(Request $request){
    	$coursearea = new Coursearea();
    	$coursearea->name = $request->name;
    	$coursearea->save();
    	return redirect('/courseareas');
    }

    public function edit($id) {
      $coursearea = Coursearea::findOrFail($id);
      return view('courseareas.edit')->with('coursearea', $coursearea);
    }

    public function update(Request $request, $id) {
      $coursearea = Coursearea::findOrFail($id);
      $coursearea->name = $request->name;
      $coursearea->save();
      return redirect('/courseareas')->with('flash_message', 'Coursearea Updated!');
    }

    public function destroy($id) {
      $coursearea = Coursearea::findOrFail($id);
      $coursearea->delete();
      return redirect('/courseareas')->with('flash_message', 'Deleted!');
  }
}
