<?php

namespace Field\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Field\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesAdmins;
use Auth;

class AdminLoginController extends Controller
{

    protected $redirectTo = '/admin';

	public function __construct(){
		$this->middleware('guest:admin')->except('logout');
	}
    public function showLoginForm(){
    	return view('auth.admin-login');
    }

    public function login(Request $request){
    	$this->validate($request, [
    		'email' => 'required|email',
    		'password' => 'required|min:6'
    	]);

   	if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])){
   		return redirect(route('staff.user'));
    }
   	return redirect()->back()->withInput($request->only('email', 'remember'));
    }
}
