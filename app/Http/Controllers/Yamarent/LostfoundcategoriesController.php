<?php

namespace Field\Http\Controllers\Yamarent;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Field\Yamarent\Lostfoundcategory;

class LostfoundcategoriesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index()
	{
		$lostfoundcategories = Lostfoundcategory::all();
		return view('yamarent.lostfoundcategories.index')->with(compact('lostfoundcategories'));
	}

	public function create()
	{
		return view('yamarent.lostfoundcategories.create');
	}

	public function store(Request $request)
	{
		$lostfoundcategory = new Lostfoundcategory();
		$lostfoundcategory->fill($request->all())->save();
		return redirect()->route('lostfoundcategories.index');
	}

	public function edit($id)
	{
		$lostfoundcategory = Lostfoundcategory::findOrFail($id);
		return view('yamarent.lostfoundcategories.edit')->with(compact('lostfoundcategory'));
	}

	public function update(Request $request, $id)
	{
		$lostfoundcategory = Lostfoundcategory::findOrFail($id);
		$lostfoundcategory->fill($request->all())->save();
		return redirect()->route('lostfoundcategories.index');
	}
}
