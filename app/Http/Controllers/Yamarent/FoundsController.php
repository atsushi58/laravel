<?php

namespace Field\Http\Controllers\Yamarent;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Field\Yamarent\Found;
use Field\Yamarent\Foundprocess;
use Field\Yamarent\Lostfoundcategory;

class FoundsController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index(Request $request)
	{
        $search = $request->search;
        if ($search)
        {
        	$founds = Found::orderBy('created_at', 'DESC')->where('name', 'LIKE', "%$search%")->orwhere('tel', 'LIKE', "%$search%")->orwhere('item', 'LIKE', "%$search%")->orwhere('orderid', 'LIKE', "%$search%")->orwhere('comment', 'LIKE', "%$search%")->paginate(10);

        }else{
			$founds = Found::orderBy('created_at', 'DESC')->paginate(10);
		}
		return view('yamarent.founds.index')->with(compact('founds', 'search'));
	}

	public function create()
	{
		$categories = Lostfoundcategory::all()->pluck('name', 'id');
		return view('yamarent.founds.create')->with(compact('categories'));
	}

	public function store(Request $request)
	{
		$found = new Found();
		$found->fill($request->all())->save();
		return redirect()->route('founds.index');
	}

	public function edit($id)
	{
		$found = Found::findOrFail($id);
		$categories = Lostfoundcategory::all()->pluck('name', 'id');
		return view('yamarent.founds.edit')->with(compact('found', 'categories'));
	}

	public function update(Request $request, $id)
	{
		$found = Found::findOrFail($id);
		$found->fill($request->all())->save();
		return redirect()->route('founds.index');
	}

	public function editprocess($id)
	{
		$found = Found::findOrFail($id);
		$processes = Foundprocess::all()->pluck('name', 'id');
		return view('yamarent.founds.editprocess')->with(compact('found', 'processes'));
	}

	public function updateprocess(Request $request, $id)
	{
		$found = Found::findOrFail($id);
		$found->fill($request->all())->save();
		return redirect()->route('founds.index');
	}

	public function editvouchar($id)
	{
		$found = Found::findOrFail($id);
		return view('yamarent.founds.editvouchar')->with(compact('found'));
	}

	public function updatevouchar(Request $request, $id)
	{
		$found = Found::findOrFail($id);
		$found->fill($request->all())->save();
		return redirect()->route('founds.index');
	}
}
