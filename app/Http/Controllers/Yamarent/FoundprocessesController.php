<?php

namespace Field\Http\Controllers\Yamarent;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;

use Field\Yamarent\Foundprocess;

class FoundprocessesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index()
	{
		$foundprocesses = Foundprocess::all();
		return view('yamarent.foundprocesses.index')->with(compact('foundprocesses'));
	}

	public function create()
	{
		return view('yamarent.foundprocesses.create');
	}

	public function store(Request $request)
	{
		$foundprocess = new Foundprocess();
		$foundprocess->fill($request->all())->save();
		return redirect()->route('foundprocesses.index');
	}

	public function edit($id)
	{
		$foundprocess = Foundprocess::findOrFail($id);
		return view('yamarent.foundprocesses.edit')->with(compact('foundprocess'));
	}

	public function update(Request $request, $id)
	{
		$foundprocess = Foundprocess::findOrFail($id);
		$foundprocess->fill($request->all())->save();
		return redirect()->route('foundprocesses.index');
	}
}
