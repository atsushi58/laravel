<?php

namespace Field\Http\Controllers\Yamarent;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Field\Yamarent\Lostprocess;

class LostprocessesController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index()
	{
		$lostprocesses = Lostprocess::all();
		return view('yamarent.lostprocesses.index')->with(compact('lostprocesses'));
	}

	public function create()
	{
		return view('yamarent.lostprocesses.create');
	}

	public function store(Request $request)
	{
		$lostprocess = new Lostprocess();
		$lostprocess->fill($request->all())->save();
		return redirect()->route('lostprocesses.index');
	}

	public function edit($id)
	{
		$lostprocess = Lostprocess::findOrFail($id);
		return view('yamarent.lostprocesses.edit')->with(compact('lostprocess'));
	}

	public function update(Request $request, $id)
	{
		$lostprocess = lostprocess::findOrFail($id);
		$lostprocess->fill($request->all())->save();
		return redirect()->route('lostprocesses.index');
	}
}
