<?php

namespace Field\Http\Controllers\Yamarent;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Field\Yamarent\Lost;
use Field\Yamarent\Lostprocess;
use Field\Yamarent\Lostfoundcategory;

class LostsController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:admin');
	}

	public function index(Request $request)
	{
        $search = $request->search;
        if ($search)
        {
        	$losts = Lost::orderBy('created_at', 'DESC')->where('name', 'LIKE', "%$search%")->orwhere('tel', 'LIKE', "%$search%")->orwhere('item', 'LIKE', "%$search%")->orwhere('orderid', 'LIKE', "%$search%")->orwhere('comment', 'LIKE', "%$search%")->paginate(10);

        }else{
			$losts = Lost::orderBy('created_at', 'DESC')->paginate(10);
		}
		return view('yamarent.losts.index')->with(compact('losts', 'search'));
	}

	public function create()
	{
		$categories = Lostfoundcategory::all()->pluck('name', 'id');
		return view('yamarent.losts.create')->with(compact('categories'));
	}

	public function store(Request $request)
	{
		$lost = new Lost();
		$lost->fill($request->all())->save();
		return redirect()->route('losts.index');
	}

	public function edit($id)
	{
		$lost = Lost::findOrFail($id);
		$categories = Lostfoundcategory::all()->pluck('name', 'id');
		return view('yamarent.losts.edit')->with(compact('lost', 'categories'));
	}

	public function update(Request $request, $id)
	{
		$lost = Lost::findOrFail($id);
		$lost->fill($request->all())->save();
		return redirect()->route('losts.index');
	}

	public function editprocess($id)
	{
		$lost = Lost::findOrFail($id);
		$processes = Lostprocess::all()->pluck('name', 'id');
		return view('yamarent.losts.editprocess')->with(compact('lost', 'processes'));
	}

	public function updateprocess(Request $request, $id)
	{
		$lost = Lost::findOrFail($id);
		$lost->fill($request->all())->save();
		return redirect()->route('losts.index');
	}

	public function editvouchar($id)
	{
		$lost = Lost::findOrFail($id);
		return view('yamarent.losts.editvouchar')->with(compact('lost'));
	}

	public function updatevouchar(Request $request, $id)
	{
		$lost = Lost::findOrFail($id);
		$lost->fill($request->all())->save();
		return redirect()->route('losts.index');
	}
}
