<?php

namespace Field\Http\Controllers;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Field\Event;
use Field\Course;
use Field\Eventtype;
use Field\Eventstatus;

class EventsController extends Controller
{
    //
    public function index(){
    	$events = Event::all();
        $events = Event::Paginate(10);

    	return view('events.index')->with('events', $events);
    }

    public function show($id){
    	$event = Event::findOrFail($id);
    	return view('events.show')->with('event', $event);
    }

    public function edit($id){
        $event = Event::findOrFail($id);
        $courses = Course::all()->pluck('name', 'id');
        $eventtypes = Eventtype::all()->pluck('name', 'id');
        $eventstatuses = Eventstatus::all()->pluck('name', 'id');
        return view('events.edit')->with(compact('event', 'courses', 'eventtypes', 'eventstatuses'));

    }

    public function create(){
        $courses = Course::all()->pluck('name', 'id');
        $eventtypes = Eventtype::all()->pluck('name', 'id');
        $eventstatuses = Eventstatus::all()->pluck('name', 'id');
    	return view('events.create')->with(compact('courses', 'eventtypes', 'eventstatuses'));
    }

    public function store(Request $request){
    	$event = new Event();
        $event->course_id = $request->course_id;
        $event->eventtype_id = $request->eventtype_id;
    	$event->start = $request->start;
        $event->eventstatus_id = $request->eventstatus_id;
    	$event->maxpax = $request->maxpax;
    	$event->baseprice = $request->baseprice;
        $event->photobookcreation = 0;
        $event->photobooksending = 0;
    	$event->save();
    	return redirect('/events');
    }

    public function update(Request $request, $id) {
        $event = Event::findOrFail($id);
        $event->course_id = $request->course_id;
        $event->start = $request->start;
        $event->maxpax = $request->maxpax;
        $event->baseprice = $request->baseprice;
        $event->save();
        return redirect('/courses')->with('flash_message', 'course Updated!');
    }

    public function destroy($id) {
      $event = Event::findOrFail($id);
      $event->delete();
      return redirect('/events')->with('flash_message', 'Deleted!');
  }
}
