<?php

namespace Field\Http\Controllers;

use Illuminate\Http\Request;

use Field\Http\Requests;
use Field\Http\Controllers\Controller;
use Field\Depplace;

class DepplacesController extends Controller
{
    //
    public function index(){
    	$depplaces = Depplace::all();

    	return view('depplaces.index')->with('depplaces', $depplaces);
    }

    public function edit($id){
        $depplace = Depplace::findOrFail($id);
        return view('depplaces.edit')->with(compact('depplace'));
    }

    public function create(){
    	return view('depplaces.create');
    }

    public function store(Request $request){
    	$depplace = new Depplace();
        $depplace->name = $request->name;
    	$depplace->save();
    	return redirect('/depplaces');
    }

    public function update(Request $request, $id) {
        $event = Event::findOrFail($id);
        $event->course_id = $request->course_id;
        $event->start = $request->start;
        $event->maxpax = $request->maxpax;
        $event->baseprice = $request->baseprice;
        $event->save();
        return redirect('/courses')->with('flash_message', 'course Updated!');
    }

    public function destroy($id) {
      $depplace = Depplace::findOrFail($id);
      $depplace->delete();
      return redirect('/depplaces')->with('flash_message', 'Deleted!');
  }
}
