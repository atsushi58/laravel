<?php

namespace Field\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Field\Http\Controllers\Controller;
use Field\Admin;
use Field\Role;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth:admin');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = Admin::where('active', '1')->get();
        return view('admin.admins.index')->with(compact('admins'));
    }

    public function allstaffs()
    {
        $admins = Admin::all();
        return view('admin.staffs.index')->with(compact('admins'));
    }

    public function create(){
        $roles = Role::where('id', '>', 2)->pluck('name', 'id');
        return view('admin.staffs.create')->with(compact('roles'));
    }

    public function store(Request $request){
        Admin::create([
            'name' => $request->name,
            'kana' => $request->kana,
            'tel' => $request->tel,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'birthday' => $request->birthday,
            'role_id' => $request->role_id,
            'active' => $request->active,
            'reference' => $request->reference
        ]);

        return redirect()->route('admin.admins');
    }

    public function edit($id){
        $admin = Admin::findOrFail($id);
        $roles = Role::where('id', '>', 2)->pluck('name', 'id');
        return view('admin.staffs.edit')->with(compact('admin', 'roles'));
    }

    public function update(Request $request, $id){
        $admin = Admin::findOrFail($id);
        $admin->name = $request->name;
        $admin->kana = $request->kana;
        $admin->tel = $request->tel;
        $admin->email = $request->email;
        $admin->birthday = $request->birthday;
        $admin->role_id = $request->role_id;
        $admin->active = $request->active;
        $admin->reference = $request->reference;
        $admin->save();
        return redirect()->route('admin.dashboard');
    }


    public function destroy($id){
        $admin = Admin::findOrFail($id);
        $admin->active = 0;
        $admin->save();

        return redirect()->route('admin.dashboard');
    }
}
