<?php

namespace Field\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('admin')->check() === false) {
        return redirect()->away('http://www.yamakara.com');
        }
        return $next($request);
    }
}
