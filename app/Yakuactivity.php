<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Yakuactivity extends Model
{
    protected $fillable = ['depdate', 'yakuactivitytype_id', 'memo' ];

    public function yakuactivitytype()
    {
        return $this->belongsTo('Field\Yakuactivitytype');
    }

    public function yakuguides()
    {
    	return $this->belongsToMany('Field\Yakuguide');
    }
}
