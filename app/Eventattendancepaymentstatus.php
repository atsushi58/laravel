<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Eventattendancepaymentstatus extends Model
{

    protected $fillable = ['name'];

    public function eventattendances()
    {
    	$this->hasMany('Field\Eventattendance');
    }

    public function yakueventattendances()
    {
    	$this->hasMany('Field\Yakushima\yakueventattendances');
    }
}
