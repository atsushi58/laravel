<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Inquiry extends Model
{
    protected $fillable = [
        'name', 'body', 'inquirytype_id', 'inquirystatus_id', 'email', 'title'
    ];

    public function inquirytype(){
    	return $this->belongsTo('Field\Inquirytype');
    }

    public function inquirystatus(){
    	return $this->belongsTo('Field\Inquirystatus');
    }
}
