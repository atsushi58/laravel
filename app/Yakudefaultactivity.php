<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Yakudefaultactivity extends Model
{
    //
    protected $fillable = ['yakucourse_id', 'yakuactivitytype_id', 'day'];

    public function course(){
        return $this->belongsTo('Field\Course');
    }

    public function yakuactivitytype(){
    	return $this->belongsTo('Field\Yakuactivitytype');
    }
}
