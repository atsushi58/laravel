<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
	protected $fillable = ['accountingconfirm', 'adjustment', 'admin_id', 'balancesalary', 'basesalary', 'biztransportation',  'biztrip', 'biztripallowance', 'deducationbalance', 'month', 'overtime', 'summerspecial', 'healthinsurance', 'nursinginsurance', 'pension', 'employmentinsurance', 'basetransportation', 'transportationbalance', 'reimbursement', 'incometax', 'transferamount', 'year', 'staffconfirm', 'workingdays', 'workingminutes'
    ];

    public function admin(){
        return $this->belongsTo('Field\Admin');
    }

    public function getTotalAttribute(){
    	return $this->basesalary + $this->balancesalary + $this->summerspecial + $this->overtime;
    }

    public function getTotalSocialInsuranceAttribute(){
    	return $this->healthinsurance + $this->nursinginsurance + $this->pension + $this->employmentinsurance + $this->deductionbalance;
    }

    public function getTaxableAmountAttribute(){
    	return $this->Total - $this->TotalSocialInsurance;
    }

    public function getTotalTransportationAttribute(){
    	return $this->basetransportation + $this->transportationbalance;
    }

    public function getEmploymentinsuranceAttribute()
    {
        if($this->admin->taxsection == 0)
        {
            return 0;
        }else
        {
            return round(($this->Total + $this->basetransportation + $this->transportationbalance)*3/1000);
        }
    }
}
