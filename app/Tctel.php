<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Tctel extends Model
{
    //
    protected $fillable = ['num'];

    public function events()
    {
        return $this->hasMany('Field\Event');
    }

    public function getPrevtourAttribute()
    {
    	if(!empty($this->events))
    	{
	    	$prevtour = Event::orderBy('depdate', 'DESC')->where('depdate', '<=', date('Y-m-d'))->where('tctel_id', $this->id)->first();
		    	if(!empty($prevtour->shifts))
		    	{
			    	$tcs = [];
			    	foreach($prevtour->shifts as $shift){
			    		array_push($tcs, $shift->admin->name);
			    	}
		    	return '('.$prevtour->depdate.'発)'. $prevtour->course->name. '('. implode(",", $tcs). ')';
		    	}
		    	return '';
		}
		return '';
    }

    public function getNexttourAttribute()
    {
    	if(!empty($this->events))
    	{
	    	$prevtour = Event::orderBy('depdate', 'ASC')->where('depdate', '>', date('Y-m-d'))->where('tctel_id', $this->id)->first();
		    	if(!empty($prevtour->shifts))
		    	{
			    	$tcs = [];
			    	foreach($prevtour->shifts as $shift){
			    		array_push($tcs, $shift->admin->name);
			    	}
		    	return '('.$prevtour->depdate.'発)'. $prevtour->course->name. '('. implode(",", $tcs). ')';
		    	}
		    	return '';
		}
		return '';
    }
}
