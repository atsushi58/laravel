<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Eventattendancepaymentmethod extends Model
{

    protected $fillable = ['name'];

    public function eventattendances()
    {
    	$this->hasMany('Field\Eventattendance');
    }
}
