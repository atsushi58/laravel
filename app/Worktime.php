<?php

namespace Field;

use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class Worktime extends Model
{
	protected $fillable = [
        'worktime', 'active'
    ];

    public function starts(){
    	return $this->hasMany('Field\Shift');
    }

    public function ends(){
    	return $this->hasMany('Field\Shift', 'end');
    }
}
