<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Staffmailcategory extends Model
{
	protected $fillable = [
        'name'
    ];

    public function staffmails(){
    	return $this->hasMany('Field\Staffmail');
    }
}
