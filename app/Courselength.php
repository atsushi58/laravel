<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Courselength extends Model
{
    //
    protected $fillable = ['name', 'days'];

	public function courses(){
    	return $this->hasMany('Field\Course');
    }
}
