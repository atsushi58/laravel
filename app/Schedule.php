<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $fillable = ['course_id', 'day', 'coursetime', 'distance', 'ascend', 'descend', 'schedule'];

    public function course(){
    	return $this->belongsTo('Field\Course');
    }
}
