<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Topslider extends Model
{
    protected $fillable = [
        'filename', 'path', 'title', 'caption', 'course_id'
    ];

    public function course(){
    	return $this->belongsTo('Field\Course');
    }
}
