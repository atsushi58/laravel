<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Tourmailcategory extends Model
{
	protected $fillable = [
        'name'
    ];

    public function tourmails()
    {
        return $this->hasMany('Field\Tourmail');
    }

    public function tourmailtemplates()
    {
    	return $this->hasMany('Field\Tourmailtemplate');
    }
}
