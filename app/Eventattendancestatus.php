<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Eventattendancestatus extends Model
{

    protected $fillable = ['name'];

    public function eventattendances()
    {
    	$this->hasMany('Field\Eventattendance');
    }

    public function yakueventattendances()
    {
    	$this->hasMany('Field\Yakushima\yakueventattendance');
    }
}
