<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Scenariocost extends Model
{
    protected $fillable = ['scenario_id', 'eventcostcategory_id', 'eventcosttype_id', 'amount', 'payee', 'limit', 'counttc'];

    public function scenario()
    {
    	return $this->belongsTo('Field\Scenario');
    }

    public function eventcostcategory()
    {
    	return $this->belongsTo('Field\Eventcostcategory');
    }

    public function eventcosttype()
    {
    	return $this->belongsTo('Field\Eventcosttype');
    }

    public function getCostppattribute($id)
    {
        $costpp = [];
        if($this->eventcosttype_id == 2)
        {
            for($i = 1;$i < 41;$i++){
            $costpp += [$i => round($this->amount + ($this->amount*$this->counttc / $i))];};
        }elseif($this->eventcosttype_id == 3)
        {
            for($i = 1;$i < 41;$i++){
            $costpp += [$i => round($this->amount/$i)];};
        }elseif($this->eventcosttype_id == '4')
        {
            for($i = 1;$i < 41;$i++){
            $costpp += [$i => round(ceil(($i + $this->counttc)/ $this->limit) * $this->amount / $i)];};
        }
        return $costpp;
    }

}
