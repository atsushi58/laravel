<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Eventtype extends Model
{
    //

    protected $fillable = ['name'];

    public function courses(){
    	return $this->hasMany('Field\Course');
    }
}
