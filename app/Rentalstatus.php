<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Rentalstatus extends Model
{
    //
    protected $fillable = ['name'];

    public function eventattendances()
    {
    	return $this->hasMany('Field\Eventattendance');
    }
}
