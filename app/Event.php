<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;
use Field\Eventattendance;
use Field\Eventprice;

class Event extends Model
{
    protected $dates = ['depdate'];
    protected $fillable = ['admincheck', 'advancepayment', 'course_id', 'eventtype_id', 'depdate', 'subtitle', 'eventstatus_id', 'eventbus_id', 'maxpax', 'admin_id', 'memo', 'photobookstatus_id', 'tctel_id', 'tourmailtemplate_id'];

    public function course(){
    	return $this->belongsTo('Field\Course');
    }

    public function eventbus()
    {
        return $this->belongsTo('Field\Eventbus');
    }

    public function eventtype(){
    	return $this->belongsTo('Field\Eventtype');
    }

	public function eventstatus(){
    	return $this->belongsTo('Field\Eventstatus');
    }

    public function photobookstatus()
    {
        return $this->belongsTo('Field\Photobookstatus');
    }

    public function tctel()
    {
        return $this->belongsTo('Field\Tctel');
    }

    public function tourmailtemplate()
    {
        return $this->belongsTo('Field\Tourmailtemplate');
    }

    public function admin(){
        return $this->belongsTo('Field\Admin');
    }

    public function contactlists()
    {
        return $this->belongsTo('Field\Contactlist');
    }

    public function eventprices(){
        return $this->hasMany('Field\Eventprice');
    }

    public function eventcosts()
    {
        return $this->hasMany('Field\Eventcost');
    }

    public function shifts(){
    	return $this->hasMany('Field\Shift');
    }

    public function tourmails()
    {
        return $this->hasMany('Field\Tourmail');
    }

    public function getClientsAttribute()
    {
        return Eventattendance::whereIn('eventprice_id', $this->eventprices->pluck('id'))->orderBy('eventattendancestatus_id', 'ASC')->orderBy('created_at', 'ASC')->get();
    }

    public function getWaitingclientsAttribute()
    {
        return Eventattendance::where('eventattendancestatus_id', 4)->whereIn('eventprice_id', $this->eventprices->pluck('id'))->orderBy('eventattendancestatus_id', 'ASC')->orderBy('created_at', 'ASC')->get();
    }

    public function getClientinfoAttribute()
    {
        return User::join('eventattendances', 'users.id', '=', 'eventattendances.user_id')->whereIn('eventattendances.eventprice_id', $this->eventprices->pluck('id'))->whereIn('eventattendances.eventattendancestatus_id', [2,4,5,8])->get();
    }

    public function getActiveclientinfoAttribute()
    {
        return User::join('eventattendances', 'users.id', '=', 'eventattendances.user_id')->whereIn('eventattendances.eventprice_id', $this->eventprices->pluck('id'))->whereIn('eventattendances.eventattendancestatus_id', [2,8])->get();
    }

    public function getTotalclientsAttribute()
    {
        return $this->Clients->sum('numofpeople') + $this->Clients->sum('numofchildren');
    }

    public function getTotalpossibleclientsAttribute()
    {
        return $this->Clients->whereIn('eventattendancestatus_id', ['2', '4', '5', '8'])->sum('numofpeople') + $this->Clients->whereIn('eventattendancestatus_id', ['2', '4', '5', '8'])->sum('numofchildren');
    }

    public function getActivetotalclientsAttribute()
    {
        return $this->Clients->whereIn('eventattendancestatus_id', ['2', '5', '8'])->sum('numofpeople') + $this->Clients->whereIn('eventattendancestatus_id', ['2', '8'])->sum('numofchildren');
    }

    public function getActivesalesAttribute()
    {
        return $this->Clients->whereIn('eventattendancestatus_id', ['2', '8'])->sum('Sales');
    }

    public function getPaidsalesAttribute()
    {
        return $this->eventprices->sum('Paidsales');
    }

    public function getLoadfactorAttribute()
    {
        if($this->maxpax != 0)
        {
            return round($this->Activetotalclients / $this->maxpax *100, 2);
        }else
        {
            return 0;
        }
    }

    public function getEventpowerAttribute()
    {
        if($this->maxpax != 0)
        {
            return round($this->Totalclients / $this->maxpax *100, 2);
        }else
        {
            return 0;
        }
    }

    public function getCostAttribute()
    {
        return $this->eventcosts->sum('amount');
    }

    public function getProfitabilityAttribute()
    {
        if($this->Paidsales != 0){
            return round((1- ($this->Cost/$this->Paidsales)) *100, 2);
        }else{
            return 0;
        }
    }

    public function getDefaulteventpriceAttribute()
    {
        $defaulteventprices = $this->eventprices;
 
            return $defaulteventprices->first();
    }

    public function getTotalPotentialAttribute()
    {
        if(!empty($this->Defaulteventprice) && !empty($this->maxpax))
        {
            if($this->Defaulteventprice->price * $this->maxpax !=0)
                {
                    return $this->Defaulteventprice->price * $this->maxpax;
                }else{
                    return 1;
                }
        }else{
            return 1;
        }
    }

    public function getDeparturedateAttribute()
    {
        $week = [
            '日', //0
            '月', //1
            '火', //2
            '水', //3
            '木', //4
            '金', //5
            '土', //6
        ];

        $youbino = date('w', strtotime($this->depdate));

        $youbi = $week[$youbino];
        if(date("Y", strtotime($this->depdate)) == date("Y"))
        {
            return date("m月d日($youbi)", strtotime($this->depdate));
        }else{
                    return date("Y年m月d日($youbi)", strtotime($this->depdate));
        }
    }

    public function getTitleAttribute()
    {
        if($this->course->subtitle){
            return $this->course->subtitle."<br>".$this->course->name;
        }else
        {
            return $this->course->name;
        }
        
    }
}
