<?php

namespace Field\Facades;

use Illuminate\Support\Facades\Facade;

class Eventattendance extends Facade
{
    protected static function getFacadeAccessor(){
        return 'eventattendance';
    }
}
