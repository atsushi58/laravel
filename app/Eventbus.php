<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Eventbus extends Model
{
	protected $fillable = ['name'];
	
	public function events(){
    	return $this->hasMany('Field\Event');
    }
}
