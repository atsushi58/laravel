<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Yakuattendancetype extends Model
{
    //
    protected $fillable = ['name', 'defaultflightarrangestatus', '	defaulthostelarrangestatus', 'defaultpickupstatus'];
}
