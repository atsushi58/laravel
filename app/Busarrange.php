<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Busarrange extends Model
{
	protected $fillable = [
        'user_id', 'balancetype_id', 'eventattendance_id', 'day', 'deposit', 'suspend', 'memo'
    ];

    public function user()
    {
    	return $this->belongsTo('Field\User');
    }

    public function balancetype(){
        return $this->belongsTo('Field\Balancetype');
    }

    public function eventattendance(){
        return $this->belongsTo('Field\Eventattendance');
    }
}
