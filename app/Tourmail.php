<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Tourmail extends Model
{
	protected $fillable = [
        'tourmailcategory_id', 'title', 'body', 'to', 'event_id'
    ];

    public function event()
    {
    	return $this->belongsTo('Field\Event');
    }

    public function tourmailcategory(){
        return $this->belongsTo('Field\Tourmailcategory');
    }


}
