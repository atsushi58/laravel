<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Eventstatus extends Model
{
	protected $fillable = ['name'];
	
	public function events(){
    	return $this->hasMany('Field\Event');
    }

    public function getEventscountAttribute()
    {
    	return $this->events->count();
    }
}
