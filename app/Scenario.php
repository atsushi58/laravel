<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Scenario extends Model
{
    protected $fillable = ['admin_id', 'course_id', 'name', 'maxpax', 'price'];

    public function course()
    {
    	return $this->belongsTo('Field\Course');
    }

    public function admin()
    {
    	return $this->belongsTo('Field\Admin');
    }

    public function scenariocosts()
    {
    	return $this->hasMany('Field\Scenariocost');
    }

    public function getPaxAttribute()
    {
        if(!empty($this->maxpax)){
            return $this->maxpax+1;
        }else{
            return 41;
        }
    }
}
