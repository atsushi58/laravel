<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Balancetype extends Model
{
	protected $fillable = [
        'name'
    ];

    public function balances(){
        return $this->hasMany('Field\Balance');
    }
}
