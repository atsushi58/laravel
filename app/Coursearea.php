<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Coursearea extends Model
{
    //
    protected $fillable = ['order', 'name'];

    public function courses()
    {
    	return $this->hasMany('Field\Course');
    }

    public function getCoursecountAttribute()
    {
    	return $this->courses->count();
    }
}
