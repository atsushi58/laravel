<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Inquirytype extends Model
{
    protected $fillable = [
        'name'
    ];

    public function inquies(){
    	return $this->hasMany('Field\Inquiry');
    }
}
