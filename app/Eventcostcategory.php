<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Eventcostcategory extends Model
{
    //
    protected $fillable = ['name'];

    public function eventcosts(){
        return $this->hasMany('Field\Eventcosts');
    }

}
