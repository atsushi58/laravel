<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Yakutourimage extends Model
{
	protected $fillable = [
        'filename', 'title', 'caption', 'yakucourse_id', 'topsquare', 'bigimage'
    ];

    public function yakucourse(){
        return $this->belongsTo('Field\Yakucourse');
    }
}
