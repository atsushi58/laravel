<?php

namespace Field\Yakushima;

use Illuminate\Database\Eloquent\Model;

class Yakudepplace extends Model
{
    //
    protected $fillable = ['name'];

    public function yakucourses(){
    	return $this->hasMany('Field\Yakushima\Yakucourse');
    }
}
