<?php

namespace Field\Yakushima;

use Illuminate\Database\Eloquent\Model;

class Yakudefaultactivity extends Model
{
    //
    protected $fillable = ['yakucourse_id', 'yakuactivitytype_id', 'day'];

    public function yakucourse(){
        return $this->belongsTo('Field\Yakushima\Yakucourse');
    }

    public function yakuactivitytype(){
    	return $this->belongsTo('Field\Yakushima\Yakuactivitytype');
    }
}
