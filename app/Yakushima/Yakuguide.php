<?php

namespace Field\Yakushima;

use Illuminate\Database\Eloquent\Model;

class Yakuguide extends Model
{
    protected $fillable = ['name', 'kana', 'tel', 'fax', 'email', 'address', 'bizname', 'web'];

    public function yakuactivities()
    {
    	return $this->belongsToMany('Field\Yakushima\Yakuactivity');
    }

    public function yakuguideschedules()
    {
    	return $this->hasMany('Field\Yakushima\Yakuguideschedule');
    }
}
