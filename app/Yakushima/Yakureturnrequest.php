<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Yakureturnrequest extends Model
{
    //
    protected $fillable = ['name', 'defaultyakureturnfromkum', 'defaultyakureturnfromkoj'];

    public function getFromkumAttribute()
    {
    	$id = $this->defaultyakureturnfromkum;
    	return Yakureturnformkoj::findOrFail($id);
    }

    public function getFromkojAttribute()
    {
    	$id = $this->defaultyakureturnfromkoj;
    	return Yakureturnfromkoj::findOrFail($id);    	
    }

}
