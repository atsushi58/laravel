<?php

namespace Field\Yakushima;

use Illuminate\Database\Eloquent\Model;

class Yakucourseprice extends Model
{
    //
    protected $fillable = ['yakucourse_id', 'yakucoursepricetype_id', 'depdate', 'priceone', 'pricetwo', 'pricethree', 'pricefour', 'pricefive'];

    public function yakucourse()
    {
        return $this->belongsTo('Field\Yakushima\Yakucourse');
    }

    public function yakucoursepricetype()
    {
    	return $this->belongsTo('Field\Yakushima\Yakucoursepricetype');
    }
}
