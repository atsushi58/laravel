<?php

namespace Field\Yakushima;

use Illuminate\Database\Eloquent\Model;

class Yakuguidescheduletype extends Model
{
    protected $fillable = ['name'];

    public function yakuguideschedules()
    {
        return $this->hasMany('Field\Yakushima\Yakuguideschedule');
    }
}
