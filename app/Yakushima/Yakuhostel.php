<?php

namespace Field\Yakushima;

use Illuminate\Database\Eloquent\Model;

class Yakuhostel extends Model
{
    //
    protected $fillable = ['name', 'kana'];

    public function yakueventattendances(){
    	return $this->hasMany('Field\Yakushima\Yakueventattendance');
    }
}
