<?php

namespace Field\Yakushima;

use Illuminate\Database\Eloquent\Model;

class Yakuactivity extends Model
{
    protected $fillable = ['depdate', 'yakuactivitytype_id', 'memo'];

    public function yakuactivitytype()
    {
        return $this->belongsTo('Field\Yakushima\Yakuactivitytype');
    }

    public function yakuguides()
    {
    	return $this->belongsToMany('Field\Yakushima\Yakuguide');
    }

    public function yakueventattendances()
    {
    	return $this->belongsToMany('Field\Yakushima\Yakueventattendance');
    }

    public function getNumattendantsAttribute()
    {
        return $this->yakueventattendances->sum('numofpeople');
    }

    public function getGuidecostAttribute()
    {
        if($this->Numattendants == 1)
        {
            return $this->yakuactivitytype->guidecostone;
        }
        if($this->Numattendants == 2)
        {
            return $this->yakuactivitytype->guidecosttwo;
        }
        if($this->Numattendants == 3)
        {
            return $this->yakuactivitytype->guidecostthree;
        }
        if($this->Numattendants == 4)
        {
            return $this->yakuactivitytype->guidecostfour;
        }
        if($this->Numattendants == 5)
        {
            return $this->yakuactivitytype->guidecostfive;
        }
        if($this->Numattendants == 6)
        {
            return $this->yakuactivitytype->guidecostsix;
        }
        if($this->Numattendants == 7)
        {
            return $this->yakuactivitytype->guidecostseven;
        }

    }
}
