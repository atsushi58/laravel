<?php

namespace Field\Yakushima;

use Illuminate\Database\Eloquent\Model;

class Yakueventattendance extends Model
{
    protected $fillable = ['user_id', 'yakucourse_id', 'yakucourseprice_id', 'yakuhostel_id', 'depdate', 'leavedate', 'eventattendancestatus_id', 'eventattendancepaymentstatus_id', 'group', 'numofpeople', 'numofchildren', 'rental', 'memo' ];

    public function yakucourse()
    {
        return $this->belongsTo('Field\Yakushima\Yakucourse');
    }

    public function yakuactivities()
    {
        return $this->belongsToMany('Field\Yakushima\Yakuactivity');
    }

    public function yakuhostel()
    {
        return $this->belongsTo('Field\Yakushima\Yakuhostel');
    }

    public function user()
    {
        return $this->belongsTo('Field\User');
    }

    public function yakuaccompanies()
    {
        return $this->hasMany('Field\Yakushima\Yakuaccompany');
    }

    public function eventattendancestatus()
    {
        return $this->belongsTo('Field\Eventattendancestatus');
    }

    public function eventattendancepaymentstatus()
    {
        return $this->belongsTo('Field\Eventattendancepaymentstatus');
    }

    public function yakuattendancetype()
    {
        return $this->belongsTo('Field\Yakushima\Yakuattendancetype');
    }
}
