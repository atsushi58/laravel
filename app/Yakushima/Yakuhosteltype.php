<?php

namespace Field\Yakushima;

use Illuminate\Database\Eloquent\Model;

class Yakuhosteltype extends Model
{
    //
    protected $fillable = ['name'];

    public function yakudefaulthostelarranges()
    {
    	return $this->hasMany('Field\Yakushima\yakudefaulthostelarrange');
    }

}
