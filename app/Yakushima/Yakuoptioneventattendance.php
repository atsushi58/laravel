<?php

namespace Field\Yakushima;

use Illuminate\Database\Eloquent\Model;

class Yakuoptioneventattendance extends Model
{
    protected $fillable = ['user_id', 'yakucourse_id', 'yakucourseprice_id', 'yakuhostel_id', 'depdate', 'leavedate', 'eventattendancestatus_id', 'eventattendancepaymentstatus_id', 'group', 'numofpeople', 'numofchildren', 'rental', 'memo' ];

}
