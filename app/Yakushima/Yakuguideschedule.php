<?php

namespace Field\Yakushima;

use Illuminate\Database\Eloquent\Model;

class Yakuguideschedule extends Model
{
    protected $fillable = ['yakuguide_id', 'day', 'yakuguidescheduletype_id', 'memo'];

    public function yakuguide()
    {
        return $this->belongsTo('Field\Yakushima\Yakuguide');
    }

    public function yakuguidescheduletype()
    {
        return $this->belongsTo('Field\Yakushima\Yakuguidescheduletype');
    }

    public function getWorknameAttribute()
    {
    	if($this->yakuguidescheduletype_id == 1)
    	{
    		return $this->yakuguidescheduletype->name.'('. $this->memo.')';
    	}else
    	{
    		return $this->yakuguidescheduletype->name;
    	}
    }
}
