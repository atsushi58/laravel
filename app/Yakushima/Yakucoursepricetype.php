<?php

namespace Field\Yakushima;

use Illuminate\Database\Eloquent\Model;

class Yakucoursepricetype extends Model
{
    protected $fillable = ['name'];

    public function yakucourseprices()
    {
        return $this->hasMany('Field\Yakushima\Yakucourseprice');
    }
}
