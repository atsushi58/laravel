<?php

namespace Field\Yakushima;

use Illuminate\Database\Eloquent\Model;

class Yakucoursetype extends Model
{
    //
    protected $fillable = ['name'];

    public function yakucourses()
    {
    	return $this->hasMany('Field\Yakushima\Yakucourse');
    }
}
