<?php

namespace Field\Yakushima;

use Illuminate\Database\Eloquent\Model;

class Yakuactivitytype extends Model
{
    protected $fillable = ['name' , 'limit', 'length', 'optionprice', 'guidecostone', 'guidecosttwo', 'guidecostthree', 'guidecostfour', 'guidecostfive', 'guidecostsix', 'guidecostseven'];

    public function yakuactivities()
    {
        return $this->hasMany('Field\Yakushima\Yakuactivity');
    }

    public function yakucourses()
    {
    	return $this->belongsToMany('Field\Yakushima\Yakucourse');
    }
}
