<?php

namespace Field\Yakushima;

use Illuminate\Database\Eloquent\Model;

class Yakucourse extends Model
{
    //
    protected $fillable = ['name', 'yakudepplace_id', 'courselength_id', 'yakucoursetype_id','memo'];

    public function courselength(){
        return $this->belongsTo('Field\Courselength');
    }

    public function yakuactivitytypes()
    {
        return $this->belongsToMany('Field\Yakushima\Yakuactivitytype');
    }

    public function yakudepplace(){
    	return $this->belongsTo('Field\Yakushima\Yakudepplace');
    }

    public function yakuschedules(){
        return $this->hasMany('Field\Yakushima\Yakuschedule');
    }

    public function topsliders(){
        return $this->hasMany('Field\Topslider');
    }

    public function yakudefaultactivities()
    {
        return $this->hasMany('Field\Yakushima\Yakudefaultactivity');
    }

    public function yakudefaulthostelarranges()
    {
        return $this->hasMany('Field\Yakushima\Yakudefaulthostelarrange');
    }

    public function yakucourseprices()
    {
        return $this->hasMany('Field\Yakushima\Yakucourseprice');
    }


    public function yakueventattendances(){
        return $this->hasMany('Field\Yakushima\Yakueventattendance');
    }

    public function yakutourimages(){
        return $this->hasMany('Field\Yakutourimage');
    }

    public function yakucoursetype()
    {
        return $this->belongsTo('Field\Yakushima\Yakucoursetype');
    }
}
