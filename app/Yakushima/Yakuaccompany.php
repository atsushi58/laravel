<?php

namespace Field\Yakushima;

use Illuminate\Database\Eloquent\Model;

class Yakuaccompany extends Model
{
    protected $fillable = ['name', 'sex', 'agebracket'];

    public function yakueventattendance()
    {
        return $this->belongsTo('Field\Yakushima\Yakueventattendance');
    }

}
