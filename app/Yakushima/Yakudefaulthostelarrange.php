<?php

namespace Field\Yakushima;

use Illuminate\Database\Eloquent\Model;

class Yakudefaulthostelarrange extends Model
{
    protected $fillable = ['name', 'yakucourse_id', 'day', 'yakuhosteltype_id'];

    public function yakucourse()
    {
        return $this->belongsTo('Field\Yakushima\Yakucourse');
    }

    public function yakuhosteltype()
    {
    	return $this->belongsTo('Field\Yakushima\Yakuhosteltype');
    }
}
