<?php

namespace Field\Yakushima;

use Illuminate\Database\Eloquent\Model;

class Yakuschedule extends Model
{
    protected $fillable = ['course_id', 'day', 'coursetime', 'distance', 'ascend', 'descend', 'schedule'];

    public function yakucourse(){
    	return $this->belongsTo('Field\Yakucourse');
    }
}
