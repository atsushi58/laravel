<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Depplace extends Model
{
    //
    protected $fillable = ['name'];

    public function courses(){
    	return $this->hasMany('Field\Course');
    }

    public function events(){
    	return $this->hasMany('Field\Event');
    }

    public function eventprices(){
        return $this->hasMany('Field\Eventprice');
    }
}
