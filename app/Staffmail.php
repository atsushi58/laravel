<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Staffmail extends Model
{
	protected $fillable = [
        'title', 'body', 'staffmailcategory_id'
    ];

    public function staffmailcategory(){
    	return $this->belongsTo('Field\Staffmailcategory');
    }
}
