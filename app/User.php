<?php

namespace Field;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'address', 'emcontactrelation', 'emcontactperson', 'emcontacttel', 'birthday', 'agebracket', 'email', 'kana', 'memo', 'name', 'password', 'pref_id', 'sex', 'tel', 'zipcode', 'pref', 'defaultrental'
    ];

    public $appends = ['Thisyearpaid'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function eventattendances(){
        return $this->hasMany('Field\Eventattendance');
    }

    public function balances()
    {
        return $this->hasMany('Field\Balance');
    }

    public function referrals()
    {
        return $this->hasMany('Field\Referral');
    }

    public function yakueventattendances()
    {
        return $this->hasMany('Field\Yakushima\Yakueventattendance');
    }

    public function getTotalpaidAttribute()
    {
        return $this->eventattendances->sum('Paid');
    }

    public function getThisyearpaidAttribute()
    {
        return Eventattendance::select('eventattendances.*')->join('eventprices', 'eventattendances.eventprice_id', '=', 'eventprices.id')->join('events', 'eventprices.event_id', '=', 'events.id')->whereIn('eventattendances.eventattendancestatus_id', [2,8])->where('eventattendances.user_id', $this->id)->whereYear('events.depdate', date('Y'))->get()->sum('Paid');
    }

    public function getTotalattendanceAttribute()
    {
        return $this->eventattendances->whereNotIn('eventattendancestatus_id', [4, 5, 6, 7])->count();
    }

    public function getThisyearattendanceAttribute()
    {
        return Eventattendance::select('eventattendances.*')->join('eventprices', 'eventattendances.eventprice_id', '=', 'eventprices.id')->join('events', 'eventprices.event_id', '=', 'events.id')->whereIn('eventattendances.eventattendancestatus_id', [2,8])->where('eventattendances.user_id', $this->id)->whereYear('events.depdate', date('Y'))->count();
    }

    public function getKatakanaAttribute()
    {
        return mb_convert_kana($this->kana, "hk");
    }

    public function getNamelistAttribute()
    {
        return $this->name. "(". $this->katakana. "<".$this->email. ">)";
    }

    public function getShortmemoAttribute()
    {
        if(strlen($this->memo) < 51)
        {
            return $this->memo;    
        }else
        {
            return substr($this->memo, 0,50). '...';    
        }
    }

    public function getPrefnameAttribute()
    {
        return config('pref.'.$this->pref_id);
    }

    public function getSexnameAttribute()
    {
        if($this->sex == 0){return '女性';}
        elseif($this->sex == 1){return '男性';}
    }

    public function getDepositAttribute()
    {
        return $this->balances->sum('deposit');
    }

    public function getSuspendAttribute()
    {
        return $this->balances->sum('suspend');
    }

    public function getGoingtopayAttribute()
    {
        return $this->eventattendances->where('eventattendancestatus_id', 2)->where('eventattendancepaymentstatus_id', 1)->sum('Sales');
    }
}
