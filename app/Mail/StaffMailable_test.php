<?php

namespace Field\Mail;

use Field\Staffmail;

use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;
use Sichikawa\LaravelSendgridDriver\SendGrid;

use Field\Admin;

/**
 * SendGrid を使った大量送信を行うための Mailable クラスのサンプルです。
 * Mailable を継承したクラスを作成し、以下の4つのステップで実装してください。
 *
 * 1. SendGrid トレイトを使用する
 * 2. 宛先を持つデータのリストをコンストラクタで受け取る
 * 3. SendGrid API 向けのデータ整形
 * 4. メール送信情報を整形
 *
 */
class StaffMailable extends Mailable
{
    //
    // 1. SendGrid トレイトを使用する
    //
    use SendGrid;

    //
    // 2. 宛先を持つデータのリストをコンストラクタで受け取る
    //
    protected $admin;
    public function __construct($admin)
    {
        // 例) ユーザリスト
        $this->tos = $admin;
    }

    public function build()
    {
        //
        // 3. SendGrid API 向けのデータ整形
        //
        // 送信対象毎の情報を下記の形式で整形します。
        //
        // 3-1. 送信先(to)
        // 3-2. 置換文字列と置換するデータのリスト(substitutions)
        // 3-3. 件名(subject)
        //
        $personalizations = collect([]);
        foreach ($this->tos as $user) {
            if (($user instanceof Admin) === false) {
                continue;
            }
            $personalization = [
                'to' => [
                    'email' => $user->email,
                    'name' => $user->name,
                ],
                'substitutions' => [
                    // 例) テンプレート内の ":name" をユーザ名に置換する
                    ':name' => $user->name,
                ],
                'subject' => "こんにちは、{$user->name} さん"
            ];
            $personalizations->push($personalization);
        }

        //
        // 4. メール送信情報を設定
        //
        // メール送信情報を下記の形式で設定します。
        //
        // 4-2. テンプレートを指定(view)
        // 4-3. 送信元を指定(from)
        // 4-4. 返信先を指定(replyTo)
        // 4-5. SendGrid API 向けのデータ(sendgrid)
        //
        return $this->view('mail.inquiry.customer')
            ->from(config('mail.from.address'), config('mail.from.name'))
            ->replyTo(config('mail.from.address'), config('mail.from.name'))
            ->to(config('mail.from.address'), config('mail.from.name'))
            ->sendgrid([

                // 個別情報
                'personalizations' => $personalizations->toArray(),

                //
                // その他、パラメータは下記を参照ください
                // https://sendgrid.kke.co.jp/docs/API_Reference/Web_API_v3/Mail/index.html
                //

                // 'custom_args' => ['user_group1'], // カスタム引数
                // 'send_at' => $send_at->getTimestamp(),// 送信日時の指定
            ]);
    }
}
