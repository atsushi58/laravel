<?php

namespace Field\Mail;

use Field\Staffmail;

use Sichikawa\LaravelSendgridDriver\SendGrid;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

use Field\Admin;

class Staff extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $staffmail;
    use SendGrid;
    public function __construct(Staffmail $staffmail)
    {
        $this->title = $staffmail->title;
        $this->body = $staffmail->body;
        $this->staffmailcategory = $staffmail->staffmailcategory;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->from('customer@field-mt.com', 'Field&Mountain')
             ->subject($this->title)
             ->view('mail.staffmail')
             ->with(['body' => $this->body]);
    }
}
