<?php

namespace Field\Mail\Yamakara;

use Sichikawa\LaravelSendgridDriver\SendGrid;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

use Field\Event;
use Field\Eventattendance;
use Field\Tourmail;
use Field\Tourmailtemplate;

class Adminwaitingalert extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    use SendGrid;
    public function __construct(Event $event)
    {
        $this->title = '<新システムテスト中>'.'['.$event->depdate. '発'. $event->course->name .']'.'満席になりました';
        $this->event = $event;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->from('customer@field-mt.com', 'Field&Mountain')
             ->subject($this->title)
             ->text('mail.yamakara.admin.adminwaitingalertmail')
             ->with(['event' => $this->event]);
    }
}
