<?php

namespace Field\Mail\Yamakara;

use Sichikawa\LaravelSendgridDriver\SendGrid;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

use Field\Balance;
use Field\Tourmail;

class Pay extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public function __construct($balance)
    {
        $this->balance = $balance;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->from('customer@field-mt.com', 'Field&Mountain')
             ->subject($this->balance->eventattendance->Tourname. 'がお支払い済みになりました')
             ->text('mail.yamakara.pay')
             ->with(['balance' => $this->balance]);
        $tourmail = new Tourmail();
        $tourmail->tourmailcategory_id = 3;
        $tourmail->user_id = $this->balance->user_id;
        $tourmail->event_id = $this->balance->eventattendance->eventprice->event_id;
        $tourmail->title = $this->balance->eventattendance->Tourname. 'がお支払い済みになりました';
        $tourmail->body = view('mail.yamakara.pay')->with(['balance' => $this->balance]);
        $tourmail->to = $this->balance->eventattendance->user->email;
        $tourmail->save();
    }
}
