<?php

namespace Field\Mail\Yamakara;

use Sichikawa\LaravelSendgridDriver\SendGrid;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

use Field\Eventattendance;
use Field\Eventprice;
use Field\Event;
use Field\Tourmail;
use Field\Tourmailtemplate;
use Field\User;

class Attend extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $tourmailtemplate;
    public $eventattendance;
    public $user;
    protected $body;
    public function __construct($eventattendance, $tourmailtemplate)
    {
        $this->eventattendance = $eventattendance;
        $this->tourmailtemplate = $tourmailtemplate;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->from('customer@field-mt.com', 'Field&Mountain')
             ->subject('['.$this->eventattendance->eventprice->event->Departuredate.'発'.$this->eventattendance->eventprice->event->course->name. ']'. $this->tourmailtemplate->title)
             ->text('mail.yamakara.attendmail')
             ->with(['eventattendance' => $this->eventattendance, 'body' => $this->tourmailtemplate->template, 'attendlist' => $this->eventattendance->Tourlist]);
        $tourmail = new Tourmail();
        $tourmail->tourmailcategory_id = 1;
        $tourmail->user_id = $this->eventattendance->user_id;
        $tourmail->event_id = $this->eventattendance->eventprice->event_id;
        $tourmail->title = '['.$this->eventattendance->eventprice->Tourname. ']'. $this->tourmailtemplate->title;
        $tourmail->body = view('mail.yamakara.attendmail')->with(['eventattendance' => $this->eventattendance, 'body' => $this->tourmailtemplate->template, 'attendlist' => $this->eventattendance->Tourlist]);
        $tourmail->to = $this->eventattendance->user->email;
        $tourmail->save();
    }
}
