<?php

namespace Field\Mail\Yamakara;

use Sichikawa\LaravelSendgridDriver\SendGrid;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

use Field\Balance;

class Inbyuser extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public function __construct($balance)
    {
        $this->balance = $balance;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->from('customer@field-mt.com', 'Field&Mountain')
             ->subject('ご入金ありがとうございます')
             ->text('mail.yamakara.inbyuser')
             ->with(['balance' => $this->balance]);
    }
}
