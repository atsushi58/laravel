<?php

namespace Field\Mail\Yamakara;

use Sichikawa\LaravelSendgridDriver\SendGrid;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

use Field\Tourmail;
use Field\Eventattendance;

class Rentalconfirm extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $eventattendance;
    public function __construct($eventattendance)
    {
        $this->eventattendance = $eventattendance;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->from('customer@field-mt.com', 'Field&Mountain')
             ->subject('['.$this->eventattendance->eventprice->event->Departuredate.'発'.$this->eventattendance->eventprice->event->course->name. ']レンタルを承りました')
             ->text('mail.yamakara.rentalconfirm')
             ->with(['eventattendance' => $this->eventattendance]);
        $tourmail = new Tourmail();
        $tourmail->tourmailcategory_id = 8;
        $tourmail->user_id = $this->eventattendance->user_id;
        $tourmail->event_id = $this->eventattendance->eventprice->event_id;
        $tourmail->title = '['.$this->eventattendance->eventprice->Tourname. ']レンタルを承りました';
        $tourmail->body = view('mail.yamakara.rentalconfirm')->with(['eventattendance' => $this->eventattendance]);
        $tourmail->to = $this->eventattendance->user->email;
        $tourmail->save();
    }
}
