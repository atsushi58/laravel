<?php

namespace Field\Mail\Yamakara;

use Sichikawa\LaravelSendgridDriver\SendGrid;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

use Field\Eventattendance;
use Field\Eventprice;
use Field\Event;
use Field\Tourmail;
use Field\Tourmailtemplate;
use Field\User;

class Waiting extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $eventattendance;
    use SendGrid;
    public function __construct(Eventattendance $eventattendance)
    {
        $tourmailtemplate = Tourmailtemplate::findOrFail(1);
        $this->title = '['.$eventattendance->eventprice->Tourname. ']'. $tourmailtemplate->title;
        $this->eventattendance = $eventattendance;
        $this->body = $tourmailtemplate->template;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->from('customer@field-mt.com', 'Field&Mountain')
             ->subject($this->title)
             ->text('mail.yamakara.waitingmail')
             ->with(['eventattendance' => $this->eventattendance, 'body' => $this->body, 'attendlist' => $this->eventattendance->Tourlist]);
        $tourmail = new Tourmail();
        $tourmail->tourmailcategory_id = 2;
        $tourmail->user_id = $this->eventattendance->user_id;
        $tourmail->event_id = $this->eventattendance->eventprice->event_id;
        $tourmail->title = $this->title;
        $tourmail->body = view('mail.yamakara.waitingmail')->with(['eventattendance' => $this->eventattendance, 'body' => $this->body, 'attendlist' => $this->eventattendance->Tourlist]);
        $tourmail->to = $this->eventattendance->user->email;
        $tourmail->save();
    }
}
