<?php

namespace Field\Mail\Yamakara;

use Sichikawa\LaravelSendgridDriver\SendGrid;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;


use Field\Eventattendance;
use Field\Eventprice;
use Field\Event;
use Field\Tourmail;

class Cancel extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $eventattendance;
    protected $body;
    public function __construct($eventattendance)
    {
        $this->eventattendance = $eventattendance;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->from('customer@field-mt.com', 'Field&Mountain')
             ->subject('['.$this->eventattendance->eventprice->event->Departuredate.'発'.$this->eventattendance->eventprice->event->course->name. ']キャンセル承りました')
             ->text('mail.yamakara.cancel')
             ->with(['eventattendance' => $this->eventattendance,  'attendlist' => $this->eventattendance->Tourlist]);
        $tourmail = new Tourmail();
        $tourmail->tourmailcategory_id = 7;
        $tourmail->user_id = $this->eventattendance->user_id;
        $tourmail->event_id = $this->eventattendance->eventprice->event_id;
        $tourmail->title = '['.$this->eventattendance->eventprice->Tourname. ']キャンセル承りました';
        $tourmail->body = view('mail.yamakara.cancel')->with(['eventattendance' => $this->eventattendance,  'attendlist' => $this->eventattendance->Tourlist]);
        $tourmail->to = $this->eventattendance->user->email;
        $tourmail->save();
    }
}
