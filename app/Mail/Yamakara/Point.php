<?php

namespace Field\Mail\Yamakara;

use Sichikawa\LaravelSendgridDriver\SendGrid;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

use Field\Eventattendance;
use Field\Tourmailtemplate;
use Field\Tourmail;

class Point extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $eventattendance;
    public function __construct($eventattendance)
    {
        $this->eventattendance = $eventattendance;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->from('customer@field-mt.com', 'Field&Mountain')
             ->subject('本日発の'.$this->eventattendance->Tourname. 'をポイント利用にて承りました')
             ->text('mail.yamakara.pointpay')
             ->with(['eventattendance' => $this->eventattendance]);
        $tourmail = new Tourmail();
        $tourmail->tourmailcategory_id = 3;
        $tourmail->user_id = $this->eventattendance->user_id;
        $tourmail->event_id = $this->eventattendance->eventprice->event_id;
        $tourmail->title = $this->eventattendance->Tourname. 'をポイント利用にて承りました';
        $tourmail->body = view('mail.yamakara.pointpay')->with(['eventattendance' => $this->eventattendance]);
        $tourmail->to = $this->eventattendance->user->email;
        $tourmail->save();
    }
}
