<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Yakucomerequest extends Model
{
    //
    protected $fillable = ['name', 'defaultyakucometokoj', 'defaultyakucometokum'];

    public function getTokojAttribute()
    {
    	$id = $this->defaultyakucometokoj;
    	return Yakucometokoj::findOrFail($id);
    }

    public function getTokumAttribute()
    {
    	$id = $this->defaultyakucometokum;
    	return Yakucometokum::findOrFail($id);    	
    }

}
