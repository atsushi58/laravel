<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Workplace extends Model
{
    protected $fillable = [
        'name', 'lunchbreaksetting'
    ];

    public function payments(){
    	return $this->hasMany('Field\Payment');
    }

    public function shifts(){
    	return $this->hasMany('Field\Shift');
    }
}
