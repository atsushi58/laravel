<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Yakuactivitytype extends Model
{
    protected $fillable = ['name'];

    public function yakuactivities()
    {
        return $this->hasMany('Field\Yakuactivity');
    }
}
