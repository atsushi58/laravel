<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    //
    protected $fillable = ['contactlist_id', 'admin_id', 'body'];

    public function admin()
    {
    	return $this->belongsTo('Field\Admin');
    }

    public function contactlist()
    {
    	return $this->belongsTo('Field\Contactlist');
    }


}
