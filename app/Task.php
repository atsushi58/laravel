<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
	protected $fillable = [
        'name', 'active', 'division_id'
    ];

	public function primarytasks(){
    	return $this->hasMany('Field\Shift', 'id');
    }

    public function division()
    {
    	return $this->belongsTo('Field\Admin\Division');
    }
}
