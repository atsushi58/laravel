<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Yakucourse extends Model
{
    //
    protected $fillable = ['name', 'depplace_id', 'courselength_id', 'memo'];

    public function courselength(){
        return $this->belongsTo('Field\Courselength');
    }

    public function depplace(){
    	return $this->belongsTo('Field\Depplace');
    }

    public function yakuschedules(){
        return $this->hasMany('Field\Yakuschedule');
    }

    public function topsliders(){
        return $this->hasMany('Field\Topslider');
    }

    public function yakudefaultactivities()
    {
        return $this->hasMany('Field\Yakudefaultactivity');
    }

    public function yakuevents(){
        return $this->hasMany('Field\YakuEvent');
    }

    public function yakutourimages(){
        return $this->hasMany('Field\Yakutourimage');
    }
}
