<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Tourmailtemplate extends Model
{
	protected $fillable = [
        'name', 'title', 'template', 'tourmailcategory_id'
    ];

    public function tourmailcategory()
    {
    	return $this->belongsTo('Field\Tourmailcategory');
    }

    public function events()
    {
    	return $this->hasMany('Field\Event');
    }
}
