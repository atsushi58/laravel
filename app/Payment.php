<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'admin_id', 'workplace_id', 'payment', 'transportation', 'route'
    ];
    
    public function admin(){
    	return $this->belongsTo('Field\Admin');
    }

    public function workplace(){
    	return $this->belongsTo('Field\Workplace');
    }
}
