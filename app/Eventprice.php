<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Eventprice extends Model
{
    //
    protected $fillable = ['event_id', 'depplace_id', 'price', 'kidsprice', 'description', 'hpactive'];

    public function event()
    {
    	return $this->belongsTo('Field\Event');
    }

    public function depplace()
    {
    	return $this->belongsTo('Field\Depplace');
    }

    public function eventattendances()
    {
    	return $this->hasMany('Field\Eventattendance');
    }

    public function getNameAttribute(){
        return $this->depplace->name. "(" . number_format($this->price) . "円)";
    }

    public function getDepdateAttribute()
    {
        return $this->event->depdate;
    }

    public function getClientsAttribute()
    {
        return $this->eventattendances->whereIn('eventattendancestatus_id', ['2', '8'])->sum('numofpeople') + $this->eventattendances->whereIn('eventattendancestatus_id', ['2', '8'])->sum('numofchildren');
    }

    public function getPaidsalesAttribute()
    {
        return $this->eventattendances->sum('Paid');
    }

    public function getTournameAttribute()
    {
        if($this->description)
        {
            return $this->event->id. ')'. $this->event->course->name. '('. $this->event->Departuredate.  $this->depplace->name . '発'. ' '. number_format($this->price). '円)<'. $this->description . '>';
        }else{
        return $this->event->id. ')'. $this->event->course->name. '('. $this->event->Departuredate.  $this->depplace->name . '発'. ' '. number_format($this->price). '円)';
        }
    }

    public function getNamepriceAttribute()
    {
        if($this->price > 499)
        {
            return number_format($this->price). '円('. $this->depplace->name. '発)';
        }else
        {
            return '未定';
        }
    }
}
