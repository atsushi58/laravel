<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
	protected $fillable = [
        'bankcode', 'name'
    ];

    public function bankaccount(){
        return $this->hasMany('Field\Bankaccount');
    }
}
