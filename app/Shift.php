<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Shift extends Model
{
    protected $fillable = [
        'admin_id', 'event_id', 'workday', 'workplace_id', 'confirm', 'start', 'end', 'primarytask_id', 'secondarytask_id', 'actual_start', 'actual_end', 'break', 'hourlypay', 'paybalance', 'transportation', 'comment', 'summerspecial', 'transportationbalance'
    ];

	public function admin()
    {
    	return $this->belongsTo('Field\Admin');
    }

    public function event()
    {
        return $this->belongsTo('Field\Event');
    }

    public function starttime(){
    	return $this->belongsTo('Field\Worktime', 'start');
    }

    public function endtime(){
    	return $this->belongsTo('Field\Worktime', 'end');
    }

    public function workplace(){
    	return $this->belongsTo('Field\Workplace');
    }

    public function primarytask(){
        return $this->belongsTo('Field\Task', 'primarytask_id');
    }

    public function secondarytask(){
        return $this->belongsTo('Field\Task', 'secondarytask_id');
    }

    public function getWorktimeAttribute(){
        if(empty($this->actual_end)){
            return 0;
        }else{
        return (strtotime($this->actual_end)-strtotime($this->actual_start))/60 - $this->break;
        }
    }

    public function getBasepayAttribute(){
        if(in_array($this->admin->role_id, [4,5]) && $this->workplace_id == 2)
        {
            if(!empty($this->event)){
                $days = $this->event->course->courselength->days;
            }else{
                $days = 0;
            }
            if($this->primarytask_id == 13)
            {
                return 8000 * $days;
            }elseif ($this->primarytask_id == 21) 
            {
                return 7000 * $days;
            }elseif ($this->primarytask_id ==22)
            {
                return 20000 * $days;
            }
        }elseif(empty($this->hourlypay)){
            return 0;
        }else{
        return floor($this->worktime * $this->hourlypay/60);
        }
    }

    public function getOvertimePayAttribute()
    {
        if(($this->worktime) > 480)
        {
            return floor(($this->worktime-480)*$this->hourlypay/60*0.25);
        }else
        {
            return 0;
        }
    }

    public function getTotalpaymentAttribute(){
        return $this->Basepay + $this->Overtimepay + $this->summerspecial + $this->paybalance + $this->transportation + $this->transportationbalance;
    }


}