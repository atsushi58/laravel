<?php

namespace Field\Providers;

use Illuminate\Support\ServiceProvider;

class EventattendanceServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'eventattendance',
            'Field\Services\Eventattendance'
        );
    }
}
