<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = ['name', 'subtitle', 'depplace_id', 'courselength_id', 'coursearea_id', 'eventtype_id', 'wpid', 'metatitle', 'metadescription', 'metakeywords', 'slug', 'memo'];

    public function coursearea(){
    	return $this->belongsTo('Field\Coursearea');
    }

    public function courselength(){
        return $this->belongsTo('Field\Courselength');
    }

    public function eventtype(){
    	return $this->belongsTo('Field\Eventtype');
    }

    public function depplace(){
    	return $this->belongsTo('Field\Depplace');
    }

    public function contactlists()
    {
        return $this->belongsToMany('Field\Contactlist');
    }

    public function schedules(){
        return $this->hasMany('Field\Schedule');
    }

    public function scenarios()
    {
        return $this->hasMany('Field\Scenario');
    }

    public function topsliders(){
        return $this->hasMany('Field\Topslider');
    }

    public function events(){
        return $this->hasMany('Field\Event');
    }

    public function tourimages(){
        return $this->hasMany('Field\Tourimage');
    }

    public function getTcreportsAttribute()
    {
        $events = $this->events->pluck('id');
        return Tcreport::whereIn('event_id', $events)->get();
    }

    public function getActiveeventsAttribute()
    {
        return $this->events->sortBy('depdate')->where('depdate', '>', date('Y-m-d'))->whereIn('eventstatus_id', [4,5,6]);
    }
}
