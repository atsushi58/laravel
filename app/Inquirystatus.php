<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Inquirystatus extends Model
{
    protected $fillable = [
        'name'
    ];

    public function inquies(){
    	return $this->hasMany('Field\Inquiry');
    }
}
