<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Eventcost extends Model
{
    //
    protected $fillable = ['amount', 'event_id', 'eventcostcategory_id', 'eventcosttype_id', 'eventcostpaymentstatus_id', 'memo', 'payee'];

    public function event()
    {
        return $this->belongsTo('Field\Event');
    }

    public function eventcosttype(){
        return $this->belongsTo('Field\Eventcosttype');
    }

    public function eventcostcategory()
    {
    	return $this->belongsTo('Field\Eventcostcategory');
    }

    public function eventcostpaymentstatus()
    {
        return $this->belongsTo('Field\Eventcostpaymentstatus');
    }
}
