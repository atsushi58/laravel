<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
    protected $fillable = [
        'kara', 'made', 'zerodependants', 'onedependants', 'twodependants', 'threedependants', 'fourdependants', 'fivedependants', 'sixdependants', 'sevendependants', 'otsu'
    ];
}
