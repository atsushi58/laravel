<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Tourimage extends Model
{
	protected $fillable = [
        'filename', 'title', 'caption', 'course_id'
    ];

    public function course(){
        return $this->belongsTo('Field\Course');
    }
}
