<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Referral extends Model
{
	protected $fillable = [
        'user_id', 'eventattendance_id', 'stamp'
    ];

    public function user()
    {
    	return $this->belongsTo('Field\User');
    }

    public function eventattendance(){
        return $this->belongsTo('Field\Eventattendance');
    }
}
