<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Tctimereport extends Model
{
    protected $fillable = ['uptime','downtime', 'day', 'tcreport_id', 'point', 'memo'];

    public function tcreport()
    {
    	return $this->belongsTo('Field\Tcreport');
    }
}
