<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Eventcosttype extends Model
{
    //
    protected $fillable = ['name'];

    public function eventcosts(){
        return $this->hasMany('Field\Eventcost');
    }

    public function scenariocosts()
    {
    	return $this->hasMany('Field\Scenariocost');
    }

}
