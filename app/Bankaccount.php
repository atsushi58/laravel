<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Bankaccount extends Model
{
    protected $fillable = ['bank_id', 'admin_id', 'branch', 'accountno', 'branchcode'];

    public function admin(){
    	return $this->belongsTo('Field\Admin');
    }

    public function bank(){
    	return $this->belongsTo('Field\Bank');
    }
}
