<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Contactlist extends Model
{
    //
    protected $fillable = ['name', 'tel', 'email', 'person', 'memo'];

    public function event()
    {
    	return $this->belongsTo('Field\Contactlist');
    }

    public function courses()
    {
    	return $this->belongsToMany('Field\Course');
    }

    public function contacts()
    {
        return $this->hasMany('Field\Contact');
    }


}
