<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;


class Eventattendance extends Model
{

    protected $fillable = ['user_id', 'eventprice_id', 'eventattendancepaymentmethod_id', 'eventattendancepaymentstatus_id', 'eventattendancestatus_id', 'group', 'numofpeople', 'numofchildren', 'friendinfo', 'rentaldelivery_id', 'rental', 'bookdate', 'rentalpack', 'rentaldeliver', 'memo'];

    public function user(){
    	return $this->belongsTo('Field\User');
    }

    public function eventprice(){
    	return $this->belongsTo('Field\Eventprice');
    }

    public function referrals()
    {
        return $this->hasMany('Field\Referral');
    }

    public function eventattendancestatus()
    {
    	return $this->belongsTo('Field\Eventattendancestatus');
    }

    public function eventattendancepaymentstatus()
    {
        return $this->belongsTo('Field\Eventattendancepaymentstatus');
    }

    public function eventattendancepaymentmethod()
    {
        return $this->belongsTo('Field\Eventattendancepaymentmethod');
    }

    public function rentaldelivery()
    {
        return $this->belongsTo('Field\Rentaldelivery');
    }

    public function rentalstatus()
    {
        return $this->belongsTo('Field\Rentalstatus');
    }

    public function getTournameAttribute()
    {
        return $this->eventprice->event->course->name.'('.$this->eventprice->event->Departuredate . '/' .$this->eventprice->depplace->name.'発'.$this->eventprice->price.'円'.')';
    }

    public function balances()
    {
        return $this->hasMany('Field/Balance');
    }

    public function getClientageAttribute()
    {
        if(!empty($this->user->birthday))
        {
        $depdate_age = date("Ymd", strtotime($this->eventprice->event->depdate));
        $birthday_age = date("Ymd", strtotime($this->user->birthday));
        return floor(($depdate_age - $birthday_age)/10000);
    }
    }

    public function getBirthdaycheckAttribute()
    {
        $date_dep = date("md", strtotime($this->eventprice->event->depdate));
        $date_arr = $date_dep + $this->eventprice->event->course->courselength->days;
        $date_birthday = date("md", strtotime($this->user->birthday));
        if(empty($this->user->birthday)){ return '空';}
        elseif($date_birthday >= $date_dep && $date_birthday < $date_arr){
            return date("m/d", strtotime($this->user->birthday)) . '★';}
        else{
            return date("m/d", strtotime($this->user->birthday));
        }
    }

    public function getSalesAttribute()
    {
        return $this->eventprice->price * $this->numofpeople + $this->eventprice->kidsprice * $this->numofchildren;
    }

    public function getPaidAttribute()
    {
        if($this->eventattendancestatus_id == 2 && $this->eventattendancepaymentstatus_id == 2)
        {
            return $this->Sales;
        }elseif($this->eventattendancestatus_id == 8 && $this->eventattendancepaymentstatus_id == 2)
        {
            return $this->Sales;
        }elseif($this->eventattendancepaymentmethod_id == 5)
        {
            return $this->Sales;
        }else
        {
            0;
        }
    }

    public function getShortmemoAttribute()
    {
        if(strlen($this->memo) < 51)
        {
            return $this->memo;    
        }else
        {
            return substr($this->memo, 0,50).'...';    
        }
    }

    public function getClientnameAttribute()
    {
        return $this->user->name;
    }

    public function getClientidAttribute()
    {
        return $this->user->id;
    }

    public function getTourlistAttribute()
    {
        return Eventattendance::join('eventprices', 'eventattendances.eventprice_id', '=', 'eventprices.id')->join('events', 'eventprices.event_id', '=', 'events.id')->whereIn('eventattendances.eventattendancestatus_id', [2,4,5])->where('eventattendances.user_id', $this->user_id)->where('events.depdate', '>', date('Y-m-d'))->orderBy('events.depdate')->get();
    }

}
