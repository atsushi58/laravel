<?php

namespace Field;

use Illuminate\Database\Eloquent\Model;

class Yakuguide extends Model
{
    protected $fillable = ['name', 'kana', 'tel', 'fax', 'email', 'address', 'bizname', 'web'];

    public function Yakuactivities()
    {
    	return $this->belongsToMany('Field\Yakuactivity');
    }
}
