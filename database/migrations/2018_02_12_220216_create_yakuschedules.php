<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYakuschedules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yakuschedules', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('yakucourse_id');
            $table->integer('day')->nullable();
            $table->integer('coursetime')->nullable();
            $table->float('distance')->nullable();
            $table->integer('ascend')->nullable();
            $table->integer('descend')->nullable();
            $table->string('schedule')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yakuschedules');
    }
}
