<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddYakuhostelIdToYakueventattendances extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('yakueventattendances', function (Blueprint $table) {
            $table->integer('yakuhostel_id')->nullable()->after('yakucourseprice_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('yakueventattendances', function (Blueprint $table) {
            //
        });
    }
}
