<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYakuactivitiesYakueventattendances extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yakuactivities_yakueventattendances', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('yakuactivity_id');
            $table->integer('yakueventattendance_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yakuactivities_yakueventattendances');
    }
}
