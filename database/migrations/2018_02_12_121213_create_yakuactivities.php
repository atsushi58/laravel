<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYakuactivities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yakuactivities', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('yakuactivitytype_id');
            $table->date('depdate');
            $table->text('memo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yakuactivities');
    }
}
