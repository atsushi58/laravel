<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYakudefaultactivities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yakudefaultactivities', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('yakucourse_id');
            $table->integer('day');
            $table->integer('yakuactivitytype_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yakudefaultactivities');
    }
}
