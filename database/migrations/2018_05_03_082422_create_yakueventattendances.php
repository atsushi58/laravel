<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYakueventattendances extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yakueventattendances', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('yakuevent_id');
            $table->integer('user_id');
            $table->integer('eventattendancestatus_id');
            $table->integer('eventattendancepaymentstatus_id');
            $table->string('group')->nullable();
            $table->integer('numofpeople')->nullable();
            $table->integer('numofchildren')->nullable();
            $table->string('rental')->nullable();
            $table->text('memo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yakueventattendances');
    }
}
