<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYakuoptioneventattendances extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yakuoptioneventattendances', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('yakueventattendance_id');
            $table->boolean('mainattendant');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('optioneventattenances');
    }
}
