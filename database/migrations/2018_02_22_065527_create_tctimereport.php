<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTctimereport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tctimereports', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('tcreport_id');
            $table->time('uptime')->nullable();
            $table->time('downtime')->nullable();
            $table->string('point');
            $table->text('memo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tctimereport');
    }
}
