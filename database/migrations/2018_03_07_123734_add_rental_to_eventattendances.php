<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRentalToEventattendances extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('eventattendances', function (Blueprint $table) {
            $table->text('numofchildren')->nullable()->after('numofpeople');
            $table->text('rental')->nullable()->after('numofchildren');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('eventattendances', function (Blueprint $table) {
            //
        });
    }
}
