<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYakucourseprices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yakucourseprices', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('yakucourse_id');
            $table->integer('depdate');
            $table->integer('priceone');
            $table->integer('pricetwo');
            $table->integer('pricethree');
            $table->integer('pricefour');
            $table->integer('pricefive');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yakucourseprices');
    }
}
