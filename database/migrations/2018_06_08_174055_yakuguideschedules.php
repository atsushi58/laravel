<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Yakuguideschedules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yakuguideschedules', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('yakuguide_id');
            $table->date('day');
            $table->string('yakuguidescheduletype_id');
            $table->string('memo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yakuguideschedules');
    }
}
