<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddYakuguidecostToYakuactivitytypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('yakuactivitytypes', function (Blueprint $table) {
            $table->integer('guidecostone')->nullable();
            $table->integer('guidecosttwo')->nullable();
            $table->integer('guidecostthree')->nullable();
            $table->integer('guidecostfour')->nullable();
            $table->integer('guidecostfive')->nullable();
            $table->integer('guidecostsix')->nullable();
            $table->integer('guidecostseven')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('yakuactivitytypes', function (Blueprint $table) {
            //
        });
    }
}
