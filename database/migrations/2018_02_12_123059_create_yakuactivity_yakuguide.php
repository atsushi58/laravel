<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYakuactivityYakuguide extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yakuactivity_yakuguide', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('yakuactivity_id');
            $table->integer('yakuguide_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yakuactivity_yakuguide');
    }
}
