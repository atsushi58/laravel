<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYakucourses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yakucourses', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('depplace_id');
            $table->integer('courselength_id');
            $table->string('name');
            $table->text('memo')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yakucourses');
    }
}
