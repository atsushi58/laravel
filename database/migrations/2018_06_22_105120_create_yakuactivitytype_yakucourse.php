<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYakuactivitytypeYakucourse extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yakuactivitytype_yakucourse', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('yakuactivitytype_id');
            $table->integer('yakucourse_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yakuactivitytype_yakucourse');
    }
}
