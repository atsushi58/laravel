<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEventcostcategoryIdToScenariocosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scenariocosts', function (Blueprint $table) {
            $table->integer('eventcostcategory_id')->after('eventcosttype_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scenariocosts', function (Blueprint $table) {
            //
        });
    }
}
