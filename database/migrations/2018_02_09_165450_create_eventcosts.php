<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventcosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventcosts', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('eventcostcategory_id');
            $table->integer('eventcosttype_id');
            $table->integer('eventcostpaymentstatus_id');
            $table->integer('amount');
            $table->string('memo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventcosts');
    }
}
