<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYakuaccompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yakuaccompanies', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('yakueventattendance_id');
            $table->string('name');
            $table->string('sex');
            $table->string('agebracket');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yakuaccompanies');
    }
}
