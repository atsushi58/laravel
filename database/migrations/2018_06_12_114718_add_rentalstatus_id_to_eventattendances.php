<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRentalstatusIdToEventattendances extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('eventattendances', function (Blueprint $table) {
            $table->integer('rentalstatus_id')->nullable()->after('rentaldelivery_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('eventattendances', function (Blueprint $table) {
            //
        });
    }
}
