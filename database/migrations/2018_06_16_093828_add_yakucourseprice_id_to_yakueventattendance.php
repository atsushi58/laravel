<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddYakucoursepriceIdToYakueventattendance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('yakueventattendances', function (Blueprint $table) {
            $table->integer('yakucourseprice_id')->after('yakucourse_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('yakueventattendances', function (Blueprint $table) {
            //
        });
    }
}
