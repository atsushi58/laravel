<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYakudefaulthostelarranges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yakudefaulthostelarranges', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('yakucourse_id');
            $table->integer('day');
            $table->integer('yakuhosteltype_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yakudefaulthostelarranges');
    }
}
