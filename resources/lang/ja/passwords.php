<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'パスワードは6文字以上で確認欄と同じでなければなりません',
    'reset' => 'パスワードが再設定されました',
    'sent' => 'パスワード再設定メールをお送りしました',
    'token' => 'このパスワードは無効です',
    'user' => "そのメールアドレスは登録されていません",

];
