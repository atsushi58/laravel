@extends('layouts.default')

@section('title', 'イベントマスタの編集')

@section('content')
<h1>イベントマスタの編集</h1>
<div class="jumbotron">
    <form method="post" action="{{ url('/events', $event->id) }}">
    {{ csrf_field() }}
    {{ method_field('patch') }}
        <div class="form-group row">
            <div class="col-xs-6">
                {{ Form::label('course_id', '登山コース', ['class' => 'col-2 col-form-label']) }}
                {{ Form::select('course_id', $courses, $event->course_id, ['class' => 'form-control col-4']) }}
            </div>
            <div class="col-xs-6">
                {{ Form::label('eventtype_id', 'イベントタイプ', ['class' => 'col-2 col-form-label']) }}
                {{ Form::select('eventtype_id', $eventtypes, $event->eventtype_id, ['class' => 'form-control col-4']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-xs-6">
                {{ Form::label('start', '出発日') }}
                {{ Form::date('start', $event->start ,['class' => 'form-control']) }}
            </div>
            <div class="col-xs-6">
                {{ Form::label('eventstatus_id', 'イベントステータス') }}
                {{ Form::select('eventstatus_id', $eventstatuses ,null, ['class' => 'form-control col-4']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-xs-6">
                {{ Form::label('maxpax', '定員') }}
                {{ Form::number('maxpax', $event->maxpax, ['class' => 'form-control']) }}
            </div>
            <div class="col-xs-6">
                {{ Form::label('baseprice', '基本旅行代金') }}
                {{ Form::number('baseprice', $event->baseprice, ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-xs-2">
            {{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
            {{ Form::close()}}
            </div>
        </div>
</div>
@endsection