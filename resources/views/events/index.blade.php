@extends('layouts.app')

@section('title', 'イベントマスタ')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>イベントマスタ</h3></div>
        	<div class="panel-body">


<form class="form-horizontal">
  <div class="form-group">
    <label for="name" class="control-label col-xs-1">ビュー : </label>
    <div class="col-xs-3">
		<select onChange="top.location.href=value" class="form-control">
			<option value="#"></option>
			<option value="(移動したい先URL)">(移動したい先名前)</option>
			<option value="(移動したい先URL)">(移動したい先名前)</option>
		</select>
	</div>
  </div>
</form>

<a href="{{ action('EventsController@create') }}"><button type="button" class="btn btn-default navbar-btn">新規イベントマスタ</button></a>


<table class="table table-hover table-striped">
	<thead>
	<tr>
		<th>アクション</th>
		<th>イベント名</th>
		<th>イベントタイプ</th>
		<th>イベントステータス</th>
		<th>出発日</th>
		<th>定員</th>
		<th>出発地</th>
		<th>基本旅行代金</th>
	</tr>
	</thead>
	<tbody>
	@forelse ($events as $event)
	<tr>
		<td><a href="{{ action('EventsController@show', $event->id) }}">詳細</a> | 
		<a href="{{ action('EventsController@edit', $event->id) }}">編集</a> | 
		<form action="{{ action('EventsController@destroy', $event->id) }}" id="form_{{ $event->id }}" method="post" style="display:inline">
		    {{ csrf_field() }}
    		{{ method_field('delete') }}
      		<a href="#" data-id="{{ $event->id }}" onclick="deleteEvents(this);">削除</a></form>
		<td>{{ $event->course->name }}</td>
		<td>{{ $event->eventtype->name}}</td>
		<td>{{ $event->eventstatus->name}}</td>
		<td>{{ $event->start }}</td>
		<td>{{ $event->maxpax }}</td>
		<td></td>
		<td>{{ $event->baseprice }}</td>
	</tr>
	@empty

	@endforelse
	</tbody>
</table>
</div>
<script>
function deleteEvents(e) {
  'use strict';

  if (confirm('本当に削除しますか？')) {
    document.getElementById('form_' + e.dataset.id).submit();
  }
}
</script>



@endsection