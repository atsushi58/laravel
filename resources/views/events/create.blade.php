@extends('layouts.default')

@section('title', '新規イベントマスタ')

@section('content')
<h1>新規イベントマスタ</h1>
<div class="jumbotron">
    {{ Form::open(['action' => 'EventsController@store', 'method' => 'post', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-xs-6">
                {{ Form::label('course_id', '登山コース', ['class' => 'col-2 col-form-label']) }}
                {{ Form::select('course_id', $courses, null, ['class' => 'form-control col-4']) }}
            </div>
            <div class="col-xs-6">
                {{ Form::label('eventtype_id', 'イベントタイプ', ['class' => 'col-2 col-form-label']) }}
                {{ Form::select('eventtype_id', $eventtypes, null, ['class' => 'form-control col-4']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-xs-6">
                {{ Form::label('start', '出発日') }}
                {{ Form::date('start', null ,['class' => 'form-control']) }}
            </div>
            <div class="col-xs-6">
                {{ Form::label('eventstatus_id', 'イベントステータス') }}
                {{ Form::select('eventstatus_id', $eventstatuses ,null, ['class' => 'form-control col-4']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-xs-6">
                {{ Form::label('maxpax', '定員') }}
                {{ Form::number('maxpax', null, ['class' => 'form-control']) }}
            </div>
            <div class="col-xs-6">
                {{ Form::label('baseprice', '基本旅行代金') }}
                {{ Form::number('baseprice', null, ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-xs-2">
            {{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
            {{ Form::close()}}
            </div>
        </div>
</div>
@endsection