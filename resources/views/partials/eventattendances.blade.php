<table class="datatable display nowrap" cellspacing="0" width="100%">
	<thead>
	<tr>
		<th>アクション</th>
		<th>id</th>
		<th>申込STAT</th>
		<th>支払STAT</th>
		<th>お名前</th>
		<th>カナ</th>
		<th>郵便番号</th>
		<th>都道府県</th>
		<th>住所</th>
		<th>性別</th>		
		<th>参加時年齢</th>
		<th>電話番号</th>
		<th>メール</th>
		<th>組</th>
		<th>誕生日</th>
		<th>出発日</th>
		<th>ツアー</th>
		<th>参加人数(大人)</th>
		<th>参加人数(子供)</th>
		<th>出発地</th>
		<th>料金</th>
		<th>支払済</th>
		<th>支払方法</th>
		<th>顧客メモ</th>
		<th>参加メモ</th>
		<th>レンタル</th>
		<th>同行者情報</th>
		<th>緊急連絡先</th>
	</tr>
	</thead>
	<tbody>
	@foreach ($eventattendances as $eventattendance)
	<tr>
		<td><a href="{{ route('eventattendances.edit', $eventattendance->id) }}">編集</a> | <form action="{{ route('eventattendances.cancel', $eventattendance->id) }}" id="cancelform_{{ $eventattendance->id }}" method="post" style="display:inline">
			{{ csrf_field() }}
    		{{ method_field('patch') }}
  		<a href="#" data-id="{{ $eventattendance->id }}" onclick="cancelEventattendance(this);">CXL</a></form> | <form action="{{ route('eventattendances.attend', $eventattendance->id) }}" id="attendform_{{ $eventattendance->id }}" method="post" style="display:inline">
			{{ csrf_field() }}
    		{{ method_field('patch') }}
  		<a href="#" data-id="{{ $eventattendance->id }}" onclick="attendEventattendance(this);">繰上</a></form>
  		</td>
		<td><a href="{{ route('eventattendances.show', $eventattendance->id) }}">{{ $eventattendance->id }}</a></td>
		<td>{{ $eventattendance->eventattendancestatus->name or '' }}</td>
		<td>{{ $eventattendance->eventattendancepaymentstatus->name or '' }}</td>
		<td>{{ $eventattendance->user->name or '' }}様</td>
		<td>{{ $eventattendance->user->kana or '' }}</td>
		<td>{{ $eventattendance->user->zipcode or '' }}</td>
		<td>{{ $eventattendance->user->pref or ''  }}</td>
		<td>{{ $eventattendance->user->address or '' }}</td>
		<td></td>		
		<td>{{ $eventattendance->Clientage or '' }}</td>
		<td>{{ $eventattendance->user->tel or '' }}</td>
		<td>{{ $eventattendance->user->email or '' }}</td>
		<td>{{ $eventattendance->group or ''  }}</td>
		<td>{{ $eventattendance->Birthdaycheck or ''  }}</td>
		<td>{{ $eventattendance->eventprice->event->Departuredate or '' }}</td>
		<td><a href="{{ route('events.show', $eventattendance->eventprice->event->id) }}">{{ $eventattendance->eventprice->event->course->name }}</a></td>
		<td>{{ $eventattendance->numofpeople }}</td>
		<td>{{ $eventattendance->numofchildren }}</td>
		<td>{{ $eventattendance->eventprice->depplace->name or '' }}</td>
		<td>{{ number_format($eventattendance->Sales) }}</td>
		<td>{{ number_format($eventattendance->Paid) }}</td>
		<td>{{ $eventattendance->eventattendancepaymentmethod->name or '' }}</td>
		<td>{{ $eventattendance->user->Shortmemo }}</td>
		<td>{{ $eventattendance->memo }}</td>
		<td>{{ $eventattendance->rental }}</td>
		<td>{{ $eventattendance->friendinfo }}</td>
		<td>{{ $eventattendance->user->emcontactperson}}({{ $eventattendance->user->emcontactrelation}}){{ $eventattendance->user->emcontacttel}}</td>
	</tr>
	@endforeach
	</tbody>
</table>
<script>
function cancelEventattendance(e) {
  'use strict';
  if (confirm('キャンセルにしますか？')) {
    document.getElementById('cancelform_' + e.dataset.id).submit();
  }
}
</script>
<script>
function attendEventattendance(e) {
  'use strict';

  if (confirm('参加にしますか？')) {
    document.getElementById('attendform_' + e.dataset.id).submit();
  }
}
</script>
<script>
function payEventattendance(e) {
  'use strict';

  if (confirm('入金済にしますか？')) {
    document.getElementById('payform_' + e.dataset.id).submit();
  }
}
</script>