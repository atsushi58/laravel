<p> 売上 : {{ number_format($events->sum('Paidsales')) }}円</p>
<a href="{{ route('events.create') }}"><button type="button" class="btn btn-default navbar-btn">新規イベントマスタ</button></a>
<table class="datatable display" cellspacing="0" width="100%">
	<thead>
	<tr>
		<th>アクション</th>
		<th>イベントid</th>
		<th>出発日</th>
		<th>イベント名</th>
		<th>売上</th>
		<th>申込数</th>
		<th>定員</th>
		<th>満席率</th>
		<th>担当</th>
		<th>添乗</th>
		<th>イベント期間</th>
		<th>イベントタイプ</th>
		<th>イベントステータス</th>
	</tr>
	</thead>
	<tbody>
	@forelse ($events as $event)
	<tr>
		<td><a href="{{ route('events.show', $event->id) }}">詳細</a> | 
		<a href="{{ route('events.edit', $event->id) }}">編集</a> | 
		<a href="{{ route('events.tcset', $event->id) }}">添乗</td>
		<td>{{ $event->id }}</td>
      	<td>{{ $event->Departuredate }}</td>
		<td>{{ $event->subtitle }}{{ $event->course->name }}</td>
		<td>{{ number_format($event->Paidsales) }}</td>
		<td>{{ $event->Activetotalclients }}</td>
		<td>{{ $event->maxpax }}</td>
		<td>{{ $event->Loadfactor }}%</td>
		<td>@if($event->admin->id ==1){{ '' }}@else{{ $event->admin->name }}@endif</td>
		<td>@forelse($event->shifts as $shift){{ $shift->admin->name or '' }} @empty @endforelse </td>
		<td>{{ $event->course->courselength->name }}</td>
		<td>{{ $event->course->eventtype->name}}</td>
		<td>{{ $event->eventstatus->name}}</td>
	</tr>
	@empty
	@endforelse
	</tbody>
</table>
