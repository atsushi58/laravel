			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>コンタクト先名</th>
						<th>担当者</th>
						<th>電話番号</th>
						<th>メールアドレス</th>
						<th>メモ</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($contactlists as $contactlist)
					<tr>
						<td>
							<a href="{{ route('contactlists.show', $contactlist->id) }}">詳細</a> | <a href="{{ route('contactlists.edit', $contactlist->id) }}">編集</a></td>
						<td>{{ $contactlist->id }}</td>
						<td>{{ $contactlist->name }}</td>
						<td>{{ $contactlist->person }}</td>
					    <td>{{ $contactlist->tel }}</td>
					    <td>{{ $contactlist->email }}</td>
					    <td>{{ $contactlist->memo }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>