			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>コンタクト先名</th>
						<th>メモ作成日</th>
						<th>受人</th>
						<th>内容</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($contacts as $contact)
					<tr>
						<td>
							<a href="{{ route('contactlists.editcontact', $contact->id) }}">編集</a></td>
						<td>{{ $contact->id }}</td>
						<td>{{ $contact->contactlist->name }}</td>
						<td>{{ $contact->created_at }}</td>
						<td>{{ $contact->admin->name }}</td>
						<td>{{ $contact->body }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>