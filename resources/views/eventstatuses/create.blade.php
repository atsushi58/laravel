@extends('layouts.default')

@section('title', '新規イベントステータス')

@section('content')
<h1>新規イベントステータス</h1>
<form method="post" action="{{ url('/eventstatuses') }}">
{{ csrf_field() }}
<div class="jumbotron">
  <div class="form-horizontal">
    <div class="form-group">
      <label for="name" class="col-xs-2 control-label">イベントステータス名</label>
        <div class="col-xs-4">
          <input type="name" name="name" placeholder="イベントステータス名">
        </div>
    </div>
  <p>
    <button class="btn btn-default" type="submit">保存</button>
  </p>
</form>
@endsection