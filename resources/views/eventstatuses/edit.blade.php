@extends('layouts.default')

@section('title', 'イベントステータスの編集')

@section('content')
<h1>イベントステータスの編集</h1>
<form method="post" action="{{ url('/eventstatuses', $eventstatus->id) }}">
{{ csrf_field() }}
{{ method_field('patch') }}
<div class="jumbotron">
  <div class="form-horizontal">
    <div class="form-group">
      <label for="name" class="col-xs-2 control-label">コース名</label>
        <div class="col-xs-4">
          <input type="name" name="name" placeholder="コース名" value="{{ old('name', $eventstatus->name) }}">
        </div>
    </div>
  <p>
    <button class="btn btn-default" type="submit" value="update">保存</button>
  </p>
</form>
@endsection