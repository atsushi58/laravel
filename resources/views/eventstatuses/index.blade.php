@extends('layouts.default')

@section('title', 'イベントステータス')

@section('content')

<h1>イベントステータス</h1>

<a href="{{ url('/eventstatuses/create') }}"><button type="button" class="btn btn-default navbar-btn">新規イベントステータス</button></a>


<table class="table table-hover table-striped">
	<thead>
	<tr>
		<th>アクション</th>
		<th>イベントステータス</th>
	</tr>
	</thead>
	<tbody>
	@forelse ($eventstatuses as $eventstatus)
	<tr>
		<td><a href="{{ url('/eventstatuses', $eventstatus->id) }}">詳細</a> | 
		    <a href="{{ action('EventstatusesController@edit', $eventstatus->id) }}">編集</a> | 
		    <form action="{{ action('EventstatusesController@destroy', $eventstatus->id) }}" id="form_{{ $eventstatus->id }}" method="post" style="display:inline">
		    {{ csrf_field() }}
    		{{ method_field('delete') }}
      		<a href="#" data-id="{{ $eventstatus->id }}" onclick="deleteEventstatus(this);">削除</a></form></td>
      	<td>{{ $eventstatus->name }}</td>
	@empty

	@endforelse
	</tbody>
</table>
<script>
function deleteEventstatus(e) {
  'use strict';

  if (confirm('本当に削除しますか？')) {
    document.getElementById('form_' + e.dataset.id).submit();
  }
}
</script>


@endsection