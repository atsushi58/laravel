@extends('layouts.app')

@section('title', 'ガイド追加')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>ガイド追加</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['yakuguides.store'], 'method' => 'post', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('name', '名前')}}</div>
            <div class="col-md-4">{{ Form::input('name', 'name', null, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('kana', 'カナ')}}</div>
            <div class="col-md-4">{{ Form::input('kana', 'kana', null, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('tel', '電話番号')}}</div>
            <div class="col-md-4">{{ Form::input('tel', 'tel', null, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('fax', 'FAX')}}</div>
            <div class="col-md-4">{{ Form::input('fax', 'fax', null, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('email', 'メール')}}</div>
            <div class="col-md-4">{{ Form::input('email', 'email', null, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('address', '住所')}}</div>
            <div class="col-md-4">{{ Form::input('address', 'address', null, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('bizname', '屋号')}}</div>
            <div class="col-md-4">{{ Form::input('bizname', 'bizname', null, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('web', 'HP')}}</div>
            <div class="col-md-4">{{ Form::input('web', 'web', null, ['class' => 'form-control']) }}</div>
        </div>
            <div class="col-md-2">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection