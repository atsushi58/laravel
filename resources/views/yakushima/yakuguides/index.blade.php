@extends('layouts.app')

@section('title', 'ガイド')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>ガイド</h3></div>
		<div class="panel-body">
			<a href="{{ route('yakuguides.create') }}"><button type="button" class="btn btn-default navbar-btn">ガイド追加</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>ガイド名</th>
						<th>カナ</th>
						<th>電話番号</th>
						<th>FAX</th>
						<th>email</th>
						<th>住所</th>
						<th>屋号</th>
						<th>HP</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($yakuguides as $yakuguide)
					<tr>
						<td>
							<a href="{{ route('yakuguides.edit', $yakuguide->id) }}">編集</a></td>
						<td>{{ $yakuguide->id }}</td>
					    <td>{{ $yakuguide->name }}</td>
					    <td>{{ $yakuguide->kana }}</td>
					    <td>{{ $yakuguide->tel }}</td>
					    <td>{{ $yakuguide->fax }}</td>
					    <td>{{ $yakuguide->email }}</td>
					    <td>{{ $yakuguide->address }}</td>
					    <td>{{ $yakuguide->bizname }}</td>
					    <td>{{ $yakuguide->web }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection