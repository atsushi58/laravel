@extends('layouts.app')

@section('title', 'ガイド稼働表')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>{{ $yakuguide->name }}ガイド稼働表 / {{ $year }}年{{ $month }}月</h3></div>
		<div class="panel-body">
			<a href="{{ route('yakuguides.createschedule', $yakuguide->id) }}"><button type="button" class="btn btn-default navbar-btn">ガイドスケジュール追加</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>月日</th>
						<th>スケジュール名</th>
						<th>メモ</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($yakuguideschedules as $yakuguideschedule)
					<tr>
						<td><a href="{{ route('yakuguides.editschedule', $yakuguideschedule->id) }}">編集</a></td>
						<td>{{ $yakuguideschedule->day }}</td>
						<td>{{ $yakuguideschedule->yakuguidescheduletype->name }}</td>
						<td>{{ $yakuguideschedule->memo }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection