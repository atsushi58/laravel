@extends('layouts.app')

@section('title', 'ガイド稼働表')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>ガイド稼働表</h3></div>
		<div class="panel-body">
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th></th>
						<th>id</th>
						<th>ガイド名</th>
						<th>{{ $days[1] }}</th>
						<th>{{ $days[2] }}</th>
						<th>{{ $days[3] }}</th>
						<th>{{ $days[4] }}</th>
						<th>{{ $days[5] }}</th>
						<th>{{ $days[6] }}</th>
						<th>{{ $days[7] }}</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($yakuguides as $yakuguide)
					<tr>
						<td><a href="{{ route('yakuguides.worklistup', $yakuguide->id) }}">上へ</a></td>
						<td>{{ $yakuguide->id }}</td>
					    <td><a href="{{ route('yakuguide.worklist', [$yakuguide->id, $year, $month]) }}">{{ $yakuguide->name }}</a></td>
					    @for($i=1;$i< 8 ;$i++)
					    <td>@if(null !== $yakuguide->yakuguideschedules->where('day', '=', $days[$i])->first()){{ $yakuguide->yakuguideschedules->where('day', '=', $days[$i])->first()->Workname or ''}}@else<a href="{{ route('yakuguides.createscheduledate', [$yakuguide->id, $days[$i]]) }}">______</a> @endif</td>
					    @endfor
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection