@extends('layouts.app')

@section('title', 'ガイド編集')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>{{ $yakuguideschedule->yakuguide->name }}ガイドスケジュール編集</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['yakuguides.updateschedule', $yakuguideschedule->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('day', '月日')}}</div>
            <div class="col-md-4">{{ Form::date('day', $yakuguideschedule->day, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('yakuguidescheduletype_id', '予定')}}</div>
            <div class="col-md-4">{{ Form::select('yakuguidescheduletype_id', $yakuguidescheduletypes, $yakuguideschedule->yakuguidescheduletype_id, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('memo', 'メモ')}}</div>
            <div class="col-md-10">{{ Form::text('memo', $yakuguideschedule->memo, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection