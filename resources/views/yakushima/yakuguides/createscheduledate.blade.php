@extends('layouts.app')

@section('title', 'ガイド追加')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>{{ $yakuguide->name }}ガイドスケジュール追加</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['yakuguides.storeschedule'], 'method' => 'post', 'class' => 'form-horizontal']) }}
        {{ Form::hidden('yakuguide_id', $yakuguide->id) }}
        {{ Form::hidden('day', $date)}}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('yakuguidescheduletype_id', '予定')}}</div>
            <div class="col-md-4">{{ Form::select('yakuguidescheduletype_id', $yakuguidescheduletypes, null, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('memo', 'メモ')}}</div>
            <div class="col-md-4">{{ Form::text('memo', null, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection