@extends('layouts.app')

@section('title', '宿手配ステータス')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>宿手配ステータス</h3></div>
		<div class="panel-body">
			<a href="{{ route('yakuhostelarrangestatuses.create') }}"><button type="button" class="btn btn-default navbar-btn">宿手配ステータス</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>ステータス名</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($yakuhostelarrangestatuses as $yakuhostelarrangestatus)
					<tr>
						<td>
							<a href="{{ route('yakuhostelarrangestatuses.edit', $yakuhostelarrangestatus->id) }}">編集</a></td>
						<td>{{ $yakuhostelarrangestatus->id }}</td>
					    <td>{{ $yakuhostelarrangestatus->name }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection