@extends('layouts.app')

@section('title', '屋久島コースタイプ')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>屋久島コースタイプ</h3></div>
		<div class="panel-body">
			<a href="{{ route('yakucoursetypes.create') }}"><button type="button" class="btn btn-default navbar-btn">屋久島コースタイプ追加</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>屋久島コースタイプ</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($yakucoursetypes as $yakucoursetype)
					<tr>
						<td>
							<a href="{{ route('yakucoursetypes.edit', $yakucoursetype->id) }}">編集</a></td>
						<td>{{ $yakucoursetype->id }}</td>
					    <td>{{ $yakucoursetype->name }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection