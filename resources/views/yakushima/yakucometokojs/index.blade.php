@extends('layouts.app')

@section('title', '往路1')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>往路1</h3></div>
		<div class="panel-body">
			<a href="{{ route('yakucometokojs.create') }}"><button type="button" class="btn btn-default navbar-btn">往路1追加</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>往路1名</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($yakucometokojs as $yakucometokoj)
					<tr>
						<td>
							<a href="{{ route('yakucometokojs.edit', $yakucometokoj->id) }}">編集</a></td>
						<td>{{ $yakucometokoj->id }}</td>
					    <td>{{ $yakucometokoj->name }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection