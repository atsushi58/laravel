@extends('layouts.app')

@section('title', 'ガイド予定タイプ追加')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>ガイド予定タイプ追加</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['yakuguidescheduletypes.store'], 'method' => 'post', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('name', '予定タイプ名')}}</div>
            <div class="col-md-4">{{ Form::input('name', 'name', null, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection