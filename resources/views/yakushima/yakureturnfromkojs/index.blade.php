@extends('layouts.app')

@section('title', '復路2')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>復路2</h3></div>
		<div class="panel-body">
			<a href="{{ route('yakureturnfromkojs.create') }}"><button type="button" class="btn btn-default navbar-btn">復路2追加</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>復路2名</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($yakureturnfromkojs as $yakureturnfromkoj)
					<tr>
						<td>
							<a href="{{ route('yakureturnfromkojs.edit', $yakureturnfromkoj->id) }}">編集</a></td>
						<td>{{ $yakureturnfromkoj->id }}</td>
					    <td>{{ $yakureturnfromkoj->name }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection