@extends('layouts.app')

@section('title', '屋久島宿マスタ')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>屋久島宿マスタ</h3></div>
		<div class="panel-body">
			<a href="{{ route('yakuhostels.create') }}"><button type="button" class="btn btn-default navbar-btn">屋久島宿マスタ追加</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>屋久島宿名</th>
						<th>屋久島宿カナ</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($yakuhostels as $yakuhostel)
					<tr>
						<td>
							<a href="{{ route('yakuhostels.edit', $yakuhostel->id) }}">編集</a></td>
						<td>{{ $yakuhostel->id }}</td>
					    <td>{{ $yakuhostel->name }}</td>
					    <td>{{ $yakuhostel->kana }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection