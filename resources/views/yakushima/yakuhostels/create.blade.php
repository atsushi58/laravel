@extends('layouts.app')

@section('title', '屋久島宿追加')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>屋久島宿追加</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['yakuhostels.store'], 'method' => 'post', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('name', '屋久島宿名')}}</div>
            <div class="col-md-4">{{ Form::input('name', 'name', null, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('kana', '屋久島宿カナ')}}</div>
            <div class="col-md-4">{{ Form::input('kana', 'kana', null, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection