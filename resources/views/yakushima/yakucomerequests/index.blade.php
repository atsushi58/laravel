@extends('layouts.app')

@section('title', '往路希望便')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>往路希望便</h3></div>
		<div class="panel-body">
			<a href="{{ route('yakucomerequests.create') }}"><button type="button" class="btn btn-default navbar-btn">往路希望便追加</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>希望便名</th>
						<th>往路1</th>
						<th>往路2</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($yakucomerequests as $yakucomerequest)
					<tr>
						<td>
							<a href="{{ route('yakucomerequests.edit', $yakucomerequest->id) }}">編集</a></td>
						<td>{{ $yakucomerequest->id }}</td>
					    <td>{{ $yakucomerequest->name }}</td>
					    <td>{{ $yakucomerequest->Tokoj->name }}</td>
					    <td>{{ $yakucomerequest->Tokum->name }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection