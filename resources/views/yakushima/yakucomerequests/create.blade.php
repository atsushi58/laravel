@extends('layouts.app')

@section('title', '往路希望便追加')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>往路希望便追加</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['yakucomerequests.store'], 'method' => 'post', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('name', '往路希望便名')}}</div>
            <div class="col-md-4">{{ Form::input('name', 'name', null, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('defaultyakushimacometokoj', '既定往路1')}}</div>
            <div class="col-md-4">{{ Form::select('defaultyakushimacometokoj', $tokojs, null, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('defaultyakushimacometokum', '既定往路2')}}</div>
            <div class="col-md-4">{{ Form::select('defaultyakushimacometokum', $tokums, null, ['class' => 'form-control']) }}</div>
            <div class="col-md-6">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
        </div>
    </div>
    </div>
</div>
@endsection