@extends('layouts.app')

@section('title', 'フライト手配ステータス')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>フライト手配ステータス</h3></div>
		<div class="panel-body">
			<a href="{{ route('yakuflightarrangestatuses.create') }}"><button type="button" class="btn btn-default navbar-btn">フライト手配ステータス</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>ステータス名</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($yakuflightarrangestatuses as $yakuflightarrangestatus)
					<tr>
						<td>
							<a href="{{ route('yakuflightarrangestatuses.edit', $yakuflightarrangestatus->id) }}">編集</a></td>
						<td>{{ $yakuflightarrangestatus->id }}</td>
					    <td>{{ $yakuflightarrangestatus->name }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection