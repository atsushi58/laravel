@extends('layouts.app')

@section('title', 'フライト手配ステータス')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>フライト手配ステータス編集</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['yakuflightarrangestatuses.update', $flightarrangestatus->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('name', 'フライト手配ステータス名')}}</div>
            <div class="col-md-4">{{ Form::input('name', 'name', $flightarrangestatus->name, ['class' => 'form-control']) }}</div>
            <div class="col-md-6">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection