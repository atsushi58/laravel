@extends('layouts.app')

@section('title', '往路2')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>往路2</h3></div>
		<div class="panel-body">
			<a href="{{ route('yakucometokums.create') }}"><button type="button" class="btn btn-default navbar-btn">往路2追加</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>往路1名</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($yakucometokums as $yakucometokum)
					<tr>
						<td>
							<a href="{{ route('yakucometokums.edit', $yakucometokum->id) }}">編集</a></td>
						<td>{{ $yakucometokum->id }}</td>
					    <td>{{ $yakucometokum->name }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection