@extends('layouts.app')

@section('title', 'ガイド予定タイプ')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>ガイド予定タイプ</h3></div>
		<div class="panel-body">
			<a href="{{ route('yakuguidescheduletypes.create') }}"><button type="button" class="btn btn-default navbar-btn">ガイド予定タイプ</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>ステータス名</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($yakuguidescheduletypes as $yakuguidescheduletype)
					<tr>
						<td>
							<a href="{{ route('yakuguidescheduletypes.edit', $yakuguidescheduletype->id) }}">編集</a></td>
						<td>{{ $yakuguidescheduletype->id }}</td>
					    <td>{{ $yakuguidescheduletype->name }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection