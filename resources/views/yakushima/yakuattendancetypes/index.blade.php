@extends('layouts.app')

@section('title', '参加タイプ')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>参加タイプ</h3></div>
		<div class="panel-body">
			<a href="{{ route('yakuattendancetypes.create') }}"><button type="button" class="btn btn-default navbar-btn">参加タイプ追加</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>参加タイプ</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($yakuattendancetypes as $yakuattendancetype)
					<tr>
						<td>
							<a href="{{ route('yakuattendancetypes.edit', $yakuattendancetype->id) }}">編集</a></td>
						<td>{{ $yakuattendancetype->id }}</td>
					    <td>{{ $yakuattendancetype->name }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection