@extends('layouts.app')

@section('title', '復路希望便')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>復路希望便</h3></div>
		<div class="panel-body">
			<a href="{{ route('yakureturnrequests.create') }}"><button type="button" class="btn btn-default navbar-btn">復路希望便追加</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>希望便名</th>
						<th>復路1</th>
						<th>復路2</th>
					</tr>
				</thead>
				<tbody>
					@forelse($yakureturnrequests as $yakureturnrequest)
					<tr>
						<td><a href="{{ route('yakureturnrequests.edit', $yakureturnrequest->id) }}">編集</a></td>
						<td>{{ $yakureturnrequest->id }}</td>
					    <td>{{ $yakureturnrequest->name }}</td>
					    <td>{{ $yakureturnrequest->Fromkum->name }}</td>
					    <td>{{ $yakureturnrequest->Fromkoj->name }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection