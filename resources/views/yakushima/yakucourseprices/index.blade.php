@extends('layouts.app')

@section('title', '屋久島コース価格表')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>{{ $yakucourse->name }} / {{ $year }}年{{ $month }}月の価格表</h3></div>
		<div class="panel-body">
			@if(!$yakucourseprices)<a href="{{ route('yakucourseprices.create')}}">価格入力</a>@endif
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>屋久島コースタイプ</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($yakucourseprices as $yakucourseprice)
					<tr>
						<td>
							
						</td>
						<td>{{ $yakucourseprice->id }}</td>
					    <td>{{ $yakucourseprice->name }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection