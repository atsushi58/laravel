@extends('layouts.app')

@section('title', '屋久島コース既定価格追加')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>屋久島コース既定価格追加</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['yakucourseprices.store'], 'method' => 'post', 'class' => 'form-horizontal']) }}
        {{ Form::hidden('yakucoursepricetype_id', 2) }}
        {{ Form::hidden('yakucourse_id', $yakucourse->id) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('depdate', '出発日')}}</div>
            <div class="col-md-4">{{ Form::date('depdate', 'depdate', null, ['class' => 'form-control']) }}</div>
        </div>        
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('priceone', '1人参加価格')}}</div>
            <div class="col-md-4">{{ Form::input('priceone', 'priceone', null, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('pricetwo', '2人参加価格')}}</div>
            <div class="col-md-4">{{ Form::input('pricetwo', 'pricetwo', null, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('pricethree', '3人参加価格')}}</div>
            <div class="col-md-4">{{ Form::input('pricethree', 'pricethree', null, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('pricefour', '4人参加価格')}}</div>
            <div class="col-md-4">{{ Form::input('pricefour', 'pricefour', null, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('pricefive', '5人参加価格')}}</div>
            <div class="col-md-4">{{ Form::input('pricefive', 'pricefive', null, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
            {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection