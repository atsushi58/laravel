@extends('layouts.app')

@section('title', '屋久島コースタイプ編集')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>屋久島コースタイプ編集</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['yakucoursetypes.update', $yakucoursetype->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('name', '屋久島コースタイプ')}}</div>
            <div class="col-md-4">{{ Form::input('name', 'name', $yakucoursetype->name, ['class' => 'form-control']) }}</div>
            <div class="col-md-6">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection