@extends('layouts.app')

@section('title', '屋久島コース詳細')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>屋久島コース詳細:{{ $yakucourse->name }}の基本情報</h3></div>
		<div class="panel-body">
			<a href="{{ route('yakucourses.edit', $yakucourse->id) }}"><button type="button" class="btn btn-default navbar-btn">HP基本情報編集</button></a>
			<div class="row">
				<div class="col-md-2"><label>屋久島コースタイプ</label></div>
				<div class="col-md-4">{{ $yakucourse->yakucoursetype->name or '' }}</div>
				<div class="col-md-2"><label>コース名</label></div>
				<div class="col-md-4">{{ $yakucourse->name or '' }}</div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>コース日数</label></div>
				<div class="col-md-4">{{ $yakucourse->courselength->name }}</div>
				<div class="col-md-2"><label>出発地</label></div>
				<div class="col-md-4">{{ $yakucourse->yakudepplace->name }}</div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>wordpress ID</label></div>
				<div class="col-md-4">{{ $yakucourse->wpid or '' }}</div>
				<div class="col-md-2"><label>meta title</label></div>
				<div class="col-md-4">{{ $yakucourse->metatitle }}</div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>meta keywords(,(半角カンマ)区切り)</label></div>
				<div class="col-md-4">{{ $yakucourse->metakeywords }}</div>
				<div class="col-md-2"><label>slug</label></div>
				<div class="col-md-4">{{ $yakucourse->slug }}</div>

			</div>
			<div class="row">				
				<div class="col-md-2"><label>meta description</label></div>
				<div class="col-md-4">{{ $yakucourse->metadescription }}</div>
				<div class="col-md-2"><label>メモ</label></div>
				<div class="col-md-4">{{ $yakucourse->memo }}</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>{{ $yakucourse->name }}の価格表</h3></div>
		<div class="panel-body">
			<p>
			<a href="{{ route('yakucourseprices.createdefaultprice', $yakucourse->id)}}"><button type="button" class="btn btn-default navbar-btn">既定価格登録</button></a>
			<a href="{{ route('yakucourseprices.create30advanceprice', $yakucourse->id)}}"><button type="button" class="btn btn-default navbar-btn">30日早割価格登録</button></a>
			<a href="{{ route('yakucourseprices.create60advanceprice', $yakucourse->id)}}"><button type="button" class="btn btn-default navbar-btn">60日早割価格登録</button></a>
			<a href="{{ route('yakucourseprices.createdateprice', $yakucourse->id)}}"><button type="button" class="btn btn-default navbar-btn">特定日価格登録</button></a></p>
			<table class="table table-hover table-striped">
				<thead>
				<tr>
					<th>アクション</th>
					<th>価格タイプ</th>
					<th>出発日</th>
					<th>1人参加価格</th>
					<th>2人参加価格</th>
					<th>3人参加価格</th>
					<th>4人参加価格</th>
					<th>5人以上参加価格</th>
				</tr>
				</thead>
			@foreach($yakucourse->yakucourseprices as $yakucourseprice)
				<tbody>
				<tr>
					<td></td>
					<td>{{ $yakucourseprice->yakucoursepricetype->name }}</td>
					<td>{{ number_format($yakucourseprice->depdate) }}円</td>
					<td>{{ number_format($yakucourseprice->priceone) }}円</td>
					<td>{{ number_format($yakucourseprice->pricetwo) }}円</td>
					<td>{{ number_format($yakucourseprice->pricethree) }}円</td>
					<td>{{ number_format($yakucourseprice->pricefour) }}円</td>
					<td>{{ number_format($yakucourseprice->pricefive) }}円</td>
				</tr>
				</tbody>
			@endforeach
			</table>
		</div>
	</div>
</div>
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>{{ $yakucourse->name }}の参加者一覧</h3></div>
		<div class="panel-body">
		<a href="{{ route('yakueventattendances.create', $yakucourse->id) }}"><button type="button" class="btn btn-default navbar-btn">新規参加者登録</button></a>
		<table class="table table-hover table-striped">
			<thead>
				<tr>
					<th>アクション</th>
					<th>出発日</th>
					<th>顧客名</th>
					<th>アクティビティタイプ</th>
				</tr>
			</thead>
			<tbody>
				@foreach($yakueventattendances as $yakueventattendance)
				<tr>
					<td><a href="{{ route('yakueventattendances.show', $yakueventattendance->id) }}">詳細</a></td>
					<td>{{ $yakueventattendance->depdate }}</td>
					<td>{{ $yakueventattendance->user->name }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		</div>
	</div>
</div>
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>{{ $yakucourse->name }}の既定アクティビティ一覧</h3></div>
		<div class="panel-body">
		<a href="{{ route('yakudefaultactivities.create', $yakucourse->id) }}"><button type="button" class="btn btn-default navbar-btn">新規既定アクティビティ登録</button></a>
		<table class="table table-hover table-striped">
			<thead>
				<tr>
					<th>アクション</th>
					<th>×日目</th>
					<th>アクティビティタイプ</th>
				</tr>
			</thead>
			<tbody>
				@foreach($yakucourse->yakudefaultactivities as $yakudefaultactivity)
				<tr>
					<td class="col-md-2"><a href="{{ route('yakudefaultactivities.edit', $yakudefaultactivity->id) }}">編集</a> | 
					    <form action="{{ route('yakudefaultactivities.destroy', $yakudefaultactivity->id) }}" id="destroyyakudefaultactivityform_{{ $yakudefaultactivity->id }}" method="post" style="display:inline">
					    {{ csrf_field() }}
			    		{{ method_field('delete') }}
			      		<a href="#" data-id="{{ $yakudefaultactivity->id }}" onclick="deleteyakudefaultactivity(this);">削除</a></form></td>
					<td>{{ $yakudefaultactivity->day }}日目</td>
					<td>{{ $yakudefaultactivity->yakuactivitytype->name }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		</div>
	</div>
</div>
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>{{ $yakucourse->name }}の既定宿手配一覧</h3></div>
		<div class="panel-body">
		<a href="{{ route('yakudefaulthostelarranges.create', $yakucourse->id) }}"><button type="button" class="btn btn-default navbar-btn">新規既定宿手配登録</button></a>
		<table class="table table-hover table-striped">
			<thead>
				<tr>
					<th>アクション</th>
					<th>×日目</th>
					<th>宿タイプ</th>
				</tr>
			</thead>
			<tbody>
				@foreach($yakucourse->yakudefaulthostelarranges as $yakudefaulthostelarrange)
				<tr>
					<td class="col-md-2"><a href="{{ route('yakudefaulthostelarranges.edit', $yakudefaulthostelarrange->id) }}">編集</a> | 
					    <form action="{{ route('yakudefaulthostelarranges.destroy', $yakudefaulthostelarrange->id) }}" id="destroyyakudefaulthostelarrangeform_{{ $yakudefaulthostelarrange->id }}" method="post" style="display:inline">
					    {{ csrf_field() }}
			    		{{ method_field('delete') }}
			      		<a href="#" data-id="{{ $yakudefaulthostelarrange->id }}" onclick="deleteyakudefaulthostelarrange(this);">削除</a></form></td>
					<td>{{ $yakudefaulthostelarrange->day }}日目</td>
					<td>{{ $yakudefaulthostelarrange->yakuhosteltype->name }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		</div>
	</div>
</div>
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>選択可能オプションアクティビティ</h3></div>
		<div class="panel-body">
		<a href="{{ route('yakucourses.createoptionactivity', $yakucourse->id) }}"><button type="button" class="btn btn-default navbar-btn">新規オプションアクティビティ登録</button></a>
		<table class="table table-hover table-striped">
			<thead>
				<tr>
					<th>アクション</th>
					<th>アクティビティ名</th>
					<th>代金</th>
				</tr>
			</thead>
			<tbody>
				@foreach($yakucourse->yakuactivitytypes as $yakuactivitytype)
				<tr>
					<td><form action="{{ route('yakucourses.detachoptionactivity', [$yakucourse->id, $yakuactivitytype->id]) }}" id="detachoptionactivityform_{{ $yakucourse->id }}" method="post" style="display:inline">
						{{ csrf_field() }}
			    		{{ method_field('patch') }}
			  		<a href="#" data-id="{{ $yakucourse->id }}" onclick="detachOptionactivity(this);">オプション解除</a></form></td>
					<td>{{ $yakuactivitytype->name }}</td>
					<td>{{ number_format($yakuactivitytype->optionprice) }}円</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		</div>
	</div>
</div>
<script>
function deleteyakudefaultactivity(e) {
  'use strict';

  if (confirm('本当に削除しますか？')) {
    document.getElementById('destroyyakudefaultactivityform_' + e.dataset.id).submit();
  }
}
function detachOptionactivity(e) {
  'use strict';
  if (confirm('オプション解除しますか？')) {
    document.getElementById('detachoptionactivityform_' + e.dataset.id).submit();
  }
}
</script>

@endsection