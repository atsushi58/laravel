@extends('layouts.app')

@section('title', '屋久島コースマスタ')

@section('content')

<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>屋久島コースマスタ</h3></div>
		<div class="panel-body">
			<form class="form-horizontal">
			  <div class="form-group">
			    <label for="name" class="control-label col-md-1">ビュー: </label>
			    <div class="col-md-3">
					<select onChange="top.location.href=value" class="form-control">
						<option value="#"></option>
					</select>
				</div>
			  </div>
			</form>
			<a href="{{ route('yakucourses.create') }}"><button type="button" class="btn btn-default navbar-btn">新規コース設定</button></a>
			<table class = "datatable display" cellspacing="0" width="100%">
				<thead>
				<tr>
					<th>アクション</th>
					<th>コース名</th>
					<th>出発地</th>
					<th>コースタイプ</th>
					<th>日数</th>
					<th>メモ</th>
				</tr>
				</thead>
				<tbody>
				@forelse ($courses as $course)
				<tr>
					<td><a href="{{ route('yakucourses.show', $course->id) }}">詳細</a> | 
					    <a href="{{ route('yakucourses.edit', $course->id) }}">編集</a></td>
					<td>{{ $course->name }}</td>
					<td>{{ $course->yakucoursetype->name }}</td>
					<td>{{ $course->yakudepplace->name }}</td>
					<td>{{ $course->courselength->name }}</td>
					<td>{{ $course->memo }}</td>
				</tr>
				@empty

				@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>



@endsection