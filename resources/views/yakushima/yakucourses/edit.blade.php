@extends('layouts.app')

@section('title', '屋久島コース編集')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>屋久島コース編集</h3></div>
        <div class="panel-body">
        {{ Form::open(['route' => ['yakucourses.update', $yakucourse->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('yakucoursetype_id', 'コースタイプ', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::select('yakucoursetype_id', $yakucoursetypes, $yakucourse->yakucoursetype_id, ['class' => 'form-control']) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('name', 'コース名', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('name', 'name', $yakucourse->name, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('courselength_id', 'コース期間', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::select('courselength_id', $courselengths, $yakucourse->courselength, ['class' => 'form-control']) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('depplace_id', '出発地', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::select('depplace_id', $depplaces, $yakucourse->depplace_id, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('memo', 'メモ', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::textarea('memo', $yakucourse->memo, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection