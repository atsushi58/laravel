@extends('layouts.app')

@section('title', '新規オプションアクティビティ')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>新規オプションアクティビティ</h3></div>
        <div class="panel-body">
        {{ Form::open(['route' => 'yakucourses.storeoptionactivity', 'method' => 'post', 'class' => 'form-horizontal']) }}
        {{ Form::hidden('yakucourse_id', $yakucourse->id) }}
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('yakuactivitytype_id', 'アクティビティ名', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::select('yakuactivitytype_id', $yakuactivitytypes, null, ['class' => 'form-control']) }}
                </div>
                <div class="col-md-2">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection