@extends('layouts.app')

@section('title', '新規屋久島コース')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>新規屋久島コース</h3></div>
        <div class="panel-body">
        {{ Form::open(['route' => 'yakucourses.store', 'method' => 'post', 'class' => 'form-horizontal']) }}
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('yakucoursetype_id', 'コースタイプ', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::select('yakucoursetype_id', $yakucoursetypes, null, ['class' => 'form-control']) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('name', 'コース名', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('name', 'name', null, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('courselength_id', 'コース期間', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::select('courselength_id', $courselengths, null, ['class' => 'form-control']) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('depplace_id', '出発地', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::select('yakudepplace_id', $yakudepplaces, null, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('memo', 'メモ', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::textarea('memo', null, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection