@extends('layouts.app')

@section('title', '既定宿手配追加')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>既定宿手配追加</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['yakudefaulthostelarranges.update', $yakudefaulthostelarrange->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('day', 'XX日目')}}</div>
            <div class="col-md-4">{{ Form::input('number', 'day', $yakudefaulthostelarrange->day, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('yakuhosteltype_id', '宿タイプ')}}</div>
            <div class="col-md-4">{{ Form::select('yakuhosteltype_id', $yakuhosteltypes, $yakudefaulthostelarrange->yakuhosteltype_id, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection