@extends('layouts.app')

@section('title', '船手配ステータス')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>船手配ステータス</h3></div>
		<div class="panel-body">
			<a href="{{ route('yakushiparrangestatuses.create') }}"><button type="button" class="btn btn-default navbar-btn">船手配ステータス</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>ステータス名</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($yakushiparrangestatuses as $yakushiparrangestatus)
					<tr>
						<td>
							<a href="{{ route('yakushiparrangestatuses.edit', $yakushiparrangestatus->id) }}">編集</a></td>
						<td>{{ $yakushiparrangestatus->id }}</td>
					    <td>{{ $yakushiparrangestatus->name }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection