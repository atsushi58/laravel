@extends('layouts.app')

@section('title', 'アクティビティ追加')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>アクティビティ追加</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['yakuactivitytypes.store'], 'method' => 'post', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('name', 'アクティビティ名')}}</div>
            <div class="col-md-4">{{ Form::input('name', 'name', null, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('limit', 'ガイドレシオ')}}</div>
            <div class="col-md-4">{{ Form::select('limit', ['1' => '1人', '2' => '2人', '3' => '3人', '4' => '4人', '5' => '5人', '6' => '6人', '7' => '7人'], null, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('length', '日数')}}</div>
            <div class="col-md-4">{{ Form::select('length', ['1' => '1日/半日', '2' => '2日'], null, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('optionprice', 'オプション代金')}}</div>
            <div class="col-md-4">{{ Form::input('optionprice', 'optionprice', null, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('guidecostone', 'ガイド料(1人)')}}</div>
            <div class="col-md-4">{{ Form::input('guidecostone', 'guidecostone', null, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('guidecosttwo', 'ガイド料(2人)')}}</div>
            <div class="col-md-4">{{ Form::input('guidecosttwo', 'guidecosttwo', null, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('guidecostthree', 'ガイド料(3人)')}}</div>
            <div class="col-md-4">{{ Form::input('guidecostthree', 'guidecostthree', null, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('guidecostfour', 'ガイド料(4人)')}}</div>
            <div class="col-md-4">{{ Form::input('guidecostfour', 'guidecostfour', null, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('guidecostfive', 'ガイド料(5人)')}}</div>
            <div class="col-md-4">{{ Form::input('guidecostfive', 'guidecostfive', null, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('guidecostsix', 'ガイド料(6人)')}}</div>
            <div class="col-md-4">{{ Form::input('guidecostsix', 'guidecostsix', null, ['class' => 'form-control']) }}</div>        
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('guidecostseven', 'ガイド料(7人)')}}</div>
            <div class="col-md-4">{{ Form::input('guidecostseven', 'guidecostseven', null, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection