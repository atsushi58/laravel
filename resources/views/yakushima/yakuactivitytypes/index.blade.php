@extends('layouts.app')

@section('title', 'アクティビティマスタ')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>アクティビティマスタ</h3></div>
		<div class="panel-body">
			<a href="{{ route('yakuactivitytypes.create') }}"><button type="button" class="btn btn-default navbar-btn">アクティビティマスタ追加</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>アクティビティ名</th>
						<th>ガイドレシオ</th>
						<th>日数</th>
						<th>オプション代金</th>
						<th>ガイド料(1人)</th>
						<th>ガイド料(2人)</th>
						<th>ガイド料(3人)</th>
						<th>ガイド料(4人)</th>
						<th>ガイド料(5人)</th>
						<th>ガイド料(6人)</th>
						<th>ガイド料(7人)</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($yakuactivitytypes as $yakuactivitytype)
					<tr>
						<td>
							<a href="{{ route('yakuactivitytypes.edit', $yakuactivitytype->id) }}">編集</a></td>
						<td>{{ $yakuactivitytype->id }}</td>
					    <td>{{ $yakuactivitytype->name }}</td>
					    <td>{{ $yakuactivitytype->limit }}</td>
					    <td>{{ $yakuactivitytype->length}}</td>
					    <td>{{ number_format($yakuactivitytype->optionprice) }}円</td>
					    <td>{{ number_format($yakuactivitytype->guidecostone) }}円</td>
					    <td>{{ number_format($yakuactivitytype->guidecosttwo) }}円</td>
					    <td>{{ number_format($yakuactivitytype->guidecostthree) }}円</td>
					    <td>{{ number_format($yakuactivitytype->guidecostfour) }}円</td>
					    <td>{{ number_format($yakuactivitytype->guidecostfive) }}円</td>
					    <td>{{ number_format($yakuactivitytype->guidecostsix) }}円</td>
					    <td>{{ number_format($yakuactivitytype->guidecostseven) }}円</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection