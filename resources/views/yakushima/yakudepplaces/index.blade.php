@extends('layouts.app')

@section('title', '屋久島出発地マスタ')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>屋久島出発地マスタ</h3></div>
		<div class="panel-body">
			<a href="{{ route('yakudepplaces.create') }}"><button type="button" class="btn btn-default navbar-btn">屋久島出発地マスタ追加</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>屋久島出発地名</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($yakudepplaces as $yakudepplace)
					<tr>
						<td>
							<a href="{{ route('yakudepplaces.edit', $yakudepplace->id) }}">編集</a></td>
						<td>{{ $yakudepplace->id }}</td>
					    <td>{{ $yakudepplace->name }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection