@extends('layouts.app')

@section('title', 'ガイド設定')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>{{ $yakuactivity->depdate}} / {{ $yakuactivity->yakuactivitytype->name }}のガイド設定</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['yakuactivities.guidestore'], 'method' => 'post', 'class' => 'form-horizontal']) }}
        {{ Form::hidden('yakuactivity_id', $yakuactivity->id) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('yakuguides_id', '屋久島ガイド')}}</div>
            <div class="col-md-4">{{ Form::select('yakuguides_id', $yakuguides, null, ['class' => 'form-control']) }}</div>
        </div>
            <div class="col-md-2">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection