@extends('layouts.app')

@section('title', 'アクティビティ')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>アクティビティ</h3></div>
		<div class="panel-body">
			<a href="{{ route('yakuactivities.create') }}"><button type="button" class="btn btn-default navbar-btn">アクティビティ追加</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>実施日</th>
						<th>アクティビティ名</th>
						<th>代表者名</th>
						<th>ガイド</th>
						<th>参加人数</th>
						<th>アクティビティメモ</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($yakuactivities as $yakuactivity)
					<tr>
						<td>
							<a href="{{ route('yakuactivities.show', $yakuactivity->id)}}">詳細</a> | <a href="{{ route('yakuactivities.edit', $yakuactivity->id) }}">編集</a> | <a href="{{ route('yakuactivities.guidefix', $yakuactivity->id) }}"> ガイド確定</a></td>
						<td>{{ $yakuactivity->id }}</td>
						<td>{{ $yakuactivity->depdate }}</td>
						<td>{{ $yakuactivity->yakuactivitytype->name }}</td>
					    <td>@forelse($yakuactivity->yakueventattendances as $yakueventattendance){{ $yakueventattendance->user->name }} @empty @endforelse</td>
					    <td>@forelse($yakuactivity->yakuguides as $yakuguide){{ $yakuguide->name }} @empty @endforelse</td>
					    <td>{{ $yakuactivity->Numattendants or '' }}</td>
					    <td>{{ $yakuactivity->memo }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection