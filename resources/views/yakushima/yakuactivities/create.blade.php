@extends('layouts.app')

@section('title', 'アクティビティ追加')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>アクティビティ追加</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['yakuactivities.store'], 'method' => 'post', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('yakuactivitytype_id', 'アクティビティ')}}</div>
            <div class="col-md-4">{{ Form::select('yakuactivitytype_id', $yakuactivitytypes, null, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('depdate', '実施日')}}</div>
            <div class="col-md-4">{{ Form::date('depdate', 'depdate', ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('yakuguides', 'ガイド')}}</div>
            <div class="col-md-4">{{ Form::select('yakuguidelist[]', $yakuguides, null, ['class' => 'form-control', 'multiple'=>'multiple']) }}</div>
            <div class="col-md-2">{{ Form::label('memo', 'アクティビティメモ')}}</div>
            <div class="col-md-4">{{ Form::text('memo', null, ['class' => 'form-control']) }}</div>
        </div>
            <div class="col-md-2">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection