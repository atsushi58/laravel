@extends('layouts.app')

@section('title', '屋久島アクティビティ')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>屋久島ガイドアクティビティ: {{ $yakuactivity->depdate}}/{{ $yakuactivity->yakuactivitytype->name }}</h3></div>
		<div class="panel-body">
			<a href="{{ route('yakuactivities.edit', $yakuactivity->id) }}"><button type="button" class="btn btn-default navbar-btn">アクティビティ基本情報編集</button></a>
			<div class="row">				
				<div class="col-md-2"><label>ガイド</label></div>
				<div class="col-md-4">@forelse($yakuactivity->yakuguides as $yakuguide){{ $yakuguide->name }} @empty @endforelse</div>
				<div class="col-md-2"><label>ガイド料</label></div>
				<div class="col-md-4">{{ $yakuactivity->Guidecost }}</div>
			</div>
			<div class="row">				
				<div class="col-md-2"><label>出発日</label></div>
				<div class="col-md-4">{{ $yakuactivity->depdate }}</div>
				<div class="col-md-2"><label>ガイド料</label></div>
				<div class="col-md-4">{{ $yakuactivity->Guidecost }}</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>参加者一覧</h3></div>
		<div class="panel-body">
		<table class="table table-hover table-striped">
			<thead>
				<tr>
					<th>アクション</th>
					<th>顧客名</th>
					<th>年齢</th>
				</tr>
			</thead>
			<tbody>
				@foreach($yakuactivity->yakueventattendances as $yakueventattendance)
				<tr>
					<td></td>
					<td>{{ $yakueventattendance->user->name }}</td>
					<td></td>
				</tr>
				@endforeach
			</tbody>
		</table>
		</div>
	</div>
</div>
<script>
function deleteyakudefaultactivity(e) {
  'use strict';

  if (confirm('本当に削除しますか？')) {
    document.getElementById('destroyyakudefaultactivityform_' + e.dataset.id).submit();
  }
}
</script>

@endsection