@extends('layouts.app')

@section('title', '屋久島宿タイプマスタ')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>屋久島宿タイプマスタ</h3></div>
		<div class="panel-body">
			<a href="{{ route('yakuhosteltypes.create') }}"><button type="button" class="btn btn-default navbar-btn">屋久島宿タイプマスタ追加</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>屋久島宿タイプ名</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($yakuhosteltypes as $yakuhosteltype)
					<tr>
						<td>
							<a href="{{ route('yakuhosteltypes.edit', $yakuhosteltype->id) }}">編集</a></td>
						<td>{{ $yakuhosteltype->id }}</td>
					    <td>{{ $yakuhosteltype->name }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection