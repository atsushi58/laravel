@extends('layouts.app')

@section('title', '既定アクティビティ追加')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>既定アクティビティ追加</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['yakudefaultactivities.store'], 'method' => 'post', 'class' => 'form-horizontal']) }}
        {{ Form::hidden('yakucourse_id', $yakucourse->id)}}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('day', 'XX日目')}}</div>
            <div class="col-md-4">{{ Form::input('number', 'day', null, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('yakuactivitytype_id', 'アクティビティ')}}</div>
            <div class="col-md-4">{{ Form::select('yakuactivitytype_id', $yakuactivitytypes, null, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection