@extends('layouts.app')

@section('title', '屋久島既定アクティビティ編集')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>屋久島既定アクティビティ編集</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['yakudefaultactivities.update', $yakudefaultactivity->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('day', 'XX日目')}}</div>
            <div class="col-md-4">{{ Form::input('number', 'day', $yakudefaultactivity->day, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('yakuactivitype_id', 'アクティビティ')}}</div>
            <div class="col-md-4">{{ Form::select('yakuactivitytype_id', $yakuactivitytypes, $yakudefaultactivity->yakuactivitytype_id, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection