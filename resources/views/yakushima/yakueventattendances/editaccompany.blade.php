@extends('layouts.app')

@section('title', '同行者の編集')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>同行者の編集</h3></div>
        <div class="panel-body">
            {{ Form::open(['route' => ['yakueventattendances.updateaccompany', $accompany->id], 'method' => 'patch']) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('name', '名前', ['class' => 'form-control-label']) }}</div>
            <div class="col-md-4">{{ Form::input('name', 'name', $accompany->name, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('agebracket', '年代', ['class' => 'form-control-label']) }}</div>
            <div class="col-md-4">{{ Form::select('agebracket', ['10代'=>'10代', '20代'=>'20代', '30代'=>'30代', '40代'=>'40代', '50代'=>'50代', '60代'=>'60代', '70代以上'=>'70代以上'], $accompany->agebracket, ['class' => 'form-control'])}}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('sex', '性別', ['class' => 'form-control-label']) }}</div>
            <div class="col-md-4">{{ Form::select('sex', ['女性' => '女性', '男性' => '男性'], $accompany->sex, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
            {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
            {{ Form::close()}}
        </div>
    </div>
</div>


@endsection