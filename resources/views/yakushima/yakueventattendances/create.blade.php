@extends('layouts.app')

@section('title', '新規屋久島ツアー参加')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>新規屋久島ツアー参加</h3></div>
        <div class="panel-body">
        {{ Form::open(['route' => 'yakueventattendances.store', 'method' => 'post', 'class' => 'form-horizontal']) }}
        {{ Form::hidden('yakucourse_id', $yakucourse->id )}}
        {{ Form::hidden('eventattendancestatus_id', 1 )}}
        {{ Form::hidden('eventattendancepaymentstatus_id', 1 )}}
        {{ Form::hidden('yakucourse_id', $yakucourse->id )}}
        
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('user_id', '顧客', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::select('user_id', $clients, null, ['class' => 'form-control']) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('depdate', '入島日', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::date('depdate', null, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                {{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection