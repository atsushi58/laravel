@extends('layouts.app')

@section('title', '屋久島ツアー参加')

@section('content')

<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>屋久島ツアー参加</h3></div>
		<div class="panel-body">
			<form class="form-horizontal">
			  <div class="form-group">
			    <label for="name" class="control-label col-md-1">ビュー: </label>
			    <div class="col-md-3">
					<select onChange="top.location.href=value" class="form-control">
						<option value="#"></option>
					</select>
				</div>
			  </div>
			</form>
			<table class = "datatable display" cellspacing="0" width="100%">
				<thead>
				<tr>
					<th>アクション</th>
					<th>顧客名</th>
					<th>屋久島コース</th>
					<th>入湯日</th>
					<th>出島日</th>
					
				</tr>
				</thead>
				<tbody>
				@forelse ($eventattendances as $eventattendance)
				<tr>
					<td><a href="{{ route('yakueventattendances.show', $eventattendance->id) }}">詳細</a> | 
					    <a href="{{ route('yakueventattendances.edit', $eventattendance->id) }}">編集</a></td>
					   <td>{{ $eventattendance->user->name }}</td>
					   <td>{{ $eventattendance->yakucourse->name }}</td>
				</tr>
				@empty

				@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection