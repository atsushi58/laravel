@extends('layouts.app')

@section('title', '屋久島ツアー参加詳細')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>屋久島ツアー参加詳細:代表者{{ $yakueventattendance->user->name }}の基本情報</h3></div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-2"><label>屋久島コースタイプ</label></div>
					<div class="col-md-4">{{ $yakueventattendance->yakucourse->yakucoursetype->name or '' }}</div>
					<div class="col-md-2"><label>出発日</label></div>
					<div class="col-md-4">{{ $yakueventattendance->depdate }}</div>
				</div>
				<div class="row">
					<div class="col-md-2"><label>ツアー参加ステータス</label></div>
					<div class="col-md-4">{{ $yakueventattendance->eventattendancestatus->name or '' }}</div>
					<div class="col-md-2"><label>支払いステータス</label></div>
					<div class="col-md-4">{{ $yakueventattendance->eventattendancepaymentstatus->name or '' }}</div>
				</div>
				<div class="row">
					<div class="col-md-2"><label>レンタル</label></div>
					<div class="col-md-4">{{ $yakueventattendance->rental or '' }}</div>
					<div class="col-md-2"><label>メモ</label></div>
					<div class="col-md-4">{{ $yakueventattendance->memo or '' }}</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>同行者一覧</h3></div>
		<div class="panel-body">
		<table class="table table-hover table-striped">
			<thead>
				<tr>
					<th>アクション</th>
					<th>名前</th>
					<th>性別</th>
					<th>年代</th>
				</tr>
			</thead>
			<tbody>
				@foreach($yakueventattendance->yakuaccompanies as $yakuaccompany)
				<tr>
					<td><a href="{{ route('yakueventattendances.editaccompany', $yakuaccompany->id) }}">編集</a></td>
					<td>{{ $yakuaccompany->name }}</td>
					<td>{{ $yakuaccompany->sex }}</td>
					<td>{{ $yakuaccompany->agebracket }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		</div>
	</div>
</div>
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>{{ $yakueventattendance->user->name }}のアクティビティ一覧</h3></div>
		<div class="panel-body">
		<table class="table table-hover table-striped">
			<thead>
				<tr>
					<th>アクション</th>
					<th>出発日</th>
					<th>アクティビティタイプ</th>
				</tr>
			</thead>
			<tbody>
				@foreach($yakueventattendance->yakuactivities as $yakuactivity)
				<tr>
					<td><a href="{{ route('yakuactivities.show', $yakuactivity->id) }}">詳細</a></td>
					<td>{{ $yakuactivity->depdate }}</td>
					<td>{{ $yakuactivity->yakuactivitytype->name }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		</div>
	</div>
</div>
@endsection