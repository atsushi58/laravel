@extends('layouts.app')

@section('title', '登山コースの編集')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>登山コースの編集</h3></div>
        <div class="panel-body">
            {{ Form::open(['route' => ['yakucourses.update', $course->id], 'method' => 'patch']) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('coursearea_id', 'コースエリア', ['class' => 'form-control-label']) }}</div>
            <div class="col-md-4">{{ Form::select('coursearea_id', $courseareas, $course->coursearea_id, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('name', 'コース名', ['class' => 'form-control-label']) }}</div>
            <div class="col-md-4">{{ Form::input('name', 'name', $course->name, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('courselength_id', 'コース日数', ['class'=> 'form-control-label']) }}</div>
            <div class="col-md-4">{{ Form::select('courselength_id', $courselengths, $course->courselength_id, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('memo', 'メモ', ['class' => 'form-control-label']) }}</div>
            <div class="col-md-4">{{ Form::input('text', 'memo', $course->memo, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
            {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
            {{ Form::close()}}
        </div>
    </div>
</div>


@endsection