@extends('layouts.app')

@section('title', '復路1')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>復路1</h3></div>
		<div class="panel-body">
			<a href="{{ route('yakureturnfromkums.create') }}"><button type="button" class="btn btn-default navbar-btn">復路1追加</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>往路1名</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($yakureturnfromkums as $yakureturnfromkum)
					<tr>
						<td>
							<a href="{{ route('yakureturnfromkums.edit', $yakureturnfromkum->id) }}">編集</a></td>
						<td>{{ $yakureturnfromkum->id }}</td>
					    <td>{{ $yakureturnfromkum->name }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection