@extends('layouts.app')

@section('title', '顧客一覧')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>顧客一覧</h3></div>
    	    <div class="panel-body">
    	    	<table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th class="col-md-2">アクション</th>
                        <th class="col-md-2">id</th>
                        <th class="col-md-2">お名前</th>
                        <th class="col-md-2">カナ</th>
                        <th class="col-md-2">メールアドレス</th>
                        <th class="col-md-2">電話番号</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($clients as $client)
                    <tr>
                        <td></td>
                        <td>{{ $client->id }}</td>
                        <td>{{ $client->name }}</td>
                        <td>{{ $client->kana }}</td>
                        <td>{{ $client->email }}</td>
                        <td>{{ $client->tel }}</td>
                    </tr>
                    @endforeach
                </tbody>
                </table>
            </div>
    </div>
</div>

<script>
function deleteStaff(e) {
  'use strict';

  if (confirm('本当に削除しますか？')) {
    document.getElementById('form_' + e.dataset.id).submit();
  }
}
</script>



@endsection