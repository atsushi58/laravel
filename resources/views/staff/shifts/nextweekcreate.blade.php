@extends('layouts.app')

@section('title', '新規予定シフト')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>新規予定シフト</h3></div>
            <div class="panel-body">
            {{ Form::open(['route' => 'staff.shift.nextweekstore', 'method' => 'post', 'class' => 'form-horizontal']) }}
            {{ Form::hidden('admin_id', Auth::guard('admin')->user()->id) }}
            {{ Form::hidden('workday', $date) }}
            {{ Form::hidden('primarytask_id', '1') }}
            {{ Form::hidden('secondarytask_id', '1') }}
            {{ Form::hidden('confirm', '0') }}
                <div class="form-group row">
                    <div class="col-md-2">
                    {{ Form::label('workplace_id', '勤務地', ['class' => 'col-2 col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::select('workplace_id', $workplaces, 3, ['class' => 'form-control col-4']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                    {{ Form::label('start', '開始予定', ['class' => 'col-2 col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::select('start', $worktimes, null, ['class' => 'form-control col-4']) }}
                    </div>
                    <div class="col-md-2">
                    {{ Form::label('end', '終了予定', ['class' => 'col-2 col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::select('end', $worktimes, null, ['class' => 'form-control col-4']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-xs-2">
                    {{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
                    {{ Form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection