@extends('layouts.app')

@section('title', '今週のシフト一覧')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>今週のシフト一覧</h3></div>
        	<div class="panel-body">
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
				<tr>
					<th>勤務日</th>
					<th>開始予定</th>
					<th>終了予定</th>
					<th>勤務地</th>
					<th>優先タスク</th>
					<th>サブタスク</th>
					<th>確定</th>
				</tr>
				</thead>
				<tbody>
					@forelse($twoweeks as $twoweek)
					@if(in_array($shifts->where('workday', $twoweek)->first()['confirm'],[0,1]))
					<tr>
						<td>{{ $twoweek }}</td>
						<td>{{ $shifts->where('workday', $twoweek)->first()['starttime']['worktime'] }}</td>
						<td>{{ $shifts->where('workday', $twoweek)->first()['endtime']['worktime'] }}</td>
						<td>{{ $shifts->where('workday', $twoweek)->first()['workplace']['name'] }}</td>
						<td>{{ $shifts->where('workday', $twoweek)->first()['primarytask']['name'] }}</td>
						<td>{{ $shifts->where('workday', $twoweek)->first()['secondarytask']['name'] }}</td>
						<td><?php if($shifts->where('workday', $twoweek)->first()['confirm'] == 1){echo '確定済み';}else{echo '未確定';} ?></td>
					<tr>
					@else
					<tr>
						<td>{{ $twoweek }}</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td>確定</td>
					<tr>
					@endif
					@empty
					@endforelse
				</tbody>
			</table>
			</div>
		</div>
	</div>
</div>
<script>
function deleteShifts(e) {
  'use strict';

  if (confirm('本当に削除しますか？')) {
    document.getElementById('form_' + e.dataset.id).submit();
  }
}
</script>


@endsection