@extends('layouts.app')

@section('title', '予定シフトの編集')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>予定シフトの編集</h3></div>
            <div class="panel-body">
            {{ Form::open(['route' => ['staff.shift.nextweekupdate', $shift->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
                <div class="form-group row">
                    <div class="col-md-2">
                    {{ Form::label('workplace_id', '勤務地', ['class' => 'col-2 col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::select('workplace_id', $workplaces, $shift->workplace_id, ['class' => 'form-control col-4']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                    {{ Form::label('start', '開始予定', ['class' => 'col-2 col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::select('start', $worktimes, $shift->start, ['class' => 'form-control col-4']) }}
                    </div>
                    <div class="col-md-2">
                    {{ Form::label('end', '終了予定', ['class' => 'col-2 col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::select('end', $worktimes, $shift->end, ['class' => 'form-control col-4']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-xs-2">
                    {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                    {{ Form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection