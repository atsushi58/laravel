@extends('layouts.app')

@section('title', '来週のシフト一覧')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>シフト一覧</h3></div>
        	<div class="panel-body">
			<table class="table table-hover table-striped">
				<thead>
				<tr>
					<th class="col-md-2">アクション</th>
					<th class="col-md-2">勤務日</th>
					<th class="col-md-3">開始予定</th>
					<th class="col-md-3">終了予定</th>
					<th class="col-md-2">勤務地</th>
				</tr>
				</thead>
				<tbody>
				@forelse($nextweek as $workingday)
				<tr>
					<td>
					<?php if(empty($shifts->where('workday', $workingday)->first())): ?>
					<a href="{{ route('staff.shift.nextweekcreate', $workingday) }}">追加</a>
				    <?php else : ?>
				    <?php $id = $shifts->where('workday', $workingday)->first()['id']; ?>
				    <a href="{{ route('staff.shift.nextweekedit', $id) }}">編集</a> | 
					<form action="{{ route('staff.shift.nextweekdestroy', $id) }}" id="form_{{ $id }}" method="post" style="display:inline">
						    {{ csrf_field() }}
				    		{{ method_field('delete') }}
				      		<a href="#" data-id="{{ $id }}" onclick="deleteShifts(this);">削除</a></form>

				    <?php endif; ?>
						

					</td>
					<td>{{ $workingday }}</td>
					<td>{{ $shifts->where('workday', $workingday)->first()['starttime']['worktime'] }}</td>
					<td>{{ $shifts->where('workday', $workingday)->first()['endtime']['worktime'] }}</td>
					<td>{{ $shifts->where('workday', $workingday)->first()['workplace']['name'] }}</td>
								</tr>
				@empty
				@endforelse
				</tr>
				</tbody>
			</table>
			</div>
		</div>
	</div>
</div>
<script>
function deleteShifts(e) {
  'use strict';

  if (confirm('本当に削除しますか？')) {
    document.getElementById('form_' + e.dataset.id).submit();
  }
}
</script>


@endsection