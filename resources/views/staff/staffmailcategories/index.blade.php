@extends('layouts.app')

@section('title', 'スタッフメールカテゴリ')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>スタッフメールカテゴリ</h3></div>
        	<div class="panel-body">
				<table class="table table-hover table-striped">
					<thead>
					<tr>
						<th class="col-md-2">アクション</th>
						<th class="col-md-2">カテゴリ名</th>
						<th></th>
					</tr>
					</thead>
					<tbody>
					@forelse ($staffmailcategories as $staffmailcategory)
					<tr>
						<td><a href="{{ route('staff.staffmailcategory.edit', $staffmailcategory->id) }}">編集</a> | 
						    <form action="{{ route('staff.staffmailcategory.destroy', $staffmailcategory->id) }}" id="form_{{ $staffmailcategory->id }}" method="post" style="display:inline">
						    {{ csrf_field() }}
				    		{{ method_field('delete') }}
				      		<a href="#" data-id="{{ $staffmailcategory->id }}" onclick="deleteStaffmailcategory(this);">削除</a></form></td>
				      	<td>{{ $staffmailcategory->name }}</td>
						<td></td>
					</tr>
					@empty
					@endforelse
					<tr>
						<td colspan="4">
						{{ Form::open(['route' => 'staff.staffmailcategory.store', 'method' => 'post']) }}
						<div class="form-group row">
						<div class="col-md-2"></div>
						<div class="col-md-2">
						{{ Form::input('name', 'name', null, ['class' => 'form-control']) }}
						</div>
						<div class="col-md-1">
						{{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
				        {{ Form::close() }}
				        </div>
				        </div>
				        </td>
				    </tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script>
function deleteStaffmailcategory(e) {
  'use strict';

  if (confirm('本当に削除しますか？')) {
    document.getElementById('form_' + e.dataset.id).submit();
  }
}
</script>


@endsection