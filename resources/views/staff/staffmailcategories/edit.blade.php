@extends('layouts.app')

@section('title', 'スタッフメールカテゴリの編集')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>スタッフメールカテゴリの編集</h3></div>
        	<div class="panel-body"
        	>
				<table class="table table-hover table-striped">
					<thead>
					<tr>
						<th class="col-md-2">アクション</th>
						<th class="col-md-2">カテゴリ名</th>
						<th class="col-md-7"></th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td colspan="4">
						{{ Form::open(['route' => ['staff.staffmailcategory.update', $staffmailcategory->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
						<div class="form-group row">
						<div class="col-md-2"></div>
						<div class="col-md-2">
						{{ Form::input('name', 'name', $staffmailcategory->name, ['class' => 'form-control']) }}
						</div>
						<div class="col-md-1">
						{{ Form::submit('変更', ['class' => 'btn btn-primary']) }}
				        {{ Form::close() }}
				        </div>
				        </div>
				        </td>
				    </tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection