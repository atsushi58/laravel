@extends('layouts.app')

@section('title', 'スタッフメール')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>スタッフメール</h3></div>
    	    <div class="panel-body">
            <a href="{{ route('staff.staffmail.create') }}"><button type="button" class="btn btn-default navbar-btn">新規スタッフメール</button></a>
            <table class="minitable display" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th class="col-md-2">アクション</th>
                    <th class="col-md-2">タイトル</th>
                    <th class="col-md-2">カテゴリ</th>
                    <th class="col-md-6">内容</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($staffmails as $staffmail)
                <tr>
                    <td></td>
                    <td>{{ $staffmail->title }}</td>
                    <td>{{ $staffmail->staffmailcategory->name }}</td>
                    <td>{!! nl2br(e($staffmail->body)) !!}</td>
                </tr>
                @empty

                @endforelse
                </tbody>
            </table>
    </div>
</div>
@endsection