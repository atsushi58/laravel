@extends('layouts.app')

@section('title', '新規スタッフメール')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>新規スタッフメール</h3></div>
            <div class="panel-body">
            {{ Form::open(['route' => 'staff.staffmail.store', 'method' => 'post', 'class' => 'form-horizontal']) }}
                <div class="form-group row">
                    <div class="col-md-2">
                    {{ Form::label('title', 'タイトル', ['class' => 'col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::input('title', 'title', null, ['class' => 'form-control']) }}
                    </div>
                    <div class="col-md-2">
                    {{ Form::label('staffmailcategory_id', 'カテゴリ', ['class' => 'col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::select('staffmailcategory_id',  $staffmailcategories, 'staffmailcategory_id',  ['class' => 'form-control']) }}
                    </div>
                    
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                    {{ Form::label('body', '本文', ['class' => 'col-2 col-form-label']) }}
                    </div>
                    <div class="col-md-10">
                    {{ Form::textarea('body', null, ['class' => 'form-control col-4']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                    {{ Form::submit('送信', ['class' => 'btn btn-primary']) }}
                    {{ Form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection