@extends('layouts.app')

@section('title', '銀行口座の編集')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>銀行口座の編集</h3></div>
            <div class="panel-body">
    {{ Form::open(['action' => ['BankaccountsController@update', $bankaccount->id ], 'method' => 'patch', 'class' => 'form-horizontal']) }}
            <div class="form-group row">
                <div class="col-md-2">
        	       {{ Form::label('bank_id', '銀行名') }}
                </div>
                <div class="col-md-4">
        	       {{ Form::select('bank_id', $banks, $bankaccount->bank_id, ['class' => 'form-control']) }}
                </div>
                <div class="col-md-2">
        	       {{ Form::label('branch', '支店名') }}
               </div>
               <div class="col-md-4">
        	       {{ Form::input('branch', 'branch', $bankaccount->branch, ['class' => 'col-4 form-control']) }}
               </div>
            </div>
        <div class="form-group row">
            <div class="col-md-2">
        	{{ Form::label('accountno', '口座名') }}
            </div>
            <div class="col-md-4">
        	{{ Form::input('accountno', 'accountno', $bankaccount->accountno, ['class' => 'col-4 form-control']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
        	{{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
            {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
@endsection