@extends('layouts.app')

@section('title', '銀行口座登録')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>銀行口座登録</h3></div>
            <div class="panel-body">
    {{ Form::open(['route' => ['staff.bankaccount.store'], 'method' => 'post', 'class' => 'form-horizontal']) }}
    {{ Form::hidden('admin_id', Auth::guard('admin')->user()->id) }}
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::label('bank_id', '銀行') }}
            </div>
            <div class="col-md-4">
                {{ Form::select('bank_id', $banks, null, ['class' => 'form-control']) }}
            </div>
            <div class="col-md-2">
                {{ Form::label('branch', '支店') }}
            </div>
            <div class="col-md-4">
                {{ Form::input('branch', 'branch', null, ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::label('accountno', '口座番号') }}
            </div>
            <div class="col-md-4">
                {{ Form::input('accountno', 'accountno', null, ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
</div>
@endsection