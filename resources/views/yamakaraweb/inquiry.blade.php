@extends('yamakaraweb.layouts.header')

@section('title', 'お問い合わせ | 山から日本を見てみよう')

@section('content')

<div class="row"><div class="eyecatch">
<!-- meta slider -->
<div style="width: 100%; margin: 0 auto;">
    
</div>
<!--// meta slider--></div><!--eyecatch-->

	<div class="section bgwh mt15" id="mainsection">
		<h2 class="what">お問い合わせ</h2>
		{{ Form::open(['route' => ['yamakaraweb.inquiry.store'], 'method' => 'post']) }}
		{{ Form::hidden('inquirystatus_id', '1') }}
		<div class="form-group row">
            <div class="col-md-6">
				{{ Form::label('name', 'お名前', ['class' => 'form label'])}}
			</div>
			<div class="col-md-6">
		    	{{ Form::input('name', 'name', null, ['class' => 'form-control']) }}
			</div>
		</div>
		<div class="form-group row">
            <div class="col-md-6">
				{{ Form::label('email', 'メールアドレス', ['class' => 'form label'])}}
			</div>
			<div class="col-md-6">
		    	{{ Form::input('email', 'email', null, ['class' => 'form-control']) }}
			</div>
		</div>
		<div class="form-group row">
            <div class="col-md-6">
				{{ Form::label('inquirytype_id', '問い合わせ種類', ['class' => 'form label'])}}
			</div>
			<div class="col-md-6">
		    	{{ Form::select('inquirytype_id', $inquirytypes, ['class' => 'form-control']) }}
			</div>
		</div>
		<div class="form-group row">
            <div class="col-md-6">
				{{ Form::label('title', 'タイトル', ['class' => 'form label'])}}
			</div>
			<div class="col-md-6">
		    	{{ Form::input('title', 'title', null, ['class' => 'form-control']) }}
			</div>
		</div>
		<div class="form-group row">
			<div class="col-md-10">
				{{ Form::label('body', '問い合わせ内容', ['class' => 'form-label'])}}
			</div>
			<div class="col-md-10">
				{{ Form::textarea('body', null, null, ['class' => 'form-control']) }}
			</div>
		</div>
		<div class="form-group row">
		{{ Form::submit('送信', ['class' => 'btn']) }}
            {{ Form::close()}}
            </div>

</div>
@endsection