<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage buyers-salon_wp
 */
?>
<link rel='stylesheet' id='su-content-shortcodes-css'  href='http://yamakara.com/wp/wp-content/plugins/shortcodes-ultimate/assets/css/content-shortcodes.css?ver=4.9.3' type='text/css' media='all' />
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
			<div class="featured-post">
				<?php _e( 'Featured post', 'twentytwelve' ); ?>
			</div>
		<?php endif; ?>
	<header class="entry-header">
		<?php if ( is_single() ) : ?>
		<?php else : ?>
		<h1 class="entry-title">
			<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
		</h1>
		<?php endif; // is_single() ?>
		<?php if ( comments_open() ) : ?>
		<?php endif; // comments_open() ?>
			<?php
			$cat = get_the_category(); // 情報取得
			$catId0 = $cat[0]->cat_ID; // ID取得
			$catName0 = $cat[0]->name; // 名称取得
			$catSlug0 = $cat[0]->category_nicename; // スラッグ取得
			$link0 = get_category_link($catId0); // リンクURL取得
			if(isset($cat[1])){
			$catId1 = $cat[1]->cat_ID; // ID取得
			$catName1 = $cat[1]->name; // 名称取得
			$catSlug1 = $cat[1]->category_nicename; // スラッグ取得
			$link1 = get_category_link($catId1); // リンクURL取得
			};
			if(isset($cat[2])){
			$catId2 = $cat[2]->cat_ID; // ID取得
			$catName2 = $cat[2]->name; // 名称取得
			$catSlug2 = $cat[2]->category_nicename; // スラッグ取得
			$link2 = get_category_link($catId2);
			}; // リンクURL取得
			$comment = get_field('短文コメント');
			$price = get_field('料金');
			$start = get_field('出発日');
			$goal = get_field('発着地');
			$entry = get_field('申し込みページurl');
			$min = get_field('最少催行人数');
			$max = get_field('最大定員');
			$staff = get_field('ガイド・スタッフ');
			$meal = get_field('食事');
			$transportation = get_field('利用交通機関');
			$schedule1 = get_field('1日目スケジュール');
			$totalhours1 = get_field('1日目行動時間');
			$walkinghours1 =get_field('1日目歩行時間');
			$schedule2 = get_field('2日目スケジュール');
			$totalhours2 = get_field('2日目行動時間');
			$walkinghours2 =get_field('2日目歩行時間');
			$schedule3 = get_field('3日目スケジュール');
			$totalhours3 = get_field('3日目行動時間');
			$walkinghours3 =get_field('3日目歩行時間');
			$schedule4 = get_field('4日目スケジュール');
			$totalhours4 = get_field('4日目行動時間');
			$walkinghours4 =get_field('4日目歩行時間');
			$schedule5 = get_field('5日目スケジュール');
			$totalhours5 = get_field('5日目行動時間');
			$walkinghours5 =get_field('5日目歩行時間');
			$schedule6 = get_field('6日目スケジュール');
			$totalhours6 = get_field('6日目行動時間');
			$walkinghours6 =get_field('6日目歩行時間');
			$schedule7 = get_field('7日目スケジュール');
			$totalhours7 = get_field('7日目行動時間');
			$walkinghours7 =get_field('7日目歩行時間');
			$schedule8 = get_field('8日目スケジュール');
			$totalhours8 = get_field('8日目行動時間');
			$walkinghours8 =get_field('8日目歩行時間');
			$schedule9 = get_field('9日目スケジュール');
			$totalhours9 = get_field('9日目行動時間');
			$walkinghours9 =get_field('9日目歩行時間');
			$schedule10 = get_field('10日目スケジュール');
			$totalhours10 = get_field('10日目行動時間');
			$walkinghours10 =get_field('10日目歩行時間');
			$schedule11 = get_field('11日目スケジュール');
			$totalhours11 = get_field('11日目行動時間');
			$walkinghours11 =get_field('11日目歩行時間');
		?>
		<div class="breadcrumbs" vocab="http://schema.org/" typeof="BreadcrumbList">
				<a href="http://yamakara.com/">TOP</a> >
				<a href="<?php echo $link0 ?>"><?php echo $catName0; ?></a> >
				<a href="<?php the_permalink(); ?>"><?php the_title_attribute(); ?></a><br>
			<?php if(isset($link1)) : ?>
				<a href="http://yamakara.com/">TOP</a> >
				<a href="<?php echo $link1 ?>"><?php echo $catName1; ?></a> >
				<a href="<?php the_permalink(); ?>"><?php the_title_attribute(); ?></a><br>
			<?php endif; ?>
			<?php if(isset($link2)) : ?>
				<a href="http://yamakara.com/">TOP</a> >
				<a href="<?php echo $link2 ?>"><?php echo $catName2; ?></a> >
				<a href="<?php the_permalink(); ?>"><?php the_title_attribute(); ?></a>
			<?php endif; ?>
		</div>
      	<?php if ( ! post_password_required() && ! is_attachment() ) : ?>
				<?php the_post_thumbnail(); ?>
		<?php endif; ?>
		<div class="post-tag">
			<?php the_tags('', '', ''); ?>
		</div>
		</header><!-- .entry-header -->
		<strong><span style="font-size: large;"><?php the_title(); ?></span></strong><br/>
		<div class="su-divider su-divider-style-dotted" style="margin:15px 0;border-width:2px;border-color:#999999"></div>
		<strong><span style="font-size: large;">料金：<span style="font-size: large; color: red;"><?php echo $price; ?></span>円</span></strong><br/>
		<strong><span style="font-size: large;">出発日：<?php echo $start; ?></span></strong><br/>
		<strong><span style="font-size: large;">集客状況、満席かどうかについては、<a href="http://yamakara.com/?p=1969">ツアー一覧</a>をご確認ください</span></strong><br/>
		<strong><span style="font-size: large;">発着地：<?php echo $goal; ?></span></strong><br/>
		<div class="su-divider su-divider-style-dotted" style="margin:15px 0;border-width:2px;border-color:#999999"></div>
		<h3>■ ここがポイント！</h3>
		<p><?php echo $comment; ?></p>
		<div class="su-divider su-divider-style-dotted" style="margin:15px 0;border-width:2px;border-color:#999999"></div>
		<?php if ( is_search() ) : // Only display Excerpts for Search ?>
		<div class="entry-summary">
			<?php the_excerpt(); ?>
		</div><!-- .entry-summary -->
		<?php else : ?>
		<div class="entry-content">
			<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentytwelve' ) ); ?>
			<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'twentytwelve' ), 'after' => '</div>' ) ); ?>
		</div><!-- .entry-content -->
		<?php endif; ?>
		<div class="su-divider su-divider-style-dotted" style="margin:15px 0;border-width:2px;border-color:#999999"></div>
		<h3>■ 装備レンタル付き！登山靴や雨具、ストック等、登山装備を持っていなくても安心！</h3>
		<h4>登山装備まるごと12点無料レンタル付</h4>
			<p>①登山靴　②雨具上下　③ショートスパッツ　④ザック（ザックカバー付き）　⑤ストック　⑥ヘッドランプ ⑦フリース　⑧トレッキングパンツ　⑨着圧タイツ　⑩膝サポーター　⑪手袋　⑫帽子<br />
			※靴下のプレゼントはございません</p>
		<style type='text/css'>
			#gallery-2 {
				margin: auto;
			}
			#gallery-2 .gallery-item {
				float: left;
				margin-top: 10px;
				text-align: center;
				width: 25%;
			}
			#gallery-2 img {
				border: 2px solid #cfcfcf;
			}
			#gallery-2 .gallery-caption {
				margin-left: 0;
			}
			/* see gallery_shortcode() in wp-includes/media.php */
		</style>
		<div id='gallery-2' class='gallery galleryid-9575 gallery-columns-4 gallery-size-thumbnail'><dl class='gallery-item'>
			<dt class='gallery-icon landscape'>
				<a href='http://yamakara.com/?attachment_id=786'><img width="150" height="150" src="http://yamakara.com/wp/wp-content/uploads/2015/08/fuji_set11_male_400_15-150x150.png" class="attachment-thumbnail" alt="登山装備まるごと無料レンタル" /></a>
			</dt></dl><dl class='gallery-item'>
			<dt class='gallery-icon landscape'>
				<a href='http://yamakara.com/?attachment_id=785'><img width="150" height="150" src="http://yamakara.com/wp/wp-content/uploads/2015/08/fuji_set11_male_400_2-150x150.png" class="attachment-thumbnail" alt="登山装備まるごと無料レンタル" /></a>
			</dt></dl><dl class='gallery-item'>
			<dt class='gallery-icon portrait'>
				<a href='http://yamakara.com/?attachment_id=1826'><img width="150" height="150" src="http://yamakara.com/wp/wp-content/uploads/2015/12/fuji_set11_female_400_1-150x150.png" class="attachment-thumbnail" alt="登山装備まるごと無料レンタル" /></a>
			</dt></dl><dl class='gallery-item'>
			<dt class='gallery-icon portrait'>
				<a href='http://yamakara.com/?attachment_id=1827'><img width="150" height="150" src="http://yamakara.com/wp/wp-content/uploads/2015/12/fuji_set11_male_400_1-150x150.png" class="attachment-thumbnail" alt="登山装備まるごと無料レンタル" /></a>
			</dt></dl><br style="clear: both" />
		</div>
		<h3>■ レンタルのお申込み方法</h3>
		<p><a href="http://yamakara.com/?p=5230">お問い合わせフォーム</a>（お問い合わせの種類で、「ツアーレンタルのお申し込み」を選択してください）、または直接メール（yamakara@field-mt.com）にて、必要な装備を出発の1週間前までにご連絡ください。<br />
		ご出発前配送は行っておりません。 出発1週間前からは新宿店にご来店いただきサイズの確認後お渡しすることも可能です。</p>
		<div class="su-divider su-divider-style-dotted" style="margin:15px 0;border-width:2px;border-color:#999999"></div>
		<h3>■ 行程表</h3>
		<table class="schedule">
			<thead>
				<tr>
					<th width="20%"></th>
					<th>行程</th>
					<th>歩行時間</th>
				</tr>
			<tbody>
			<?php for($i = 1; $i <= 11; $i++) : ?>
			<?php if(!empty(${"schedule"."$i"})) : ?>
			<tr>
				<th width="20%"><?php echo $i;?>日目</th>
				<td width="60%"><?php echo "${schedule.$i}"; ?></td>
				<td width="20%">行動時間: <?php echo "${totalhours.$i}"; ?>時間<br>歩行時間: <?php echo "${walkinghours.$i}"; ?></td>
			</tr>
			<?php endif; ?>
			<?php endfor; ?>
			</tbody>
		</table>
		<div class="su-divider su-divider-style-dotted" style="margin:15px 0;border-width:2px;border-color:#999999"></div>
		<h3>■ 旅行代金(大人お1人様）・条件</h3>
		<table style="border: 1px solid #000000; text-align: left; color: #000000; border-collapse: collapse; background-color: #ffffff;" width="400">
			<tbody>
			<tr>
			<td style="border: 1px solid #000000; background-color: #808080; color: #ffffff; text-align: left;" width="150">旅行代金</td>
			<td style="border: 1px solid #000000; color: black; text-align: left;" width="250"><?php echo $price; ?>円</td>
			</tr>
			<tr>
			<td style="border: 1px solid #000000; background-color: #808080; color: #ffffff; text-align: left;" width="20">出発日</td>
			<td style="border: 1px solid #000000; color: black; text-align: left;" width="380"><?php echo $start; ?></td>
			</tr>
			<tr>
			<td style="border: 1px solid #000000; background-color: #808080; color: #ffffff; text-align: left;" width="20">発着地</td>
			<td style="border: 1px solid #000000; color: black; text-align: left;" width="380"><?php echo $goal; ?></td>
			</tr>
			<tr>
			<td style="border: 1px solid #000000; background-color: #808080; color: #ffffff; text-align: left;" width="20">ガイド・スタッフ</td>
			<td style="border: 1px solid #000000; color: black; text-align: left;" width="380"><?php echo $staff; ?></td>
			</tr>
			<tr>
			<td style="border: 1px solid #000000; background-color: #808080; color: #ffffff; text-align: left;" width="20">最少催行人数</td>
			<td style="border: 1px solid #000000; color: black; text-align: left;" width="380"><?php echo $min; ?>名</td>
			</tr>
			<tr>
			<td style="border: 1px solid #000000; background-color: #808080; color: #ffffff; text-align: left;" width="20">最大催行人数</td>
			<td style="border: 1px solid #000000; color: black; text-align: left;" width="380"><?php echo $max; ?>名</td>
			</tr>
			<?php if(!empty($meal)) : ?>
			<tr>
			<td style="border: 1px solid #000000; background-color: #808080; color: #ffffff; text-align: left;" width="20">食事</td>
			<td style="border: 1px solid #000000; color: black; text-align: left;" width="380"><?php echo $meal; ?></td>
			</tr>
			<?php endif; ?>
			<?php if(!empty($transportation)) : ?>
			<tr>
			<td style="border: 1px solid #000000; background-color: #808080; color: #ffffff; text-align: left;" width="20">利用交通機関</td>
			<td style="border: 1px solid #000000; color: black; text-align: left;" width="380"><?php echo $transportation; ?></td>
			</tr>
			<?php endif; ?>
			
			</tbody>	
		</table>
		<p class="toContact"><a href=<?php echo $entry; ?>>» <?php the_title(); ?>参加お申し込みはコチラ！</a></p>

		<footer class="entry-meta">
			<?php twentytwelve_entry_meta(); ?>
			<?php edit_post_link( __( 'Edit', 'twentytwelve' ), '<span class="edit-link">', '</span>' ); ?>
			<?php if ( is_singular() && get_the_author_meta( 'description' ) && is_multi_author() ) : // If a user has filled out their description and this is a multi-author blog, show a bio on their entries. ?>
				<div class="author-info">
					<div class="author-avatar">
						<?php
						/** This filter is documented in author.php */
						$author_bio_avatar_size = apply_filters( 'twentytwelve_author_bio_avatar_size', 68 );
						echo get_avatar( get_the_author_meta( 'user_email' ), $author_bio_avatar_size );
						?>
					</div><!-- .author-avatar -->
					<div class="author-description">
						<h2><?php printf( __( 'About %s', 'twentytwelve' ), get_the_author() ); ?></h2>
						<p><?php the_author_meta( 'description' ); ?></p>
						<div class="author-link">
							<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">
								<?php printf( __( 'View all posts by %s <span class="meta-nav">&rarr;</span>', 'twentytwelve' ), get_the_author() ); ?>
							</a>
						</div><!-- .author-link	-->
					</div><!-- .author-description -->
				</div><!-- .author-info -->
			<?php endif; ?>
		</footer><!-- .entry-meta -->
	</article><!-- #post -->
