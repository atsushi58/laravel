@extends('yamakaraweb.layouts.header')

@section('title', '山から日本を見てみよう')

@section('content')

<div class="row">	
	<div class="section bgwh mt15" id="mainsection">
	<h1 class="what" style="padding-left: 45px;">山から日本を見てみよう募集中ツアー一覧</h2>
		<div class="entry-content">
		<h3>■ ツアー募集状況（リアルタイム速報）★全コース登山装備無料レンタル付！★</h3>

		<p class="toContact"><a href='http://yamakara.com/?p=1969'>&raquo; 屋久島のツアー一覧はこちら</a></p>

		<div style="margin: 0px; padding: 0px;" align="center">
			<p><iframe style="border-width: 0;" src="https://calendar.google.com/calendar/embed?height=600&amp;wkst=1&amp;bgcolor=%23ffffcc&amp;src=field-mt.com_r76e9p8i6e2oo7nukrlqmciqs4%40group.calendar.google.com&amp;color=%2323164E&amp;ctz=Asia%2FTokyo" width="800" height="600" frameborder="0" scrolling="no"></iframe></p>
		</div>
			<div id="maincontents01" class="anchorpoint"></div>
			<h3>■ ツアー一覧</h3>
				<p>★キャンセル待ちになっているツアーも順次日程追加していきます。キャンセル待ちでも入れておいていただければ、日程追加の際、キャンセル待ちの方に先に追加日程をご案内します。</p>
				<p>★2019年1月出発ツアーより「ご出発２週間前」までにお振込もしくは新宿店にて現金払いに変更させていただきます。原則ツアーご出発当日のお支払はお受け致しません。</p>
				<p>２週間前までにお支払がない場合は、キャンセル扱いとなる可能性がございます。</p>
				<p>★詳細未定のツアーは一覧からお申込みフォームに入ることができませんのでお申込み希望の方は、ホームページの「お問合わせフォーム」からご希望のツアーを記入して送信ください。</p>
				<p>★【注意】★</p>
				<p>★詳細未定ツアーをお問合わせフォーム以外からお申込みされても対応致しかねます。（ホームページのコメントやチャット、Facebookへのコメントなど）</p>
　              <p>必ずお問合わせフォームからのお申込みをお願い致します。</p>
				<p>★詳細未定ツアーについては、詳細が決まり次第お申込みの皆様にご案内をお送りします。</p>
　              <p>★内容や代金などのお問合わせにはご対応致しかねますのでご了承ください。どんなツアーか気になる方は、とりあえずお申込みください！詳細が判明した後にキャンセルされても大丈夫です。</p>

				<p><b>メルマガ登録</b></p>
				<form method="post" 
   action="https://v.bmb.jp/bm/p/f/tf.php?id=yamakara&task=regist">
<div>
<p> 
登録はこちらから
メールアドレスを入力してお申込みください。
 </p>
<input type='text'   name='form[mail]' size='50' />
<input type='submit' name='regist' value='登録'>
</div>
				<table class="type06">
					<thead>
						<tr>
							<th>出発日</th>
							<th>ツアー名</th>
							<th>日数</th>
							<th>旅行代金(新宿/東京発)</th>
							<th>スタッフ</th>
							<th>申込数/定員</th>
							<th>催行状況</th>
						</tr>
					</thead>
					<tbody>
					@foreach($events as $event)
					<tr>
						<td>{{ $event->Departuredate }}</td>
						<td>@if($event->course->wpid)<a href="http://yamakara.com/?p={{ $event->course->wpid }}" target="_blank" rel="noopener">{!! $event->Title !!}</a>@else{!! $event->Title !!}@endif</td>
						<td>{{ $event->course->courselength->name }}</td>
						<td>@foreach($event->eventprices as $eventprice)
							@if($eventprice->hpactive == 1){{ $eventprice->Nameprice }}<br>
							@else
							@endif
							@endforeach</td>
						<td>@foreach($event->shifts as $shift)@if($shift->confirm != 2){{ $shift->admin->nickname or $shift->admin->name  }}<br>@endif
						@endforeach</td>
						<td>@if($event->eventstatus_id == 6)
							<span style="color: #ff0000;">満席(待{{ $event->Totalpossibleclients - $event->maxpax}})</span>
							@else
							{{ $event->Activetotalclients }}/{{ $event->maxpax }}
							@endif</td>
						<td>{{ $event->eventstatus->name or '' }}</td>
					</tr>
					@endforeach
					</tbody>
				</table>
		</div>
		</div>
	</div>
</div>
@endsection