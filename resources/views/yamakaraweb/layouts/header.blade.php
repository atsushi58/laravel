<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="{{ $metakeywords or ''}}" >
    <meta name="description" content="{{ $metadescription or '' }}"/>
    <!-- meta robots -->

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $metatitle or ''}} | 登山ツアー装備無料レンタル付ヤマカラ</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/slick/slick.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/slick/slick-theme.css" media="screen" />

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https//ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" /></script>  
    <script type="text/javascript" src="/slick/slick.min.js"></script>
        <script>
$(document).ready(function(){
  $('.slider').slick({
    autoplay: true
  });
});
</script>
</head>

<header id="header">
	<div class="header_innner row">
		<div id="logo"><a href= 'http://www.yamakara.com'><img src="/images/logo.png" alt="山から日本を見てみよう"></a></div>
		@if(Auth::guard('web')->check() === true)
		<p class="toContact"><a href="{{ route('user.logout') }}">&raquo; ログアウト</a></p>
		<p class="toContact"><a href="/mypage">&raquo; マイページ</a></p>
		@else
        <!--
		<p class="toContact"><a href="/login">&raquo; ログイン/登録</a></p>
        -->
		@endif    
        <p class="toContact"><a href='https://yamakara.com/?page_id=5230' >&raquo; お問い合わせ</a></p>

		<p class="toContact"><a href='https://yamakara.com/?p=1969'>&raquo; 屋久島ツアー</a></p>
        <p class="toContact"><a href='http://www.field-mt.org/tourlist'>&raquo; 全てのツアー一覧</a></p>

	</div>
</header>
@yield('content')
</div>
<footer id="footer" >

<div class="copy">
<p><a href="http://yamakara.com/?page_id=3534" style="color:#FFF;">会社案内</a>　　<a href="http://yamakara.com/?page_id=3507" style="color:#FFF;">旅行業約款・条件書</a></p>
<p>Copyright &copy; 2018 Field &amp; Mountain All Rights Reserved.</p>
</div>

</footer>
</body>
</html>
<!--タグここまで-->

