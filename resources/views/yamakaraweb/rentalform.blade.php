@extends('yamakaraweb.layouts.header')

@section('title', '山から日本を見てみよう')

@section('content')
<div class="row">
	<div id="primary" class="site-content">
		<div id="content" role="main">
    		<div class="section bgwh">
			<h2 class="bb-gradient">{{ $eventattendance->user->name }}様/{{ $eventattendance->eventprice->event->course->name }}レンタル受付フォーム</h2>
			<p>※お申込み以外のお問合せは、直接メールにて<a href="mailto:yamakara@field-mt.com">yamakara@field-mt.com</a>までご連絡ください。</p>
					{{ Form::open(['route' => ['yamakaraweb.updaterental', $eventattendance->id], 'method'=> 'patch', 'class' => 'form-horizontal']) }}
					<div class="formInner">
					<table class="contact">
						<tr>
							<th>レンタル品<br>受け取り方法</th>
							<td>{{ Form::select('rentaldelivery_id', $rentaldeliveries) }}</td>
						</tr>
						<tr>
							<th class="bn">レンタル品</th>
							<td class="bn">{{ Form::textarea('rental', $eventattendance->rental) }}
							★新宿店での受け取りは、レインウェアとシューズのみサイズをお書きください。<br>
							★その他のレンタル品は新宿店店頭在庫からお選びください。<br>
							★カラーの指定は承れません。<br>
							★心配な方は事前に新宿店でフィッティング可能です。<br>
							★配送は出発1週間前を目安にお送りします。</td>
						</tr>
						<tr>
							<td class="bn">
							{{ Form::submit('申込') }}
							</td>
							<td>
							<div class="required">送信に少し時間がかかりますが、戻るボタンを押さずにお待ちください。</div>
							</td>
						</tr>
					</table>
					{{ Form::close() }}
					</div></form>

					</div><!-- section -->
						</div><!-- #content -->
						</div><!-- #primary -->



								<div id="secondary" class="widget-area" role="complementary">
								<aside id="execphp-2" class="widget widget_execphp"><h3 class="widget-title">カテゴリ</h3>			<div class="execphpwidget">		<ul>
						<li class="cat-item cat-item-171"><a href="http://www.yamakara.com/?cat=171" >OneDayヤマカラ</a>
					</li>
						<li class="cat-item cat-item-136"><a href="http://www.yamakara.com/?cat=136" >ちょっとチャレンジ！なツアー</a>
					</li>
						<li class="cat-item cat-item-135"><a href="http://www.yamakara.com/?cat=135" >フライデーやまから</a>
					</li>
						<li class="cat-item cat-item-134"><a href="http://www.yamakara.com/?cat=134" >宿泊ツアー</a>
					</li>
						<li class="cat-item cat-item-65"><a href="http://www.yamakara.com/?cat=65" >富士山</a>
					</li>
						<li class="cat-item cat-item-192"><a href="http://www.yamakara.com/?cat=243" >屋久島ツアー</a>
					</li>
						<li class="cat-item cat-item-53"><a href="http://www.yamakara.com/?cat=53" >注目のツアー</a>
					</li>
						<li class="cat-item cat-item-170"><a href="http://www.yamakara.com/?cat=170" >海外登山</a>
					</li>
						</ul></div>
							</aside><aside id="tag_cloud-2" class="widget widget_tag_cloud"><h3 class="widget-title">タグ</h3><div class="tagcloud"><a href="http://yamakara.com/?tag=3000m%e5%b3%b0" class="tag-cloud-link tag-link-141 tag-link-position-1" style="font-size: 13.100775193798pt;" aria-label="3000m峰 (8個の項目)">3000m峰</a>
					<a href="http://yamakara.com/?tag=%e3%81%8a%e6%b0%97%e8%bb%bd" class="tag-cloud-link tag-link-8 tag-link-position-2" style="font-size: 12.015503875969pt;" aria-label="お気軽 (6個の項目)">お気軽</a>
					<a href="http://yamakara.com/?tag=%e3%82%b9%e3%83%8e%e3%83%bc%e3%82%b7%e3%83%a5%e3%83%bc" class="tag-cloud-link tag-link-27 tag-link-position-3" style="font-size: 14.077519379845pt;" aria-label="スノーシュー (10個の項目)">スノーシュー</a>
					<a href="http://yamakara.com/?tag=%e3%82%b9%e3%83%8e%e3%83%bc%e3%83%88%e3%83%ac%e3%83%83%e3%82%ad%e3%83%b3%e3%82%b0" class="tag-cloud-link tag-link-129 tag-link-position-4" style="font-size: 12.015503875969pt;" aria-label="スノートレッキング (6個の項目)">スノートレッキング</a>
					<a href="http://yamakara.com/?tag=%e3%83%86%e3%83%b3%e3%83%88%e6%b3%8a" class="tag-cloud-link tag-link-249 tag-link-position-5" style="font-size: 9.3023255813953pt;" aria-label="テント泊 (3個の項目)">テント泊</a>
					<a href="http://yamakara.com/?tag=%e3%83%95%e3%82%a7%e3%83%aa%e3%83%bc%e5%b1%8b%e4%b9%85%e5%b3%b6%ef%bc%92" class="tag-cloud-link tag-link-258 tag-link-position-6" style="font-size: 10.387596899225pt;" aria-label="フェリー屋久島２ (4個の項目)">フェリー屋久島２</a>
					<a href="http://yamakara.com/?tag=%e3%83%95%e3%83%a9%e3%83%af%e3%83%bc%e3%83%88%e3%83%ac%e3%83%83%e3%82%ad%e3%83%b3%e3%82%b0" class="tag-cloud-link tag-link-200 tag-link-position-7" style="font-size: 10.387596899225pt;" aria-label="フラワートレッキング (4個の項目)">フラワートレッキング</a>
					<a href="http://yamakara.com/?tag=%e3%83%97%e3%83%ac%e3%83%9f%e3%82%a2%e3%83%a0%e3%83%95%e3%83%a9%e3%82%a4%e3%83%87%e3%83%bc" class="tag-cloud-link tag-link-92 tag-link-position-8" style="font-size: 9.3023255813953pt;" aria-label="プレミアムフライデー (3個の項目)">プレミアムフライデー</a>
					<a href="http://yamakara.com/?tag=%e3%83%ad%e3%83%bc%e3%83%97%e3%82%a6%e3%82%a7%e3%82%a4" class="tag-cloud-link tag-link-121 tag-link-position-9" style="font-size: 12.558139534884pt;" aria-label="ロープウェイ (7個の項目)">ロープウェイ</a>
					<a href="http://yamakara.com/?tag=%e4%b8%96%e7%95%8c%e9%81%ba%e7%94%a3" class="tag-cloud-link tag-link-30 tag-link-position-10" style="font-size: 21.565891472868pt;" aria-label="世界遺産 (53個の項目)">世界遺産</a>
					<a href="http://yamakara.com/?tag=%e4%ba%8c%e7%99%be%e5%90%8d%e5%b1%b1" class="tag-cloud-link tag-link-58 tag-link-position-11" style="font-size: 12.015503875969pt;" aria-label="二百名山 (6個の項目)">二百名山</a>
					<a href="http://yamakara.com/?tag=%e4%ba%ba%e6%b0%97%e3%81%ae%e5%b1%b1%e5%b0%8f%e5%b1%8b" class="tag-cloud-link tag-link-110 tag-link-position-12" style="font-size: 14.077519379845pt;" aria-label="人気の山小屋 (10個の項目)">人気の山小屋</a>
					<a href="http://yamakara.com/?tag=%e5%85%90%e7%8e%89%e3%83%93%e3%83%aa%e3%83%bc%e5%90%8c%e8%a1%8c" class="tag-cloud-link tag-link-206 tag-link-position-13" style="font-size: 9.3023255813953pt;" aria-label="児玉(ビリー)同行 (3個の項目)">児玉(ビリー)同行</a>
					<a href="http://yamakara.com/?tag=%e5%85%ab%e3%83%b6%e5%b2%b3" class="tag-cloud-link tag-link-125 tag-link-position-14" style="font-size: 13.100775193798pt;" aria-label="八ヶ岳 (8個の項目)">八ヶ岳</a>
					<a href="http://yamakara.com/?tag=%e5%8c%97%e3%82%a2%e3%83%ab%e3%83%97%e3%82%b9" class="tag-cloud-link tag-link-115 tag-link-position-15" style="font-size: 15.271317829457pt;" aria-label="北アルプス (13個の項目)">北アルプス</a>
					<a href="http://yamakara.com/?tag=%e5%8c%97%e6%a8%aa%e5%b2%b3" class="tag-cloud-link tag-link-25 tag-link-position-16" style="font-size: 8pt;" aria-label="北横岳 (2個の項目)">北横岳</a>
					<a href="http://yamakara.com/?tag=%e5%8d%97%e3%82%a2%e3%83%ab%e3%83%97%e3%82%b9" class="tag-cloud-link tag-link-99 tag-link-position-17" style="font-size: 12.015503875969pt;" aria-label="南アルプス (6個の項目)">南アルプス</a>
					<a href="http://yamakara.com/?tag=%e5%ae%ae%e4%b9%8b%e6%b5%a6%e5%b2%b3" class="tag-cloud-link tag-link-79 tag-link-position-18" style="font-size: 16.139534883721pt;" aria-label="宮之浦岳 (16個の項目)">宮之浦岳</a>
					<a href="http://yamakara.com/?tag=%e5%af%8c%e5%a3%ab%e5%b1%b1" class="tag-cloud-link tag-link-38 tag-link-position-19" style="font-size: 13.643410852713pt;" aria-label="富士山 (9個の項目)">富士山</a>
					<a href="http://yamakara.com/?tag=%e5%b1%8b%e4%b9%85%e5%b3%b6" class="tag-cloud-link tag-link-15 tag-link-position-20" style="font-size: 21.131782945736pt;" aria-label="屋久島 (48個の項目)">屋久島</a>
					<a href="http://yamakara.com/?tag=%e5%b1%8b%e4%b9%85%e5%b3%b6%e5%90%88%e6%b5%81" class="tag-cloud-link tag-link-201 tag-link-position-21" style="font-size: 12.015503875969pt;" aria-label="屋久島合流 (6個の項目)">屋久島合流</a>
					<a href="http://yamakara.com/?tag=%e5%b1%b1%e5%b0%8f%e5%b1%8b%e6%b3%8a" class="tag-cloud-link tag-link-131 tag-link-position-22" style="font-size: 13.643410852713pt;" aria-label="山小屋泊 (9個の項目)">山小屋泊</a>
					<a href="http://yamakara.com/?tag=%e5%b1%b1%e6%a2%a8%e7%99%be%e5%90%8d%e5%b1%b1" class="tag-cloud-link tag-link-175 tag-link-position-23" style="font-size: 9.3023255813953pt;" aria-label="山梨百名山 (3個の項目)">山梨百名山</a>
					<a href="http://yamakara.com/?tag=%e5%b1%b1%e6%a2%a8%e7%9c%8c" class="tag-cloud-link tag-link-158 tag-link-position-24" style="font-size: 9.3023255813953pt;" aria-label="山梨県 (3個の項目)">山梨県</a>
					<a href="http://yamakara.com/?tag=%e5%b1%b1%e7%94%b0%e5%90%8c%e8%a1%8c" class="tag-cloud-link tag-link-137 tag-link-position-25" style="font-size: 12.558139534884pt;" aria-label="山田同行 (7個の項目)">山田同行</a>
					<a href="http://yamakara.com/?tag=%e5%b2%90%e9%98%9c%e7%9c%8c" class="tag-cloud-link tag-link-151 tag-link-position-26" style="font-size: 9.3023255813953pt;" aria-label="岐阜県 (3個の項目)">岐阜県</a>
					<a href="http://yamakara.com/?tag=%e5%be%b3%e6%be%a4%e5%9c%92" class="tag-cloud-link tag-link-50 tag-link-position-27" style="font-size: 13.643410852713pt;" aria-label="徳澤園 (9個の項目)">徳澤園</a>
					<a href="http://yamakara.com/?tag=%e6%8c%87%e5%ae%bf" class="tag-cloud-link tag-link-219 tag-link-position-28" style="font-size: 11.255813953488pt;" aria-label="指宿 (5個の項目)">指宿</a>
					<a href="http://yamakara.com/?tag=%e6%97%a5%e5%b8%b0%e3%82%8a" class="tag-cloud-link tag-link-10 tag-link-position-29" style="font-size: 17.767441860465pt;" aria-label="日帰り (23個の項目)">日帰り</a>
					<a href="http://yamakara.com/?tag=%e6%97%a5%e6%9c%ac%e7%99%be%e5%90%8d%e5%b1%b1" class="tag-cloud-link tag-link-80 tag-link-position-30" style="font-size: 22pt;" aria-label="日本百名山 (58個の項目)">日本百名山</a>
					<a href="http://yamakara.com/?tag=%e6%97%a5%e6%9c%ac%e7%99%be%e5%90%8d%e5%b1%b12%e3%81%a4" class="tag-cloud-link tag-link-190 tag-link-position-31" style="font-size: 9.3023255813953pt;" aria-label="日本百名山2つ (3個の項目)">日本百名山2つ</a>
					<a href="http://yamakara.com/?tag=%e6%b8%a9%e6%b3%89" class="tag-cloud-link tag-link-82 tag-link-position-32" style="font-size: 14.837209302326pt;" aria-label="温泉 (12個の項目)">温泉</a>
					<a href="http://yamakara.com/?tag=%e6%b8%a9%e6%b3%89%e5%ae%bf" class="tag-cloud-link tag-link-126 tag-link-position-33" style="font-size: 9.3023255813953pt;" aria-label="温泉宿 (3個の項目)">温泉宿</a>
					<a href="http://yamakara.com/?tag=%e7%99%bd%e8%b0%b7%e9%9b%b2%e6%b0%b4%e5%b3%a1" class="tag-cloud-link tag-link-76 tag-link-position-34" style="font-size: 17.116279069767pt;" aria-label="白谷雲水峡 (20個の項目)">白谷雲水峡</a>
					<a href="http://yamakara.com/?tag=%e7%ab%8b%e3%81%a1%e5%af%84%e3%82%8a%e6%b9%af" class="tag-cloud-link tag-link-55 tag-link-position-35" style="font-size: 17.333333333333pt;" aria-label="立ち寄り湯 (21個の項目)">立ち寄り湯</a>
					<a href="http://yamakara.com/?tag=%e7%b8%84%e6%96%87%e6%9d%89" class="tag-cloud-link tag-link-32 tag-link-position-36" style="font-size: 18.961240310078pt;" aria-label="縄文杉 (30個の項目)">縄文杉</a>
					<a href="http://yamakara.com/?tag=%e7%b8%a6%e8%b5%b0" class="tag-cloud-link tag-link-246 tag-link-position-37" style="font-size: 13.643410852713pt;" aria-label="縦走 (9個の項目)">縦走</a>
					<a href="http://yamakara.com/?tag=%e9%8e%96%e5%a0%b4" class="tag-cloud-link tag-link-259 tag-link-position-38" style="font-size: 8pt;" aria-label="鎖場 (2個の項目)">鎖場</a>
					<a href="http://yamakara.com/?tag=%e9%95%b7%e9%87%8e%e7%9c%8c" class="tag-cloud-link tag-link-140 tag-link-position-39" style="font-size: 9.3023255813953pt;" aria-label="長野県 (3個の項目)">長野県</a>
					<a href="http://yamakara.com/?tag=%e9%96%8b%e8%81%9e%e5%b2%b3" class="tag-cloud-link tag-link-184 tag-link-position-40" style="font-size: 11.255813953488pt;" aria-label="開聞岳 (5個の項目)">開聞岳</a>
					<a href="http://yamakara.com/?tag=%e9%9b%aa%e5%b1%b1%e7%99%bb%e5%b1%b1" class="tag-cloud-link tag-link-214 tag-link-position-41" style="font-size: 9.3023255813953pt;" aria-label="雪山登山 (3個の項目)">雪山登山</a>
					<a href="http://yamakara.com/?tag=%e9%9c%a7%e5%b3%b6%e9%9f%93%e5%9b%bd%e5%b2%b3" class="tag-cloud-link tag-link-193 tag-link-position-42" style="font-size: 11.255813953488pt;" aria-label="霧島韓国岳 (5個の項目)">霧島韓国岳</a>
					<a href="http://yamakara.com/?tag=%e9%9c%b2%e5%ba%97%e9%a2%a8%e5%91%82" class="tag-cloud-link tag-link-29 tag-link-position-43" style="font-size: 8pt;" aria-label="露天風呂 (2個の項目)">露天風呂</a>
					<a href="http://yamakara.com/?tag=%e9%b9%bf%e5%85%90%e5%b3%b6%e7%99%ba" class="tag-cloud-link tag-link-195 tag-link-position-44" style="font-size: 13.100775193798pt;" aria-label="鹿児島発 (8個の項目)">鹿児島発</a>
					<a href="http://yamakara.com/?tag=%e9%bb%92%e5%91%b3%e5%b2%b3" class="tag-cloud-link tag-link-251 tag-link-position-45" style="font-size: 9.3023255813953pt;" aria-label="黒味岳 (3個の項目)">黒味岳</a></div>
					</aside><aside id="execphp-3" class="widget widget_execphp"><h3 class="widget-title">最近募集開始したツアー</h3>			<div class="execphpwidget"><ul>
					<li>
					<h4><a href="http://yamakara.com/?p=17028">装備レンタル付き！南アルプス最南の3000ｍ峰聖岳と雄大な赤石岳５日間</a></h4>
					</li>
					<li>
					<h4><a href="http://yamakara.com/?p=16959">装備レンタル付き！南アルプスの盟主赤石岳と荒川三山縦走５日間</a></h4>
					</li>
					<li>
					<h4><a href="http://yamakara.com/?p=16946">装備レンタル付！絶景、真っ白な雪の世界へ 残雪の木曽駒ケ岳2日間</a></h4>
					</li>
					<li>
					<h4><a href="http://yamakara.com/?p=16914">写真家大沢ガイドと行く上高地のベストショットを探す旅2日間</a></h4>
					</li>
					<li>
					<h4><a href="http://yamakara.com/?p=16906">装備レンタル付！北アルプスの大自然を堪能 涸沢カール3日間</a></h4>
					</li>

					<li>
					<h4><a href="http://yamakara.com/?p=16792">装備レンタル付き！雨飾山2日間</a></h4>
					</li>
					<li>
					<h4><a href="http://yamakara.com/?p=16650">装備レンタル付き！ 岩場と変化に富んだ山歩き 乾徳山</a></h4>
					</li>
					<li>
					<h4><a href="http://yamakara.com/?p=16584">装備レンタル付！房総のマッターホルンに登ろう　伊予ヶ岳（ワンデー）</a></h4>
					</li>
					<li>
					<h4><a href="http://yamakara.com/?p=16567">装備レンタル付き！一度は歩いておきたい 富士山1-5合目（ワンデー）</a></h4>
					</li>
					</ul></div>
							</aside>	</div><!-- #secondary -->
						

					</div><!--main"-->
					<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>

					@endsection