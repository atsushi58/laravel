@extends('yamakaraweb.layouts.header')

@section('content')

<div class="row">	
	<div id="primary" class="site-content">
		<div id="content" role="main">
			<div class="section bgwh korekara">
				<h1 class="what">装備レンタル付！{{ $course->name }}</h1>
					<!-- パンくず -->
					<ol itemscope itemtype="http://schema.org/BreadcrumbList">
					  <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
					    <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="/">
					      <span itemprop="name" class="breadcrumbs">ホーム ></span>
					    </a>
					    <meta itemprop="position" content="1" />
					  </li>
					  <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
					    <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="{{ route('yamakaraweb.tourcategory', $course->coursearea->slug) }}">
					      <span itemprop="name" class="breadcrumbs">{{ $course->coursearea->name }} ></span>
					    </a>
					    <meta itemprop="position" content="2" />
					  </li>
					  <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
					    <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="{{ route('yamakaraweb.tour', $course->slug) }}">
					      <span itemprop="name" class="breadcrumbs">{{ $course->name }}</span>
					    </a>
					    <meta itemprop="position" content="3" />
					  </li>
					</ol>
					<img width="624" height="413" src="/tourimages/{{ $bigimage->filename}}" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="https://yamakara.com/wp/wp-content/uploads/2016/05/槍ヶ岳-624x413.jpg 624w, https://yamakara.com/wp/wp-content/uploads/2016/05/槍ヶ岳-300x199.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2016/05/槍ヶ岳.jpg 1024w" sizes="(max-width: 624px) 100vw, 624px" />				
      				<div class="post-tag">
			<a href="https://yamakara.com/?tag=%e3%83%95%e3%83%a9%e3%83%af%e3%83%bc%e3%83%88%e3%83%ac%e3%83%83%e3%82%ad%e3%83%b3%e3%82%b0" rel="tag">フラワートレッキング</a>
					</div>
					<h2 class="tourtitle mt10">ツアー概要</h2>
						<div class="tourdetail">
							<div class="tourdetailtitle">料金</div>
							<div class="tourdetailcontent">49,800円</div>
						</div>
						<div class="tourdetail">
							<div class="tourdetailtitle">出発日</div>
							<div class="tourdetailcontent">
								@foreach($course->Activeevents as $event){{ $event->depdate->format('Y年m月d日') }} 
								@if($event->eventstatus_id == 6)
								<span class="pricered">満席(待{{ $event->Totalpossibleclients - $event->maxpax}})({{ $event->eventstatus->name or '' }})</span>
								@else
								{{ $event->Activetotalclients }}/{{ $event->maxpax }}
								({{ $event->eventstatus->name or '' }})
								@endif
								<br>
								@endforeach
							</div>
						</div>
						<div class="tourdetail">
							<div class="tourdetailtitle">発着地</div>
							<div class="tourdetailcontent">{{ $course->depplace->name }}</div>
						</div>

		<h2 class="tourtitle">ここがポイント</h2>
		<p>2泊3日のコンパクトな日程で、登山が好きな人なら誰もが一度は登りたい憧れの山・槍ヶ岳登頂 (標高3,180m) を目指します。槍沢を登り天狗原分岐を過ぎると、槍ヶ岳の雄姿が徐々に現れ、気持ちも高まっていきますが、あせらず安全に行動しましょう。槍ヶ岳山荘では美しい槍の穂先を眺めながら、ゆっくりと贅沢な時間を楽しむことができます。</p>
		<div class="su-divider su-divider-style-dotted" style="margin:15px 0;border-width:2px;border-color:#999999"></div>
				<div class="entry-content">
			<p class="toContact"><a href=https://yamakara.com/?page_id=8772>» 装備レンタル付！日本のマッターホルンへ登頂 槍ヶ岳参加お申し込みはコチラ！</a></p>
	<h2 class="tourtitle mt10">槍ヶ岳への行程(2017年9月のツアーの様子)</h2>
<div id="attachment_17656" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/上高地・いざ槍ヶ岳へ出発.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/上高地・いざ槍ヶ岳へ出発-1024x768.jpg" alt="上高地・いざ槍ヶ岳へ" width="625" height="469" class="size-large wp-image-17656" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/上高地・いざ槍ヶ岳へ出発-1024x768.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/上高地・いざ槍ヶ岳へ出発-300x225.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/上高地・いざ槍ヶ岳へ出発-768x576.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/上高地・いざ槍ヶ岳へ出発-624x468.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">1. 槍ヶ岳出発は上高地から。上高地バスターミナルは登山客で大混雑。</p></div>
<div id="attachment_17657" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/1時間の歩行で明神.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/1時間の歩行で明神-1024x768.jpg" alt="1時間の歩行で明神" width="625" height="469" class="size-large wp-image-17657" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/1時間の歩行で明神-1024x768.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/1時間の歩行で明神-300x225.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/1時間の歩行で明神-768x576.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/1時間の歩行で明神-624x468.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">2. 1時間の歩行で明神。左手には明神岳、前穂高岳が見えてきます。槍ヶ岳はまだまだ。</p></div>
<div id="attachment_17658" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/また1時間でソフトクリームが有名な徳澤園.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/また1時間でソフトクリームが有名な徳澤園-1024x768.jpg" alt="また1時間でソフトクリームが有名な徳澤園" width="625" height="469" class="size-large wp-image-17658" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/また1時間でソフトクリームが有名な徳澤園-1024x768.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/また1時間でソフトクリームが有名な徳澤園-300x225.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/また1時間でソフトクリームが有名な徳澤園-768x576.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/また1時間でソフトクリームが有名な徳澤園-624x468.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">3. また1時間でソフトクリームが有名な徳澤園。ちょっと一休み。</p></div>
<div id="attachment_17659" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/横尾までの道のりは平たんですが道端にお花がたくさん.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/横尾までの道のりは平たんですが道端にお花がたくさん-1024x768.jpg" alt="https://yamakara.com/wp/wp-content/uploads/2018/03/横尾までの道のりは平たんですが道端にお花がたくさん" width="625" height="469" class="size-large wp-image-17659" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/横尾までの道のりは平たんですが道端にお花がたくさん-1024x768.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/横尾までの道のりは平たんですが道端にお花がたくさん-300x225.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/横尾までの道のりは平たんですが道端にお花がたくさん-768x576.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/横尾までの道のりは平たんですが道端にお花がたくさん-624x468.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">4. 横尾までの道のりは平たんですが道端にお花がたくさん。</p></div>
<div id="attachment_17660" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/上高地から平たんな道3時間で横尾大橋.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/上高地から平たんな道3時間で横尾大橋-1024x768.jpg" alt="上高地から平たんな道3時間で横尾大橋" width="625" height="469" class="size-large wp-image-17660" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/上高地から平たんな道3時間で横尾大橋-1024x768.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/上高地から平たんな道3時間で横尾大橋-300x225.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/上高地から平たんな道3時間で横尾大橋-768x576.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/上高地から平たんな道3時間で横尾大橋-624x468.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">5. 上高地から平たんな道3時間で横尾大橋。曲がると、涸沢、穂高。まっすぐ行くと槍方面です。</p></div>
<div id="attachment_17661" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/横尾を越えると一気に山っぽくなります.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/横尾を越えると一気に山っぽくなります-1024x683.jpg" alt="横尾を越えると一気に山っぽくなります" width="625" height="417" class="size-large wp-image-17661" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/横尾を越えると一気に山っぽくなります-1024x683.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/横尾を越えると一気に山っぽくなります-300x200.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/横尾を越えると一気に山っぽくなります-768x512.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/横尾を越えると一気に山っぽくなります-624x416.jpg 624w, https://yamakara.com/wp/wp-content/uploads/2018/03/横尾を越えると一気に山っぽくなります.jpg 1400w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">6. 横尾を越えると一気に山っぽくなります。橋を渡ったりしながらゆっくり登ります。</p></div>
<div id="attachment_17662" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢ロッジのお風呂の時間が気になるのでがんばります！.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢ロッジのお風呂の時間が気になるのでがんばります！-1024x683.jpg" alt="槍沢ロッジのお風呂の時間が気になるのでがんばります！" width="625" height="417" class="size-large wp-image-17662" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢ロッジのお風呂の時間が気になるのでがんばります！-1024x683.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢ロッジのお風呂の時間が気になるのでがんばります！-300x200.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢ロッジのお風呂の時間が気になるのでがんばります！-768x512.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢ロッジのお風呂の時間が気になるのでがんばります！-624x416.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">7. 槍沢ロッジのお風呂の時間が気になるのでがんばります！明日の槍ヶ岳より今日のお風呂(笑)</p></div>
<div id="attachment_17663" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢沿いを行く.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢沿いを行く-1024x683.jpg" alt="槍沢沿いを行く" width="625" height="417" class="size-large wp-image-17663" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢沿いを行く-1024x683.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢沿いを行く-300x200.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢沿いを行く-768x512.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢沿いを行く-624x416.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">8. 槍ヶ岳へと続く槍沢ぞいをゆったりと、、、(みんなそんなことより風呂であたまがいっぱい)</p></div>
<div id="attachment_17665" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/初日の宿、槍沢ロッジ到着！.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/初日の宿、槍沢ロッジ到着！-1024x768.jpg" alt="初日の宿、槍沢ロッジ到着！" width="625" height="469" class="size-large wp-image-17665" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/初日の宿、槍沢ロッジ到着！-1024x768.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/初日の宿、槍沢ロッジ到着！-300x225.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/初日の宿、槍沢ロッジ到着！-768x576.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/初日の宿、槍沢ロッジ到着！-624x468.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">9. 初日の宿、槍沢ロッジ到着！明日の登頂に備えてしっかり休みます</p></div>
<div id="attachment_17666" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/夜も楽しく！.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/夜も楽しく！-1024x683.jpg" alt="夜も楽しく！" width="625" height="417" class="size-large wp-image-17666" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/夜も楽しく！-1024x683.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/夜も楽しく！-300x200.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/夜も楽しく！-768x512.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/夜も楽しく！-624x416.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">10. 夜はもちろんみんなで楽しく。明日の槍ヶ岳の話、その先の山の話、話題は尽きません。</p></div>
<div id="attachment_17667" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/外は星空。明日は好天かな.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/外は星空。明日は好天かな-1024x683.jpg" alt="外は星空。明日は好天かな" width="625" height="417" class="size-large wp-image-17667" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/外は星空。明日は好天かな-1024x683.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/外は星空。明日は好天かな-300x200.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/外は星空。明日は好天かな-768x512.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/外は星空。明日は好天かな-624x416.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">11. 沢沿いなので360度とはいかないですが、なかなかの星空です。</p></div><div id="attachment_17668" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/2日目の朝、槍ヶ岳へレッツゴー.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/2日目の朝、槍ヶ岳へレッツゴー-1024x683.jpg" alt="2日目の朝、槍ヶ岳へレッツゴー" width="625" height="417" class="size-large wp-image-17668" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/2日目の朝、槍ヶ岳へレッツゴー-1024x683.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/2日目の朝、槍ヶ岳へレッツゴー-300x200.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/2日目の朝、槍ヶ岳へレッツゴー-768x512.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/2日目の朝、槍ヶ岳へレッツゴー-624x416.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">12. 2日目の朝、槍ヶ岳へ続く道を登り始めます。</p></div>
<div id="attachment_17669" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢ロッジでいただいた朝ごはんのお弁当.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢ロッジでいただいた朝ごはんのお弁当-1024x768.jpg" alt=" 槍沢ロッジでいただいた朝ごはんのお弁当" width="625" height="469" class="size-large wp-image-17669" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢ロッジでいただいた朝ごはんのお弁当-1024x768.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢ロッジでいただいた朝ごはんのお弁当-300x225.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢ロッジでいただいた朝ごはんのお弁当-768x576.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢ロッジでいただいた朝ごはんのお弁当-624x468.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">13. 小屋が混んでいたので槍に登るの優先、ということで朝ごはんはお弁当にしました</p></div>
<div id="attachment_17670" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/ババ平での朝ごはん.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/ババ平での朝ごはん-1024x768.jpg" alt=" ババ平での朝ごはん" width="625" height="469" class="size-large wp-image-17670" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/ババ平での朝ごはん-1024x768.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/ババ平での朝ごはん-300x225.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/ババ平での朝ごはん-768x576.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/ババ平での朝ごはん-624x468.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">14. 槍沢ロッジから1時間、ババ平で朝ごはんです。まだ槍ヶ岳は見えない。。。</p></div>
<div id="attachment_17671" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢の紅葉.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢の紅葉-1024x683.jpg" alt="槍沢の紅葉" width="625" height="417" class="size-large wp-image-17671" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢の紅葉-1024x683.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢の紅葉-300x200.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢の紅葉-768x512.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢の紅葉-624x416.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">14. 涸沢の紅葉が有名ですが、槍沢も負けず劣らずすごいです</p></div>
<div id="attachment_17672" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢をひたすら登っていきます.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢をひたすら登っていきます-1024x768.jpg" alt="槍沢をひたすら登っていきます" width="625" height="469" class="size-large wp-image-17672" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢をひたすら登っていきます-1024x768.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢をひたすら登っていきます-300x225.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢をひたすら登っていきます-768x576.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/槍沢をひたすら登っていきます-624x468.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">15. 紅葉を楽しみながら、槍沢をひたすら登っていきます</p></div>
<div id="attachment_17673" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/そろそろ槍ヶ岳が見えまーす.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/そろそろ槍ヶ岳が見えまーす-1024x768.jpg" alt="そろそろ槍ヶ岳が見えまーす" width="625" height="469" class="size-large wp-image-17673" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/そろそろ槍ヶ岳が見えまーす-1024x768.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/そろそろ槍ヶ岳が見えまーす-300x225.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/そろそろ槍ヶ岳が見えまーす-768x576.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/そろそろ槍ヶ岳が見えまーす-624x468.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">16. そろそろ槍ヶ岳が見えまーす(多分、前のツアーとの間隔開けるために何か話して時間稼ぎ中(笑))</p></div><div id="attachment_17674" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/とうとう槍ヶ岳の穂先が見えた！.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/とうとう槍ヶ岳の穂先が見えた！-1024x768.jpg" alt="とうとう槍ヶ岳の穂先が見えた！" width="625" height="469" class="size-large wp-image-17674" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/とうとう槍ヶ岳の穂先が見えた！-1024x768.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/とうとう槍ヶ岳の穂先が見えた！-300x225.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/とうとう槍ヶ岳の穂先が見えた！-768x576.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/とうとう槍ヶ岳の穂先が見えた！-624x468.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">17. とうとう槍ヶ岳の穂先が見えた！ここから槍ヶ岳山荘までは2時間ほど。</p></div>
<div id="attachment_17675" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/もうすぐ山頂！.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/もうすぐ山頂！-1024x768.jpg" alt="もうすぐ山頂！" width="625" height="469" class="size-large wp-image-17675" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/もうすぐ山頂！-1024x768.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/もうすぐ山頂！-300x225.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/もうすぐ山頂！-768x576.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/もうすぐ山頂！-624x468.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">17. 当たり前ですが、歩けば歩くだけ近づいてきます。もうすぐ山頂！</p></div>
<div id="attachment_17677" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/ついたらまずは腹ごしらえ.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/ついたらまずは腹ごしらえ-1024x683.jpg" alt="ついたらまずは腹ごしらえ" width="625" height="417" class="size-large wp-image-17677" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/ついたらまずは腹ごしらえ-1024x683.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/ついたらまずは腹ごしらえ-300x200.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/ついたらまずは腹ごしらえ-768x512.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/ついたらまずは腹ごしらえ-624x416.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">18. 槍ヶ岳山荘についたら、穂先が混んでたのでまずは腹ごしらえ</p></div>
<div id="attachment_17678" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/ホットサンドメーカーと穂先と登山ガイド.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/ホットサンドメーカーと穂先と登山ガイド-1024x688.jpg" alt="ホットサンドメーカーと穂先と登山ガイド" width="625" height="420" class="size-large wp-image-17678" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/ホットサンドメーカーと穂先と登山ガイド-1024x688.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/ホットサンドメーカーと穂先と登山ガイド-300x202.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/ホットサンドメーカーと穂先と登山ガイド-768x516.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/ホットサンドメーカーと穂先と登山ガイド-624x419.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">18. この日のお昼はホットサンド。結構ボリュームがあります。</p></div>
<div id="attachment_17679" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/いよいよ槍ヶ岳の穂先へ.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/いよいよ槍ヶ岳の穂先へ-1024x715.jpg" alt="いよいよ槍ヶ岳の穂先へ" width="625" height="436" class="size-large wp-image-17679" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/いよいよ槍ヶ岳の穂先へ-1024x715.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/いよいよ槍ヶ岳の穂先へ-300x210.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/いよいよ槍ヶ岳の穂先へ-768x536.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/いよいよ槍ヶ岳の穂先へ-624x436.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">19. いよいよ槍ヶ岳の穂先へ。アスレチックみたいです。</p></div>
<div id="attachment_17680" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/もうすぐ頂上だぜ！.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/もうすぐ頂上だぜ！-1024x683.jpg" alt="もうすぐ頂上だぜ！" width="625" height="417" class="size-large wp-image-17680" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/もうすぐ頂上だぜ！-1024x683.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/もうすぐ頂上だぜ！-300x200.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/もうすぐ頂上だぜ！-768x512.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/もうすぐ頂上だぜ！-624x416.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">20. もうすぐ頂上だぜ！眼下に広がるこの景色！</p></div>
<div id="attachment_17681" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/最後のはしご.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/最後のはしご-1024x693.jpg" alt="槍ヶ岳の穂先への最後のはしご" width="625" height="423" class="size-large wp-image-17681" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/最後のはしご-1024x693.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/最後のはしご-300x203.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/最後のはしご-768x520.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/最後のはしご-624x423.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">21. 最後のはしごを登ったら槍ヶ岳の穂先に到着！</p></div>
<div id="attachment_17682" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/P9301011.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/P9301011-1024x768.jpg" alt="360度のパノラマ" width="625" height="469" class="size-large wp-image-17682" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/P9301011-1024x768.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/P9301011-300x225.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/P9301011-768x576.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/P9301011-624x468.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">22. 槍ヶ岳の穂先から360度のパノラマが見える最高の景色でした！</p></div>
<div id="attachment_17684" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/みんなで槍ポーズ！.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/みんなで槍ポーズ！-1024x768.jpg" alt="みんなで槍ポーズ" width="625" height="469" class="size-large wp-image-17684" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/みんなで槍ポーズ！-1024x768.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/みんなで槍ポーズ！-300x225.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/みんなで槍ポーズ！-768x576.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/みんなで槍ポーズ！-624x468.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">23. みんなで槍ポーズ</p></div>
<div id="attachment_17685" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/やったぜ！.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/やったぜ！-1024x768.jpg" alt="やったぜ！" width="625" height="469" class="size-large wp-image-17685" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/やったぜ！-1024x768.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/やったぜ！-300x225.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/やったぜ！-768x576.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/やったぜ！-624x468.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">24. やったぜ！槍ヶ岳の穂先をバックに。</p></div><div id="attachment_17686" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/下山後は当然はじまります！.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/下山後は当然はじまります！-1024x683.jpg" alt="下山後は当然はじまります！" width="625" height="417" class="size-large wp-image-17686" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/下山後は当然はじまります！-1024x683.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/下山後は当然はじまります！-300x200.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/下山後は当然はじまります！-768x512.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/下山後は当然はじまります！-624x416.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">25. 下山後は当然はじまります！かんぱーい</p></div><div id="attachment_17687" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/雲ノ平行く人！って手を挙げてもらった証拠写真笑.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/雲ノ平行く人！って手を挙げてもらった証拠写真笑-1024x768.jpg" alt="雲ノ平行く人！" width="625" height="469" class="size-large wp-image-17687" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/雲ノ平行く人！って手を挙げてもらった証拠写真笑-1024x768.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/雲ノ平行く人！って手を挙げてもらった証拠写真笑-300x225.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/雲ノ平行く人！って手を挙げてもらった証拠写真笑-768x576.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/雲ノ平行く人！って手を挙げてもらった証拠写真笑-624x468.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">26. ツアー中のツアーの押し売り。雲ノ平行く人！って手を挙げてもらった証拠写真(笑)。</p></div>
<div id="attachment_17688" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/槍から富士山.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/槍から富士山-1024x700.jpg" alt="槍から富士山" width="625" height="427" class="size-large wp-image-17688" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/槍から富士山-1024x700.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/槍から富士山-300x205.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/槍から富士山-768x525.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/槍から富士山-624x426.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">27. 富士山も映えます</p></div>
<div id="attachment_17689" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/最終日は朝焼けとともに下山開始.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/最終日は朝焼けとともに下山開始-1024x683.jpg" alt="最終日は朝焼けとともに下山開始" width="625" height="417" class="size-large wp-image-17689" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/最終日は朝焼けとともに下山開始-1024x683.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/最終日は朝焼けとともに下山開始-300x200.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/最終日は朝焼けとともに下山開始-768x512.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/最終日は朝焼けとともに下山開始-624x416.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">28. 最終日は朝焼けとともに下山開始です。</p></div>
<div id="attachment_17690" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/お昼は徳澤園でカレー.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/お昼は徳澤園でカレー-1024x683.jpg" alt="お昼は徳澤園でカレー" width="625" height="417" class="size-large wp-image-17690" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/お昼は徳澤園でカレー-1024x683.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/お昼は徳澤園でカレー-300x200.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/お昼は徳澤園でカレー-768x512.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/お昼は徳澤園でカレー-624x416.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">29. お昼は徳澤園でカレーをいただきました</p></div>
<div id="attachment_17691" style="width: 635px" class="wp-caption aligncenter"><a href="https://yamakara.com/wp/wp-content/uploads/2018/03/無事下山！.jpg"><img src="https://yamakara.com/wp/wp-content/uploads/2018/03/無事下山！-1024x683.jpg" alt="無事下山！" width="625" height="417" class="size-large wp-image-17691" srcset="https://yamakara.com/wp/wp-content/uploads/2018/03/無事下山！-1024x683.jpg 1024w, https://yamakara.com/wp/wp-content/uploads/2018/03/無事下山！-300x200.jpg 300w, https://yamakara.com/wp/wp-content/uploads/2018/03/無事下山！-768x512.jpg 768w, https://yamakara.com/wp/wp-content/uploads/2018/03/無事下山！-624x416.jpg 624w" sizes="(max-width: 625px) 100vw, 625px" /></a><p class="wp-caption-text">30. 無事下山。充実の山行でした。またどこかでお会いしましょう！</p></div>
<p class="toContact"><a href=https://yamakara.com/?page_id=8772>» 装備レンタル付！日本のマッターホルンへ登頂 槍ヶ岳参加お申し込みはコチラ！</a></p>
<div class="addtoany_share_save_container addtoany_content addtoany_content_bottom"><div class="a2a_kit a2a_kit_size_32 addtoany_list" data-a2a-url="https://yamakara.com/?p=8770" data-a2a-title="装備レンタル付！日本のマッターホルンへ登頂 槍ヶ岳"><a class="a2a_button_facebook" href="https://www.addtoany.com/add_to/facebook?linkurl=https%3A%2F%2Fyamakara.com%2F%3Fp%3D8770&amp;linkname=%E8%A3%85%E5%82%99%E3%83%AC%E3%83%B3%E3%82%BF%E3%83%AB%E4%BB%98%EF%BC%81%E6%97%A5%E6%9C%AC%E3%81%AE%E3%83%9E%E3%83%83%E3%82%BF%E3%83%BC%E3%83%9B%E3%83%AB%E3%83%B3%E3%81%B8%E7%99%BB%E9%A0%82%20%E6%A7%8D%E3%83%B6%E5%B2%B3" title="Facebook" rel="nofollow noopener" target="_blank"></a><a class="a2a_button_twitter" href="https://www.addtoany.com/add_to/twitter?linkurl=https%3A%2F%2Fyamakara.com%2F%3Fp%3D8770&amp;linkname=%E8%A3%85%E5%82%99%E3%83%AC%E3%83%B3%E3%82%BF%E3%83%AB%E4%BB%98%EF%BC%81%E6%97%A5%E6%9C%AC%E3%81%AE%E3%83%9E%E3%83%83%E3%82%BF%E3%83%BC%E3%83%9B%E3%83%AB%E3%83%B3%E3%81%B8%E7%99%BB%E9%A0%82%20%E6%A7%8D%E3%83%B6%E5%B2%B3" title="Twitter" rel="nofollow noopener" target="_blank"></a><a class="a2a_button_google_plus" href="https://www.addtoany.com/add_to/google_plus?linkurl=https%3A%2F%2Fyamakara.com%2F%3Fp%3D8770&amp;linkname=%E8%A3%85%E5%82%99%E3%83%AC%E3%83%B3%E3%82%BF%E3%83%AB%E4%BB%98%EF%BC%81%E6%97%A5%E6%9C%AC%E3%81%AE%E3%83%9E%E3%83%83%E3%82%BF%E3%83%BC%E3%83%9B%E3%83%AB%E3%83%B3%E3%81%B8%E7%99%BB%E9%A0%82%20%E6%A7%8D%E3%83%B6%E5%B2%B3" title="Google+" rel="nofollow noopener" target="_blank"></a><a class="a2a_button_line" href="https://www.addtoany.com/add_to/line?linkurl=https%3A%2F%2Fyamakara.com%2F%3Fp%3D8770&amp;linkname=%E8%A3%85%E5%82%99%E3%83%AC%E3%83%B3%E3%82%BF%E3%83%AB%E4%BB%98%EF%BC%81%E6%97%A5%E6%9C%AC%E3%81%AE%E3%83%9E%E3%83%83%E3%82%BF%E3%83%BC%E3%83%9B%E3%83%AB%E3%83%B3%E3%81%B8%E7%99%BB%E9%A0%82%20%E6%A7%8D%E3%83%B6%E5%B2%B3" title="Line" rel="nofollow noopener" target="_blank"></a></div></div>					</div><!-- .entry-content -->
				<div class="su-divider su-divider-style-dotted" style="margin:15px 0;border-width:2px;border-color:#999999"></div>
		<h3>■ 装備レンタル付き！登山靴や雨具、ストック等、登山装備を持っていなくても安心！</h3>
		<h4>登山装備まるごと12点無料レンタル付</h4>
			<p>①登山靴　②雨具上下　③ショートスパッツ　④ザック（ザックカバー付き）　⑤ストック　⑥ヘッドランプ ⑦フリース　⑧トレッキングパンツ　⑨着圧タイツ　⑩膝サポーター　⑪手袋　⑫帽子<br />
			※靴下のプレゼントはございません</p>
		<style type='text/css'>
			#gallery-2 {
				margin: auto;
			}
			#gallery-2 .gallery-item {
				float: left;
				margin-top: 10px;
				text-align: center;
				width: 25%;
			}
			#gallery-2 img {
				border: 2px solid #cfcfcf;
			}
			#gallery-2 .gallery-caption {
				margin-left: 0;
			}
			/* see gallery_shortcode() in wp-includes/media.php */
		</style>
		<div id='gallery-2' class='gallery galleryid-9575 gallery-columns-4 gallery-size-thumbnail'><dl class='gallery-item'>
			<dt class='gallery-icon landscape'>
				<a href='http://yamakara.com/?attachment_id=786'><img width="150" height="150" src="http://yamakara.com/wp/wp-content/uploads/2015/08/fuji_set11_male_400_15-150x150.png" class="attachment-thumbnail" alt="登山装備まるごと無料レンタル" /></a>
			</dt></dl><dl class='gallery-item'>
			<dt class='gallery-icon landscape'>
				<a href='http://yamakara.com/?attachment_id=785'><img width="150" height="150" src="http://yamakara.com/wp/wp-content/uploads/2015/08/fuji_set11_male_400_2-150x150.png" class="attachment-thumbnail" alt="登山装備まるごと無料レンタル" /></a>
			</dt></dl><dl class='gallery-item'>
			<dt class='gallery-icon portrait'>
				<a href='http://yamakara.com/?attachment_id=1826'><img width="150" height="150" src="http://yamakara.com/wp/wp-content/uploads/2015/12/fuji_set11_female_400_1-150x150.png" class="attachment-thumbnail" alt="登山装備まるごと無料レンタル" /></a>
			</dt></dl><dl class='gallery-item'>
			<dt class='gallery-icon portrait'>
				<a href='http://yamakara.com/?attachment_id=1827'><img width="150" height="150" src="http://yamakara.com/wp/wp-content/uploads/2015/12/fuji_set11_male_400_1-150x150.png" class="attachment-thumbnail" alt="登山装備まるごと無料レンタル" /></a>
			</dt></dl><br style="clear: both" />
		</div>
		<h3>■ レンタルのお申込み方法</h3>
		<p><a href="http://yamakara.com/?p=5230">お問い合わせフォーム</a>（お問い合わせの種類で、「ツアーレンタルのお申し込み」を選択してください）、または直接メール（yamakara@field-mt.com）にて、必要な装備を出発の1週間前までにご連絡ください。<br />
		ご出発前配送は行っておりません。 出発1週間前からは新宿店にご来店いただきサイズの確認後お渡しすることも可能です。</p>
		<div class="su-divider su-divider-style-dotted" style="margin:15px 0;border-width:2px;border-color:#999999"></div>
		<h3>■ 行程表</h3>
		<table class="type6">
			<thead>
				<tr>
					<th width="20%"></th>
					<th>行程</th>
					<th>歩行時間</th>
				</tr>
			</thead>
			<tbody>
									<tr>
				<th width="20%">1日目</th>
				<td width="60%">新宿(7:00発)→&lt;高速道&gt;(途中サービスエリアにて各自、昼食購入)→上高地･･･明神･･･徳沢･･･横尾･･･槍沢ロッジ泊</td>
				<td width="20%">行動時間: 6時間<br>歩行時間: 5</td>
			</tr>
												<tr>
				<th width="20%">2日目</th>
				<td width="60%">槍沢ロッジ･･･天狗原分岐･･･槍ヶ岳･･･槍ヶ岳山荘泊</td>
				<td width="20%">行動時間: 7時間<br>歩行時間: 5.5</td>
			</tr>
												<tr>
				<th width="20%">3日目</th>
				<td width="60%">槍ヶ岳山荘･･･槍沢･･･横尾･･･徳沢･･･明神･･･上高地→温泉入浴→&lt;高速道&gt;→新宿(20:00～21:00着)</td>
				<td width="20%">行動時間: 9時間<br>歩行時間: 7.5</td>
			</tr>
		</tbody>
																																																									</tbody>
		</table>
		<div class="su-divider su-divider-style-dotted" style="margin:15px 0;border-width:2px;border-color:#999999"></div>
		<h3>■ 旅行代金(大人お1人様）・条件</h3>
		<table style="border: 1px solid #000000; text-align: left; color: #000000; border-collapse: collapse; background-color: #ffffff;" width="400">
			<tbody>
			<tr>
			<td style="border: 1px solid #000000; background-color: #808080; color: #ffffff; text-align: left;" width="150">旅行代金</td>
			<td style="border: 1px solid #000000; color: black; text-align: left;" width="250">49,800円</td>
			</tr>
			<tr>
			<td style="border: 1px solid #000000; background-color: #808080; color: #ffffff; text-align: left;" width="20">出発日</td>
			<td style="border: 1px solid #000000; color: black; text-align: left;" width="380">2018年8月28日(火)・9月22日(土)</td>
			</tr>
			<tr>
			<td style="border: 1px solid #000000; background-color: #808080; color: #ffffff; text-align: left;" width="20">発着地</td>
			<td style="border: 1px solid #000000; color: black; text-align: left;" width="380">新宿西口(やまどうぐレンタル屋前)</td>
			</tr>
			<tr>
			<td style="border: 1px solid #000000; background-color: #808080; color: #ffffff; text-align: left;" width="20">ガイド・スタッフ</td>
			<td style="border: 1px solid #000000; color: black; text-align: left;" width="380">ガイド：8月28日辻本　9月22日やまたみ・スタッフ8月22日中野　9月22日未定</td>
			</tr>
			<tr>
			<td style="border: 1px solid #000000; background-color: #808080; color: #ffffff; text-align: left;" width="20">最少催行人数</td>
			<td style="border: 1px solid #000000; color: black; text-align: left;" width="380">10名</td>
			</tr>
			<tr>
			<td style="border: 1px solid #000000; background-color: #808080; color: #ffffff; text-align: left;" width="20">最大催行人数</td>
			<td style="border: 1px solid #000000; color: black; text-align: left;" width="380">20名</td>
			</tr>
						<tr>
			<td style="border: 1px solid #000000; background-color: #808080; color: #ffffff; text-align: left;" width="20">食事</td>
			<td style="border: 1px solid #000000; color: black; text-align: left;" width="380">朝2弁2夕2</td>
			</tr>
									<tr>
			<td style="border: 1px solid #000000; background-color: #808080; color: #ffffff; text-align: left;" width="20">利用交通機関</td>
			<td style="border: 1px solid #000000; color: black; text-align: left;" width="380">いずみ観光バス等同等</td>
			</tr>
						
			</tbody>	
		</table>
				<p class="toContact"><a href='http://www.field-mt.org/attendanceform/128'>» 装備レンタル付！日本のマッターホルンへ登頂 槍ヶ岳参加お申し込みはコチラ！</a></p>
				<footer class="entry-meta">
			カテゴリー: <a href="https://yamakara.com/?cat=181" rel="category">宿泊ヤマカラ</a> | 投稿日: <a href="https://yamakara.com/?p=8770" title="4:20 PM" rel="bookmark"><time class="entry-date" datetime="2018-03-28T16:20:24+00:00">2018年3月28日</time></a> | <span class="by-author">投稿者: <span class="author vcard"><a class="url fn n" href="https://yamakara.com/?author=1" title="yamakara の投稿をすべて表示" rel="author">yamakara</a></span></span>			<span class="edit-link"><a class="post-edit-link" href="https://yamakara.com/wp/wp-admin/post.php?post=8770&#038;action=edit">編集</a></span>					</footer><!-- .entry-meta -->
	</article><!-- #post -->
<p class="toContact"><a href="http://yamakara.com/?p=1969">&raquo; 全てのツアー一覧はコチラ！</a></p>
				</div>

				<div class="section bgwh korekara">
				
<div id="comments" class="comments-area">

	
		</div></div>
	<div class="section bgwh korekara">
		<div id="respond" class="comment-respond">
		<h3 id="reply-title" class="comment-reply-title">コメントを残す <small><a rel="nofollow" id="cancel-comment-reply-link" href="/?p=8770#respond" style="display:none;">コメントをキャンセル</a></small></h3>			<form action="https://yamakara.com/wp/wp-comments-post.php" method="post" id="commentform" class="comment-form">
				<p class="logged-in-as"><a href="https://yamakara.com/wp/wp-admin/profile.php" aria-label="yamakara としてログイン中。プロフィールを編集。">yamakara としてログイン中</a>。<a href="https://yamakara.com/wp/wp-login.php?action=logout&amp;redirect_to=https%3A%2F%2Fyamakara.com%2F%3Fp%3D8770&amp;_wpnonce=dfd81c01fe">ログアウトしますか ?</a></p><p class="comment-form-comment"><label for="comment">コメント</label> <textarea id="comment" name="comment" cols="45" rows="8" maxlength="65525" aria-required="true" required="required"></textarea></p><p class="form-submit"><input name="submit" type="submit" id="submit" class="submit" value="コメントを送信" /> <input type='hidden' name='comment_post_ID' value='8770' id='comment_post_ID' />
<input type='hidden' name='comment_parent' id='comment_parent' value='0' />
</p><input type="hidden" id="_wp_unfiltered_html_comment_disabled" name="_wp_unfiltered_html_comment_disabled" value="223a255619" /><script>(function(){if(window===window.parent){document.getElementById('_wp_unfiltered_html_comment_disabled').name='_wp_unfiltered_html_comment';}})();</script>
<div id="comment-image-reloaded-wrapper"><p id="comment-image-reloaded-error"></p><label for='comment_image_reloaded_8770'>Select an image for your comment (GIF, PNG, JPG, JPEG):</label><p class='comment-image-reloaded'><input type='file' name=comment_image_reloaded_8770[] id='comment_image_reloaded' multiple='multiple' /></p><a href="http://wp-puzzle.com/" rel="external nofollow" target="_blank" class="cir-link"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFoAAAAoCAYAAAB+Qu3IAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpGNzU2NkRBNDY2RDFFNTExQkUxNUNGNEE1REE1M0E4RiIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo0RUMxMzg5OEQ0QjYxMUU1ODQwQTlDMDI4MjI5QTdFQyIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo0RUMxMzg5N0Q0QjYxMUU1ODQwQTlDMDI4MjI5QTdFQyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M2IChXaW5kb3dzKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjgxMTA0OEQzQjVENEU1MTFCREQ1QTE1NDk0MjRBRjI1IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkY3NTY2REE0NjZEMUU1MTFCRTE1Q0Y0QTVEQTUzQThGIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+v54z5QAAClVJREFUeNrsW2twlFcZfrLZTchlc9lcCeQCITcIpFxSAtJMKlCsglrQGRjLoNXqdFB+1LbaOh2mU3UEdEadOrWWsWNRfzBKKwxTbLiUEtJEJAnQkGxISLIJ5H7ZZJPNff2es5x1N/n2FtLpn31mDpt9993vnO953/O855xvCSotLQ0BcExpTyvNgAAWEgNKO6G0F7XKP0fT0tIOLVu2DDqdLkDNAmJycjK2ubn5kMlksmqU9/sDJH82IKfkVsGzzGiDO5KnpqYwODiIgYEBmM1mBAUFCVtSUhISExMRFhYmbAF4Jpscaz05dXV14fr164iJiYFGoxEkj4yMCPL7+/uRn5+PkJCQAJs+QJVom80mSGXWsk1MTDikJSoqSmRxRESETyRXV1ejr68PBQUFSEhIELby8nKMjo4iOzsbSn1w8Vu5ciXu3LlDfXNcIzw8HEuXLnX4OuPy5cs++z4MxsbGUFlZKa5fWFi4MERTKjo6OgQxBoNBZC8zmmTPzMwI4q1Wq7AzIPwsPj5etYO4uDhBIP2lHJFkXmtoaMjhJz9nfxIbN26EXq8XQWhoaBA2dwT64zsfDA8PuwTUX2jUjJ2dnejt7UV9fT16enqEjVnMxmynNo+Pj6OpqUncVF1dnYOo2eDNE5JUDlgGQNok+cyWRYsWzbkGawLBgHmDP76fu3Sw8DFLhYPW7kJiZeEj2fycEZ6enhYkd3d3Iz09fc61YmNjXbKXRPM9M1eS4Uy+h4IiAuJj8RG+TJIbN24gOjpaTHf2w+lPH8qLsvSa8/3U1FS0tbXNsVP6nMFrU+KYIISzDPpMNEklkVIu5GqDBJNUSgczjxpNKWFj4VQjWuq6JJrf53tmOgPFm5dEy+xXWY+6BN3L2tUn38zMTNEIo9EoyGVAcnJyRJttn70au337tuCmpKTEMbM9yZWqdHCQoaGhoknZiIyMFESwCEqdZoGQAeAscAdmqiRVSgSvxYHSpqbPs1c/njJ+vr6yHpFMjmX16tVe7QRnL++HfZArOW5PcqUadhLBZRxfpTbLrGa28+LMdsoGSeZnUmo86TRJZWZLHeX1SbI7feY0l37epqaar6wvnsD6QnBj4dz/bLucdc4SxkCoyYzPRC9ZsgQ1NTUio2XWcvpIomgnuYw2/+bqg3LgDiyeBP2YCfK9lBS25ORktysJX+CPrwSlgUHmvTkH0Z3decZLPZcyMy+iOSVSUlLEwNkZo0uSKRkkWGYyieamhTLiaU3NjGCmcWrxOyyQMgC0OZO/kJDES92mHDgXMzVpcGd3BvcWLIQcOxOR92cymfwvhoxYXl4egoODBbFZWVkiyiRLFjRmucViEYPKzc0Vvp7A4NFXXkMSIau2v9noC0gAN0AsXOfPnxdjkMtKWZwZhLKyMpdxqtkpR878rFu3Do2NjQ4fJiSTRW7KZiOotLTUtm3bNq9TjGtrWRxJDgfKDrnS4PQJbMXdg0HW+uLIsw5mLwlmhpNUEkyNogx4y+YA7NLRr0wTg6djUq4S5EohAL/PpMU6gGuyE9whPcw+PgD3JD/YfR5nRr+kVMwgpe3njjlAz4JCPsp6VTu974jjdDTAy2er0UfTv//UocwX9kMXqw8wspDSMWiJbTr27qHWP71nDToXv63viw2nDAGSPyOyB4ZxMXt3v/2ZoRuSx8asMJsvwDx0Ft2mmyg1F+Om9Qt4YmU6ti/PQIY+ElpN4JmhJzzg1vMzw6qqSvzzH8+j+HErYhaNYHIoFBV341Fzz4SqrFYc2bEVhvCwAJvzPeuYmZmEbaYHG9Y/gurq7UrWnkJcsgbFGiNS9BZcHtmh7PeLfSL5+vO/RO+JS1h75ggSi9YJ25Wdz2G0shG5b/0Y6bu/5OKX//fDML7yBiZb/n/kGL5xBdKe+ZrD1xkXC/f67PswsHb34ZOvHER4fgaK3vmV399XPdvs6iqHsf6gIh0nlWwGEpZMQavsvpcarPhy8n+xSXsOtaPAha4hfNjUgovN7ZixqS9a4rfYybXe7xavUyNWQbIuIw7m2kaH32iD/bjRUJDnsG3+z5+xo6cU4dmpqP/Bb9B66pzbG/HHdz4YvtvqEtAFIdpoPIM7Tddw5ephDLacQbAlBMFaDXQhNlzq3YJbun0wTY3iF9U3cPjqNbx+8RJMFjfPDLPFD0gcpA41m+wB+OoWmCtrXchnNoYlzj2wT36yWLz2llV5vSF/fD936bjXdQu6qGCEdOixYtyCyWgNmsZ00BtG8JG1EFeT9iAenRjpG4Z1ZgYt5kGcNbXh4Kq5Z7OG/Bx79j4gdbihWbyPK3oEvafLXMl/oki9oOjDH1TwIe/Fx8m3u6IK1bt+guid68V0H25uQ/mjz4j+0374TTS98Mc530979WmYXv/rHDulzxm8tvHnb4sEIZxl0OeMtnSMQt8RhJjRMcQbppEUPQNdnw4TndHIb/oEW4++jPSLpegLDUXIyDB2XbmM8BPvuO0k+rE1mOyynwWPtneJ91Er0sVU5M2TfCIqL1N9iTQ8+qCCR3lfTvnou+LAHiE1bCRXjFMJSN6hA6p2Z3AGfvqjo+LvrS2nhZ83uVIlOqE9FkWDVqzRj2AqwgZtmA2bkqexKSUY+2Lv4rnS97Dn9PuwtbciruM+9lSUIfvsv9x2Qp2WpI623EN4xhLol6WKzBpqbBXkz9ZnZ3R+8LGL3nuCP77itOdTo8hgjqXgyIte7aKGlV8X98MZqI0IQ9yjq73Klap0LIpZgQ7zNeQmT2E62IZgnbJW1mnQ1xaKe/VBmF6cgPXt93D0rTcwMz2FUGUtPR4e6bYTqdMk1XzlpkNHWcGt97vs5KvoM6e5XEl4m5pqvpze3lD709+K18yXv+3S/2w7i6Ejo4ct4pWBUJMZn4nOLtmFmjdPIjYlHA0NSQjRtGBdsQ13/6ZDz/sjWByvwVRoMDb396G+sBBVd7Uozl3ptpOwRPuvmPoqakQmhC22P4WIXpsH8w2jIH/x/idVVxLMfF/gj69E3e//IjSW0uAcRHd2B2n6SIeeU2rmTXTa2kKYNn4PQxkpyNpShAtv/gHaig+gnTaDG0HLpA3TOmDCYMCG117DMmUHGePhmR8zgpnG4sepyAIpsm9pEno/rBDk8++Fhn65/Xcmkz32n0L0V9e6FDM1aXBnd0bS5vUwKp9z7Na9O8X9UZ/9LoYhEZF47NmXkL39u0hZtQZbDnwHUxY9lpdMIvkbylJNqVn9E8rU6enC2PG3kZqfj5hVqzzeNPWMhOqSYl0kRVZtKS8LCRLADRD7+HfCdqGh8fsft8uY0V6AOaaPV+0Vn7O1nTynapf7AJGdii4Xnjwm1uzSp+PMRx6liodKNlZYT6h493cIuXUMmXFjIjJ1t7WoOxcEfVgIVu/ZjeWv/AzBbn7kGABEIHx6ZpiYVYCG1t24314LvdaC8bBI5HxrDXK+/hSiCzdAExY47/BFo/snB4Y9HpMu31QiWgDzOyaFfGbY9OsTPKQOsLLQJCucklsFx6nR8r+/BZ4ZLjwc//3tfwIMAL+pPipwLcTeAAAAAElFTkSuQmCC" alt="wp-puzzle.com logo"></a></div><!-- #comment-image-wrapper --><p class="tsa_param_field_tsa_" style="display:none;">email confirm<span class="required">*</span><input type="text" name="tsa_email_param_field___" id="tsa_email_param_field___" size="30" value="" />
	</p><p class="tsa_param_field_tsa_2" style="display:none;">post date<span class="required">*</span><input type="text" name="tsa_param_field_tsa_3" id="tsa_param_field_tsa_3" size="30" value="2018-04-03 03:32:47" />
	</p><p id="throwsSpamAway">日本語が含まれない投稿は無視されますのでご注意ください。（スパム対策）</p>			</form>
			</div><!-- #respond -->
		</div>

</div><!-- #comments .comments-area -->				<!--
				<nav class="nav-single">
					<h3 class="assistive-text">投稿ナビゲーション</h3>
					<span class="nav-previous"><a href="https://yamakara.com/?p=17650" rel="prev"><span class="meta-nav">&laquo;</span> 【新宿発】雪解けの5月にゆったり山歩きと弦楽四重奏のしらべ「山音はじめ」</a></span>
					<span class="nav-next"><a href="https://yamakara.com/?p=6327" rel="next">装備レンタル付き！日本最北の百名山利尻山登山と礼文島フラワートレッキング4日間 <span class="meta-nav">&raquo;</span></a></span>
				</nav>
				nav-single -->


			
		</div><!-- #content -->


			<div id="secondary" class="widget-area" role="complementary">
			<aside id="execphp-2" class="widget widget_execphp"><h3 class="widget-title">カテゴリ</h3>			<div class="execphpwidget">		<ul>
	<li class="cat-item cat-item-171"><a href="http://www.yamakara.com/?cat=171" >OneDayヤマカラ</a>
</li>
	<li class="cat-item cat-item-136"><a href="http://www.yamakara.com/?cat=136" >ちょっとチャレンジ！なツアー</a>
</li>
	<li class="cat-item cat-item-135"><a href="http://www.yamakara.com/?cat=135" >フライデーやまから</a>
</li>
	<li class="cat-item cat-item-134"><a href="http://www.yamakara.com/?cat=134" >宿泊ツアー</a>
</li>
	<li class="cat-item cat-item-65"><a href="http://www.yamakara.com/?cat=65" >富士山</a>
</li>
	<li class="cat-item cat-item-192"><a href="http://www.yamakara.com/?cat=243" >屋久島ツアー</a>
</li>
	<li class="cat-item cat-item-53"><a href="http://www.yamakara.com/?cat=53" >注目のツアー</a>
</li>
	<li class="cat-item cat-item-170"><a href="http://www.yamakara.com/?cat=170" >海外登山</a>
</li>
	</ul></div>
		</aside>

</div><!-- #secondary -->
	

</div><!--main"-->
@endsection