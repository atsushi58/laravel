@extends('yamakaraweb.layouts.header')

@section('title', '山から日本を見てみよう')

@section('content')

<div class="row">
	<div class="eyecatch">
    	<div id="slick">
    		@foreach($topsliders as $topslider)
    		<div>
    			<p class="caption">{{ $topslider->caption }}</p>
	    		{{ Html::image('topsliders/'. $topslider->filename, null, ['title' => "&lt;h2&gt;". $topslider->title . "&lt;/h2&gt;" . $topslider->caption, 'height' => "280", 'width' => "511"]) }}
    		</div>
    		@endforeach              
    	</div> 
		<script type="text/javascript">
		$(function() {
			$('#slick').slick({
				autoplay: true,
				variableWidth: true,
				slidesToShow: 1,
				swipe: true,
				centerMode: true,
			}); 
		});
		</script>
	</div>
	<div class="section bgwh mt10">
		<h1 class="what">登山ツアーならヤマカラ、無料登山装備レンタル付で安心</h1>
				<div class="headeing_logo"><img src="/images/logo.png" alt="山から日本を見てみよう"></div>
		<div class="headeing_txt">
			<p>登山ツアーならぜひヤマカラ(山から日本を見てみよう)へ。現地集合で500円のワンコインヤマカラ、バスで往復日帰りトレッキングのワンデーヤマカラをはじめ、北アルプス、南アルプス、燕岳、北岳、尾瀬でのテント泊、屋久島縦走、富士山など様々な登山ツアーを開催しています</p>
			<p>登山装備はすべて無料レンタル付。装備がない方も安心してご参加いただけます。</p>
			<p>登山装備を全く持っていないあなたも、雪山装備を持ってなくて雪山に行くのを忠書しているあなたも、沢装備を持っていないけど沢歩きをはじめてみたいあなたも、ぜひヤマカラで登山での新たな一歩を踏み出してください</p>
		</div>
	</div>
	<div class="section bgwh mt10">
		<div id="anchormenu">
			<nav>
			<ul>
			<li><span><a href="#maincontents01">▼ 注目のツアー</a></span></li>
			<li><span><a href="#maincontents02">▼ 現在募集中の宿泊ツアー</a></span></li>
			<li><span><a href="#maincontents03">▼ 「フライデーやまから」(金曜夕方発)</a></span></li>
			<li><span><a href="#maincontents04">▼ 「ワンデーやまから」(日帰り)</a></span></li>
			<li><span><a href="#maincontents05">▼ 現在募集中のワンコインイベント</a></span></li>
			<li><span><a href="#maincontents06">▼ 現在募集中の屋久島ツアー</a></span></li>
			<li><span><a href="#maincontents07">▼ 現在募集中の富士山ツアー</a></span></li>
			<li><span><a href="#maincontents08">▼ ちょっとチャレンジ！なツアー</a></span></li>
			<!--
			<li class="last"><span><a href="#maincontents05">▼ これまでのツアーレポート</a></span>
			-->
			</li>
			</ul>
			</nav>
		</div>

<div id="maincontents01" class="anchorpoint">&nbsp;</div>
<h2 class="bb-gradient"><span></span>注目のツアー</h2>

<div class="to_menu"><a href="#mainsection">▲ メニューへ戻る</a></div>


<div class="clear">&nbsp;</div>
<div id="maincontents02" class="anchorpoint">&nbsp;</div>
<h2 class="bb-gradient"><span></span>現在募集中の宿泊ツアー</h2>

<div class="to_menu"><a href="#mainsection">▲ メニューへ戻る</a></div>


<div id="maincontents03" class="anchorpoint">&nbsp;</div>
<h2 class="bb-gradient"><span></span>現在募集中の「フライデーやまから」(金曜夕方発ツアー)</h2>

<div class="to_menu"><a href="#mainsection">▲ メニューへ戻る</a></div>



<div id="maincontents04" class="anchorpoint">&nbsp;</div>
<h2 class="bb-gradient"><span></span>現在募集中の「ワンデーやまから」(日帰りツアー)</h2><h3><a href="http://yamakara.com/?cat=171">「ワンデーやまから」一覧へ</a></h3><br/>


<div class="to_menu"><a href="#mainsection">▲ メニューへ戻る</a></div>


<div id="maincontents05" class="anchorpoint">&nbsp;</div>
<h2 class="bb-gradient"><span></span>現在募集中のワンコインイベント</h2>

<div class="to_menu"><a href="#mainsection">▲ メニューへ戻る</a></div>

<div id="maincontents06" class="anchorpoint">&nbsp;</div>
<h2 class="bb-gradient"><span></span>現在募集中の屋久島ツアー</h2>
<h3><a href="http://yamakara.com/?cat=243">「屋久島ツアー」一覧へ</a></h3></br>




<div class="to_menu"><a href="#mainsection">▲ メニューへ戻る</a></div>

<div id="maincontents07" class="anchorpoint">&nbsp;</div>
<h2 class="bb-gradient"><span></span>現在募集中の富士山ツアー</h2>
<div class="to_menu"><a href="#mainsection">▲ メニューへ戻る</a></div>

<div id="maincontents08" class="anchorpoint">&nbsp;</div>
<h2 class="bb-gradient"><span></span>ちょっとチャレンジ！なツアー</h2>

<div class="to_menu"><a href="#mainsection">▲ メニューへ戻る</a></div>

</div>
@endsection