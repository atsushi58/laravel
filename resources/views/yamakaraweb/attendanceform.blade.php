@extends('yamakaraweb.layouts.header')

@section('title', '山から日本を見てみよう')

@section('content')
<div class="row">	
	<div class="section bgwh mt15" id="mainsection">
			<h2 class="bb-gradient">{{ $course->name }}お申込みフォーム</h2>
			<p>※お申込み以外のお問合せは、直接メールにて<a href="mailto:yamakara@field-mt.com">yamakara@field-mt.com</a>までご連絡ください。</p>
					{{ Form::open(['route' => 'yamakaraweb.storeeventattendance', 'method'=> 'post', 'class' => 'form-horizontal']) }}
					<div class="formInner">
					<table class="contact">
						<tr>
							<th>お名前(漢字)</th>
							<td>{{ Form::input('name', 'name', null, ['size' => '40']) }}
							<div class="required">{{ $errors->first('name') }}</div></td>
						</tr>
						<tr>
							<th>お名前(カナ)</th>
							<td>{{ Form::input('kana', 'kana', null, ['size' => '40']) }}
							<div class="required">{{ $errors->first('kana') }}</div></td>
						</tr>
						<tr>
							<th>性別</th>
							<td>{{ Form::select('sex', ['女性', '男性'], '女性') }}
								<div class="required">{{ $errors->first('sex') }}</div></td>
						</tr>
						<tr>
							<th>生年月日</th>
							<td>{{ Form::selectYear('year', 1930, 2018, 1960)}}年{{ Form::selectRange('month',1,12)}}月{{ Form::selectRange('day', 1,31)}}日
							</td>
						</tr>
						<tr>
							<th>参加希望のイベント</th>
							<td>{{ Form::select('eventprice_id', $eventprices, ['size' => '40']) }}
							<div class="required">{{ $errors->first('eventprice_id') }}</div></td>
						</tr>
						<tr>
							<th>お支払い方法</th>
							<td>{{ Form::select('eventattendancepaymentmethod_id', $paymentmethods) }}</td>
						</tr>
						<tr>
							<th>参加人数</th>
							<td>大人　{{ Form::select('numofpeople', ['1' => '1人', '2' => '2人', '3' => '3人', '4' => '4人', '5' => '5人'], 1) }}<br>
							   子供　{{ Form::select('numofchildren', ['0'=> '---', '1' => '1人', '2' => '2人', '3' => '3人', '4' => '4人', '5' => '5人']) }}</td>
						</tr>
						<tr>
							<th>メールアドレス</th>
							<td>{{ Form::email('email', null, ['size' => '40']) }}
							<div class="required">{{ $errors->first('email') }}</div>
							<span class="small" >※連絡事項をお送りいたします。携帯メールの場合、PCメールを指定受信 / 拒否設定されている方は「@field-mt.com」ドメインの受信を許可してください。</span ></td>
						</tr>
						<tr>
							<th>メールアドレス（確認用）</th>
							<td>{{ Form::email('email_confirmation', null, ['size' => '40']) }}</td>
						</tr>
						<tr>
							<th>お電話番号</th>
							<td>{{ Form::tel('tel') }}
							<div class="required">{{ $errors->first('tel') }}</div>
							<span class="small" >※基本的にはメールでご連絡いたしますが、緊急時やメールが届かない場合に使用いたします。</span ></td>
						</tr>
						<tr>
							<th>郵便番号（入力すると自動反映されます）</th>
							<td>{{ Form::text('zipcode', null, ['onkeyup'=>"AjaxZip3.zip2addr(this,'','prefecture','address')"]) }}</td>
						</tr>
						<tr>
							<th>住所(都道府県)</th>
							<td>{{ Form::select('prefecture', ['', '北海道','青森県','岩手県','宮城県','秋田県','山形県','福島県','茨城県','栃木県','群馬県','埼玉県','千葉県','東京都','神奈川県','新潟県','富山県','石川県','福井県','山梨県','長野県','岐阜県','静岡県','愛知県','三重県','滋賀県','京都府','大阪府','兵庫県','奈良県','和歌山県','鳥取県','島根県','岡山県','広島県','山口県','徳島県','香川県','愛媛県','高知県','福岡県','佐賀県','長崎県','熊本県','大分県','宮崎県','鹿児島県','沖縄県']) }}
							</td>
						</tr>
						<tr>
							<th>住所</th>
							<td>{{ Form::text('address', null, ['size' => '40']) }}</td>
						</tr>
						<tr>
							<th>ご同行者様情報(ご同行者皆様のお名前フルネーム/漢字・カナ、性別、生年月日<西暦/月/日>をご記入下さい。）</th>
							<td>{{ Form::textarea('friendinfo', null, ['size' => '40x5'])}}</td>
						</tr>
						<tr>
							<th>レンタル品<br>受け取り方法</th>
							<td>{{ Form::select('rentaldelivery_id', $rentaldeliveries) }}</td>
						</tr>
						<tr>
							<th class="bn">レンタル品</th>
							<td class="bn">{{ Form::textarea('rental', null, ['size' => '40x5']) }}
							★新宿店での受け取りは、レインウェアとシューズのみサイズをお書きください。<br>
							★その他のレンタル品は新宿店店頭在庫からお選びください。<br>
							★カラーの指定は承れません。<br>
							★心配な方は事前に新宿店でフィッティング可能です。<br>
							★後から記入可能ですが、出発1週間前までにご記入ください。<br>
							★配送は出発1週間前を目安にお送りします。</td>
						</tr>
						<tr>
							<td class="bn">
							</td>
							<td>
							<div class="required">
							送信に時間がかかります。
							送信ボタンの2度押しをせずにお待ちください。</div>
							</td>
						</tr>
						<tr>
							<td></td>
							<td>							
								{{ Form::submit('申込') }}
								{{ Form::close() }}
							</td>
						</tr>
					</table></div>

					</div><!-- section -->
						</div><!-- #content -->
						</div><!-- #primary -->



								
					<script src="https://ajaxzip3.github.io/ajaxzip3.js" charset="UTF-8"></script>

					@endsection