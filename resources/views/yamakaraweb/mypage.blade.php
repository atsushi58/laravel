@extends('yamakaraweb.layouts.header')

@section('title', '山から日本を見てみよう')

@section('content')

<div class="row">	
	<div class="section bgwh mt15" id="mainsection">
	<h1 class="what" style="padding-left: 45px;">{{ $user->name }}さんのマイページ</h1>
	<h2 class="bb-gradient mt15">お申込みいただいているツアー</h2>
	<table class="type06">
		<thead>
			<tr>
				<th>ツアー名</th>
				<th>出発日</th>
				<th>ステータス</th>
			</tr>
		</thead>
		<tbody>
	@foreach($user->eventattendances as $eventattendance)
			<tr>
				<td>{{ $eventattendance->eventprice->event->course->name }}</td>
				<td>{{ $eventattendance->eventprice->event->Departuredate }}</td>
				<td>{{ $eventattendance->eventattendancestatus->name or '' }}</td>
			</tr>
	@endforeach
		</tbody>
	</table>
</div></div>
@endsection