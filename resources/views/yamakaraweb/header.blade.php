<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- meta robots -->

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Field&Mountain') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

      <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>

  <script>
    $(document).ready(function(){
      $('.slider').bxSlider({
      	auto: true,
      	mode: 'fade',
      	captions: true
      });
    });
  </script>
</head>

<header id="header">
	<div class="header_innner row">
		<div id="logo"><a href="/home"><img src="/images/logo.png" alt="山から日本を見てみよう"></a></div>
		<p class="toContact"><a href="">&raquo; 会員ログイン</a></p>
		<p class="toContact"><a href="http://yamakara.com/?p=1969">&raquo; 全てのツアー一覧</a></p>
		<p class="toContact"><a href="http://yamakara.com/?p=5230">&raquo; お問い合わせ</a></p>
	</div>
</header>
@yield('content')
</div>
<footer id="footer" >

<div class="copy">
<p><a href="http://yamakara.com/?p=3534" style="color:#FFF;">会社案内</a>　　<a href="http://yamakara.com/?p=3507" style="color:#FFF;">旅行業約款・条件書</a></p>
<p>Copyright &copy; <?php echo date("Y"); ?> Field &amp; Mountain All Rights Reserved.</p>
</div>

</footer>
<noscript>
  <iframe src="//b.yjtag.jp/iframe?c=ENqAwow" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
</noscript>
<!--タグここまで-->

</body>
</html>