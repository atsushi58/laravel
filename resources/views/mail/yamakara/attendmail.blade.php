{{ $eventattendance->user->name }} 様{{ "\n" }}

お申込みいただき誠にありがとうございます。{{ "\n" }}

---お申込みいただきました内容 ---{{ "\n" }}
お名前(漢字) : {{ $eventattendance->user->name }}{{ "\n" }}
お名前(カナ)　: {{ $eventattendance->user->kana }}{{ "\n" }}
生年月日 : {{ $eventattendance->user->birthday}}{{ "\n" }}
メールアドレス : {{ $eventattendance->user->email }}{{ "\n" }}
参加希望のイベント {{ $eventattendance->eventprice->Tourname }}{{ "\n" }}
お支払い方法 : {{ $eventattendance->eventattendancepaymentmethod->name }}{{ "\n" }}
参加人数(大人) : {{ $eventattendance->numofpeople }}{{ "\n" }}
参加人数(子供) : {{ $eventattendance->numofchildren }}{{ "\n" }}
同行者情報 : {{ $eventattendance->friendinfo }}{{ "\n" }}
レンタル内容 : {{ $eventattendance->rental }}{{ "\n" }}
備考 : {{ $eventattendance->memo }}{{ "\n" }}



レンタルについて、後日お申込みご希望の方は、こちらのフォームからご記入ください。{{ "\n" }}
http://www.field-mt.org/createrental/{{ $eventattendance->user_id }}/{{ $eventattendance->eventprice_id }}/{{ "\n" }}

･･･Yamakara割が始まりました･････{{ "\n" }} 
「Yamakaraポイントカード」のご提示で{{ "\n" }} 
新宿店にある商品が店頭価格から10％OFFになります。{{ "\n" }} 
※お水、ポカリスウェットなどの飲食品を除きます。{{ "\n" }} 
Yamakaraツアー以外でレンタルをご利用頂く際も10％OFFになります。{{ "\n" }} 
ぜひご利用ください。{{ "\n" }} 

--現在お申し込みいただいているツアー---{{ "\n" }}
@foreach($attendlist as $attend)
{{ $attend->eventprice->Tourname }}{{ $attend->eventattendancestatus->name}}{{ "\n" }}
レンタルのお申し込みはこちらから{{ "\n" }}
http://www.field-mt.org/createrental/{{ $attend->user_id }}/{{ $attend->eventprice_id }}/{{ "\n" }}
キャンセルはこちらから{{ "\n" }}
http://www.field-mt.org/cancelform/{{ $attend->user_id }}/{{ $attend->eventprice_id }}/{{ "\n" }}
@endforeach

{{ "\n" }}


{{ $body }}


