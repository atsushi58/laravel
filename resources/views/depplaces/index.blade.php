@extends('layouts.default')

@section('title', '出発地マスタ')

@section('content')

<h1>出発地マスタ</h1>

<a href="{{ action('DepplacesController@create') }}"><button type="button" class="btn btn-default navbar-btn">新規出発地マスタ</button></a>


<table class="table table-hover table-striped">
	<thead>
	<tr>
		<th>アクション</th>
		<th>出発地名</th>
	</tr>
	</thead>
	<tbody>
	@forelse ($depplaces as $depplace)
	<tr>
		<td class="col-xs-2">
		<a href="{{ action('DepplacesController@edit', $depplace->id) }}">編集</a> | 
		<form action="{{ action('DepplacesController@destroy', $depplace->id) }}" id="form_{{ $depplace->id }}" method="post" style="display:inline">
		    {{ csrf_field() }}
    		{{ method_field('delete') }}
      		<a href="#" data-id="{{ $depplace->id }}" onclick="deleteDepplaces(this);">削除</a></form>
		<td>{{ $depplace->name }}</td>
	</tr>
	@empty

	@endforelse
	</tbody>
</table>
<script>
function deleteDepplaces(e) {
  'use strict';

  if (confirm('本当に削除しますか？')) {
    document.getElementById('form_' + e.dataset.id).submit();
  }
}
</script>


@endsection