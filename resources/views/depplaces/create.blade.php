@extends('layouts.default')

@section('title', '新規出発地マスタ')

@section('content')
<h1>新規出発地マスタ</h1>
<div class="jumbotron">
    {{ Form::open(['action' => 'DepplacesController@store', 'method' => 'post', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-xs-6">
                {{ Form::label('name', '出発地名', ['class' => 'col-2 col-form-label']) }}
                {{ Form::input('name', 'name', null, ['class' => 'form-control col-4']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-xs-2">
            {{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
            {{ Form::close()}}
            </div>
        </div>
</div>
@endsection