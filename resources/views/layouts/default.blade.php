<!doctype html>
<html lang="ja">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title')</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <!--
        <link rel="stylesheet" href="/css/style.css" type="text/css">
        -->

        <!-- Fonts -->
        <!--
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        -->

    </head>
    <body>
    <div class="container">
        <div class="row">
        <ul class="nav nav-tabs">
            <li class="dropdown">
                <li><a href="{{ url('/events') }}">イベントマスタ</a></li>
            </li>
            <li class="dropdown">
                <li><a href="{{ url('/events/index') }}">イベント参加</a></li>
            </li>
            <li class="dropdown">
                <li><a href="{{ url('/courses') }}">登山コース</a></li>
            </li>
            <li class="dropdown">
                <a href="#" data-toggle="dropdown">Yamakara設定<span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li><b>イベントマスタ</b></li>
                    <li><a href="{{ url('/eventtypes') }}">イベントタイプ</a></li>
                    <li><a href="{{ url('/eventstatuses') }}">イベントステータス</a></li>
                    <li><b>イベント参加</b></li>
                    <li><b>登山コース</b></li>
                    <li><a href="{{ url('/courseareas') }}">コースエリア</a></li>
                    <li><a href="{{ url('/courselengths') }}">イベント期間</a></li>
                    <li><a href="{{ url('/depplaces') }}">出発地</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" data-toggle="dropdown">スタッフ<span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{ url('/admins') }}">スタッフ一覧</a></li>
                    <li><a href="{{ url('/shifts') }}">予定シフト一覧</a></li>
                    <li><a href="{{ url('/shifts/past') }}">終了シフト一覧</a></li>
                    <li><a href="{{ url('/timecards/2') }}">タイムカード</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" data-toggle="dropdown">シフト関連設定<span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{ url('/worktimes') }}">開始・終了時間マスタ</a></li>
                    <li><a href="{{ url('/workplaces') }}">勤務地マスタ</a></li>
                    <li><a href="{{ url('/payments') }}">時給マスタ</a></li>
                    <li><a href="{{ url('/tasks') }}">タスクマスタ</a></li>
                    <li><a href="{{ url('roles') }}">役職マスタ</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" data-toggle="dropdown">経理用設定<span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{ url('/banks') }}">銀行マスタ</a></li>
                    <li><a href="{{ url('/workplaces') }}">勤務地マスタ</a></li>
                    <li><a href="{{ url('payments') }}">時給マスタ</a></li>
                    <li><a href="{{ url('roles') }}">役職マスタ</a></li>
                </ul>
            </li>
        </ul>
    @if (session('flash_message'))
    <div class="flash_message" onclick="this.classList.add('hidden')">{{ session('flash_message') }}</div>
    @endif
    @yield('content')
    </div>
    </div>
        <!-- Scripts --><!-- ③ 追加 -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>
