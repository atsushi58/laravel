<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- meta robots -->
    <meta name="robots" content="none">
    <meta name="robots" content="noindex">
    <meta name="robots" content="nofollow">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">



    <title>{{ config('app.name', 'Field&Mountain') }}</title>

    <!-- Styles -->

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.css"/>

<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Field&Mountain') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                    @if(Auth::guard('admin')->check() === true)
                        <li class="dropdown">
                        <a href="#" data-toggle="dropdown">ヤマカラ<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('events.bymonth', [date('Y'), date('m')]) }}">ツアーマスタ</a></li>
                            <li><a href="{{ route('courses.index') }}">コースマスタ</a></li>
                            <li><a href="{{ route('eventattendances.index', [date('Y'), date('m')]) }}">ツアー参加</a></li>
                            <li><a href="{{ route('eventprices.index') }}">ツアー料金マスタ</a></li>
                            <li><a href="{{ route('tctels.index') }}">添乗携帯</a></li>
                            <li><a href="{{ route('yamakara.topsliders') }}">トップスライダー</a></li>
                            <li><a href="{{ route('yamakara.mailmagazines') }}">ヤマカラメルマガ</a></li>
                            <li><a href="{{ route('yamakara.inquiries') }}">問い合わせ一覧</a></li>

                        </ul>
                        </li>
                       <li class="dropdown">
                        <a href="#" data-toggle="dropdown">屋久島<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('yakucourses.index') }}">コースマスタ</a></li>
                            <li><a href="{{ route('yakuactivities.index') }}">アクティビティ</a></li>
                        </ul>
                        </li>
                        <li class="dropdown">
                        <a href="#" data-toggle="dropdown">顧客<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('clients.index') }}">顧客一覧</a></li>
                        </ul>
                        </li>
                        @if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2 || Auth::user()->role_id == 3) 
                        <li class="dropdown">
                        <a href="#" data-toggle="dropdown">スタッフ<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('admin.admins') }}">スタッフ一覧</a></li>
                            <li><a href="{{ route('staff.staffmails') }}">スタッフメール</a></li>
                            <li><a href="{{ route('staff.staffmailcategories') }}">スタッフメールカテゴリ</a></li>
                        </ul>
                        </li>
                        <li class="dropdown">
                        <a href="#" data-toggle="dropdown">シフト<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('admin.shifts', [3, date('Y-m-d')]) }}">浦和のシフト一覧</a></li>
                            <li><a href="{{ route('admin.shifts', [4, date('Y-m-d')]) }}">新宿のシフト一覧</a></li>
                            <li><a href="{{ route('admin.shifts', [5, date('Y-m-d')]) }}">屋久島のシフト一覧</a></li>
                            <li><a href="{{ route('admin.shifts', [2, date('Y-m-d')]) }}">添乗のシフト一覧</a></li>
                        </ul>
                        </li>
                        @if(Auth::user()->role_id == 1)
                        <li class="dropdown">
                        <a href="#" data-toggle="dropdown">経理<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('payments.index') }}">時給・交通費一覧</a></li>
                            <li><a href="{{ route('bankaccounts.index') }}">振込先マスタ</a></li>
                            <li><a href="{{ route('admin.salarylist', [ date('Y'), date('m')]) }}">給与一覧</a></li>
                            <li><a href="{{ route('eventcosts.unpaid') }}">未払ツアーコスト</a></li>
                        </ul>
                        </li>
                        @else
                        @endif
                        <li class="dropdown">
                        <a href="#" data-toggle="dropdown">シフト関連設定<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('admin.worktimes') }}">開始・終了時間マスタ</a></li>
                            <li><a href="{{ route('admin.tasks') }}">タスクマスタ</a></li>
                            <li><a href="{{ route('admin.workplaces') }}">勤務地マスタ</a></li>
                            <li><a href="{{ route('roles.index') }}">役割マスタ</a></li>
                        </ul>
                        </li>
                        <li class="dropdown">
                        <a href="#" data-toggle="dropdown">経理用設定<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('banks.index') }}">銀行マスタ</a></li>
                            <li><a href="{{ route('admin.insurances') }}">健康保険・厚生年金保険料額表</a></li>
                            <li><a href="{{ route('admin.taxes') }}">源泉徴収税額表</a></li>
                            <li><a href="{{ route('balancetypes.index') }}">出入金履歴項目</a></li>
                        </ul>
                        </li>
                        @else
                        <li class="dropdown">
                            <a href="#" data-toggle="dropdown">シフト<span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ route('staff.shifts.thisweek') }}">今週のシフト確認</a></li>
                                <li><a href="{{ route('staff.shifts.nextweek') }}">次週のシフト入力</a></li>
                            </ul>
                        </li>
                        @endif
                        <li class="dropdown">
                        <a href="#" data-toggle="dropdown">レンタル設定<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('lostfoundcategories.index') }}">忘れ物カテゴリマスタ</a></li>
                        </ul>
                        </li>
                        <li class="dropdown">
                        <a href="#" data-toggle="dropdown">ヤマカラ設定<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('courseareas.index') }}">コースエリア</a></li>
                            <li><a href="{{ route('eventtypes.index') }}">ツアータイプ</a></li>
                            <li><a href="{{ route('eventstatuses.index') }}">ツアーステータス</a></li>
                            <li><a href="{{ route('courselengths.index') }}">コース日数</a></li>
                            <li><a href="{{ route('depplaces.index') }}">出発地</a></li>
                            <li><a href="{{ route('eventattendancestatuses.index') }}">ツアー参加ステータス</a></li>
                            <li><a href="{{ route('eventattendancepaymentmethods.index')}}">ツアー参加支払方法</a></li>
                            <li><a href="{{ route('eventattendancepaymentstatuses.index')}}">ツアー参加支払ステータス</a></li>
                            <li><a href="{{ route('tourmailcategories.index') }}">ツアーメールカテゴリ</a></li>
                            <li><a href="{{ route('eventcosttypes.index') }}">ツアーコストタイプ</a></li>
                            <li><a href="{{ route('eventcostcategories.index') }}">ツアーコストカテゴリ</a></li>
                            <li><a href="{{ route('eventcostpaymentstatuses.index') }}">ツアーコスト支払ステータス</a></li>
                            <li><a href="{{ route('photobookstatuses.index') }}">フォトブックステータス</a></li>
                            <li><a href="{{ route('contactlists.index') }}">コンタクト先マスタ</a>
                            <li><a href="{{ route('inquirystatuses.index') }}">問い合わせステータス</a></li>
                            <li><a href="{{ route('inquirytypes.index') }}">問い合わせタイプ</a></li>
                        </ul>
                        </li>
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown">屋久島設定<span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ route('yakuguides.index') }}">ガイドマスタ</a></li>
                            <li><a href="{{ route('yakuactivitytypes.index') }}">アクティビティマスタ</a></li>
                            <li><a href="{{ route('yakuflightarrangestatuses.index') }}">フライト手配ステータス</a></li>
                            <li><a href="{{ route('yakuhostelarrangestatuses.index') }}">宿手配ステータス</a></li>
                            <li><a href="{{ route('yakushiparrangestatuses.index') }}">船手配ステータス</a></li>
                            <li><a href="{{ route('yakucomerequests.index') }}">往路希望便</a></li>
                            <li><a href="{{ route('yakucometokojs.index') }}">往路1</a></li>
                            <li><a href="{{ route('yakucometokums.index') }}">往路2</a></li>
                            <li><a href="{{ route('yakureturnrequests.index') }}">復路希望便</a></li>
                            <li><a href="{{ route('yakureturnfromkums.index') }}">復路1</a></li>
                            <li><a href="{{ route('yakureturnfromkojs.index') }}">復路2</a></li>
                            <li><a href="{{ route('yakuattendancetypes.index') }}">屋久島参加タイプ</a></li>
                        </ul>
                    </li>
                </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guard('admin')->check() === true)
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }}
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ route('admin.logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    ログアウト
                                    </a>
                                <li><a href="{{ route('staff.user') }}">個人情報</a></li>
                                <li><a href="{{ route('staff.salary', [ date('Y'), date('m')]) }}">給与明細</a></li>
                                <li><a href="{{ route('staff.password.changepassword') }}">パスワード変更</a></li>
                                <li><a href="{{ route('timecards', 3) }}">タイムカード</a></li>

                                <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                            </li>
                        @endif
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        @yield('content')
    </div>

    <!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.js"></script>


<script>
    jQuery(function($)
    {
    $(".datatable").DataTable(
        {
            dom: 'B<"wrapper"flipt>',
            stateSave: true,
            buttons: [{extend: 'colvis'},
                        {extend: 'csvHtml5',
                            exportOptions: 
                            {
                                columns: ':visible'
                            }
                        },
                        {extend: 'excelHtml5',
                            exportOptions: 
                            {
                                columns: ':visible'
                            }
                        }, 
                        {extend: 'print'}],
            scrollX: true,
            pageLength: 25,
            lengthMenu: [25,50,100]
        }
    );
    }); 
        jQuery(function($)
    {
    $(".minitable").DataTable(
        {
            paging: false,
            searching: false,
            scrollX: true,
        }
    );
    }); 
</script>
