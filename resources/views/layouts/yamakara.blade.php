<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="登山ツアー,トレッキングツアー,ハイキングツアー,屋久島ツアー" >
    <meta name="description" content="{{ $metadescriotion }}"/>
    <!-- meta robots -->
    @section('description', {{ $metadescriotion }})

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $metatitle or ''}}| 登山ツアー装備無料レンタル付ヤマカラ</title>

    <!-- Styles -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>

<header id="header">
<div class="header_innner row">
<div id="logo"><a href="/home"><img src="/images/logo.png" alt="山から日本を見てみよう"></a></div>
<p class="toContact"><a href="">&raquo; 会員ログイン</a></p>
<p class="toContact"><a href="http://yamakara.com/?p=1969">&raquo; 全てのツアー一覧</a></p>
<p class="toContact"><a href="http://yamakara.com/?p=5230">&raquo; お問い合わせ</a></p>
</div>

</header>
