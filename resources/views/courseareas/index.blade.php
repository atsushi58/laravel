@extends('layouts.default')

@section('title', 'コースエリア')

@section('content')

<h1>コースエリア</h1>

<a href="{{ url('/courseareas/create') }}"><button type="button" class="btn btn-default navbar-btn">新規コースエリア</button></a>


<table class="table table-hover table-striped">
	<thead>
	<tr>
		<th>アクション</th>
		<th>コースエリア名</th>
	</tr>
	</thead>
	<tbody>
	@forelse ($courseareas as $coursearea)
	<tr>
		<td><a href="{{ url('/courseareas', $coursearea->id) }}">詳細</a> | 
		    <a href="{{ action('CourseareasController@edit', $coursearea->id) }}">編集</a> | 
		    <form action="{{ action('CourseareasController@destroy', $coursearea->id) }}" id="form_{{ $coursearea->id }}" method="post" style="display:inline">
		    {{ csrf_field() }}
    		{{ method_field('delete') }}
      		<a href="#" data-id="{{ $coursearea->id }}" onclick="deleteCoursearea(this);">削除</a></form></td>
		<td>{{ $coursearea->name }}</td>
	</tr>
	@empty

	@endforelse
	</tbody>
</table>
<script>
function deleteCoursearea(e) {
  'use strict';

  if (confirm('本当に削除しますか？')) {
    document.getElementById('form_' + e.dataset.id).submit();
  }
}
</script>


@endsection