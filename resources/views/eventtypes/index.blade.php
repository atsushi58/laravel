@extends('layouts.default')

@section('title', 'イベントタイプ')

@section('content')

<h1>イベントタイプ</h1>

<a href="{{ url('/eventtypes/create') }}"><button type="button" class="btn btn-default navbar-btn">新規イベントタイプ</button></a>


<table class="table table-hover table-striped">
	<thead>
	<tr>
		<th>アクション</th>
		<th>イベントタイプ名</th>
	</tr>
	</thead>
	<tbody>
	@forelse ($eventtypes as $eventtype)
	<tr>
		<td><a href="{{ url('/eventtypes', $eventtype->id) }}">詳細</a> | 
		    <a href="{{ action('EventtypesController@edit', $eventtype->id) }}">編集</a> | 
		    <form action="{{ action('EventtypesController@destroy', $eventtype->id) }}" id="form_{{ $eventtype->id }}" method="post" style="display:inline">
		    {{ csrf_field() }}
    		{{ method_field('delete') }}
      		<a href="#" data-id="{{ $eventtype->id }}" onclick="deleteEventtype(this);">削除</a></form></td>
		<td>{{ $eventtype->name }}</td>
	</tr>
	@empty

	@endforelse
	</tbody>
</table>
<script>
function deleteEventtype(e) {
  'use strict';

  if (confirm('本当に削除しますか？')) {
    document.getElementById('form_' + e.dataset.id).submit();
  }
}
</script>


@endsection