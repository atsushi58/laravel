@extends('layouts.app')

@section('title', 'jsonツアー参加マスタ')

@section('content')

<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>ツアー参加マスタ{{ $description }}</h3></div>
		<div class="panel-body">
			<form class="form-horizontal">
			  <div class="form-group">
			    <label for="name" class="control-label col-md-1">ビュー: </label>
			    <div class="col-md-3">
					<select onChange="top.location.href=value" class="form-control">
						<option value="#"></option>
						<option value="{{ route('eventattendances.bydate', date('Y-m-d'))}}">本日のお申し込み一覧</option>
						<option value="{{ route('eventattendances.unpaid', date('Y-m-d'))}}">本日の未払いツアー参加</option>
						<option value="{{ route('eventattendances.point',  [date('Y'), date('m')])}}">ポイント利用参加</option>
						<option value="{{ route('eventattendances.multiple') }}">複数人参加</option>
						@for($i = 1;$i < 13; $i++)
						<option value="{{ route('eventattendances.index', ['2018', $i])}}">2018年{{ $i }}月</option>
						@endfor
						@for($i = 1;$i < 13; $i++)
						<option value="{{ route('eventattendances.index', ['2017', $i])}}">2017年{{ $i }}月</option>
						@endfor
					</select>
				</div>
			  </div>
			</form>
			<table class="eventattendances display nowrap" cellspacing="0" width="100%">
				<script>
					$(document).ready(function() {
         				$('.eventattendances').dataTable( {
           				data: <?php echo $dataSet; ?>,
           				columns: [
           				{ render: function ( data, type, full, meta ) {
                 				var buttonId = full[0];
                 var href = "<a class='btn btn-sm' role='button' href='"+ "" + "eventattendances/edit/" + buttonId + "'>編集</a>";
                 return href;}},
             				{ title: "参加ID" },
             				{ title: "申込STS"},
             				{ title: "支払いSTS"},
             				{ title: "名前" },
             				{ title: "カナ"},
             				{ title: "年齢"},
             				{ title: "出発日" },
             				{ title: "参加ツアー" },
             				{ title: "組" },
             				{ title: "参加メモ" }
           				],
			           /* 好きなようにdatatablesのオプション入れてください */
            dom: 'B<"wrapper"flipt>',
            stateSave: true,
            buttons: [{extend: 'colvis'},
                        {extend: 'csvHtml5',
                            exportOptions: 
                            {
                                columns: ':visible'
                            }
                        },
                        {extend: 'excelHtml5',
                            exportOptions: 
                            {
                                columns: ':visible'
                            }
                        }, 
                        {extend: 'print'}],
            paging: true,
            scrollX: true,
            searching: true,
			         });
			      });
				</script>
			</table>
		</div>
	</div>
</div>
@endsection
