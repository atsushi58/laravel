@extends('layouts.app')

@section('title', 'ツアー参加マスタ')

@section('content')

<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>ツアー参加マスタ{{ $description }}</h3></div>
		<div class="panel-body">
			<form class="form-horizontal">
			  <div class="form-group">
			    <label for="name" class="control-label col-md-1">ビュー: </label>
			    <div class="col-md-3">
					<select onChange="top.location.href=value" class="form-control">
						<option value="#"></option>
						<option value="{{ route('eventattendances.bydate', date('Y-m-d'))}}">本日のお申し込み一覧</option>
						<option value="{{ route('eventattendances.unpaid', date('Y-m-d'))}}">本日の未払いツアー参加</option>
						<option value="{{ route('eventattendances.point',  [date('Y'), date('m')])}}">ポイント利用参加</option>
						<option value="{{ route('eventattendances.multiple') }}">複数人参加</option>
						@for($i = 1;$i < 13; $i++)
						<option value="{{ route('eventattendances.index', ['2018', $i])}}">2018年{{ $i }}月</option>
						@endfor
						@for($i = 1;$i < 13; $i++)
						<option value="{{ route('eventattendances.index', ['2017', $i])}}">2017年{{ $i }}月</option>
						@endfor
					</select>
				</div>
			  </div>
			</form>
			<a href="{{ route('eventattendances.create') }}"><button type="button" class="btn btn-default navbar-btn">新規コース設定</button></a>
			@include('partials.eventattendances', ['eventattendances' => $eventattendances])
		</div>
	</div>
</div>
@endsection
