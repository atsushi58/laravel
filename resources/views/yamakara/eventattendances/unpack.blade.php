@extends('layouts.app')

@section('title', 'ツアー参加マスタ')

@section('content')

<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>yamakaraレンタル:{{ $description }}</h3></div>
		<div class="panel-body">
			<form class="form-horizontal">
			  <div class="form-group">
			    <label for="name" class="control-label col-md-1">ビュー: </label>
			    <div class="col-md-3">
					<select onChange="top.location.href=value" class="form-control">
						<option value="#"></option>
						<option value="{{ route('eventattendances.rentalunpack')}}">今後2週間分の未梱包一覧</option>
						<option value="{{ route('eventattendances.rentalall')}}">今後2週間分のレンタル全一覧</option>
						<option value="{{ route('eventattendances.rentalundeliver')}}">梱包済み未配送一覧</option>
						<option value="{{ route('eventattendances.booked', date("Y-m-d"))}}">本日の来店予約一覧</option>
					</select>
				</div>
			  </div>
			</form>
<table class="datatable display nowrap" cellspacing="0" width="100%">
	<thead>
	<tr>
		<th>アクション</th>	
		<th>出発日</th>
		<th>ツアー</th>	
		<th>お名前</th>
		<th>レンタル受け渡し方法</th>
		<th>レンタル梱包</th>		
		<th>いつものレンタル</th>
		<th>レンタル</th>
		<th>id</th>
		<th>申込STAT</th>
		<th>支払STAT</th>		
		<th>支払方法</th>
		<th>カナ</th>
		<th>料金</th>
		<th>参加人数(大人)</th>
		<th>参加人数(子供)</th>
	</tr>
	</thead>
	<tbody>
	@foreach ($eventattendances as $eventattendance)
	<tr>
		<td><a href="{{ route('eventattendances.editrental', $eventattendance->id) }}">編集</a> | <form action="{{ route('eventattendances.rentalpack', $eventattendance->id) }}" id="rentalpackform_{{ $eventattendance->id }}" method="post" style="display:inline">
						{{ csrf_field() }}
			    		{{ method_field('patch') }}
			  		<a href="#" data-id="{{ $eventattendance->id }}" onclick="rentalpackEventattendance(this);">梱包済</a></form>
  		</td>
  		<td>{{ $eventattendance->eventprice->event->Departuredate }}</a></td>	
  		<td><a href="{{ route('events.show', $eventattendance->eventprice->event->id) }}">{{ $eventattendance->eventprice->event->course->name }}</a></td>
  		<td><a href="{{ route('clients.show', $eventattendance->user->id) }}">{{ $eventattendance->user->name }}様</a></td>	
  		<td>{{ $eventattendance->rentaldelivery->name or '' }}</td>
  		<td>@if($eventattendance->rentalpack == 1){{ '済' }}@endif</td>		
  		<td>{{ $eventattendance->user->defaultrental or '' }}</td>
		<td>{{ $eventattendance->rental }}</td>
		<td><a href="{{ route('eventattendances.show', $eventattendance->id) }}">{{ $eventattendance->id }}</a></td>
		<td>{{ $eventattendance->eventattendancestatus->name or '' }}</td>
		<td>{{ $eventattendance->eventattendancepaymentstatus->name or '' }}</td>
		<td>{{ $eventattendance->eventattendancepaymentmethod->name or '' }}</td>
		<td>{{ $eventattendance->user->kana }}</td>		
		<td>{{ number_format($eventattendance->Sales) }}</td>
		<td>{{ $eventattendance->numofpeople }}</td>
		<td>{{ $eventattendance->numofchildren }}</td>
	</tr>
	@endforeach
	</tbody>
</table>
		</div>
	</div>
</div>
<script>
function rentalpackEventattendance(e) {
  'use strict';
  if (confirm('梱包済みにしますか？')) {
    document.getElementById('rentalpackform_' + e.dataset.id).submit();
  }
}
</script>
@endsection
