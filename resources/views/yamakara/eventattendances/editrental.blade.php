@extends('layouts.app')

@section('title', 'ツアー参加の編集')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>レンタルの編集</h3></div>
            <div class="panel-body">
                {{ Form::open(['route' => ['eventattendances.updaterental', $eventattendance->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
                <div class="form-group row">
                    <div class="col-md-2">
                        {{ Form::label('rentaldelivery_id', 'レンタル受け渡し方法', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::select('rentaldelivery_id', $rentaldeliveries, $eventattendance->rentaldelivery_id, ['class' => 'form-control']) }}
                    </div>
                    <div class="col-md-2">
                        {{ Form::label('bookdate', '新宿来店予約', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::date('bookdate', $eventattendance->bookdate, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                        {{ Form::label('rental', 'レンタル品', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::textarea('rental', $eventattendance->rental, ['class' => 'form-control']) }}
                    </div>                   
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                    {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                    {{ Form::close()}}
                    </div>
                </div>
            </div>
    </div>
</div>
@endsection