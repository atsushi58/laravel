@extends('layouts.app')

@section('title', 'ツアー参加マスタ')

@section('content')

<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>ツアー参加支払い待ち一覧{{ $description }}</h3></div>
		<div class="panel-body">
			<form class="form-horizontal">
			  <div class="form-group">
			    <label for="name" class="control-label col-md-1">ビュー: </label>
			    <div class="col-md-3">
					<select onChange="top.location.href=value" class="form-control">
						<option value="#"></option>
						<option value="{{ route('eventattendances.bydate', date('Y-m-d'))}}">本日のお申し込み一覧</option>
						<option value="{{ route('eventattendances.unpaid', date('Y-m-d'))}}">本日の未払いツアー参加</option>
						<option value="{{ route('eventattendances.point',  [date('Y'), date('m')])}}">ポイント利用参加</option>
						@for($i = 1;$i < 13; $i++)
						<option value="{{ route('eventattendances.index', ['2018', $i])}}">2018年{{ $i }}月</option>
						@endfor
						@for($i = 1;$i < 13; $i++)
						<option value="{{ route('eventattendances.index', ['2017', $i])}}">2017年{{ $i }}月</option>
						@endfor
					</select>
				</div>
			  </div>
			</form>
			<a href="{{ route('eventattendances.create') }}"><button type="button" class="btn btn-default navbar-btn">新規コース設定</button></a>
<table class="datatable display nowrap" cellspacing="0" width="100%">
	<thead>
	<tr>
		<th>アクション</th>
		<th>id</th>
		<th>申込STAT</th>
		<th>支払STAT</th>		
		<th>支払方法</th>
		<th>お名前</th>
		<th>カナ</th>
		<th>ツアー</th>		
		<th>料金</th>
		<th>参加人数(大人)</th>
		<th>参加人数(子供)</th>

	</tr>
	</thead>
	<tbody>
	@foreach ($eventattendances as $eventattendance)
	<tr>
		<td><a href="{{ route('eventattendances.edit', $eventattendance->id) }}">編集</a> | <form action="{{ route('eventattendances.cancel', $eventattendance->id) }}" id="cancelform_{{ $eventattendance->id }}" method="post" style="display:inline">
			{{ csrf_field() }}
    		{{ method_field('patch') }}
  		<a href="#" data-id="{{ $eventattendance->id }}" onclick="cancelEventattendance(this);">CXL</a></form> | <form action="{{ route('eventattendances.inpay', $eventattendance->id) }}" id="inpayform_{{ $eventattendance->id }}" method="post" style="display:inline">
			{{ csrf_field() }}
    		{{ method_field('patch') }}
  		<a href="#" data-id="{{ $eventattendance->id }}" onclick="inpayEventattendance(this);">入金/引当</a></form>
  	</td>
		<td><a href="{{ route('eventattendances.show', $eventattendance->id) }}">{{ $eventattendance->id }}</a></td>
		<td>{{ $eventattendance->eventattendancestatus->name or '' }}</td>
		<td>{{ $eventattendance->eventattendancepaymentstatus->name or '' }}</td>
		<td>{{ $eventattendance->eventattendancepaymentmethod->name or '' }}</td>
		<td><a href="{{ route('clients.show', $eventattendance->user->id) }}">{{ $eventattendance->user->name }}様</a></td>
		<td>{{ $eventattendance->user->kana }}</td>
		<td><a href="{{ route('events.show', $eventattendance->eventprice->event->id) }}">{{ $eventattendance->eventprice->event->course->name }}</a></td>		
		<td>{{ number_format($eventattendance->Sales) }}</td>
		<td>{{ $eventattendance->numofpeople }}</td>
		<td>{{ $eventattendance->numofchildren }}</td>

		
	</tr>
	@endforeach
	</tbody>
</table>
<script>
function cancelEventattendance(e) {
  'use strict';

  if (confirm('キャンセルにしますか？')) {
    document.getElementById('cancelform_' + e.dataset.id).submit();
  }
}
</script>
<script>
function inpayEventattendance(e) {
  'use strict';

  if (confirm('入金/引当済にしますか？')) {
    document.getElementById('inpayform_' + e.dataset.id).submit();
  }
}
</script>
<script>
function payEventattendance(e) {
  'use strict';

  if (confirm('入金済にしますか？')) {
    document.getElementById('payform_' + e.dataset.id).submit();
  }
}
</script>
		</div>
	</div>
</div>
@endsection
