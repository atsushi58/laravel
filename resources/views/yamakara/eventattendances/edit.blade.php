@extends('layouts.app')

@section('title', 'ツアー参加の編集')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>ツアー参加の編集</h3></div>
            <div class="panel-body">
                {{ Form::open(['route' => ['eventattendances.update', $eventattendance->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
                <div class="form-group row">
                    <div class="col-md-2">
                        {{ Form::label('user_id', '顧客', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::select('user_id', $clients, $eventattendance->user_id, ['class' => 'form-control']) }}
                    </div>
                    <div class="col-md-2">
                        {{ Form::label('group', '組', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::input('group', 'group', $eventattendance->group, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                        {{ Form::label('eventprice_id', '出発地', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::select('eventprice_id', $eventprices, $eventattendance->eventprice_id, ['class' => 'form-control']) }}
                    </div>
                    <div class="col-md-2">
                        {{ Form::label('eventattendancepaymentmethod_id', '支払方法', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::select('eventattendancepaymentmethod_id', $eventattendancepaymentmethods, $eventattendance->eventattendancepaymentmethod_id, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                        {{ Form::label('eventattendancepaymentstatus_id', '支払ステータス', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::select('eventattendancepaymentstatus_id', $eventattendancepaymentstatuses, $eventattendance->eventattendancepaymentstatus_id, ['class' => 'form-control']) }}
                    </div>                    
                    <div class="col-md-2">
                        {{ Form::label('eventattendancestatus_id', '申込ステータス', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::select('eventattendancestatus_id', $eventattendancestatuses, $eventattendance->eventattendancestatus_id, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                        {{ Form::label('numofpeople', '参加人数(大人)', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::input('numpfpeople', 'numofpeople', $eventattendance->numofpeople, ['class' => 'form-control']) }}
                    </div>
                    <div class="col-md-2">
                        {{ Form::label('numofchildren', '参加人数(子供)', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::input('numpfchildren', 'numofchildren', $eventattendance->numofchildren, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group row">
                     <div class="col-md-2">
                        {{ Form::label('rental', 'レンタル品', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::textarea('rental', $eventattendance->rental, ['class' => 'form-control']) }}
                    </div>                   
                    <div class="col-md-2">
                        {{ Form::label('memo', '備考', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::textarea('memo', $eventattendance->memo, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                    {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                    {{ Form::close()}}
                    </div>
                </div>
            </div>
    </div>
</div>
@endsection