@extends('layouts.app')

@section('title', 'ツアー参加詳細')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>{{ $eventattendance->user->name }}の{{ $eventattendance->eventprice->event->course->name }}({{ $eventattendance->eventprice->event->depdate }}発)基本情報</h3></div>
		<div class="panel-body">
			<a href="{{ route('eventattendances.edit', $eventattendance->id) }}"><button type="button" class="btn btn-default navbar-btn">基本情報編集</button></a>
			<div class="row">
				<div class="col-md-2"><label>お名前</label></div>
				<div class="col-md-4">{{ $eventattendance->user->name }}</div>
				<div class="col-md-2"><label>カナ</label></div>
				<div class="col-md-4">{{ $eventattendance->user->kana }}</div>
			</div>
		</div>
	</div>
</div>

@endsection