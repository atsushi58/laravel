@extends('layouts.app')

@section('title', '新規スケジュール編集')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>{{ $coursename }}のスケジュール編集</h3></div>
        <div class="panel-body">
        {{ Form::open(['route' => ['yamakara.course.schedule.update', $schedule->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('date', '×日目', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('number', 'day', $schedule->day, ['class' => 'form-control']) }}
                </div>
            <div class="col-md-6"></div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('schedule', 'スケジュール', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-10">
                    {{ Form::textarea('schedule', $schedule->schedule, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('coursetime', 'コースタイム(分)', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::number('coursetime', $schedule->coursetime, ['class' => 'form-control'])}}
                </div>
                <div class="col-md-2">
                    {{ Form::label('distance', '距離(km)', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('float', 'distance', $schedule->distance, ['class' => 'form-control'])}}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('ascend', '登り累積(km)', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('float', 'ascend', $schedule->ascend, ['class' => 'form-control']) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('descend', '下り累積(km)', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('float', 'descend', $schedule->descend, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2"></div>
                <div class="col-md-10">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
                </div>
            </div>
        </div>
@endsection