@extends('layouts.app')

@section('title', '新規登山コース')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>新規登山コース</h3></div>
        <div class="panel-body">
        {{ Form::open(['route' => 'courses.store', 'method' => 'post', 'class' => 'form-horizontal']) }}
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('subtitle', 'サブタイトル', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('subtitle', 'subtitle', null, ['class' => 'form-control']) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('name', 'コース名', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('name', 'name', null, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('coursearea_id', 'コースエリア', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::select('coursearea_id', $courseareas, null, ['class' => 'form-control']) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('courselength_id', 'コース期間', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::select('courselength_id', $courselengths, null, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('depplace_id', '出発地', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::select('depplace_id', $depplaces, null, ['class' => 'form-control']) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('eventtype_id', 'イベントタイプ', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::select('eventtype_id', $eventtypes, null, ['class' => 'form-control']) }}
                </div>            
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('wpid', 'wordpressID', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('wpid', 'wpid', null, ['class' => 'form-control']) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('metatitle', 'meta title', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('metatitle', 'metatitle', null, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('metakeywords', 'meta keywords(,(半角カンマ)区切り)', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('metakeywords', 'metakeywords', null, ['class' => 'form-control']) }}
                </div>                         
                <div class="col-md-2">
                    {{ Form::label('slug', 'slug', ['class' => 'form-label']) }}
                </div>

                <div class="col-md-4">
                    {{ Form::input('slug', 'slug', null, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">   
                <div class="col-md-2">
                    {{ Form::label('metadescription', 'meta description', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::textarea('metadescription', null, ['class' => 'form-control']) }}
                </div>            
                <div class="col-md-2">
                    {{ Form::label('memo', 'メモ', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::textarea('memo', null, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                {{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection