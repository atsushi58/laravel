@extends('layouts.app')

@section('title', 'コンタクト先解除')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>コンタクト先解除</h3></div>
        <div class="panel-body">
        {{ Form::open(['route' => ['courses.updatecontacts', $course->id], 'method' => 'post', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('contactlists', 'コンタクト先一覧')}}</div>
            <div class="col-md-4">{{ Form::select('contactlist[]', $contactlists, null, ['class' => 'form-control', 'multiple'=>'multiple']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
            {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
            {{ Form::close()}}
            </div>
        </div>
    </div>
</div>
@endsection