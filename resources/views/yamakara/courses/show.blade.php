@extends('layouts.app')

@section('title', '登山コース詳細')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>コース詳細:{{ $course->name }}の基本情報</h3></div>
		<div class="panel-body">
			<a href="{{ route('courses.edit', $course->id) }}"><button type="button" class="btn btn-default navbar-btn">HP基本情報編集</button></a>
			<div class="row">
				<div class="col-md-2"><label>サブタイトル</label></div>
				<div class="col-md-4">{{ $course->subtitle or '' }}</div>
				<div class="col-md-2"><label>コース名</label></div>
				<div class="col-md-4">{{ $course->name or '' }}</div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>エリア</label></div>
				<div class="col-md-4">{{ $course->coursearea->name }}</div>
				<div class="col-md-2"><label>コース日数</label></div>
				<div class="col-md-4">{{ $course->courselength->name }}</div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>出発地</label></div>
				<div class="col-md-4">{{ $course->depplace->name }}</div>
				<div class="col-md-2"><label>イベントタイプ</label></div>
				<div class="col-md-4">{{ $course->eventtype->name }}</div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>wordpress ID</label></div>
				<div class="col-md-4">{{ $course->wpid or '' }}</div>
				<div class="col-md-2"><label>meta title</label></div>
				<div class="col-md-4">{{ $course->metatitle }}</div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>meta keywords(,(半角カンマ)区切り)</label></div>
				<div class="col-md-4">{{ $course->metakeywords }}</div>
				<div class="col-md-2"><label>slug</label></div>
				<div class="col-md-4">{{ $course->slug }}</div>

			</div>
			<div class="row">				
				<div class="col-md-2"><label>meta description</label></div>
				<div class="col-md-4">{{ $course->metadescription }}</div>
				<div class="col-md-2"><label>メモ</label></div>
				<div class="col-md-4">{{ $course->memo }}</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>{{ $course->name }}の画像一覧</h3></div>
		<div class="panel-body">
			<a href="{{ route('tourimages.create', $course->id) }}"><button type="button" class="btn btn-default navbar-btn">新規画像登録</button></a>
		<table class="table table-hover table-striped">
			<thead>
			<tr>
				<th>アクション</th>
				<th>画像</th>
				<th>ツアー</th>
				<th>タイトル</th>
				<th>キャプション</th>
				<th>トップスクエア画像</th>
				<th>ツアートップ画像</th>
			</tr>
		</thead><tbody>
			@foreach($course->tourimages as $tourimage)
			<tr>
				<td><form action="{{ route('tourimages.destroy', $tourimage->id) }}" id="form_{{ $tourimage->id }}" method="post" style="display:inline">
						    {{ csrf_field() }}
				    		{{ method_field('delete') }}
				      		<a href="#" data-id="{{ $tourimage->id }}" onclick="deleteTourimage(this);">削除</a></form></td>
				<td>{{ Html::image('tourimages/thumbnail/'. $tourimage->filename) }}</td>
				<td>{{ $tourimage->course->name }}</td>
				<td>{{ $tourimage->title }}</td>
				<td>{{ $tourimage->caption }}</td>
				<td>{{ $tourimage->topsquare }}</td>
				<td>{{ $tourimage->bigimage }}</td>
			</tr>
			@endforeach
			</tbody>
		</table>
		</div>
	</div>
</div>
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>{{ $course->name }}のスケジュール</h3></div>
		<div class="panel-body">
			<a href="{{ route('schedules.create', $course->id) }}"><button type="button" class="btn btn-default navbar-btn">新規スケジュール登録</button></a>
		<table class="table table-hover table-striped">
			<thead>
				<tr>
					<th>アクション</th>
					<th>×日目</th>
					<th>スケジュール</th>
					<th>コースタイム(分)</th>
					<th>距離(km)</th>
					<th>登り累積(km)</th>
					<th>下り累積(km)</th>
				</tr>
			</thead>
			<tbody>
				@foreach($course->schedules as $schedule)
				<tr>
					<td class="col-md-2"><a href="{{ route('schedules.edit', $schedule->id) }}">編集</a> | 
					    <form action="{{ route('schedules.destroy', $schedule->id) }}" id="destroyscheduleform_{{ $schedule->id }}" method="post" style="display:inline">
					    {{ csrf_field() }}
			    		{{ method_field('delete') }}
			      		<a href="#" data-id="{{ $schedule->id }}" onclick="deleteSchedule(this);">削除</a></form></td>
					<td>{{ $schedule->day }}日目</td>
					<td>{{ $schedule->schedule }}</td>
					<td>{{ $schedule->coursetime }}</td>
					<td>{{ $schedule->distance }}</td>
					<td>{{ $schedule->ascend }}</td>
					<td>{{ $schedule->descend }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		</div>
	</div>
</div>
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>{{ $course->name }}のコンタクト先一覧</h3></div>
		<div class="panel-body">
			<a href="{{ route('contactlists.index') }}"><button type="button" class="btn btn-default navbar-btn">コンタクト先全一覧</button></a>
			<a href="{{ route('courses.createcontacts', $course->id) }}"><button type="button" class="btn btn-default navbar-btn">一覧からコースコンタクト先に追加</button></a>
			@include('partials.yamakara.contactlists', ['contactlists' => $course->contactlists])
		</div>
	</div>
</div>
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>{{ $course->name }}の設定出発日一覧</h3></div>
		<div class="panel-body">
<p> 売上 : {{ number_format($events->sum('Paidsales')) }}円</p>
<a href="{{ route('courses.createevent', $course->id) }}"><button type="button" class="btn btn-default navbar-btn">新規イベントマスタ</button></a>
<table class="table table-hover table-striped">
	<thead>
	<tr>
		<th>アクション</th>
		<th>イベントid</th>
		<th>出発日</th>
		<th>売上</th>
		<th>申込人数(cxl含まず意向確認含)</th>
		<th>出発予定/出発済人数</th>
		<th>定員</th>
		<th>満席率</th>
		<th>担当</th>
		<th>添乗</th>
		<th>イベントステータス</th>
	</tr>
	</thead>
	<tbody>
	@forelse ($events as $event)
	<tr>
		<td><a href="{{ route('events.show', $event->id) }}">詳細</a> | 
		<a href="{{ route('events.edit', $event->id) }}">編集</a> | 
		<a href="{{ route('events.tcset', $event->id) }}">添乗</td>
		<td>{{ $event->id }}</td>
      	<td>{{ $event->Departuredate }}</td>
		<td>{{ number_format($event->Paidsales) }}</td>
		<td>{{ $event->Totalpossibleclients }}</td>
		<td>{{ $event->Activetotalclients }}</td>
		<td>{{ $event->maxpax }}</td>
		<td>{{ $event->Loadfactor }}%</td>
		<td>@if($event->admin->id ==1){{ '' }}@else{{ $event->admin->name }}@endif</td>
		<td>@forelse($event->shifts as $shift){{ $shift->admin->name or '' }} @empty @endforelse </td>
		<td>{{ $event->eventstatus->name}}</td>
	</tr>
	@empty
	@endforelse
	</tbody>
</table>
	</div>
</div></div>
<script>
function deleteTourimage(e) {
  'use strict';
  if (confirm('本当に削除しますか？')) {
    document.getElementById('form_' + e.dataset.id).submit();
  }
}
</script>
<script>
function deleteSchedule(e) {
  'use strict';
  if (confirm('本当に削除しますか？')) {
    document.getElementById('destroyscheduleform_' + e.dataset.id).submit();
  }
}
</script>
@endsection