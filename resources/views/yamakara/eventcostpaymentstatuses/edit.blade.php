@extends('layouts.app')

@section('title', 'イベントコスト支払ステータスの編集')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>イベントコスト支払ステータスの編集</h3></div>
    <div class="panel-body">
        <div class="form-group row">
            {{ Form::open(['route' => ['eventcostpaymentstatuses.update', $eventcostpaymentstatus->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
        <div class="col-md-2"><label>イベントコスト支払ステータス名</label></div>
            <div class="col-md-4">{{ Form::input('name', 'name', $eventcostpaymentstatus->name, ['class' => 'form-control']) }}</div>
            <div class="col-md-6">{{ Form::submit('変更', ['class' => 'btn btn-primary']) }}
            {{ Form::close()}}
    </div>
    </div>
</div>


@endsection