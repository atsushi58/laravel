@extends('layouts.app')

@section('title', 'ツアーコスト支払ステータス')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>ツアーコスト支払ステータス</h3></div>
        <div class="panel-body">
        <a href="{{ route('eventcostpaymentstatuses.create') }}"><button type="button" class="btn btn-default navbar-btn">新規ツアーコスト支払ステータス</button></a>
			<table class="datatable display" cellspacing="0" width="100%">
			<thead>
			<tr>
				<th>アクション</th>
				<th>id</th>
				<th>ツアーコスト支払いステータス</th>
			</tr>
			</thead>
			<tbody>
			@foreach ($eventcostpaymentstatuses as $eventcostpaymentstatus)
			<tr>
				<td><a href="{{ route('eventcostpaymentstatuses.edit', $eventcostpaymentstatus->id) }}">編集</a></td>
		      	<td>{{ $eventcostpaymentstatus->id }}</td>
		      	<td>{{ $eventcostpaymentstatus->name }}</td>
		     </tr>
			@endforeach
			</tbody>
		</table></div></div></div>
@endsection