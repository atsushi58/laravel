@extends('layouts.app')

@section('title', 'ツアーメール送信')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>{{ $event->depdate }}発{{ $event->course->name }} / {{ $pagetitle }}ツアーメール送信</h3></div>
        <div class="panel-body">           
         {{ Form::open(['route' => ['tourmails.storefullmail'], 'method' => 'post', 'class' => 'form-horizontal']) }}
         {{ Form::hidden('event_id', $event_id )}} 
            <div class="form-group row">
                <div class="col-md-2"><label>カテゴリ名</label></div>
                <div class="col-md-4">{{ Form::select('tourmailcategory_id', $tourmailcategories, 6, ['class' => 'form-control']) }}</div>
            </div>
            <div class="form-group row">
                <div class="col-md-2"><label>メールタイトル</label></div>
                <div class="col-md-10">{{ Form::input('title', 'title', null, ['class' => 'form-control']) }}</div>
            </div>
            <div class="form-group row">
                <div class="col-md-2"><label>本文</label></div>
                <div class="col-md-10">{{ Form::textarea('body', null, ['class' => 'form-control']) }}</div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">{{ Form::submit('送信', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}</div>
            </div>
        </div>
    </div>
</div>
@endsection