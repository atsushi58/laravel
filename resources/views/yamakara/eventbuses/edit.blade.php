@extends('layouts.app')

@section('title', 'バス会社の編集')

@section('content')
<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>バス会社の編集</h3></div>
    <div class="panel-body">
    {{ Form::open(['route' => ['eventbuses.update', $eventbus->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
    <div class="form-group row">
            <div class="col-md-2">{{ Form::label('name', 'バス会社名') }}</div>
            <div class="col-md-4">{{ Form::input('name', 'name', $eventbus->name, ['class' => 'form-control']) }}</div>
            <div class="col-md-6">
                {{ Form::submit('変更', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection