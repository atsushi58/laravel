@extends('layouts.app')

@section('title', 'バスマスタ')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>バスマスタ</h3></div>
        <div class="panel-body">
        	<a href="{{ route('eventbuses.create') }}"><button type="button" class="btn btn-default navbar-btn">新規バス会社</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
				<tr>
					<th>アクション</th>
					<th>id</th>
					<th>バス会社名</th>
				</tr>
				</thead>
				<tbody>
				@forelse ($eventbuses as $eventbus)
				<tr>
					<td>
					    <a href="{{ route('eventbuses.edit', $eventbus->id) }}">編集</a>
					</td>
					<td>{{ $eventbus->id }}</td>
					<td>{{ $eventbus->name }}</td>
				</tr>
				@empty
				@endforelse	
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection