@extends('layouts.app')

@section('title', 'フォトブックステータス')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>フォトブックステータス</h3></div>
		<div class="panel-body">
			<a href="{{ route('photobookstatuses.create') }}"><button type="button" class="btn btn-default navbar-btn">新規フォトブックステータス</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>ステータス名</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($photobookstatuses as $photobookstatus)
					<tr>
						<td><a href="{{ route('photobookstatuses.edit', $photobookstatus->id) }}">編集</a></td>
						<td>{{ $photobookstatus->id }}</td>
					    <td>{{ $photobookstatus->name }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection