@extends('layouts.app')

@section('title', 'フォトブックステータス編集')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>フォトブックステータス編集</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['photobookstatuses.update', $photobookstatus->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('name', 'ステータス名')}}</div>
            <div class="col-md-4">{{ Form::input('name', 'name', $photobookstatus->name, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">
                {{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection