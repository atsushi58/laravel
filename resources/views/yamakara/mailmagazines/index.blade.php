@extends('layouts.app')

@section('title', 'ヤマカラメルマガ')

@section('content')

<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>ヤマカラメルマガ</h3></div>
		<div class="panel-body">
			<a href="{{ route('yamakara.mailmagazine.create') }}"><button type="button" class="btn btn-default navbar-btn">新規メールマガジン</button></a>
		<table class="table table-hover table-striped">
			<thead>
			<tr>
				<th class="col-md-1">アクション</th>
				<th class="col-md-3">配信日</th>
				<th class="col-md-2">配信先一覧</th>
				<th class="col-md-3">タイトル</th>
				<th class="col-md-3">内容</th>
			</tr>
			</thead>
			@forelse ($mailmagazines as $mailmagazine)
			<tbody>
			<tr>
				<td></td>
				<td>{{ $mailmagazine->created_at }}</td>
				<td></td>
				<td>{{ $mailmagazine->title }}</td>
				<td>{{ $mailmagazine->body }}</td>
			</tr>
			</tbody>
			@empty
			@endforelse
			</table>
		</div>
	</div>
</div>
<script>
function deleteTopslider(e) {
  'use strict';

  if (confirm('本当に削除しますか？')) {
    document.getElementById('form_' + e.dataset.id).submit();
  }
}
</script>


@endsection