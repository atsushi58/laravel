@extends('layouts.app')

@section('title', 'コンタクトメモ編集')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>コンタクトメモ編集</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['contactlists.updatecontact', $contact->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('body', 'メモ内容')}}</div>
            <div class="col-md-10">{{ Form::textarea('body', $contact->body, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection