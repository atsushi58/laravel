@extends('layouts.app')

@section('title', 'コンタクト先追加')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>コンタクト先追加</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['contactlists.update', $contactlist->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('name', 'コンタクト先名')}}</div>
            <div class="col-md-4">{{ Form::input('name', 'name', $contactlist->name, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('person', '担当者')}}</div>
            <div class="col-md-4">{{ Form::input('person', 'person', $contactlist->person, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('tel', '電話番号')}}</div>
            <div class="col-md-4">{{ Form::input('tel', 'tel', $contactlist->tel, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('email', 'メールアドレス')}}</div>
            <div class="col-md-4">{{ Form::input('email', 'email', $contactlist->email, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('memo', 'メモ')}}</div>
            <div class="col-md-4">{{ Form::input('memo', 'memo', $contactlist->memo, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection