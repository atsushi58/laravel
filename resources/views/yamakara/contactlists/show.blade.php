@extends('layouts.app')

@section('title', 'コンタクト先詳細')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>{{ $contactlist->name }}の詳細</h3></div>
    	    <div class="panel-body">
    	    <div class="row">
    	    	<div class="col-md-2"><label>コンタクト先</label></div>
    	    	<div class="col-md-4">{{ $contactlist->name }}</div>
				<div class="col-md-2"><label>担当者</label></div>
    	    	<div class="col-md-4">{{ $contactlist->person }}</div>
    	    </div>
    	    <div class="row">
    	    	<div class="col-md-2"><label>電話番号</label></div>
    	    	<div class="col-md-4">{{ $contactlist->tel }}</div>
				<div class="col-md-2"><label>メールアドレス</label></div>
    	    	<div class="col-md-4">{{ $contactlist->email }}</div>
    	    </div>
    	    <div class="row">
    	    	<div class="col-md-2"><label>メモ</label></div>
    	    	<div class="col-md-10">{{ $contactlist->memo }}</div>
    	    </div>
    	</div>
    </div>
    <div class="panel panel-default">
    	<div class="panel-heading"><h3>{{ $contactlist->name }}のコンタクト内容</h3></div>
    	    <div class="panel-body">
    	    <a href="{{ route('contactlists.createcontact', $contactlist->id) }}"><button type="button" class="btn btn-default navbar-btn">新規コンタクトメモ</button></a>
    	    @include('partials.yamakara.contacts', ['contacts' => $contactlist->contacts])
    	</div>
    </div>
</div>
@endsection