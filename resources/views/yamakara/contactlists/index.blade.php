@extends('layouts.app')

@section('title', 'コンタクトリスト')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>コンタクトリスト</h3></div>
		<div class="panel-body">
			<a href="{{ route('contactlists.create') }}"><button type="button" class="btn btn-default navbar-btn">新規コンタクト先</button></a>
			@include('partials.yamakara.contactlists', ['contactlists' => $contactlists])
		</div>
	</div>
</div>
@endsection