@extends('layouts.app')

@section('title', 'ツアータイプの編集')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>ツアータイプの編集</h3></div>
            {{ Form::open(['route' => ['eventtypes.update', $eventtype->id], 'method' => 'patch']) }}
        <div class="panel-body">
            <div class="form-group row">
                <div class="col-md-2">{{ Form::label('name', 'ツアータイプ名', ['class' => 'form-control-label']) }}</div>
                <div class="col-md-4">{{ Form::input('name', 'name', $eventtype->name, ['class' => 'form-control']) }}</div>
                <div class="col-md-6">{{ Form::submit('変更', ['class' => 'btn btn-primary']) }}
                                      {{ Form::close()}}</div>
            </div>
        </div>
    </div>
</div>
@endsection