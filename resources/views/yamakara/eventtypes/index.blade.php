@extends('layouts.app')

@section('title', 'ツアータイプ')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>ツアータイプ</h3></div>
        <div class="panel-body">
        	<a href="{{ route('eventtypes.create') }}"><button type="button" class="btn btn-default navbar-btn">新規ツアータイプ</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
				<tr>
					<th>アクション</th>
					<th>id</th>
					<th>ツアータイプ名</th>
				</tr>
				</thead>
				<tbody>
				@forelse ($eventtypes as $eventtype)
				<tr>
					<td>
					    <a href="{{ route('eventtypes.edit', $eventtype->id) }}">編集</a>
					</td>
					<td>{{ $eventtype->id }}</td>
					<td>{{ $eventtype->name }}</td>
				</tr>
				@empty
				@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection