@extends('layouts.app')

@section('title', '問い合わせステータスの追加')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>問い合わせステータスの追加</h3></div>
            {{ Form::open(['route' => ['inquirystatuses.store'], 'method' => 'post']) }}
        <div class="panel-body">
            <div class="form-group row">
                <div class="col-md-2">{{ Form::label('name', '問い合わせステータス名', ['class' => 'form-control-label']) }}</div>
                <div class="col-md-4">{{ Form::input('name', 'name', null, ['class' => 'form-control']) }}</div>
                <div class="col-md-6">{{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
                                      {{ Form::close()}}</div>
            </div>
        </div>
    </div>
</div>
@endsection