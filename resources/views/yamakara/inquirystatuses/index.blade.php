@extends('layouts.app')

@section('title', '問い合わせステータス')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>問い合わせステータス</h3></div>
        <div class="panel-body">			
        	<a href="{{ route('inquirystatuses.create') }}"><button type="button" class="btn btn-default navbar-btn">新規問い合わせステータス</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
				<tr>
					<th>アクション</th>
					<th>id</th>
					<th>問い合わせステータス名</th>
				</tr>
				</thead>
				<tbody>
				@forelse ($inquirystatuses as $inquirystatus)
				<tr>
					<td><a href="{{ route('inquirystatuses.edit', $inquirystatus->id) }}">編集</a></td>
					<td>{{ $inquirystatus->id }}</td>
					<td>{{ $inquirystatus->name }}</td>
				</tr>
				@empty
				@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>


@endsection