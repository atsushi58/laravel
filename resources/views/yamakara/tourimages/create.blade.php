@extends('layouts.app')

@section('title', '新規スケジュール')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>{{ $course->name}}の新規スケジュール設定</h3></div>
        <div class="panel-body">
        {{ Form::open(['route' => 'tourimages.store', 'files' => 'true']) }}
        {{ Form::hidden('course_id', $course->id)}}
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::label('title', 'タイトル') }}
            </div>
            <div class="col-md-4">
                {{ Form::input('title', 'title', null,  ['class' => 'form-control']) }}
            </div>
            <div class="col-md-2">
                {{ Form::label('caption', 'キャプション') }}
            </div>
            <div class="col-md-4">
                {{ Form::input('caption', 'caption', null,  ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
            {{ Form::file('tourimage') }}
            </div> 
            <div class="col-md-6">
            {{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
            {{ Form::close() }}
            </div>
        </div>
        </div>
    </div>
</div>
@endsection