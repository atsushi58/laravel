@extends('layouts.app')

@section('title', 'ツアーメールカテゴリ')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>ツアーメールカテゴリ</h3></div>
        <div class="panel-body">
        	<a href="{{ route('tourmailcategories.create') }}"><button type="button" class="btn btn-default navbar-btn">新規ツアーメールカテゴリ</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
				<tr>
					<th>アクション</th>
					<th>id</th>
					<th>ツアーメールカテゴリ</th>
				</tr>
				</thead>
				<tbody>
				@foreach ($tourmailcategories as $tourmailcategory)
				<tr>
					<td>
						<a href="{{ route('tourmailcategories.edit', $tourmailcategory->id) }}">編集</a>
					</td>
					<td>{{ $tourmailcategory->id }}</td>
			      	<td>{{ $tourmailcategory->name }}</td>
			     </tr>
				@endforeach
				</tbody>
		</table>
	</div>
</div>
</div>
@endsection