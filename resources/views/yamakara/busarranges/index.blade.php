@extends('layouts.app')

@section('title', '出入金履歴')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>出入金履歴</h3></div>
		<div class="panel-body">
			<a href="{{ route('balances.create') }}"><button type="button" class="btn btn-default navbar-btn">新規出入金項目</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>実行日</th>
						<th>顧客名</th>
						<th>出入金タイプ</th>
						<th>ツアー</th>
						<th>金額</th>
						<th>キャンセル料計算待ち</th>
						<th>メモ</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($balances as $balance)
					<tr>
						<td><a href="{{ route('balances.edit', $balance->id) }}">編集</a></td>
						<td>{{ $balance->day }}</td>
						<td>{{ $balance->user->name }}</td>
						<td>{{ $balance->balancetype->name }}</td>
						<td>{{ $balance->eventattendance->Tourname or '' }}</td>
						<td>{{ $balance->deposit }}</td>
						<td>{{ $balance->suspend }}</td>
						<td>{{ $balance->memo }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection