@extends('layouts.app')

@section('title', '出入金項目編集')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>出入金項目編集</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['balances.update', $balance->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('day', '出入金日')}}</div>
            <div class="col-md-4">{{ Form::date('day', $balance->day, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('balancetype_id', '項目')}}</div>
            <div class="col-md-4">{{ Form::select('balancetype_id', $balancetypes, $balance->balancetype_id, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('deposit', '金額')}}</div>
            <div class="col-md-4">{{ Form::input('deposit', 'deposit', $balance->deposit, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('suspend', 'キャンセル待ち金額')}}</div>
            <div class="col-md-4">{{ Form::input('suspend', 'suspend', $balance->suspend, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('memo', 'メモ')}}</div>
            <div class="col-md-4">{{ Form::input('memo', 'memo', $balance->memo, ['class' => 'form-control']) }}</div>
        </div>

            <div class="col-md-2">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection