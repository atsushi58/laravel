@extends('layouts.app')

@section('title', '出入金履歴追加')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>出入金履歴追加</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['balances.store'], 'method' => 'post', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('name', '顧客名')}}</div>
            <div class="col-md-4">{{ Form::select('user_id', $clients, null, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('day', '出入金日')}}</div>
            <div class="col-md-4">{{ Form::date('day', date('Y-m-d'), ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('balancetype_id', '項目')}}</div>
            <div class="col-md-4">{{ Form::select('balancetype_id', $balancetypes, null, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('deposit', '金額')}}</div>
            <div class="col-md-4">{{ Form::input('deposit', null, null, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('memo', 'メモ')}}</div>
            <div class="col-md-4">{{ Form::input('memo', 'memo', null, ['class' => 'form-control']) }}</div>
        </div>

            <div class="col-md-2">
                {{ Form::submit('入金', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection