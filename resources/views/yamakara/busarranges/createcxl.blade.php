@extends('layouts.app')

@section('title', 'キャンセル返金確定')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>キャンセル返金確定</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['balances.storecxl', $balance->id], 'method' => 'post', 'class' => 'form-horizontal']) }}
        {{ Form::hidden('day', date("Y-m-d")) }}
        {{ Form::hidden('user_id', $balance->user_id) }}
        {{ Form::hidden('eventattendance_id', $balance->eventattendance_id) }}
        {{ Form::hidden('balancetype_id', 8) }}
        {{ Form::hidden('suspend', $balance->suspend) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('cxlcharge', 'キャンセルチャージ')}}</div>
            <div class="col-md-4">{{ Form::input('cxlcharge', 'cxlcharge', null, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('memo', 'キャンセル料内容')}}</div>
            <div class="col-md-4">{{ Form::input('memo', 'memo', null, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::submit('設定', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection