@extends('layouts.app')

@section('title', '問い合わせ詳細')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>問い合わせの詳細</h3></div>
    	    <div class="panel-body">
				<div class="row">
					<div class="col-md-2"><label>問い合わせ主</label></div>
					<div class="col-md-4">{{ $inquiry->name }}</div>
					<div class="col-md-2"><label>メールアドレス</label></div>
					<div class="col-md-4">{{ $inquiry->email }}</div>
				</div>
				<div class="row">
					<div class="col-md-2"><label>ステータス</label></div>
					<div class="col-md-4">{{ $inquiry->inquirystatus->name }}</div>
					<div class="col-md-2"><label>問い合わせ種類</label></div>
					<div class="col-md-4">{{ $inquiry->inquirytype->name }}</div>
				</div>
				<div class="row">
					<div class="col-md-2"><label>担当</label></div>
					<div class="col-md-4"></div>
					<div class="col-md-6"></div>
				</div>
				<div class="row">
					<div class="col-md-2"><label>タイトル</label></div>
					<div class="col-md-4">{{ $inquiry->title }}</div>
					<div class="col-md-2"><label>問い合わせ内容</label></div>
					<div class="col-md-4">{{ $inquiry->body }}</div>
				</div>
			<a href="{{ route('admin.admin.create') }}"><button type="button" class="btn btn-default">返信</button></a>
			</div>
		</div>
	</div>
</div>
<script>
function deleteStaff(e) {
  'use strict';

  if (confirm('本当に削除しますか？')) {
    document.getElementById('form_' + e.dataset.id).submit();
  }
}
</script>



@endsection