@extends('layouts.app')

@section('title', '問い合わせ一覧')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>問い合わせ一覧</h3></div>
        <div class="panel-body">
        	<form class="form-horizontal">
				  <div class="form-group">
				    <label for="name" class="control-label col-md-1">ビュー: </label>
				    <div class="col-md-3">
						<select onChange="top.location.href=value" class="form-control">
							<option value="#"></option>
							<option value="{{ route('admin.admins.all') }}">要返信の問い合わせ一覧</option>
							<option value="{{ route('admin.admins')}}">全ての問い合わせ一覧</option>
						</select>
					</div>
				  </div>
				</form>
			<table class="table table-hover table-striped">
				<thead>
				<tr>
					<th class="col-md-1">アクション</th>
					<th class="col-md-1">ステータス	</th>
					<th class="col-md-1">問い合わせ主</th>
					<th class="col-md-2">メール</th>
					<th class="col-md-2">問い合わせ種類</th>
					<th class="col-md-2">タイトル</th>
					<th class="col-md-3">内容</th>
				</tr>
				</thead>
				<tbody>
				@forelse ($inquiries as $inquiry)
				<tr>
					<td><a href="{{ route('yamakara.inquiry.show', $inquiry->id) }}">詳細</a> | 返信</td>
					<td>{{ $inquiry->inquirystatus->name or '' }}</td>
					<td>{{ $inquiry->name }}</td>
					<td>{{ $inquiry->email }}</td>
					<td>{{ $inquiry->inquirytype->name or '' }}</td>
					<td>{{ $inquiry->title }}</td>
					<td>{{ $inquiry->body }}</td>
				</tr>
				@empty
				@endforelse
				</tbody>
			</table>
<script>
function deleteInquirystatus(e) {
  'use strict';

  if (confirm('本当に削除しますか？')) {
    document.getElementById('form_' + e.dataset.id).submit();
  }
}
</script>


@endsection