@extends('layouts.app')

@section('title', '問い合わせタイプ')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>問い合わせタイプ</h3></div>
        <div class="panel-body">
        	<a href="{{ route('inquirytypes.create') }}"><button type="button" class="btn btn-default navbar-btn">新規問い合わせタイプ</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
				<tr>
					<th>アクション</th>
					<th>id</th>
					<th>問い合わせタイプ名</th>
				</tr>
				</thead>
				<tbody>
				@forelse ($inquirytypes as $inquirytype)
				<tr>
					<td><a href="{{ route('inquirytypes.edit', $inquirytype->id) }}">編集</a></td>
					<td>{{ $inquirytype->id }}</td>
					<td>{{ $inquirytype->name }}</td>
				</tr>
				@empty
				@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>


@endsection