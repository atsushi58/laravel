@extends('layouts.app')

@section('title', '出入金履歴')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>{{ $description }}の出入金履歴</h3></div>
		<div class="panel-body">
			<form class="form-horizontal">
			  <div class="form-group">
			    <label for="name" class="control-label col-md-1">ビュー: </label>
			    <div class="col-md-3">
					<select onChange="top.location.href=value" class="form-control">
						<option value="#"></option>
						<option value="{{ route('balances.index') }}">全ての出入金履歴</option>
						@for($i = 1;$i < 13; $i++)
						<option value="{{ route('balances.bymonth', ['2018', $i])}}">2018年{{ $i }}月</option>
						@endfor
						@for($i = 1;$i < 13; $i++)
						<option value="{{ route('balances.bymonth', ['2017', $i])}}">2017年{{ $i }}月</option>
						@endfor
					</select>
				</div>
			  </div>
			</form>
			<table class="datatable display nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>実行日</th>
						<th>顧客名</th>
						<th>出入金タイプ</th>
						<th>ツアー</th>
						<th>金額</th>
						<th>キャンセル料計算待ち</th>
						<th>メモ</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($balances as $balance)
					<tr>
						<td><a href="{{ route('balances.edit', $balance->id) }}">編集</a></td>
						<td>{{ $balance->day }}</td>
						<td>{{ $balance->user->name or '' }}</td>
						<td>{{ $balance->balancetype->name }}</td>
						<td>{{ $balance->eventattendance->Tourname or '' }}</td>
						<td>{{ $balance->deposit }}</td>
						<td>{{ $balance->suspend }}</td>
						<td>{{ $balance->memo }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection