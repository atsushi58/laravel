@extends('layouts.app')

@section('title', 'ツアー引き当て追加')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>{{ $client->name }}様 / ツアー引き当て追加(メールあり)</h3></div>
    <div class="panel-body">
        <p>現在の預り金残高 : {{ number_format($client->Deposit) }}円</p>
        {{ Form::open(['route' => ['balances.storepaywm'], 'method' => 'post', 'class' => 'form-horizontal']) }}
        {{ Form::hidden('user_id', $client->id) }}
        {{ Form::hidden('balancetype_id', 3) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('day', '入金日')}}</div>
            <div class="col-md-4">{{ Form::date('day', date('Y-m-d'), ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('eventattendance_id', '項目')}}</div>
            <div class="col-md-4">{{ Form::select('eventattendance_id', $unpaideventattendances, null, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('memo', 'メモ')}}</div>
            <div class="col-md-4">{{ Form::input('memo', 'memo', null, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::submit('入金', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
        </div>
    </div>
    </div>
</div>
@endsection