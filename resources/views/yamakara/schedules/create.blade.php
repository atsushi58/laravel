@extends('layouts.app')

@section('title', '新規スケジュール')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>{{ $course->name}}の新規スケジュール設定</h3></div>
        <div class="panel-body">
        {{ Form::open(['route' => ['schedules.store'], 'method' => 'post', 'class' => 'form-horizontal']) }}
        {{ Form::hidden('course_id', $course->id)}}
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('date', '×日目', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('number', 'day', null, ['class' => 'form-control']) }}
                </div>
            <div class="col-md-6"></div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('schedule', 'スケジュール', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-10">
                    {{ Form::textarea('schedule', null, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('coursetime', 'コースタイム(分)', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::number('coursetime', 'coursetime', ['class' => 'form-control'])}}
                </div>
                <div class="col-md-2">
                    {{ Form::label('distance', '距離(km)', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('float', 'distance', null, ['class' => 'form-control'])}}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('ascend', '登り累積(km)', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('float', 'ascend', null, ['class' => 'form-control']) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('descend', '下り累積(km)', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('float', 'descend', null, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2"></div>
                <div class="col-md-10">
                {{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
                </div>
            </div>
        </div>
@endsection