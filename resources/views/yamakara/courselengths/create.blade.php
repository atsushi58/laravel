@extends('layouts.app')

@section('title', 'コース日数追加')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>コース日数追加</h3></div>
        <div class="panel-body">
        {{ Form::open(['route' => ['courselengths.store'], 'method' => 'post', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::label('name', 'コース日数', ['class' => 'col-form-label']) }}
            </div>
            <div class="col-md-4">
                {{ Form::input('name', 'name', null, ['class' => 'form-control']) }}
            </div>
            <div class="col-md-2">
                {{ Form::label('days', '日数', ['class' => 'col-form-label']) }}
            </div>
            <div class="col-md-4">
                {{ Form::input('days', 'days', null, ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
            {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
            {{ Form::close()}}
            </div>
        </div>
    </div>
</div>
@endsection