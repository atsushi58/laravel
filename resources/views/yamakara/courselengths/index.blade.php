@extends('layouts.app')

@section('title', 'コース日数')

@section('content')

<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>コース日数</h3></div>
		<div class="panel-body">
			<a href="{{ route('courselengths.create') }}"><button type="button" class="btn btn-default navbar-btn">コース日数</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
				<tr>
					<th>アクション</th>
					<th>id</th>
					<th>コース日数</th>
					<th>日数</th>
				</tr>
				</thead>
				<tbody>
				@forelse ($courselengths as $courselength)
				<tr>
					<td>
						<a href="{{ route('courselengths.edit', $courselength->id) }}">編集</a></td>
					<td>{{ $courselength->id }}</td>
					<td>{{ $courselength->name }}</td>
					<td>{{ $courselength->days }}</td>
				</tr>
				@empty
				@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
<script>
function deleteCourselength(e) {
  'use strict';

  if (confirm('本当に削除しますか？')) {
    document.getElementById('form_' + e.dataset.id).submit();
  }
}
</script>


@endsection