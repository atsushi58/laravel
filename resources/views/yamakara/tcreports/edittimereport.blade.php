@extends('layouts.app')

@section('title', '添乗レポ通過時間追加')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>添乗レポ通過時間追加</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['tcreports.updatetimereport', $timereport->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('day', 'XX日目')}}</div>
            <div class="col-md-4">{{ Form::input('number', 'day', $timereport->day, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('point', '地点')}}</div>
            <div class="col-md-4">{{ Form::input('point', 'point', $timereport->point, ['class' => 'form-control', 'type' => 'time']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('uptime', '到着時間')}}</div>
            <div class="col-md-4">{{ Form::time('uptime', $timereport->uptime, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('downtime', '出発時間')}}</div>
            <div class="col-md-4">{{ Form::time('downtime', $timereport->downtime, ['class' => 'form-control', 'type' => 'time']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('memo', 'メモ')}}</div>
            <div class="col-md-10">{{ Form::input('memo', 'memo', $timereport->memo, ['class' => 'form-control', 'type' => 'time']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection