@extends('layouts.app')

@section('title', '添乗レポ追加')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>添乗レポ追加</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['tcreports.store'], 'method' => 'post', 'class' => 'form-horizontal']) }}
        {{ Form::hidden('admin_id', Auth::user()->id) }}
        {{ Form::hidden('event_id', $event_id) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('body', 'レポ内容')}}</div>
            <div class="col-md-10">{{ Form::textarea('body', null, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection