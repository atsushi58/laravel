@extends('layouts.app')

@section('title', 'イベント詳細')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>添乗レポ</h3></div>
		<div class="panel-body">
			<table class="minitable display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>アクション</th>
					<th>id</th>
					<th>ツアー名</th>
					<th>出発日</th>
					<th>作成者</th>
					<th>作成日</th>
				</tr>
			</thead>
			<tbody>
				@forelse($tcreports as $tcreport)
				<tr>
					<td><a href="{{ route('tcreports.show', $tcreport->id) }}">詳細</a></td>
					<td>{{ $tcreport->id }}</td>
					<td>{{ $tcreport->event->course->name }}</td>
					<td>{{ $tcreport->event->depdate }}</td>
					<td>{{ $tcreport->admin->name }}</td>
					<td>{{ $tcreport->created_at }}</td>
				</tr>
				@empty
				@endforelse
			</tbody>
			</table>
		</div>
	</div>
</div>
@endsection