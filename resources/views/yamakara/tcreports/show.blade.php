@extends('layouts.app')

@section('title', '添乗レポ')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>{{ $tcreport->event->depdate }}/{{ $tcreport->event->course->name }}のレポ</h3>
			<p><a href="{{ route('events.show', $tcreport->event_id) }}"><<ツアー詳細に戻る</a></div>
		<div class="panel-body">
			<a href="{{ route('tcreports.edit', $tcreport->id) }}"><button type="button" class="btn btn-default navbar-btn">レポ編集</button></a>
			<div class="row">
				<div class="col-md-2"><label>コース名</label></div>
				<div class="col-md-4">{{ $tcreport->event->course->name }}</div>
				<div class="col-md-2"><label>作成者</label></div>
				<div class="col-md-4">{{ $tcreport->admin->name }}</div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>添乗メモ</label></div>
				<div class="col-md-10">{{ $tcreport->body }}</div>
			</div>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading"><h3>{{ $tcreport->event->depdate }}/{{ $tcreport->event->course->name }}レポの通過時間</h3></div>
		<div class="panel-body">
			<a href="{{ route('tcreports.createtimereport', $tcreport->id) }}"><button type="button" class="btn btn-default navbar-btn">新規通過時間</button></a>
			<table class="minitable display nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>XX日目</th>
						<th>到着時間</th>
						<th>出発時間</th>
						<th>地点</th>
						<th>メモ</th>
					</tr>
				</thead>
				<tbody>
					@forelse($tcreport->tctimereports as $timereport)
					<tr>
						<td><a href="{{ route('tcreports.edittimereport', $timereport->id) }}">編集</a></td>
						<td>{{ $timereport->day }}</td>
						<td>{{ $timereport->uptime }}</td>
						<td>{{ $timereport->downtime }}</td>
						<td>{{ $timereport->point }}</td>
						<td>{{ $timereport->memo }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection