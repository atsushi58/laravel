@extends('layouts.app')

@section('title', '顧客統合')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>顧客統合</h3></div>
    	    <div class="panel-body">

                <div class="table-responsive">
    	    	<table class="table nowrap table-hover table-striped">
                <thead>
                    <tr>
                        <th>アクション</th>
                        <th>id</th>
                        <th>お名前</th>
                        <th>カナ</th>
                        <th>メールアドレス</th>
                        <th>電話番号</th>
                        <th>Yamakara本年支払総額</th>
                        <th>Yamakara支払総額</th>
                        <th>Yamakara本年回数</th>
                        <th>Yamakara総回数</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                </table>
               </div>
            </div>
    </div>
</div>
@endsection