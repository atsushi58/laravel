@extends('layouts.app')

@section('title', '顧客一覧')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>顧客一覧</h3></div>
    	    <div class="panel-body">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label for="name" class="control-label col-md-1">ビュー : </label>
                        <div class="col-md-3">
                            <select onChange="top.location.href=value" class="form-control">
                                <option value="#"></option>
                                <option value="{{ route('clients.thisyearpaid', 10000) }}">10万円以上お支払いのお客様</option>
                            </select>
                        </div>
                    </div>
                </form>
            <div class="row">
                {{ Form::open(['method' => 'get']) }} 
                <div class="col-md-2">
                    <a href="{{ route('clients.create') }}"><button type="button" class="btn btn-default">新規顧客</button></a>
                </div>
                <div class="col-md-6">
                    {{ Form::input('検索する', 'search', null, ['class' => 'form-control']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::submit('検索', ['class' => 'btn btn-primary']) }}
                    {{ Form::close() }}
                </div>
            </div>
                <div class="table-responsive">
    	    	<table class="table nowrap table-hover table-striped">
                <thead>
                    <tr>
                        <th>アクション</th>
                        <th>id</th>
                        <th>お名前</th>
                        <th>カナ</th>
                        <th>メールアドレス</th>
                        <th>電話番号</th>
                        <th>Yamakara本年支払総額</th>
                        <th>Yamakara支払総額</th>
                        <th>Yamakara本年回数</th>
                        <th>Yamakara総回数</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($clients as $client)
                    <tr>
                        <td><a href="{{ route('clients.show', $client->id) }}">詳細</a></td>
                        <td>{{ $client->id }}</td>
                        <td>{{ $client->name }}</td>
                        <td>{{ $client->Namelist }}</td>
                        <td>{{ $client->email }}</td>
                        <td>{{ $client->tel }}</td>
                        <td>{{ $client->Thisyearpaid }}</td>
                        <td>{{ $client->Totalpaid }}</td>
                        <td>{{ $client->Thisyearattendance }}</td>
                        <td>{{ $client->Totalattendance }}</td>
                    </tr>
                    @endforeach
                </tbody>
                </table>
               </div>
            </div>
    </div>
</div>

<script>
function deleteStaff(e) {
  'use strict';

  if (confirm('本当に削除しますか？')) {
    document.getElementById('form_' + e.dataset.id).submit();
  }
}
</script>



@endsection