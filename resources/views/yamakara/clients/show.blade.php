@extends('layouts.app')

@section('title', 'イベント詳細')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>{{ $client->name }}の基本情報</h3></div>
		<div class="panel-body">
			<a href="{{ route('clients.edit', $client->id) }}"><button type="button" class="btn btn-default navbar-btn">基本情報編集</button></a>
			<div class="row">
				<div class="col-md-2"><label>お名前</label></div>
				<div class="col-md-4">{{ $client->name }}</div>
				<div class="col-md-2"><label>カナ</label></div>
				<div class="col-md-4">{{ $client->kana }}</div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>メール</label></div>
				<div class="col-md-4">{{ $client->email }}</div>
				<div class="col-md-2"><label>電話番号</label></div>
				<div class="col-md-4">{{ $client->tel }}</div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>性別</label></div>
				<div class="col-md-4">{{ $client->Sexname }}</div>
				<div class="col-md-2"><label>生年月日</label></div>
				<div class="col-md-4">{{ $client->birthday }}</div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>郵便番号</label></div>
				<div class="col-md-4">{{ $client->zipcode }}</div>
				<div class="col-md-2"><label>都道府県</label></div>
				<div class="col-md-4">{{ $client->Prefname }}</div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>住所</label></div>
				<div class="col-md-4">{{ $client->address }}</div>
				<div class="col-md-2"></div>
				<div class="col-md-4"></div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>緊急連絡先(人)</label></div>
				<div class="col-md-4">{{ $client->emcontactperson }}</div>
				<div class="col-md-2"><label>緊急連絡先(関係)</label></div>
				<div class="col-md-4">{{ $client->emcontactrelation }}</div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>緊急連絡先(電話番号)</label></div>
				<div class="col-md-4">{{ $client->emcontacttel }}</div>
				<div class="col-md-2"><label>メモ</label></div>
				<div class="col-md-4">{{ $client->memo }}</div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>顧客メモ</label></div>
				<div class="col-md-4">{{ $client->memo }}</div>
				<div class="col-md-2"><label>いつものレンタル</label></div>
				<div class="col-md-4">{{ $client->defaultrental }}</div>
			</div>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading"><h3>ツアー参加</h3></div>
		<div class="panel-body">
			<a href="{{ route('clients.createeventattendance', $client->id) }}"><button type="button" class="btn btn-default navbar-btn">新規ツアー参加</button></a>
			@include('partials.eventattendances', ['eventattendances' => $eventattendances])
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading"><h3>出入金履歴</h3></div>
		<div class="panel-body">
			@if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2 || Auth::user()->role_id == 3) 
			<a href="{{ route('balances.inbyuser', $client->id) }}"><button type="button" class="btn btn-default navbar-btn">新規入金履歴</button></a>			
			<a href="{{ route('balances.createpay', $client->id) }}"><button type="button" class="btn btn-default navbar-btn">新規ツアー引き当て</button></a>
			@endif
			<a href="{{ route('balances.inbyuserwm', $client->id) }}"><button type="button" class="btn btn-default navbar-btn">新規入金履歴(メールあり)</button></a>

			<a href="{{ route('balances.createpaywm', $client->id) }}"><button type="button" class="btn btn-default navbar-btn">新規ツアー引き当て(メールあり)</button></a>
			<p>現在の預り金残高 : {{ $client->Deposit }}</p>
			<p>現在のキャンセル料計算待ち残高 : {{ $client->Suspend }}</p>
			<p>現在の支払い待ち残高: {{ $client->Goingtopay }}</p>
			<table class="datatable display nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>実行日</th>
						<th>出入金タイプ</th>
						<th>ツアー</th>
						<th>金額</th>
						<th>キャンセル料計算待ち</th>
						<th>メモ</th>
					</tr>
				</thead>
				<tbody>
					@forelse($balances as $balance)
					<tr>
						<td> <form action="{{ route('balances.destroy', $balance->id) }}" id="form_{{ $balance->id }}" method="post" style="display:inline">
						    {{ csrf_field() }}
				    		{{ method_field('delete') }}
				      		<a href="#" data-id="{{ $balance->id }}" onclick="deleteBalance(this);">削除</a></form> | 
							<a href="{{ route('balances.edit', $balance->id) }}">編集</a>
							@if($balance->suspend > 0)
							| <a href="{{ route('balances.createcxl', $balance->id) }}">cxl料設定</a>
							@endif
						<td>{{ $balance->day }}</td>
						<td>{{ $balance->balancetype->name }}</td>
						<td>{{ $balance->eventattendance->Tourname or '' }}</td>
						<td>{{ number_format($balance->deposit) }}</td>
						<td>{{ number_format($balance->suspend) }}</td>
						<td>{{ $balance->memo }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
<script>
function deleteBalance(e) {
  'use strict';
  if (confirm('削除しますか？')) {
    document.getElementById('form_' + e.dataset.id).submit();
  }
}
</script>

@endsection