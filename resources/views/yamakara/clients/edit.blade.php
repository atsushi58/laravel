@extends('layouts.app')

@section('title', '顧客の編集')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>顧客の編集</h3></div>
        <div class="panel-body">
        {{ Form::open(['route' => ['clients.update', $client->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('name', 'お名前', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('name', 'name', $client->name, ['class' => 'form-control']) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('kana', 'カナ', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('kana', 'kana', $client->kana, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('email', 'メール', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('email', 'email', $client->email, ['class' => 'form-control']) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('tel', '電話番号', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('tel', 'tel', $client->tel, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('sex', '性別', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::select('sex', ['女性', '男性'], $client->sex, ['class' => 'form-control']) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('zipcode', '郵便番号', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('zipcode', 'zipcode', $client->zipcode, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('pref', '都道府県', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('pref', 'pref' , $client->pref, ['class' => 'form-control']) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('address', '住所', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('address', 'address', $client->address, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('birthday', '生年月日', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::date('birthday', $client->birthday, ['class' => 'form-control']) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('emcontactperson', '緊急連絡先', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('emcontactperson', 'emcontactperson', $client->emcontactperson, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('emcontactrelation', '緊急連絡先続柄', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('emcontactrelation', 'emcontactrelation', $client->emcontactrelation, ['class' => 'form-control']) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('emcontacttel', '緊急連絡先電話番号', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('emcontacttel', 'emcontacttel', $client->emcontacttel, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('memo', '顧客メモ', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::textarea('memo', $client->memo,  ['class' => 'form-control']) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('defaultrental', 'いつものレンタル', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::textarea('defaultrental', $client->defaultrental,  ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection