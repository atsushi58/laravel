@extends('layouts.app')

@section('title', '個人情報')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>個人情報</h3></div>
    	    <div class="panel-body">
    	    	<div class="row">
    	    		<label class="col-md-2">名前</label>
    	    		<div class="col-md-4">{{ $admin->name }}</div>
    	    		<label class="col-md-2">カナ</label>
    	    		<div class="col-md-4">{{ $admin->kana }}</div>
    	    	</div>
    	    	<div class="row">
    	    		<label class="col-md-2">電話番号</label>
    	    		<div class="col-md-4">{{ $admin->tel }}</div>
    	    		<label class="col-md-2">メールアドレス</label>
    	    		<div class="col-md-4">{{ $admin->email }}</div>
    	    	</div>
    	    	<div class="row">
    	    		<label class="col-md-2">生年月日</label>
    	    		<div class="col-md-4">{{ $admin->birthday }}</div>
    	    		<label class="col-md-2">紹介者</label>
    	    		<div class="col-md-4">{{ $admin->reference }}</div>
    	    	</div>
    	    	<div class="row">
    	    		<label class="col-md-2">役割</label>
    	    		<div class="col-md-4">{{ $admin->role->name }}</div>
    	    		<label class="col-md-2">現役</label>
    	    		<div class="col-md-4">{{ $admin->active }}</div>
    	    	</div>
			</div>
		</div>
	<div class="panel panel-default">
		<div class="panel-heading"><h3>振込銀行口座</h3></div>
    	    <div class="panel-body">
                @if ($admin->bankaccount == null)
                <a href="{{ route('staff.bankaccount.create') }}"><button type="button" class="btn btn-default navbar-btn">振込銀行口座登録</button></a>
                @else
                <a href="{{ route('staff.bankaccount.edit', $admin->bankaccount->id) }}"><button type="button" class="btn btn-default navbar-btn">振込銀行口座編集</button></a>
            <div class="row">
                <label class="col-md-2">銀行</label>
                <div class="col-md-4">{{ $admin->bankaccount->bank->name }}</div>
                <label class="col-md-2">支店</label>
                <div class="col-md-4">{{ $admin->bankaccount->branch }}</div>
            </div>
            <div class="row">
                <label class="col-md-2">口座番号</label>
                <div class="col-md-4">{{ $admin->bankaccount->accountno }}</div>
            </div>
                @endif
        </div>
    </div>
	<div class="panel panel-default">
        <div class="panel-heading"><h3>時給・交通費情報</h3></div>
            
        </div>
    </div>
</div>

<script>
function deleteStaff(e) {
  'use strict';

  if (confirm('本当に削除しますか？')) {
    document.getElementById('form_' + e.dataset.id).submit();
  }
}
</script>



@endsection