@extends('layouts.app')

@section('title', 'レンタル受け渡し編集')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>レンタル受け渡し編集</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['rentaldeliveries.update', $rentaldelivery->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('name', '受け渡し方法')}}</div>
            <div class="col-md-10">{{ Form::input('name', $rentaldelivery->name, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection