@extends('layouts.app')

@section('title', 'レンタル受け渡し')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>レンタル受け渡し</h3></div>
		<div class="panel-body">
		<a href="{{ route('rentaldeliveries.create') }}"><button type="button" class="btn btn-default navbar-btn">新規受け渡し方法</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>アクション</th>
					<th>id</th>
					<th>受け渡し方法</th>
				</tr>
			</thead>
			<tbody>
				@forelse($rentaldeliveries as $rentaldelivery)
				<tr>
					<td><a href="{{ route('rentaldeliveries.edit', $rentaldelivery->id) }}">編集</a></td>
					<td>{{ $rentaldelivery->id }}</td>
					<td>{{ $rentaldelivery->name }}</td>
				</tr>
				@empty
				@endforelse
			</tbody>
			</table>
		</div>
	</div>
</div>
@endsection