@extends('layouts.app')

@section('title', '未払いツアーコスト')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-body">
			<table class="minitable display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>アクション</th>
					<th>出発日</th>
					<th>ツアー名</th>
					<th>担当</th>
					<th>ツアーコストカテゴリ</th>
					<th>ツアーコストタイプ</th>
					<th>ツアーコスト支払ステータス</th>
					<th>支払先</th>
					<th>金額</th>
					<th>内訳/詳細</th>
				</tr>
			</thead>
			<tbody>
				@foreach($eventcosts as $eventcost)
				<tr>
					<td><form action="{{ route('eventcosts.pay', $eventcost->id) }}" id="payform_{{ $eventcost->id }}" method="post" style="display:inline">
			{{ csrf_field() }}
    		{{ method_field('patch') }}
  		<a href="#" data-id="{{ $eventcost->id }}" onclick="payEventcost(this);">支払</a></form></td>
  					<td>{{ $eventcost->event->depdate or '' }}</td>
  					<td>{{ $eventcost->event->course->name or '' }}</td>
  					<td>{{ $eventcost->event->admin->name or '' }}</td>
					<td>{{ $eventcost->eventcostcategory->name }}</td>
					<td>{{ $eventcost->eventcosttype->name }}</td>
					<td>{{ $eventcost->eventcostpaymentstatus->name }}</td>
					<td>{{ $eventcost->payee }}</td>
					<td>{{ number_format($eventcost->amount) }}円</td>
					<td>{{ $eventcost->memo }}</td>
				</tr>
				@endforeach
			</tbody>
			</table>
		</div>
	</div>
</div>
<script>
function payEventcost(e) {
  'use strict';

  if (confirm('入金済にしますか？')) {
    document.getElementById('payform_' + e.dataset.id).submit();
  }
}
</script>
@endsection