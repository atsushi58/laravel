@extends('layouts.app')

@section('title', 'イベントコストの追加')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>イベントコストの追加</h3></div>
    <div class="panel-body">
            {{ Form::open(['route' => ['eventcosts.store'], 'method' => 'post', 'class' => 'form-horizontal']) }}
        {{ Form::hidden('event_id', $event->id) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('eventcostcategory_id', 'ツアーコストカテゴリ')}}</div>
            <div class="col-md-4">{{ Form::select('eventcostcategory_id', $eventcostcategories, null, ['class' => 'form-control']) }}</div>           
            <div class="col-md-2">{{ Form::label('eventcosttype_id', 'ツアーコストタイプ')}}</div>
            <div class="col-md-4">{{ Form::select('eventcosttype_id', $eventcosttypes, null, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('eventcostpaymentstatus_id', 'ツアーコスト支払ステータス')}}</div>
            <div class="col-md-4">{{ Form::select('eventcostpaymentstatus_id', $eventcostpaymentstatuses, null, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('payee', '支払先')}}</div>
            <div class="col-md-4">{{ Form::input('payee', 'payee', null, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">        
            <div class="col-md-2">{{ Form::label('amount', '金額')}}</div>
            <div class="col-md-4">{{ Form::input('amount', 'amount', null, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('memo', '内訳/詳細')}}</div>
            <div class="col-md-4">{{ Form::input('memo', 'memo', null, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
        </div>
    </div></div>


@endsection