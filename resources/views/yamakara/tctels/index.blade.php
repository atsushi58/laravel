@extends('layouts.app')

@section('title', '添乗携帯')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>添乗携帯</h3></div>
		<div class="panel-body">
			<a href="{{ route('tctels.create') }}"><button type="button" class="btn btn-default navbar-btn">新規添乗携帯</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>電話番号</th>
						<th>前のツアー</th>
						<th>次のツアー</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($tctels as $tctel)
					<tr>
						<td><a href="{{ route('tctels.edit', $tctel->id) }}">編集</a></td>
						<td>{{ $tctel->id }}</td>
					    <td>{{ $tctel->num }}</td>
						<td>{{ $tctel->Prevtour }}</td>
						<td>{{ $tctel->Nexttour }}</td>					
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection