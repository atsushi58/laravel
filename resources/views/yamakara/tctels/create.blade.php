@extends('layouts.app')

@section('title', '添乗携帯追加')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>添乗携帯追加</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['tctels.store'], 'method' => 'post', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('num', '電話番号')}}</div>
            <div class="col-md-4">{{ Form::input('num', 'num', null, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">
                {{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection