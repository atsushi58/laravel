@extends('layouts.app')

@section('title', 'イベント参加支払い方法編集')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>イベント参加支払い方法の編集</h3></div>
    <div class="panel-body">
        <div class="form-group row">
            {{ Form::open(['route' => ['yamakara.eventattendancepaymentmethod.update', $eventattendancepaymentmethod->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
        <div class="col-md-2"><label>コース名</label></div>
            <div class="col-md-4">{{ Form::input('name', 'name', $eventattendancepaymentmethod->name, ['class' => 'form-control']) }}</div>
            <div class="col-md-6">{{ Form::submit('変更', ['class' => 'btn btn-primary']) }}
            {{ Form::close()}}
    </div>
    </div>
</div>


@endsection