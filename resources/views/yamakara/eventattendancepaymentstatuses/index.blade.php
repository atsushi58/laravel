@extends('layouts.app')

@section('title', 'イベント参加支払いステータス')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>イベント参加支払いステータス</h3></div>
        <div class="panel-body">
			<table class="minitable display" cellspacing="0" width="100%">
			<thead>
			<tr>
				<th>アクション</th>
				<th>id</th>
				<th>イベント参加支払い方法</th>
			</tr>
			</thead>
			<tbody>
			@foreach ($eventattendancepaymentstatuses as $eventattendancepaymentstatus)
			<tr>
				<td><a href="{{ route('eventattendancepaymentstatuses.edit', $eventattendancepaymentstatus->id) }}">編集</a></td>
		      	<td>{{ $eventattendancepaymentstatus->id }}</td>
		      	<td>{{ $eventattendancepaymentstatus->name }}</td>
		     </tr>
			@endforeach
			</tbody>
		</table>
		</div>
	</div>
</div>
@endsection