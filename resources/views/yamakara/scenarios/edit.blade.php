@extends('layouts.app')

@section('title', 'コストシナリオ編集')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>コストシナリオ編集</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['scenarios.update', $scenario->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('name', 'シナリオ名')}}</div>
            <div class="col-md-4">{{ Form::input('name', 'name', $scenario->name, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('price', '旅行代金(仮)')}}</div>
            <div class="col-md-4">{{ Form::input('price', 'price', $scenario->price, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('maxpax', '定員')}}</div>
            <div class="col-md-4">{{ Form::input('maxpax', 'maxpax', $scenario->maxpax, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection