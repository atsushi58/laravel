@extends('layouts.app')

@section('title', 'コストシナリオ')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>{{ $scenario->course->name }} / {{ $scenario->name }}</h3></div>
		<div class="panel-body">
			<a href="{{ route('scenarios.edit', $scenario->id) }}"><button type="button" class="btn btn-default navbar-btn">基本情報編集</button></a>
			<div class="row">
				<div class="col-md-2"><label>シナリオ名</label></div>
				<div class="col-md-4">{{ $scenario->name }}</div>
				<div class="col-md-2"><label>作成者</label></div>
				<div class="col-md-4">{{ $scenario->admin->name }}</div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>作成日</label></div>
				<div class="col-md-4">{{ $scenario->created_at }}</div>
				<div class="col-md-2"><label>コース</label></div>
				<div class="col-md-4">{{ $scenario->course->name }}</div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>価格</label></div>
				<div class="col-md-4">{{ number_format($scenario->price) }}</div>
				<div class="col-md-2"><label>定員</label></div>
				<div class="col-md-4">{{ $scenario->maxpax }}</div>
			</div>
			<a href="{{ route('scenarios.createcost', $scenario->id) }}"><button type="button" class="btn btn-default navbar-btn">新規コスト</button></a>
			<table class="table table-hover table-striped">
				<thead>
					<tr>
				<th>人数</th>
				@foreach($scenario->scenariocosts as $cost)
				<th>{{ $cost->eventcostcategory->name }}<br>
					支払先 : {{ $cost->payee }}<br>
					コストタイプ : {{ $cost->eventcosttype->name }}<br> 
					<a href="{{ route('scenarios.editcost', $cost->id) }}">編集</a><br>
					<form action="{{ route('scenarios.destroycost', $cost->id) }}" id="form_{{ $cost->id }}" method="post" style="display:inline">
						    {{ csrf_field() }}
				    		{{ method_field('delete') }}
				      		<a href="#" data-id="{{ $cost->id }}" onclick="deleteCost(this);">削除</a></form>
				</th>
				@endforeach
				<th>1人あたりコスト</th>
				<th>利益率</th>
				<th>利益総額</th>
			</tr>
				</thead>
				<tbody>
				@for($i = 1;$i < $scenario->Pax;$i++)
				<tr>
				<td>{{ $i }}人</td>
				<?php $sum = 0; ?>
				@foreach($scenario->scenariocosts as $cost)
				<td>{{ number_format($cost->Costpp[$i]) }}</td>
				<?php $sum += $cost->Costpp[$i]; ?>
				@endforeach
				<td>{{ number_format($sum) }}</td>
				<td>@if(isset($scenario->price)){{ round((1-$sum/$scenario->price)*100, 1) }}%@endif</td>
				<td>@if(isset($scenario->price)){{ number_format($i*($scenario->price-$sum)) }}@endif</td>
				</tr>
				@endfor
				</tbody>
			</table>
		</div>
	</div>
</div>
<script>
function deleteCost(e) {
  'use strict';

  if (confirm('本当に削除しますか？')) {
    document.getElementById('form_' + e.dataset.id).submit();
  }
}
</script>
@endsection