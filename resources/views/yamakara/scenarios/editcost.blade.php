@extends('layouts.app')

@section('title', 'コストの編集')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>コストの編集</h3></div>
    <div class="panel-body">
            {{ Form::open(['route' => ['scenarios.updatecost', $cost->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('eventcostcategory_id', 'ツアーコストカテゴリ')}}</div>
            <div class="col-md-4">{{ Form::select('eventcostcategory_id', $eventcostcategories, null, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('eventcosttype_id', 'ツアーコストタイプ')}}</div>
            <div class="col-md-4">{{ Form::select('eventcosttype_id', $eventcosttypes, $cost->eventcosttype_id, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">        
            <div class="col-md-2">{{ Form::label('amount', '金額')}}</div>
            <div class="col-md-4">{{ Form::input('amount', 'amount', $cost->amount, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('payee', '支払先')}}</div>
            <div class="col-md-4">{{ Form::input('payee', 'payee', $cost->payee, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">     
            <div class="col-md-2">{{ Form::label('limit', '階段状コストの人数(ジャンボタクシーの乗車人数のようなもの)') }}</div>
            <div class="col-md-4">{{ Form::input('limit', 'limit', $cost->limit, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('counttc', 'コストカウントする添乗員数') }}</div>
            <div class="col-md-4">{{ Form::input('counttc', 'counttc', $cost->counttc, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">  
            <div class="col-md-2">{{ Form::label('memo', '内訳/詳細')}}</div>
            <div class="col-md-4">{{ Form::input('memo', 'memo', $cost->memo, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
        </div>
    </div></div>


@endsection