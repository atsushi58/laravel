@extends('layouts.app')

@section('title', 'コストシナリオ作成')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>コストシナリオ作成</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['scenarios.store'], 'method' => 'post', 'class' => 'form-horizontal']) }}
        {{ Form::hidden('admin_id', Auth::user()->id)}}
        {{ Form::hidden('course_id', $course_id) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('name', 'シナリオ名')}}</div>
            <div class="col-md-4">{{ Form::input('name', 'name', null, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('price', '旅行代金(仮)')}}</div>
            <div class="col-md-4">{{ Form::input('price', 'price', null, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('maxpax', '定員')}}</div>
            <div class="col-md-4">{{ Form::input('maxpax', 'maxpax', 40, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection