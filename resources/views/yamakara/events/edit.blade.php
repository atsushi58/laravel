@extends('layouts.app')

@section('title', 'ツアーマスタの編集')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>ツアーマスタの編集</h3></div>
        <div class="panel-body">
        {{ Form::open(['route' => ['events.update', $event->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::label('subtittle', 'サブタイトル', ['class' => 'form-label']) }}
            </div>
            <div class="col-md-4">
                {{ Form::input('subtitle', 'subtitle', $event->subtitle, ['class' => 'form-control']) }}
            </div>
            <div class="col-md-2">
                {{ Form::label('course_id', '登山コース', ['class' => 'form-label']) }}
            </div>
            <div class="col-md-4">
                {{ Form::select('course_id', $courses, $event->course_id, ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::label('admin_id', '企画責任者', ['class' => 'form-label']) }}
            </div>
            <div class="col-md-4">
                {{ Form::select('admin_id', $admins, $event->admin_id, ['class' => 'form-control']) }}
            </div>
            <div class="col-md-2">
                {{ Form::label('depdate', '出発日', ['class' => 'form-label']) }}
            </div>
            <div class="col-md-4">
                {{ Form::date('depdate', $event->depdate, ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::label('eventstatus_id', 'ツアーステータス') }}
            </div>
            <div class="col-md-4">
                {{ Form::select('eventstatus_id', $eventstatuses, $event->eventstatus_id, ['class' => 'form-control col-4']) }}
            </div>
            <div class="col-md-2">
                {{ Form::label('maxpax', '定員') }}
            </div>
            <div class="col-md-4">
                {{ Form::number('maxpax', $event->maxpax, ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::label('eventbus_id', 'バス会社') }}
            </div>
            <div class="col-md-4">
                {{ Form::select('eventbus_id', $eventbuses, $event->eventbus_id, ['class' => 'form-control col-4']) }}
            </div>
        </div>
        <div class="form-group row">            
            <div class="col-md-2">
                {{ Form::label('advancepayment', '前渡金') }}
            </div>
            <div class="col-md-4">
                {{ Form::input('advancepayment', 'advancepayment', $event->advancepayment, ['class' => 'form-control']) }}
            </div>
            <div class="col-md-2">
                {{ Form::label('photobookstatus_id', 'フォトブックステータス', ['class' => 'form-label']) }}
            </div>
            <div class="col-md-4">
                {{ Form::select('photobookstatus_id', $photobookstatuses, $event->photobookstatus_id, ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="form-group row">            
            <div class="col-md-2">
                {{ Form::label('tctel_id', '添乗携帯') }}
            </div>
            <div class="col-md-4">
                {{ Form::select('tctel_id', $tctels, $event->tctel_id, ['class' => 'form-control', 'placeholder' => '携帯を選んでください']) }}
            </div>
            <div class="col-md-2">
                {{ Form::label('tourmailtemplate_id', '案内メールテンプレ', ['class' => 'form-label']) }}
            </div>
            <div class="col-md-4">
                {{ Form::select('tourmailtemplate_id', $tourmailtemplates, $event->tourmailtemplate_id, ['class' => 'form-control', 'placeholder' => '案内メールテンプレを選んでください']) }}
            </div>
        </div>
        <div class="form-group row">            
            <div class="col-md-2">
                {{ Form::label('memo', 'ツアー詳細') }}
            </div>
            <div class="col-md-10">
                {{ Form::textarea('memo', $event->memo, ['class' => 'form-control']) }}
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-2">
            {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
            {{ Form::close()}}
            </div>
        </div>
    </div>
</div>
@endsection