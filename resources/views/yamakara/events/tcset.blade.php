@extends('layouts.app')

@section('title', '新規添乗設定')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>新規添乗設定</h3></div>
        <div class="panel-body">
        {{ Form::open(['route' => ['events.tcstore'], 'method' => 'post', 'class' => 'form-horizontal']) }}
        {{ Form::hidden('event_id', $event->id) }}
        {{ Form::hidden('workday', $event->depdate) }}
        {{ Form::hidden('workplace_id', 2)}}
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::label('admin_id', '添乗', ['class' => 'form-label']) }}
            </div>
            <div class="col-md-4">
                {{ Form::select('admin_id', $tcs, null, ['class' => 'form-control']) }}
            </div>
            <div class="col-md-2">
                {{ Form::label('primarytask_id', '役割', ['class' => 'form-label']) }}
            </div>
            <div class="col-md-4">
                {{ Form::select('primarytask_id', $tasks, null, ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
            {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
            {{ Form::close()}}
            </div>
        </div>
    </div>
</div>
@endsection