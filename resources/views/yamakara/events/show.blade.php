
@extends('layouts.app')

@section('title', 'イベント詳細')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-body"><a href="#hpinfo">HP基本情報</a> | <a href="#depinfo">出発日/旅行代金</a> | <a href="#attendants">参加者情報</a> | <a href="#mail">メール</a> | <a href="#contactlist">コンタクト先</a> | <a href="#cost">ツアーコスト</a> | <a href="#adpay">前渡金清算</a>
		</div>
	</div>
	<div class="panel panel-default" id="hpinfo">
		<div class="panel-heading"><h3>ツアー詳細:{{ $event->Departuredate }}発「{{ $event->subtittle }} {{ $event->course->name }}」の基本情報</h3></div>
		<div class="panel-body">
			<a href="{{ route('events.edit', $event->id) }}"><button type="button" class="btn btn-default navbar-btn">基本情報編集</button></a>
			<div class="row">
				<div class="col-md-2"><label>サブタイトル</label></div>
				<div class="col-md-4">{{ $event->subtittle }}</div>
				<div class="col-md-2"><label>登山コース</label></div>
				<div class="col-md-4"><a href="{{ route('courses.show', $event->course->id) }}">{{ $event->course->name }}</a></div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>出発日</label></div>
				<div class="col-md-4">{{ $event->Departuredate }}</div>
				<div class="col-md-2"><label>ツアーステータス</label></div>
				<div class="col-md-4">{{ $event->eventstatus->name}}</div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>企画担当者</label></div>
				<div class="col-md-4">{{ isset($event->admin) ? $event->admin->name : '未定' }}</div>
				<div class="col-md-2"><label>添乗</label></div>
				<div class="col-md-4">@forelse($event->shifts as $shift){{ $shift->admin->name or '' }} @empty @endforelse</div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>バス会社</label></div>
				<div class="col-md-4">{{ isset($event->eventbus) ? $event->eventbus->name : '未定' }}</div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>定員</label></div>
				<div class="col-md-4">{{ $event->maxpax }}人</div>
				<div class="col-md-2"><label>出発予定/出発済人数</label></div>
				<div class="col-md-4">{{ $event->Activetotalclients }}人</div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>申込総人数(cxl含)</label></div>
				<div class="col-md-4">{{ $event->Totalclients }}人</div>
				<div class="col-md-2"><label>申込人数(cxl含まず意向確認含)</label></div>
				<div class="col-md-4">{{ $event->Totalpossibleclients }}人</div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>満席率</label></div>
				<div class="col-md-4">{{ $event->Loadfactor }}%</div>
				<div class="col-md-2"><label>人気率</label></div>
				<div class="col-md-4">{{ $event->Eventpower }}%</div>
			</div>
			<div class="row">				
				<div class="col-md-2"><label>売上(支払ベース)</label></div>
				<div class="col-md-4">{{ number_format($event->Paidsales) }}円</div>
				<div class="col-md-2"><label>売上(申込ベース)</label></div>
				<div class="col-md-4">{{ number_format($event->Activesales) }}円</div>
			<div class="row">	
			</div>
				<div class="col-md-2"><label>コスト</label></div>
				<div class="col-md-4">{{ number_format($event->Cost) }}円</div>
				<div class="col-md-2"><label>利益額</label></div>
				<div class="col-md-4">{{ number_format($event->Paidsales - $event->Cost) }}円</div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>利益率</label></div>
				<div class="col-md-4">{{ number_format($event->Profitability) }}%</div>
				<div class="col-md-2"><label>前渡金</label></div>
				<div class="col-md-4">{{ number_format($event->advancepayment) }}円</div>
			</div>
			<div class="row">				
				<div class="col-md-2"><label>添乗携帯</label></div>
				<div class="col-md-4">{{ $event->tctel->num or '' }}</div>
				<div class="col-md-2"><label>既定料金</label></div>
				<div class="col-md-4">{{ $event->Defaulteventprice->Nameprice or '' }}</div>
			</div>			
			<div class="row">				
				<div class="col-md-2"><label>最大売上見込</label></div>
				<div class="col-md-4">{{ number_format($event->Totalpotential) }}円</div>
				<div class="col-md-2"><label>案内メールテンプレ</label></div>
				<div class="col-md-4">{{ $event->tourmailtemplate->name or '' }}</div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>フォトブック</label></div>
				<div class="col-md-4">{{ $event->photobookstatus->name or '' }}</div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>ツアー詳細</label></div>
				<div class="col-md-10">{!! nl2br(e($event->memo)) !!}</div>
			</div>
		</div>
	</div>
	<div class="panel panel-default" id="depinfo">
		<div class="panel-heading"><h3>出発日/旅行代金</h3></div>
		<div class="panel-body">
			<a href="{{ route('eventprices.create', $event->id) }}"><button type="button" class="btn btn-default navbar-btn">新規旅行代金設定</button></a>
			<table class="table table-hover table-striped">
			<thead>
				<tr>
					<th>アクション</th>
					<th>出発地</th>
					<th>旅行代金(大人)</th>
					<th>旅行代金(子供)</th>
					<th>設定説明(現地発、など。HP掲載なので短く)</th>
					<th>HP掲載</th>
					<th>申し込み数</th>
				</tr>
			</thead>
			<tbody>
			@foreach($event->eventprices as $eventprice)
			<tr>
				<td><a href="{{ route('eventprices.edit', $eventprice->id) }}">編集</a></td>
				<td>{{ $eventprice->depplace->name }}</td>
				<td>{{ number_format($eventprice->price) }}円</td>
				<td>{{ number_format($eventprice->kidsprice) }}円</td>
				<td>{{ $eventprice->description }}</td>
				<td>@if($eventprice->hpactive ==1){{ '掲載'}}@else{{ '掲載しない' }}@endif
				<td>{{ $eventprice->Clients }}人</td>
			</tr>
			@endforeach
			</tbody>
			</table>
		</div>
	</div>
	<div class="panel panel-default" id="attendants">
		<div class="panel-heading"><h3>参加者情報</h3></div>
		<div class="panel-body">
			<a href="{{ route('events.createeventattendance', $event->id) }}"><button type="button" class="btn btn-default navbar-btn">新規ツアー参加</button></a>
			<a href="{{ route('events.createeventattendancewm', $event->id) }}"><button type="button" class="btn btn-default navbar-btn">新規ツアー参加(メールあり)</button></a>
			<a href="{{ route('events.depevents', $event->id) }}"><button type="button" class="btn btn-default navbar-btn">出発済みにする</button></a>
			<a href="{{ route('events.cancelevents', $event->id) }}"><button type="button" class="btn btn-default navbar-btn">中止にする(ツアーステータスはそのまま)</button></a>

			<p>CXLボタンで自動でキャンセルメールが送信されます</p>
			<table class="datatable display nowrap" cellspacing="0" width="100%">
				<thead>
				<tr>
					<th>アクション</th>
					<th>id</th>
					<th>申込STAT</th>
					<th>支払STAT</th>
					<th>お名前</th>
					<th>カナ</th>
					<th>郵便番号</th>
					<th>都道府県</th>
					<th>住所</th>
					<th>性別</th>		
					<th>参加時年齢</th>
					<th>電話番号</th>
					<th>メール</th>
					<th>組</th>
					<th>誕生日</th>
					<th>出発日</th>
					<th>ツアー</th>
					<th>参加人数(大人)</th>
					<th>参加人数(子供)</th>
					<th>出発地</th>
					<th>料金</th>
					<th>支払済</th>
					<th>支払方法</th>
					<th>顧客メモ</th>
					<th>参加メモ</th>
					<th>レンタル受け渡し方法</th>
					<th>レンタル梱包</th>
					<th>レンタル</th>
					<th>同行者情報</th>
					<th>緊急連絡先</th>
				</tr>
				</thead>
				<tbody>
				@foreach ($event->Clients as $eventattendance)
				<tr>
					<td><a href="{{ route('eventattendances.edit', $eventattendance->id) }}">編集</a> | <form action="{{ route('eventattendances.cancel', $eventattendance->id) }}" id="cancelform_{{ $eventattendance->id }}" method="post" style="display:inline">
						{{ csrf_field() }}
			    		{{ method_field('patch') }}
			  		<a href="#" data-id="{{ $eventattendance->id }}" onclick="cancelEventattendance(this);">CXL</a></form> | <form action="{{ route('eventattendances.attend', $eventattendance->id) }}" id="attendform_{{ $eventattendance->id }}" method="post" style="display:inline">
						{{ csrf_field() }}
			    		{{ method_field('patch') }}
			  		<a href="#" data-id="{{ $eventattendance->id }}" onclick="attendEventattendance(this);">繰上(申込案内再送)</a></form> | <form action="{{ route('eventattendances.attendcheck', $eventattendance->id) }}" id="attendcheckform_{{ $eventattendance->id }}" method="post" style="display:inline">
						{{ csrf_field() }}
			    		{{ method_field('patch') }}
			  		<a href="#" data-id="{{ $eventattendance->id }}" onclick="attendcheckEventattendance(this);">意向確認再送</a></form><br>
			  		<form action="{{ route('eventattendances.rentalpack', $eventattendance->id) }}" id="rentalpackform_{{ $eventattendance->id }}" method="post" style="display:inline">
						{{ csrf_field() }}
			    		{{ method_field('patch') }}
			    	<a href="#" data-id="{{ $eventattendance->id }}" onclick="rentalpackEventattendance(this);">レンタル品梱包/配送済</a></form> | <a href="{{ route('eventattendances.editrental', $eventattendance->id) }}">レンタル編集</a> | <a href="{{ route('referrals.create', $eventattendance->id) }}">ポイント用紹介者</a>
			  	</td>
					<td><a href="{{ route('eventattendances.show', $eventattendance->id) }}">{{ $eventattendance->id }}</a></td>
					<td>{{ $eventattendance->eventattendancestatus->name or '' }}</td>
					<td>{{ $eventattendance->eventattendancepaymentstatus->name or '' }}</td>
					<td><a href="{{ route('clients.show', $eventattendance->user->id) }}">{{ $eventattendance->user->name }}様</a></td>
					<td>{{ $eventattendance->user->kana }}</td>
					<td>{{ $eventattendance->user->zipcode }}</td>
					<td>{{ $eventattendance->user->pref }}</td>
					<td>{{ $eventattendance->user->address }}</td>
					<td>{{ $eventattendance->user->Sexname or $eventattendance->user->sex }}</td>		
					<td>{{ $eventattendance->Clientage }}</td>
					<td>{{ $eventattendance->user->tel }}</td>
					<td>{{ $eventattendance->user->email }}</td>
					<td>{{ $eventattendance->group }}</td>
					<td>{{ $eventattendance->Birthdaycheck }}</td>
					<td>{{ $eventattendance->eventprice->event->depdate }}</td>
					<td><a href="{{ route('events.show', $eventattendance->eventprice->event->id) }}">{{ $eventattendance->eventprice->event->course->name }}</a></td>
					<td>{{ $eventattendance->numofpeople }}</td>
					<td>{{ $eventattendance->numofchildren }}</td>
					<td>{{ $eventattendance->eventprice->depplace->name or '' }}</td>
					<td>{{ number_format($eventattendance->Sales) }}</td>
					<td>{{ number_format($eventattendance->Paid) }}</td>
					<td>{{ $eventattendance->eventattendancepaymentmethod->name or '' }}</td>
					<td>{{ $eventattendance->user->Shortmemo }}</td>
					<td>{{ $eventattendance->memo }}</td>
					<td>{{ $eventattendance->rentaldelivery->name or '' }}</td>
					<td>@if($eventattendance->rentalpack == 1){{ '済' }}@endif</td>
					<td>{{ $eventattendance->rental }}</td>
					<td>{{ $eventattendance->friendinfo }}</td>
					<td>{{ $eventattendance->user->emcontactperson}}({{ $eventattendance->user->emcontactrelation}}){{ $eventattendance->user->emcontacttel}}</td>
				</tr>
				@endforeach
				</tbody>
			</table>
<script>
function cancelEventattendance(e) {
  'use strict';
  if (confirm('キャンセルにしますか？')) {
    document.getElementById('cancelform_' + e.dataset.id).submit();
  }
}
</script>
<script>
function attendEventattendance(e) {
  'use strict';

  if (confirm('参加にしますか？')) {
    document.getElementById('attendform_' + e.dataset.id).submit();
  }
}
</script>
<script>
function attendcheckEventattendance(e) {
  'use strict';
  if (confirm('再送しますか？')) {
    document.getElementById('attendcheckform_' + e.dataset.id).submit();
  }
}
</script>
<script>
function rentalpackEventattendance(e) {
  'use strict';
  if (confirm('梱包済みにしますか？')) {
    document.getElementById('rentalpackform_' + e.dataset.id).submit();
  }
}
</script>
		</div>
	</div>
	<div class="panel panel-default" id="mail">
		<div class="panel-heading"><h3>メール</h3></div>
		<div class="panel-body">
			<a href="{{ route('tourmails.create', $event->id) }}"><button type="button" class="btn btn-default navbar-btn">新規顧客メール</button></a>
			<a href="{{ route('tourmails.createfullmail', $event->id) }}"><button type="button" class="btn btn-default navbar-btn">申込者(含むキャンセル待ち/意向確認中)一斉メール</button></a>
			<a href="{{ route('tourmails.createallmail', $event->id) }}"><button type="button" class="btn btn-default navbar-btn">参加者/参加予定者一斉メール</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>アクション</th>
					<th>カテゴリ</th>
					<th>送信日</th>
					<th>宛先</th>
					<th>タイトル</th>
				</tr>
			</thead>
			<tbody>
				@foreach($event->tourmails as $tourmail)
				<tr>
					<td></td>
					<td>{{ $tourmail->tourmailcategory->name }}</td>
					<td>{{ $tourmail->created_at }}</td>
					<td>{{ $tourmail->to }}</td>
					<td>{{ $tourmail->title }}</td>
				</tr>
				@endforeach
			</tbody>
			</table>
		</div>
	</div>
	<div class="panel panel-default" id="contactlist">
		<div class="panel-heading"><h3>{{ $event->course->name }}のコンタクト先一覧</h3></div>
		<div class="panel-body">
			<a href="{{ route('contactlists.index') }}"><button type="button" class="btn btn-default navbar-btn">コンタクト先全一覧</button></a>
			<a href="{{ route('courses.createcontacts', $event->course->id) }}"><button type="button" class="btn btn-default navbar-btn">一覧からコースコンタクト先に追加</button></a>
			<a href="{{ route('courses.destroycontacts', $event->course->id) }}"><button type="button" class="btn btn-default navbar-btn">コースコンタクト先の解除</button></a>
			<!--
			<a href="{{ route('events.createcontacts', $event->id) }}"><button type="button" class="btn btn-default navbar-btn">このイベントのみのコンタクト先追加</button></a>
			-->
			<table class="table nowrap table-hover table-striped">
				<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>コンタクト先名</th>
						<th>担当者</th>
						<th>電話番号</th>
						<th>メールアドレス</th>
						<th>特記事項</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($contactlists as $contactlist)
					<tr>
						<td>
							<a href="{{ route('contactlists.show', $contactlist->id) }}">詳細</a> | <a href="{{ route('contactlists.edit', $contactlist->id) }}">編集</a> | <a href="{{ route('events.createcontactmemo', [$event->id, $contactlist->id]) }}">メモ追加</a></td>
						<td>{{ $contactlist->id }}</td>
						<td>{{ $contactlist->name }}</td>
						<td>{{ $contactlist->person }}</td>
					    <td>{{ $contactlist->tel }}</td>
					    <td>{{ $contactlist->email }}</td>
					    <td>{{ $contactlist->memo }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading"><h3>{{ $event->course->name }}のコンタクトメモ一覧</h3></div>
		<div class="panel-body">
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
				<tr>
					<th>アクション</th>
					<th>メモ作成日</th>
					<th>コンタクト先</th>
					<th>メモ作成者</th>
					<th>コンタクト内容</th>
				</tr>
			</thead>
			<tbody>
				@forelse($contacts as $contact)
				<tr>
					<td><a href="{{ route('contactlists.editcontact', $contact->id) }}">編集</a></td>
					<td>{{ $contact->created_at }}</td>
					<td>{{ $contact->contactlist->name }}</td>
					<td>{{ $contact->admin->name }}</td>
					<td>{{ $contact->body }}</td>
				</tr>
				@empty
				@endforelse
			</tbody>
			</table>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading"><h3>ツアー手配状況</h3></div>
		<div class="panel-body">
			<table class="minidatatable display">
			<thead>
				<tr>
					<th>出発地</th>					
					<th>アクション</th>
					<th>旅行代金</th>
					<th>申し込み数</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
			</table>
		</div>
	</div>
	<div class="panel panel-default" id="cost">
		<div class="panel-heading"><h3>ツアーコスト</h3></div>
		<div class="panel-body">
			<a href="{{ route('eventcosts.create', $event->id) }}"><button type="button" class="btn btn-default navbar-btn">新規ツアーコスト設定</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>アクション</th>
					<th>id</th>
					<th>ツアーコストカテゴリ</th>
					<th>ツアーコストタイプ</th>
					<th>ツアーコスト支払ステータス</th>
					<th>支払先</th>
					<th>金額</th>
					<th>内訳/詳細</th>
				</tr>
			</thead>
			<tbody>
				@foreach($event->eventcosts as $eventcost)
				<tr>
					<td><a href="{{ route('eventcosts.edit', $eventcost->id) }}">編集</a> | <form action="{{ route('eventcosts.destroy', $eventcost->id) }}" id="form_{{ $eventcost->id }}" method="post" style="display:inline">
						    {{ csrf_field() }}
				    		{{ method_field('delete') }}
				      		<a href="#" data-id="{{ $eventcost->id }}" onclick="deleteEventcost(this);">削除</a></form></td>
					<td>{{ $eventcost->id }}</td>
					<td>{{ $eventcost->eventcostcategory->name }}</td>
					<td>{{ $eventcost->eventcosttype->name }}</td>
					<td>{{ $eventcost->eventcostpaymentstatus->name }}</td>
					<td>{{ $eventcost->payee }}</td>
					<td>{{ number_format($eventcost->amount) }}円</td>
					<td>{{ $eventcost->memo }}</td>
				</tr>
				@endforeach
			</tbody>
			</table>
		</div>
	</div>
	<div class="panel panel-default" id="adpay">
		<div class="panel-heading"><h3>前渡金清算</h3></div>
		<div class="panel-body">
			前渡金 : {{ number_format($event->advancepayment) }}円
			支出額 : {{ number_format($advancepayments->sum('amount')) }}円
			清算額 : {{ number_format($event->advancepayment - $advancepayments->sum('amount')) }}円
			<table class="minitable display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>id</th>
					<th>ツアーコストカテゴリ</th>
					<th>支払先</th>
					<th>金額</th>
					<th>内訳/詳細</th>
				</tr>
			</thead>
			<tbody>
				@foreach($advancepayments as $advancepayment)
				<tr>
					<td>{{ $advancepayment->id }}</td>
					<td>{{ $advancepayment->eventcostcategory->name }}</td>
					<td>{{ $advancepayment->payee }}</td>
					<td>{{ number_format($advancepayment->amount) }}円</td>
					<td>{{ $advancepayment->memo }}</td>
				</tr>
				@endforeach
			</tbody>
			</table>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading"><h3>コストブレイクダウン</h3></div>
		<div class="panel-body">
			<a href="{{ route('scenarios.create', $event->course_id) }}"><button type="button" class="btn btn-default navbar-btn">新規コストシナリオ作成</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>アクション</th>
					<th>id</th>
					<th>シナリオ名</th>
					<th>作成者</th>
					<th>作成日</th>
				</tr>
			</thead>
			<tbody>
				@forelse($event->course->scenarios as $scenario)
				<tr>
					<td><a href="{{ route('scenarios.show', $scenario->id) }}">詳細</a></td>
					<td>{{ $scenario->id }}</td>
					<td>{{ $scenario->name }}</td>
					<td>{{ $scenario->admin->name }}</td>
					<td>{{ $scenario->created_at }}</td>
				</tr>
				@empty
				@endforelse
			</tbody>
			</table>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading"><h3>添乗レポ</h3></div>
		<div class="panel-body">
			<a href="{{ route('tcreports.create', $event->id) }}"><button type="button" class="btn btn-default navbar-btn">新規レポ作成</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>アクション</th>
					<th>id</th>
					<th>出発日</th>
					<th>作成者</th>
					<th>作成日</th>
				</tr>
			</thead>
			<tbody>
				@forelse($event->course->Tcreports as $tcreport)
				<tr>
					<td><a href="{{ route('tcreports.show', $tcreport->id) }}">詳細</a></td>
					<td>{{ $tcreport->id }}</td>
					<td>{{ $tcreport->event->depdate }}</td>
					<td>{{ $tcreport->admin->name }}</td>
					<td>{{ $tcreport->created_at }}</td>
				</tr>
				@empty
				@endforelse
			</tbody>
			</table>
		</div>
	</div>
</div>
<script>
function deleteEventcost(e) {
  'use strict';
  if (confirm('本当に削除しますか？')) {
    document.getElementById('form_' + e.dataset.id).submit();
  }
}
</script>
@endsection