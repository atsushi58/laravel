@extends('layouts.app')

@section('title', 'コンタクトメモ追加')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>{{ $contactlist->name }} / コンタクトメモ追加</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['events.storecontactmemo', $event_id], 'method' => 'post', 'class' => 'form-horizontal']) }}
        {{ Form::hidden('contactlist_id', $contactlist->id) }}
        {{ Form::hidden('admin_id', Auth::user()->id) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('body', 'メモ内容')}}</div>
            <div class="col-md-10">{{ Form::textarea('body', '<<('. $event->depdate . '発)'. $event->course->name .'>>', ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection