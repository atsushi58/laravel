@extends('layouts.app')

@section('title', '新規ツアー参加')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>新規ツアー参加</h3></div>
            <div class="panel-body">
                {{ Form::open(['route' => ['events.storeeventattendancewm', $event->id], 'method' => 'post', 'class' => 'form-horizontal']) }}
                <div class="form-group row">
                    <div class="col-md-2">
                        {{ Form::label('user_id', '顧客', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::select('user_id', $clients, null, ['placeholder' => '顧客を選んでください', 'class' => 'form-control']) }}
                    </div>
                    <div class="col-md-2">
                        {{ Form::label('group', '組', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::input('group', 'group', null, ['placeholder' => '組', 'class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                        {{ Form::label('eventprice_id', '出発地', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::select('eventprice_id', $eventprices, null, ['class' => 'form-control']) }}
                    </div>
                    <div class="col-md-2">
                        {{ Form::label('eventattendancepaymentmethod_id', '支払方法', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::select('eventattendancepaymentmethod_id', $eventattendancepaymentmethods, null, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                        {{ Form::label('eventattendancepaymentstatus_id', '支払ステータス', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::select('eventattendancepaymentstatus_id', $eventattendancepaymentstatuses, null, ['class' => 'form-control']) }}
                    </div>
                    <div class="col-md-2">
                        {{ Form::label('eventattendancestatus_id', '申込ステータス', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::select('eventattendancestatus_id', $eventattendancestatuses, null, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                        {{ Form::label('numofpeople', '参加人数(大人)', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::input('numpfpeople', 'numofpeople', 1, ['class' => 'form-control']) }}
                    </div>
                    <div class="col-md-2">
                        {{ Form::label('numofchildren', '参加人数(子供)', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::input('numofchildren', 'numofchildren', 0, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                        {{ Form::label('memo', '備考', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::textarea('memo', null, ['class' => 'form-control']) }}
                    </div>
                    <div class="col-md-2">
                    {{ Form::submit('登録', ['class' => 'btn btn-primary']) }}
                    {{ Form::close()}}
                    </div>
                </div>
            </div>
    </div>
</div>
@endsection