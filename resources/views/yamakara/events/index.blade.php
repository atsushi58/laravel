@extends('layouts.app')

@section('title', 'ツアーマスタ')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>ツアーマスタ({{ $description }})</h3></div>
        	<div class="panel-body">
			<form class="form-horizontal">
			  <div class="form-group">
			    <label for="name" class="control-label col-md-1">ビュー : </label>
			    <div class="col-md-3">
					<select onChange="top.location.href=value" class="form-control">
						<option value="#"></option>
						<option value="{{ route('events.bystatus', 1) }}">造成中のツアー</option>
						<option value="{{ route('events.bystatus', 2) }}">手配中のツアー</option>
						<option value="{{ route('events.bystatus', 3) }}">HPアップ中のツアー</option>
						<option value="{{ route('events.bystatus', 7) }}">受付終了のツアー</option>
						<option value="{{ route('events.bystatus', 8) }}">不催行(実施不可)のツアー</option>
						<option value="{{ route('events.bystatus', 9) }}">不催行(人数集まらず)のツアー</option>
						<option value="{{ route('events.bystatus', 10) }}">中止(天候等)のツアー</option>
						<option value="{{ route('events.bystatus', 12) }}">クローズドのツアー</option>
						<option value="{{ route('events.bystatus', 6) }}">キャンセル待ちのツアー</option>
						<option value="{{ route('events.byadmin', 2) }}">渡辺担当ツアー</option>
						<option value="{{ route('events.byadmin', 171) }}">中野担当ツアー</option>
						<option value="{{ route('events.byadmin', 172) }}">朝妻担当ツアー</option>
						@for($i = 1;$i < 13; $i++)
						<option value="{{ route('events.bymonth', ['2018', $i])}}">2018年{{ $i }}月</option>
						@endfor						
						@for($i = 1;$i < 13; $i++)
						<option value="{{ route('events.bymonth', ['2019', $i])}}">2019年{{ $i }}月</option>
						@endfor
						@for($i = 1;$i < 13; $i++)
						<option value="{{ route('events.bymonth', ['2017', $i])}}">2017年{{ $i }}月</option>
						@endfor
						<option value="(移動したい先URL)">(移動したい先名前)</option>
					</select>
				</div>
			  </div>
			</form>
			<div class="row">
				<div class="col-md-2"><label>売上総額(支払いベース)</label></div>
				<div class="col-md-4">{{ number_format($events->sum('Paidsales')) }}円</div>
				<div class="col-md-2"><label>売上総額(申込ベース)</label></div>
				<div class="col-md-4">{{ number_format($events->sum('Activesales')) }}円</div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>設定最大売上見込み</label></div>
				<div class="col-md-4">{{ number_format($events->sum('Totalpotential')) }}円</div>
				<div class="col-md-2"><label>コスト総額</label></div>
				<div class="col-md-4">{{ number_format($events->sum('Cost')) }}円</div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>利益額</label></div>
				<div class="col-md-4">{{ number_format($events->sum('Paidsales') - $events->sum('Cost')) }}円</div>
				<div class="col-md-2"><label>設定売り上げ率</label></div>
				<div class="col-md-4">@if($events->sum('Totalpotential') !=0){{ round(($events->sum('Paidsales')/$events->sum('Totalpotential')) *100,2) }}@else{{0}}@endif%</div>			
			</div>
			<div class="row">
				<div class="col-md-2"><label>利益率</label></div>
				<div class="col-md-4">@if($events->sum('Paidsales') !=0){{ round((1-($events->sum('Cost')/$events->sum('Paidsales'))) *100,2) }}@else{{0}}@endif%</div>

				<div class="col-md-2"><label>申込者数</label></div>
				<div class="col-md-4">{{ number_format($events->sum('Activetotalclients')) }}人</div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>最大参加者数</label></div>
				<div class="col-md-4">{{ number_format($events->sum('maxpax')) }}人</div>
			</div>
			<a href="{{ route('events.create') }}"><button type="button" class="btn btn-default navbar-btn">新規イベントマスタ</button></a>
                <div class="table-responsive">
    	    	<table class="table text-nowrap table-hover table-striped">
				<thead>
				<tr>
					<th>アクション</th>
					<th>出発日</th>
					<th>イベント名</th>
					<th>イベントステータス</th>
					<th>担当確認</th>
					<th>売上</th>
					<th>コスト</th>
					<th>利益額</th>
					<th>利益率</th>
					<th>申込人数(cxl含まず意向確認含)</th>
					<th>定員</th>
					<th>満席率</th>
					<th>担当</th>
					<th>添乗</th>
					<th>イベント期間</th>
					<th>イベントタイプ</th>
					<th>フォトブック</th>
					<th>ツアーメールテンプレ</th>
				</tr>
				</thead>
				<tbody>
				@forelse ($events as $event)
				<tr>
					<td><a href="{{ route('events.show', $event->id) }}">詳細</a> | 
					<a href="{{ route('events.edit', $event->id) }}">編集</a> | <br>
					<a href="{{ route('events.tcset', $event->id) }}">添乗</a> | <form action="{{ route('events.admincheck', $event->id) }}" id="admincheckform_{{ $event->id }}" method="post" style="display:inline">
					    {{ csrf_field() }}
			    		{{ method_field('patch') }}
			      		<a href="#" data-id="{{ $event->id }}" onclick="admincheckEvents(this);">担当確認</a></form></td>
			      	<td>{{ $event->Departuredate }}</td>
					<td>{{ $event->subtitle }}{{ $event->course->name }}</td>
					<td>{{ $event->eventstatus->name}}</td>
					<td>@if($event->admincheck == '1'){{ '済' }}@endif</td>
					<td>{{ number_format($event->Paidsales) }}</td>
					<td>{{ number_format($event->Cost) }}</td>
					<td>{{ number_format($event->Paidsales - $event->Cost) }}</td>
					<td>{{ number_format($event->Profitability) }}%</td>
					<td>{{ $event->Totalpossibleclients }}</td>
					<td>{{ $event->maxpax }}</td>
					<td>{{ $event->Loadfactor }}%</td>
					<td>@if($event->admin->id ==1){{ '' }}@else{{ $event->admin->name }}@endif</td>
					<td>@foreach($event->shifts as $shift)@if($shift->confirm != 2){{ $shift->admin->nickname or $shift->admin->name  }}<br>@endif
						@endforeach</td>
					<td>{{ $event->course->courselength->name }}</td>
					<td>{{ $event->course->eventtype->name}}</td>
					<td>{{ $event->photobookstatus->name or ''}}</td>
					<td>{{ $event->tourmailtemplate->name or '' }}</td>
				</tr>
				@empty
				@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div></div>
<script>
function admincheckEvents(e) {
  'use strict';

  if (confirm('確認済みにしますか￥？')) {
    document.getElementById('admincheckform_' + e.dataset.id).submit();
  }
}

</script>
@endsection