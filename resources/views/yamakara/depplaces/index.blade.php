@extends('layouts.app')

@section('title', '出発地マスタ')

@section('content')

<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>出発地マスタ</h3></div>
		<div class="panel-body">
		<a href="{{ route('depplaces.create') }}"><button type="button" class="btn btn-default navbar-btn">新規出発地</button></a>
		<table class="minitable display" cellspacing="0" width="100%">
			<thead>
			<tr>
				<th>アクション</th>
				<th>id</th>
				<th>出発地名</th>
			</tr>
			</thead>
			<tbody>
			@forelse ($depplaces as $depplace)
			<tr>
				<td>
					<a href="{{ route('depplaces.edit', $depplace->id) }}">編集</a>
				</td>
				<td>{{ $depplace->id }}</td>
				<td>{{ $depplace->name }}</td>
			</tr>
			@empty
			@endforelse
			</tbody>
		</table>
		</div>
	</div>
</div>
@endsection