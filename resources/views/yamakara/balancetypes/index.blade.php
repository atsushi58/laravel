@extends('layouts.app')

@section('title', '出入金項目')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>出入金項目</h3></div>
		<div class="panel-body">
			<a href="{{ route('balancetypes.create') }}"><button type="button" class="btn btn-default navbar-btn">新規出入金項目</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>出入金項目名</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($balancetypes as $balancetype)
					<tr>
						<td><a href="{{ route('balancetypes.edit', $balancetype->id) }}">編集</a></td>
						<td>{{ $balancetype->id }}</td>
					    <td>{{ $balancetype->name }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection