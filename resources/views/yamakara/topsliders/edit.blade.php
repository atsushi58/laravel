@extends('layouts.app')

@section('title', 'コースエリア編集')

@section('content')

<div class="container">
    <div class="panel panel-default">
{{ csrf_field() }}
{{ method_field('patch') }}
    <div class="panel-heading"><h3>コースエリア編集</h3></div>
    <div class="panel-body">
        <div class="form-group row">
            {{ Form::open(['route' => ['yamakara.coursearea.update', $coursearea->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
            <div class="col-md-2"><label>コース名</label></div>
            <div class="col-md-4">{{ Form::input('name', 'name', $coursearea->name, ['class' => 'form-control']) }}</div>
            <div class="col-md-6">{{ Form::submit('変更', ['class' => 'btn btn-primary']) }}
            {{ Form::close()}}
    </div>
    </div>
</div>
@endsection