@extends('layouts.app')

@section('title', 'トップスライダー')

@section('content')

<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>トップスライダー</h3>
		<p>スライダー画像を削除すると、サーバーから完全削除されますので、気を付けてください</p>
		</div>
		<div class="panel-body">
		<table class="minitable display" cellspacing="0" width="100%">
			<thead>
			<tr>
				<th>アクション</th>
				<th>画像</th>
				<th>ツアー</th>
				<th>タイトル</th>
				<th>キャプション</th>
			</tr>
			@foreach($topsliders as $topslider)
			<tr>
				<td><form action="{{ route('yamakara.topslider.destroy', $topslider->id) }}" id="form_{{ $topslider->id }}" method="post" style="display:inline">
						    {{ csrf_field() }}
				    		{{ method_field('delete') }}
				      		<a href="#" data-id="{{ $topslider->id }}" onclick="deleteTopslider(this);">削除</a></form></td>
				<td>{{ Html::image('topsliders/thumbnail/'. $topslider->filename) }}</td>
				<td>{{ $topslider->course->name }}</td>
				<td>{{ $topslider->title }}</td>
				<td>{{ $topslider->caption }}</td>
			</tr>
			@endforeach
			</thead>
			<tbody>
			<tr>
				<td colspan="5">
					<div class="form-group row">
						{{ Form::open(['route' => 'yamakara.topslider.store','files' => 'true']) }}
						<div class="col-md-1"></div>
						<div class="col-md-3">
						{{ Form::file('sliderimage') }}
						</div>
						<div class="col-md-2">
						{{ Form::select('course_id', $courses, null,  ['class' => 'form-control']) }}
						</div>
						<div class="col-md-3">
						{{ Form::input('title', 'title', null,  ['class' => 'form-control']) }}
						</div>
						<div class="col-md-3">
						{{ Form::input('caption', 'caption', null,  ['class' => 'form-control']) }}
						</div>
						<div class="col-md-1"></div>
						<div class="col-md-2">
						{{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
						{{ Form::close() }}
						</div>
					</div>
				</td>
			</tr>
			</tbody>
		</table>
		</div>
	</div>
</div>
<script>
function deleteTopslider(e) {
  'use strict';

  if (confirm('本当に削除しますか？')) {
    document.getElementById('form_' + e.dataset.id).submit();
  }
}
</script>


@endsection