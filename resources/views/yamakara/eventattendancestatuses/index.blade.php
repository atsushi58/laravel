@extends('layouts.app')

@section('title', 'ツアー参加ステータス')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>ツアー参加ステータス</h3></div>
        <div class="panel-body">
        	<a href="{{ route('eventattendancestatuses.create') }}"><button type="button" class="btn btn-default navbar-btn">新規ツアー参加ステータス</button></a>
			<table class="datatable display" cellspacing="0" width="100%">
				<thead>
				<tr>
					<th>アクション</th>
					<th>id</th>
					<th>ツアー参加ステータス</th>
				</tr>
				</thead>
				<tbody>
				@foreach ($eventattendancestatuses as $eventattendancestatus)
				<tr>
					<td>
						<a href="{{ route('eventattendancestatuses.edit', $eventattendancestatus->id) }}">編集</a>
					</td>
					<td>{{ $eventattendancestatus->id }}</td>
			      	<td>{{ $eventattendancestatus->name }}</td>
			     </tr>
				@endforeach
				</tbody>
		</table>
	</div>
</div>
</div>
@endsection