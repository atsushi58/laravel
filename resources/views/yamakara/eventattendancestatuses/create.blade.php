@extends('layouts.app')

@section('title', 'ツアー参加ステータス編集')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>ツアー参加ステータスの編集</h3></div>
        <div class="panel-body">
            <div class="form-group row">
                {{ Form::open(['route' => ['eventattendancestatuses.store'], 'method' => 'post', 'class' => 'form-horizontal']) }}
            <div class="col-md-2"><label>コース名</label></div>
                <div class="col-md-4">{{ Form::input('name', 'name', null, ['class' => 'form-control']) }}</div>
                <div class="col-md-6">{{ Form::submit('変更', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}</div>
            </div>
        </div>
    </div>
</div>
@endsection