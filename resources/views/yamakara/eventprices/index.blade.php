@extends('layouts.app')

@section('title', 'ツアー料金マスタ')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>ツアー料金マスタ</h3></div>
        <div class="panel-body">
			<table class = "datatable display" cellspacing="0" width="100%">
				<thead>
				<tr>
					<th>アクション</th>
					<th>id</th>
					<th>ツアー名</th>
					<th>出発日</th>
					<th>出発地</th>
					<th>料金</th>
					<th>子供料金</th>
				</tr>
				</thead>
				<tbody>
				@forelse ($eventprices as $eventprice)
				<tr>
					<td>
					    <a href="{{ route('eventprices.edit', $eventprice->id) }}">編集</a>
					</td>
					<td>{{ $eventprice->id }}</td>
					<td>{{ $eventprice->event->course->name or '' }}</td>
					<td>{{ $eventprice->event->depdate }}</td>
					<td>{{ $eventprice->depplace->name }}</td>
					<td>{{ $eventprice->price }}</td>
					<td>{{ $eventprice->kidsprice }}</td>
				</tr>
				@empty
				@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection