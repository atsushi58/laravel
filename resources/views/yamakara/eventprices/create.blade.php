@extends('layouts.app')

@section('title', 'ツアー料金の追加')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>ツアー料金の追加</h3></div>
            <div class="panel-body">
                {{ Form::open(['route' => ['eventprices.store'], 'method' => 'post', 'class' => 'form-horizontal']) }}
                {{ Form::hidden('event_id', $event->id) }}
                <div class="form-group row">
                    <div class="col-md-2">
                        {{ Form::label('depplace_id', '出発地', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::select('depplace_id', $depplaces, null, ['class' => 'form-control']) }}
                    </div>
                    <div class="col-md-2">
                        {{ Form::label('price', '旅行代金(大人)', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::input('price', 'price', null, ['class' => 'form-control']) }}
                        {{ $errors->first('price') }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                        {{ Form::label('price', '旅行代金(子供)', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::input('kidsprice', 'kidsprice', null, ['class' => 'form-control']) }}
                        {{ $errors->first('kidsprice') }}
                    </div>
                    <div class="col-md-2">
                        {{ Form::label('description', '設定説明(子供料金、など。HP掲載なので短く)', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::input('description', 'description', null, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                        {{ Form::label('hpactive', 'HP掲載', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::select('hpactive', ['0' => '載せない', '1' => '掲載'], null, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                    {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                    {{ Form::close()}}
                    </div>
                </div>
            </div>
    </div>
</div>
@endsection