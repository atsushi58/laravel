@extends('layouts.app')

@section('title', 'ツアー旅行代金の編集')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>ツアー参加の編集</h3></div>
            <div class="panel-body">
                {{ Form::open(['route' => ['eventprices.update', $eventprice->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
                <div class="form-group row">
                    <div class="col-md-2">
                        {{ Form::label('depplace_id', '出発地', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::select('depplace_id', $depplaces, $eventprice->depplace_id, ['class' => 'form-control']) }}
                    </div>
                    <div class="col-md-2">
                        {{ Form::label('price', '旅行代金(大人)', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::input('price', 'price', $eventprice->price, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                        {{ Form::label('price', '旅行代金(子供)', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::input('kidsprice', 'kidsprice', $eventprice->kidsprice, ['class' => 'form-control']) }}
                    </div>
                    <div class="col-md-2">
                        {{ Form::label('description', '設定説明(子供料金、など。HP掲載なので短く)', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::input('description', 'description', $eventprice->description, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                        {{ Form::label('hpactive', 'HP掲載', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::select('hpactive', ['0' => '載せない', '1' => '掲載'], $eventprice->hpactive, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                    {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                    {{ Form::close()}}
                    </div>
                </div>
            </div>
    </div>
</div>
@endsection