@extends('layouts.app')

@section('title', 'イベントコストカテゴリ')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>イベントコストカテゴリ</h3></div>
        <div class="panel-body">
        <a href="{{ route('eventcostcategories.create') }}"><button type="button" class="btn btn-default navbar-btn">新規イベントコストカテゴリ</button></a>
			<table class="datatable display" cellspacing="0" width="100%">
			<thead>
			<tr>
				<th>アクション</th>
				<th>id</th>
				<th>イベントコストカテゴリ</th>
			</tr>
			</thead>
			<tbody>
			@foreach ($eventcostcategories as $eventcostcategory)
			<tr>
				<td><a href="{{ route('eventcostcategories.edit', $eventcostcategory->id) }}">編集</a></td>
		      	<td>{{ $eventcostcategory->id }}</td>
		      	<td>{{ $eventcostcategory->name }}</td>
		     </tr>
			@endforeach
			</tbody>
		</table></div></div></div>
@endsection