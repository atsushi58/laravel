@extends('layouts.app')

@section('title', 'イベントコストカテゴリの編集')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>イベントコストカテゴリの編集</h3></div>
    <div class="panel-body">
        <div class="form-group row">
            {{ Form::open(['route' => ['eventcostcategories.update', $eventcostcategory->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
            <div class="col-md-2">{{ Form::label('name', 'カテゴリ名' )}}</div>
            <div class="col-md-4">{{ Form::input('name', 'name', $eventcostcategory->name, ['class' => 'form-control']) }}</div>
            <div class="col-md-6">{{ Form::submit('変更', ['class' => 'btn btn-primary']) }}
            {{ Form::close()}}
    </div>
    </div>
</div>


@endsection