@extends('layouts.app')

@section('title', 'コースエリア追加')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>コースエリア追加</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['courseareas.store'], 'method' => 'post', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('order', '並び順')}}</div>
            <div class="col-md-4">{{ Form::input('order', 'order', null, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('name', 'エリア名')}}</div>
            <div class="col-md-4">{{ Form::input('name', 'name', null, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection