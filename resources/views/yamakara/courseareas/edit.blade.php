@extends('layouts.app')

@section('title', 'コースエリア編集')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>コースエリア編集</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['courseareas.update', $coursearea->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('order', '並び順')}}</div>
            <div class="col-md-4">{{ Form::input('order', 'order', $coursearea->order, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">{{ Form::label('name', 'エリア名')}}</div>
            <div class="col-md-4">{{ Form::input('name', 'name', $coursearea->name, ['class' => 'form-control']) }}</div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::submit('変更', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection