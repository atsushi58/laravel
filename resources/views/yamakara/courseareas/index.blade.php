@extends('layouts.app')

@section('title', 'コースエリア')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>コースエリア</h3></div>
		<div class="panel-body">
			<a href="{{ route('courseareas.create') }}"><button type="button" class="btn btn-default navbar-btn">新規コースエリア</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>並び順</th>
						<th>コースエリア</th>
						<th>設定コース数</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($courseareas as $coursearea)
					<tr>
						<td>
							<a href="{{ route('coursesbyarea', $coursearea->id) }}">詳細</a> | 
							    <a href="{{ route('courseareas.edit', $coursearea->id) }}">編集</a></td>
						<td>{{ $coursearea->id }}</td>
						<td>{{ $coursearea->order }}</td>
					    <td>{{ $coursearea->name }}</td>
					    <td>{{ $coursearea->Coursecount }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection