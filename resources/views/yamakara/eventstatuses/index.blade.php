@extends('layouts.app')

@section('title', 'イベントステータス')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>イベントステータス</h3></div>
        <div class="panel-body">
        	<a href="{{ route('eventstatuses.create') }}"><button type="button" class="btn btn-default navbar-btn">新規コースエリア</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
				<tr>
					<th>アクション</th>
					<th>id</th>
					<th>イベントステータス名</th>
					<th>ツアー数</th>
				</tr>
				</thead>
				<tbody>
				@forelse ($eventstatuses as $eventstatus)
				<tr>
					<td>
						<a href="{{ route('events.bystatus', $eventstatus->id) }}">詳細</a> | 
					    <a href="{{ route('eventstatuses.edit', $eventstatus->id) }}">編集</a>
					</td>
					<td>{{ $eventstatus->id }}</td>
					<td>{{ $eventstatus->name }}</td>
					<td>{{ $eventstatus->Eventscount }}</td>
				</tr>
				@empty
				@endforelse	
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection