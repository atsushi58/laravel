@extends('layouts.app')

@section('title', 'イベントステータスの編集')

@section('content')
<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>イベントステータスの編集</h3></div>
    <div class="panel-body">
    {{ Form::open(['route' => ['eventstatuses.update', $eventstatus->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
    <div class="form-group row">
            <div class="col-md-2">{{ Form::label('name', 'イベントステータス名') }}</div>
            <div class="col-md-4">{{ Form::input('name', 'name', $eventstatus->name, ['class' => 'form-control']) }}</div>
            <div class="col-md-6">
                {{ Form::submit('変更', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection