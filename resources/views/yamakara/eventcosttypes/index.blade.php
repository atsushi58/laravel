@extends('layouts.app')

@section('title', 'イベントコストタイプ')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>イベントコストタイプ</h3></div>
        <div class="panel-body">
			<table class="datatable display" cellspacing="0" width="100%">
			<thead>
			<tr>
				<th>アクション</th>
				<th>id</th>
				<th>イベントコストタイプ</th>
			</tr>
			</thead>
			<tbody>
			@foreach ($eventcosttypes as $eventcosttype)
			<tr>
				<td><a href="{{ route('eventcosttypes.edit', $eventcosttype->id) }}">編集</a></td>
		      	<td>{{ $eventcosttype->id }}</td>
		      	<td>{{ $eventcosttype->name }}</td>
		     </tr>
			@endforeach
			</tbody>
		</table></div></div></div>
@endsection