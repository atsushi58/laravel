@extends('layouts.app')

@section('title', 'ツアーメールテンプレ追加')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>ツアーメールテンプレ追加</h3></div>
        <div class="panel-body">               
         {{ Form::open(['route' => ['tourmailtemplates.store'], 'method' => 'post', 'class' => 'form-horizontal']) }}
            <div class="form-group row">
                <div class="col-md-2"><label>カテゴリ名</label></div>
                <div class="col-md-4">{{ Form::select('tourmailcategory_id', $tourmailcategories, null, ['class' => 'form-control']) }}</div>
                <div class="col-md-2"><label>テンプレ名</label></div>
                <div class="col-md-4">{{ Form::input('name', 'name', null, ['class' => 'form-control']) }}</div>
            </div>
            <div class="form-group row">
                <div class="col-md-2"><label>メールタイトル</label></div>
                <div class="col-md-4">{{ Form::input('title', 'title', null, ['class' => 'form-control']) }}</div>
            </div>
            <div class="form-group row">
                <div class="col-md-2"><label>本文</label></div>
                <div class="col-md-10">{{ Form::textarea('template', null, ['class' => 'form-control']) }}</div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">{{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}</div>
            </div>
        </div>
    </div>
</div>
@endsection