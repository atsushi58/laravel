@extends('layouts.app')

@section('title', 'ツアーメールテンプレ')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>ツアーメールテンプレ</h3></div>
        <div class="panel-body">
        	<a href="{{ route('tourmailtemplates.create') }}"><button type="button" class="btn btn-default navbar-btn">新規ツアーメールテンプレ</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
				<tr>
					<th>アクション</th>
					<th>id</th>
					<th>カテゴリ名</th>
					<th>テンプレ名</th>
					<th>メールタイトル</th>
					<th>本文</th>
				</tr>
				</thead>
				<tbody>
				@foreach ($tourmailtemplates as $tourmailtemplate)
				<tr>
					<td>
						<a href="{{ route('tourmailtemplates.edit', $tourmailtemplate->id) }}">編集</a>
					</td>
					<td>{{ $tourmailtemplate->id }}</td>
					<td>{{ $tourmailtemplate->tourmailcategory->name or '' }}</td>
			      	<td>{{ $tourmailtemplate->name }}</td>
			      	<td>{{ $tourmailtemplate->title }}</td>
			      	<td>{{ $tourmailtemplate->template }}</td>
			     </tr>
				@endforeach
				</tbody>
		</table>
	</div>
</div>
</div>
@endsection