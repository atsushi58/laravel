@extends('layouts.app')

@section('title', '紹介ポイントマスタ')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>紹介ポイントマスタ</h3></div>
        <div class="panel-body">
        	<form class="form-horizontal">
			  <div class="form-group">
			    <label for="name" class="control-label col-md-1">ビュー: </label>
			    <div class="col-md-3">
					<select onChange="top.location.href=value" class="form-control">
						<option value="#"></option>
						<option value="{{ route('referrals.prepoint')}}">ポイント未付与一覧</option>
						<option value="{{ route('referrals.index')}}">全紹介一覧</option>
					</select>
				</div>
			  </div>
			</form>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
				<tr>
					<th>アクション</th>
					<th>id</th>
					<th>紹介してくれた人</th>
					<th>紹介された人</th>
					<th>紹介された人が行ったツアー</th>
					<th>スタンプ</th>
				</tr>
				</thead>
				<tbody>
				@forelse ($referrals as $referral)
				<tr>
					<td><a href="{{ route('referrals.point', $referral->id) }}">ポイント付与</a> | <a href="{{ route('referrals.edit', $referral->id) }}">編集</a></td>
					<td>{{ $referral->id }}</td>
					<td>{{ $referral->user->name }}</td>
					<td>{{ $referral->eventattendance->user->name }}</td>
					<td>{{ $referral->eventattendance->eventprice->event->course->name }}</td>
					<td>@if($referral->stamp == 1){{ '済'}}@endif</td>
				</tr>
				@empty
				@endforelse	
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection