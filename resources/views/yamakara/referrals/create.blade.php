@extends('layouts.app')

@section('title', '紹介者の設定')

@section('content')
<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>紹介者の設定</h3></div>
    <div class="panel-body">
    {{ Form::open(['route' => ['referrals.store'], 'method' => 'post', 'class' => 'form-horizontal']) }}
    {{ Form::hidden('eventattendance_id', $eventattendance->id) }}
    {{ Form::hidden('stamp', 0) }}
    <div class="form-group row">
            <div class="col-md-2">{{ Form::label('user_id', '紹介者') }}</div>
            <div class="col-md-4">{{ Form::select('user_id', $clients, null, ['class' => 'form-control']) }}</div>
            <div class="col-md-6">
                {{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection