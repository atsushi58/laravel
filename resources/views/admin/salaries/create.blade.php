@extends('layouts.app')

@section('title', '月次給与明細登録')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>月次給与明細登録</h3></div>
            <div class="panel-body">
                {{ Form::open(['route' => ['admin.salary.store'], 'method' => 'post', 'class' => 'form-horizontal']) }}
                {{ Form::hidden('admin_id', $user_id) }}
                {{ Form::hidden('year', $year) }}
                {{ Form::hidden('month', $month) }}
                {{ Form::hidden('basesalary', $amounts['amounta']) }}
                {{ Form::hidden('overtime', $amounts['amountc']) }}
                {{ Form::hidden('summerspecial', $amounts['amountd']) }}
                {{ Form::hidden('basetransportation', $amounts['amounte']) }}
                <div class="form-group row">
                    <div class="col-md-2">
                        {{ Form::label('balancesalary', '給与額調整(B)', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::input('balancesalary', 'balancesalary', 0, ['class' => 'form-control']) }}
                    </div>
                    <div class="col-md-2">
                        {{ Form::label('biztransportation', '営業交通費(G)', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::input('biztransportation', 'biztransportation',0 , ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                        {{ Form::label('reimbursement', '立替経費精算 (H)', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::input('reimbursement', 'reimbursement', 0, ['class' => 'form-control']) }}
                    </div>
                    <div class="col-md-2">
                        {{ Form::label('biztrip', '出張旅費費 (I)', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::input('biztrip', 'biztrip', 0, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                        {{ Form::label('biztripallowance', '出張手当 (J)', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::input('biztripallowance', 'biztripallowance', 0, ['class' => 'form-control']) }}
                    </div>
                    <div class="col-md-2">
                        {{ Form::label('deducationbalance', 'その他控除(U)', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::input('deducationbalance', 'deducationbalance', 0, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                        {{ Form::label('adjustment', '年末調整還付額(W)', ['class' => 'form-label']) }}
                    </div>
                    <div class="col-md-4">
                        {{ Form::input('adjustment', 'adjustment', 0, ['class' => 'form-control']) }}
                    </div>
                </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
</div></div></div>
@endsection