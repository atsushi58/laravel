@extends('layouts.app')

@section('title', '給与一覧')

@section('content')
<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>{{ $year }}年{{ $month }}月の給与一覧</h3></div>
    	    <div class="panel-body">
				<form class="form-horizontal">
				  <div class="form-group">
				    <label for="name" class="control-label col-md-1">ビュー : </label>
				    <div class="col-md-3">
						<select onChange="top.location.href=value" class="form-control">
							<option value="#"></option>
							<option value="{{ route('admin.unconfirmedsalarylist', [date('Y'), date('m') ])}}">今月の一覧</option>
							@for($i=1;$i<13;$i++)
							<option value="{{ route('admin.salarylist', ['2017', $i])}}">2017年{{ $i }}月</option>
							@endfor
							@for($i=1;$i<13;$i++)
							<option value="{{ route('admin.salarylist', ['2016', $i])}}">2016年{{ $i }}月</option>
							@endfor
							@for($i=1;$i<13;$i++)
							<option value="{{ route('admin.salarylist', ['2015', $i])}}">2015年{{ $i }}月</option>
							@endfor
						</select>
					</div>
				  </div>
				</form>
				<a href="{{ route('admin.salary.create.fulltime') }}"><button type="button" class="btn btn-default navbar-btn">新規社員給与登録</button></a>
				<table class="datatable display nowrap" cellspacing="0" width="100%">
					<thead>
					<tr>
						<th>アクション</th>
						<th>スタッフ名</th>
						<th>確定</th>
						<th>基本給与総額(A)</th>
						<th>給与調整総額(B)</th>
						<th>超過時間手当(C)</th>
						<th>夏季特別手当 (D)</th>
						<th>通勤交通費 (E)</th>
						<th>通勤交通費調整額 (F)</th>
						<th>交通費精算 (G)</th>
						<th>立替経費精算 (H)</th>
						<th>出張旅費費 (I)</th>
						<th>出張手当 (J)</th>
						<th>課税支給額(K: A+B+C+D)</th>
						<th>非課税支給額 (L: E+F+G+H+I+J)</th>
						<th>雇用保険対象合計(M: A+B+C+D+E+F)</th>
						<th>総支給額(N: K+L)</th>
						<th>健康保険料(O)</th>
						<th>介護保険料(P)</th>
						<th>厚生年金(Q)</th>
						<th>雇用保険料(R: M×0.003)</th>
						<th>所得税(S)</th>
						<th>住民税(T)</th>
						<th>その他控除(U)</th>
						<th>控除合計(V:O+P+Q+R+S+T+U)</th>
						<th>年末調整還付額(W)</th>
						<th>差引支給額(X)</th>
						<th>勤務日数</th>
						<th>総労働時間</th>

					</tr>
					</thead>
					<tbody>
						@foreach($statementlist as $statement)
						<tr>
							<td><a href="{{ route('admin.salary', [$statement['admin_id'], $year, $month]) }}">詳細</a></td>
							<td>{{ $statement['name'] }}</td>
							<td>@if($statement['accountingconfirm'] ==1){{'確定'}}@else{{'未確定'}}@endif</td>
							<td>{{ $statement['amounta'] }}</td>
							<td>{{ $statement['amountb'] }}</td>
							<td>{{ $statement['amountc'] }}</td>
							<td>{{ $statement['amountd'] }}</td>
							<td>{{ $statement['amounte'] }}</td>
							<td>{{ $statement['amountf'] }}</td>
							<td>{{ $statement['amountg'] }}</td>
							<td>{{ $statement['amounth'] }}</td>
							<td>{{ $statement['amounti'] }}</td>
							<td>{{ $statement['amountj'] }}</td>
							<td>{{ $statement['amountk'] }}</td>
							<td>{{ $statement['amountl'] }}</td>
							<td>{{ $statement['amountm'] }}</td>
							<td>{{ $statement['amountn'] }}</td>
							<td>{{ $statement['amounto'] }}</td>
							<td>{{ $statement['amountp'] }}</td>
							<td>{{ $statement['amountq'] }}</td>
							<td>{{ $statement['amountr'] }}</td>
							<td>{{ $statement['amounts'] }}</td>
							<td>{{ $statement['amountt'] }}</td>
							<td>{{ $statement['amountu'] }}</td>
							<td>{{ $statement['amountv'] }}</td>
							<td>{{ $statement['amountw'] }}</td>
							<td>{{ $statement['amountx'] }}</td>
							<td>{{ $statement['workingdays'] }}</td>
							<td>{{ floor($statement['workingminutes']/60) }}時間{{ $statement['workingminutes']%60 }}分</td>
					</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>


@endsection