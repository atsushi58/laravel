
@extends('layouts.app')

@section('title', '月次給与明細')

@section('content')
<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading">
    		<h3>{{ $user->name }} / {{ $year }}年{{ $month }}月のシフト明細</h3>
    		@if(Route::currentRouteName() == 'staff.salary')
    		<p><a href="{{ route('staff.salary', [$prevyear, $prevmonth]) }}"><< 前月の給与明細</a> | 
    			<a href="{{ route('staff.salary', [$nextyear, $nextmonth]) }}">翌月の給与明細>></a></p>
    		@elseif(Route::currentRouteName() == 'admin.salary')
    		<p><a href="{{ route('admin.salary', [$user->id, $prevyear, $prevmonth]) }}"><< 前月の給与明細</a> | 
    			<a href="{{ route('admin.salary', [$user->id, $nextyear, $nextmonth]) }}">翌月の給与明細>></a></p>
    		<a href="{{ route('admin.salaryrenew', [$user->id, $year, $month]) }}"><button type="button" class="btn btn-default navbar-btn">新規時給適用</button></a>
    		@endif
    	</div>
	    <div class="panel-body">
	    	@if(Route::currentRouteName() == 'admin.salary')
			<form class="form-horizontal">
			  <div class="form-group">
			    <label for="name" class="control-label col-xs-1">ビュー : </label>
			    <div class="col-xs-3">
					<select onChange="top.location.href=value" class="form-control">
						<option value="#"></option>
						@foreach($admins as $key => $value)
						<option value="{{ route('admin.salary', [$key, $year, $month]) }}">{{ $value }}</option>
						@endforeach
					</select>
				</div>
			  </div>
			</form>
			@endif
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
				<tr>
					<th>@if(Route::currentRouteName() == 'admin.salary')アクション@endif</th>
					<th>月日</th>
					<th>勤務地(時給)</th>
					<th>打刻時刻</th>
					<th>昼休み</th>
					<th>勤務時間</th>
					<th>基本給与</th>
					<th>超過時間手当</th>
					<th>夏季特別手当</th>
					<th>給与調整</th>
					<th>交通費(調整込)</th>
					<th>給与総額</th>
				</tr>
				</thead>
				<tbody>
				@forelse($shifts as $shift)
				<tr>
					<td>@if(Route::currentRouteName() == 'admin.salary')
						<a href="{{ route('admin.shift.show', $shift->id) }}">詳細</a> | 
						<a href="{{ route('admin.shift.edit', $shift->id) }}">編集</a> 
						@endif
			      	<td>{{ date('m/d', strtotime($shift->workday)) }}</td>
			      	<td>{{ $shift->workplace->name }}({{ $shift->hourlypay }})</td>
			      	<td>@if(isset($shift->actual_start)){{ date('H:i', strtotime($shift->actual_start)) }}@endif-
			      	@if(isset($shift->actual_end)){{ date('H:i', strtotime($shift->actual_end)) }}@endif</td>
			      	<td>{{ $shift->break }}</td>
			      	<td>{{ floor($shift->worktime/60) }}時間{{ $shift->worktime%60 }}分</td>
			      	<td>{{ number_format($shift->Basepay) }}</td>
			      	<td>{{ number_format($shift->Overtimepay) }}</td>
			      	<td>{{ number_format($shift->summerspecial) }}</td>
			      	<td>{{ number_format($shift->paybalance) }}</td>
			      	<td>{{ number_format($shift->transportation + $shift->transportationbalance) }}</td>
			      	<td>{{ number_format($shift->Totalpayment) }}</td>
				</tr>
				@empty
				@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>{{ $user->name }} / {{ $year }}年{{ $month }}月の給与明細</h3>
    	    <div class="panel-body">
    	    	@if(Route::currentRouteName() == 'admin.salary')
	    	    	@if(isset($salary))
	    	    		@if($salary->accountingconfirm == 1)
	    	    			<form action="{{ route('admin.salary.releaseaccountingconfirm', $salary->id) }}" id="releasesalaryform_{{ $salary->id }}" method="post" style="display:inline">
					    	{{ csrf_field() }}
			    			{{ method_field('patch') }}
			    			<a href="#" data-id="{{ $salary->id }}" onclick="releaseSalary(this);"><button type="button" class="btn btn-default navbar-btn">給与明細経理確認解除</button></a></form>
	    	    		@else
	    	    			<a href="{{ route('admin.salary.edit', $salary->id) }}"><button type="button" class="btn btn-default navbar-btn">月次給与調整等編集</button></a>
		    	    		{{ Form::open(['route' => ['admin.salary.update', $salary->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
		    	    		{{ Form::hidden('accountingconfirm', '1') }}
							{{ Form::hidden('basesalary', $amounts['amounta']) }}
							{{ Form::hidden('balancesalary', $amounts['amountb']) }}
							{{ Form::hidden('overtime', $amounts['amountc']) }}
							{{ Form::hidden('summerspecial', $amounts['amountd']) }}
							{{ Form::hidden('basetransportation', $amounts['amounte']) }}
							{{ Form::hidden('transportationbalance', $amounts['amountf']) }}
							{{ Form::hidden('biztransportation', $amounts['amountg']) }}
							{{ Form::hidden('reimbursement', $amounts['amounth']) }}
							{{ Form::hidden('biztrip', $amounts['amounti']) }}
							{{ Form::hidden('biztripallowance', $amounts['amountj']) }}
							{{ Form::hidden('healthinsurance', $amounts['amounto']) }}
							{{ Form::hidden('nursinginsurance', $amounts['amountp']) }}
							{{ Form::hidden('pension', $amounts['amountq']) }}
							{{ Form::hidden('incometax', $amounts['amounts']) }}
							{{ Form::hidden('inhabitanttax', $amounts['amountt']) }}
							{{ Form::hidden('deducationbalance', $amounts['amountu']) }}
							{{ Form::hidden('adjustment', $amounts['amountw']) }}
							{{ Form::hidden('transferamount', $amounts['amountx']) }}
							{{ Form::hidden('workingdays', $amounts['workingdays']) }}
							{{ Form::hidden('workingminutes', $amounts['workingminutes']) }}
		    	    	@endif
	    	    	@else
	    	    		<a href="{{ route('admin.salary.create', [$user->id, $year, $month]) }}"><button type="button" class="btn btn-default navbar-btn">月次給与調整等登録</button></a>
						{{ Form::open(['route' => ['admin.salary.store'], 'method' => 'post', 'class' => 'form-horizontal']) }}
						{{ Form::hidden('admin_id', $user->id) }}
						{{ Form::hidden('year', $year) }}
						{{ Form::hidden('month', $month) }}
						{{ Form::hidden('accountingconfirm', '1')}}
						{{ Form::hidden('basesalary', $amounts['amounta']) }}
						{{ Form::hidden('balancesalary', $amounts['amountb']) }}
						{{ Form::hidden('overtime', $amounts['amountc']) }}
						{{ Form::hidden('summerspecial', $amounts['amountd']) }}
						{{ Form::hidden('basetransportation', $amounts['amounte']) }}
						{{ Form::hidden('transportationbalance', $amounts['amountf']) }}
						{{ Form::hidden('biztransportation', $amounts['amountg']) }}
						{{ Form::hidden('reimbursement', $amounts['amounth']) }}
						{{ Form::hidden('biztrip', $amounts['amounti']) }}
						{{ Form::hidden('biztripallowance', $amounts['amountj']) }}
						{{ Form::hidden('healthinsurance', $amounts['amounto']) }}
						{{ Form::hidden('nursinginsurance', $amounts['amountp']) }}
						{{ Form::hidden('pension', $amounts['amountq']) }}
						{{ Form::hidden('incometax', $amounts['amounts']) }}
						{{ Form::hidden('inhabitanttax', $amounts['amountt']) }}
						{{ Form::hidden('deducationbalance', $amounts['amountu']) }}
						{{ Form::hidden('adjustment', $amounts['amountw']) }}
						{{ Form::hidden('transferamount', $amounts['amountx']) }}
						{{ Form::hidden('workingdays', $amounts['workingdays']) }}
						{{ Form::hidden('workingminutes', $amounts['workingminutes']) }}
					@endif
				@endif
    	    	<div class="form-group row">
					<div class="col-md-2"><label>基本給与総額 (A)</label></div>
					<div class="col-md-4">{{ number_format($amounts['amounta']) }}円</div>
					<div class="col-md-2"><label>給与調整総額(B)</label></div>
					<div class="col-md-4">{{ number_format($amounts['amountb']) }}円</div>
				</div>
				<div class="form-group row">
					<div class="col-md-2"><label>超過時間手当 (C)</label></div>
					<div class="col-md-4">{{ number_format($amounts['amountc']) }}円</div>
					<div class="col-md-2"><label>夏季特別手当 (D)</label></div>
					<div class="col-md-4">{{ number_format($amounts['amountd']) }}円</div>
				</div>
				<div class="form-group row">
					<div class="col-md-2"><label>通勤交通費 (E)</label></div>
					<div class="col-md-4">{{ number_format($amounts['amounte']) }}円</div>
					<div class="col-md-2"><label>通勤交通費調整額 (F)</label></div>
					<div class="col-md-4">{{ number_format($amounts['amountf']) }}円</div>
				</div>
				<div class="form-group row">
					<div class="col-md-2"><label>営業交通費 (G)</label></div>
					<div class="col-md-4">{{ number_format($amounts['amountg']) }}円</div>
					<div class="col-md-2"><label>立替経費精算 (H)</label></div>
					<div class="col-md-4">{{ number_format($amounts['amounth']) }}円</div>
				</div>
				<div class="form-group row">
					<div class="col-md-2"><label>出張旅費費 (I)</label></div>
					<div class="col-md-4">{{ number_format($amounts['amounti']) }}円</div>
					<div class="col-md-2"><label>出張手当 (J)</label></div>
					<div class="col-md-4">{{ number_format($amounts['amountj']) }}円</div>
				</div>
				<div class="form-group row">
					<div class="col-md-2"><label>課税支給額(K: A+B+C+D-O-P-Q-R)</label></div>
					<div class="col-md-4">{{ number_format($amounts['amountk']) }}円</div>
					<div class="col-md-2"><label>非課税支給額 (L: E+F+G+H+I+J)</label></div>
					<div class="col-md-4">{{ number_format($amounts['amountl']) }}円</div>
				</div>
				<div class="form-group row">
					<div class="col-md-2"><label>雇用保険対象合計(M: A+B+C+D+E+F)</label></div>
					<div class="col-md-4">{{ number_format($amounts['amountm']) }}円</div>
					<div class="col-md-2"><label>総支給額(N: K+L)</label></div>
					<div class="col-md-4">{{ number_format($amounts['amountn']) }}円</div>
				</div>
				<div class="form-group row">
					<div class="col-md-2"><label>健康保険料(O)</label></div>
					<div class="col-md-4">{{ number_format($amounts['amounto']) }}円</div>
					<div class="col-md-2"><label>介護保険料(P)</label></div>
					<div class="col-md-4">{{ number_format($amounts['amountp']) }}円</div>
				</div>
				<div class="form-group row">
					<div class="col-md-2"><label>厚生年金(Q)</label></div>
					<div class="col-md-4">{{ number_format($amounts['amountq']) }}円</div>
					<div class="col-md-2"><label>雇用保険料(R: M×0.003)</label></div>
					<div class="col-md-4">{{ number_format($amounts['amountr']) }}円</div>
				</div>
				<div class="form-group row">
					<div class="col-md-2"><label>所得税(S)</label></div>
					<div class="col-md-4">{{ number_format($amounts['amounts']) }}円</div>
					<div class="col-md-2"><label>住民税(T)</label></div>
					<div class="col-md-4">{{ number_format($amounts['amountt']) }}円</div>
				</div>
				<div class="form-group row">
					<div class="col-md-2"><label>その他控除(U)</label></div>
					<div class="col-md-4">{{ number_format($amounts['amountu']) }}円</div>
					<div class="col-md-2"><label>控除合計(V:O+P+Q+R+S+T+U)</label></div>
					<div class="col-md-4">{{ number_format($amounts['amountv']) }}円</div>
				</div>
				<div class="form-group row">
					<div class="col-md-2"><label>年末調整還付額(W)</label></div>
					<div class="col-md-4">{{ number_format($amounts['amountw']) }}円</div>
					<div class="col-md-2"><label>差引支給額(X)</label></div>
					<div class="col-md-4">{{ number_format($amounts['amountx']) }}円</div>
				</div>
				<div class="form-group row">
					<div class="col-md-2"><label>勤務日数</label></div>
					<div class="col-md-4">{{ $amounts['workingdays'] }}日</div>
					<div class="col-md-2"><label>総労働時間</label></div>
					<div class="col-md-4">{{ floor($amounts['workingminutes']/60) }}時間{{ $amounts['workingminutes']%60 }}分</div>
				</div>
    	    	@if(Route::currentRouteName() == 'admin.salary')
    	    	@if(isset($salary))
    	    		@if($salary->accountingconfirm == 1)
    	    		@else
    	    		{{ Form::submit('確認', ['class' => 'btn btn-primary']) }}
                	{{ Form::close()}}
    	    		@endif
    	    	@else
    	    		{{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                	{{ Form::close()}}
				@endif
				@endif
			</div>
		</div>
	</div>
</div>
<script>
function releaseSalary(e) {
  'use strict';
  if (confirm('解除しますか？')) {
    document.getElementById('releasesalaryform_' + e.dataset.id).submit();
  }
}
</script>


@endsection