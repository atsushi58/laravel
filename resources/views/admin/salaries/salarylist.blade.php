@extends('layouts.app')

@section('title', '給与一覧')

@section('content')
<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>{{ $year }}年{{ $month }}月の給与一覧</h3></div>
    	    <div class="panel-body">
				<form class="form-horizontal">
				  <div class="form-group">
				    <label for="name" class="control-label col-md-1">ビュー : </label>
				    <div class="col-md-3">
						<select onChange="top.location.href=value" class="form-control">
							<option value="#"></option>
							<option value="{{ route('admin.unconfirmedsalarylist', [date('Y'), date('m') ])}}">今月の一覧</option>
							@for($i=1;$i<13;$i++)
							<option value="{{ route('admin.salarylist', ['2018', $i])}}">2018年{{ $i }}月</option>
							@endfor
							@for($i=1;$i<13;$i++)
							<option value="{{ route('admin.salarylist', ['2017', $i])}}">2017年{{ $i }}月</option>
							@endfor
							@for($i=1;$i<13;$i++)
							<option value="{{ route('admin.salarylist', ['2016', $i])}}">2016年{{ $i }}月</option>
							@endfor
							@for($i=1;$i<13;$i++)
							<option value="{{ route('admin.salarylist', ['2015', $i])}}">2015年{{ $i }}月</option>
							@endfor
						</select>
					</div>
				  </div>
				</form>
				<p>総支給額: {{ $totalamount }}円</p>
				<p>総稼働人数: {{ $workingpeople }}人</p>
				<a href="{{ route('admin.salary.create.fulltime') }}"><button type="button" class="btn btn-default navbar-btn">新規社員給与登録</button></a>
				<table class="datatable display" cellspacing="0" width="100%">
					<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>経理確認</th>
						<th>振込区分</th>
						<th>振込日</th>
						<th>銀行名</th>
						<th>銀行コード</th>
						<th>支店コード</th>
						<th>支店名</th>
						<th>口座番号</th>
						<th>スタッフ名</th>
						<th>スタッフ名(カナ)</th>
						<th>基本給与総額(A)</th>
						<th>給与調整総額(B)</th>
						<th>超過時間手当(C)</th>
						<th>夏季特別手当 (D)</th>
						<th>通勤交通費 (E)</th>
						<th>通勤交通費調整額 (F)</th>
						<th>交通費精算 (G)</th>
						<th>立替経費精算 (H)</th>
						<th>出張旅費費 (I)</th>
						<th>出張手当 (J)</th>
						<th>課税支給額(K: A+B+C+D-O-P-Q-R)</th>
						<th>非課税支給額 (L: E+F+G+H+I+J)</th>
						<th>雇用保険対象合計(M: A+B+C+D+E+F)</th>
						<th>総支給額(N: K+L)</th>
						<th>健康保険料(O)</th>
						<th>介護保険料(P)</th>
						<th>厚生年金(Q)</th>
						<th>雇用保険料(R: M×0.003)</th>
						<th>社保合計(O+P+Q+R)</th>
						<th>所得税(S)</th>
						<th>住民税(T)</th>
						<th>その他控除(U)</th>
						<th>控除合計(V:O+P+Q+R+S+T+U)</th>
						<th>年末調整還付額(W)</th>
						<th>差引支給額(X)</th>
						<th>admin_id</th>
					</tr>
					</thead>
					<tbody>
					@forelse($salaries as $salary)
					<tr>
						<td><a href="{{ route('admin.salary', [$salary->admin->id, $year, $month]) }}">詳細</a> | <a href="{{ route('admin.shift.edit', $salary->id) }}">編集</a></td>
						<td>{{ $salary->id }}</td>
						<td>@if($salary->accountingconfirm == 1){{'済'}}@else{{'未'}}@endif</td>
						<td>6</td>
						<td>{{ sprintf('%04d',date('m').'15') }}</td>
						<td>{{ $salary->admin->bankaccount->bank->name or '' }}</td>
						<td>@if(!empty($salary->admin->bankaccount->bank->bankcode)){{ sprintf('%04d',$salary->admin->bankaccount->bank->bankcode) }}@endif</td>
						<td>@if(!empty($salary->admin->bankaccount->branchcode)){{ sprintf('%03d',$salary->admin->bankaccount->branchcode) }}@endif</td>
						<td>{{ $salary->admin->bankaccount->branch or '' }}</td>
						<td>@if(!empty($salary->admin->bankaccount->accountno)){{ sprintf('%04d',$salary->admin->bankaccount->accountno) }}@endif</td>
						<td>{{ $salary->admin->name }}</td>
						<td>@if(!empty($salary->admin->kana)){{ mb_convert_kana($salary->admin->kana, "KC") }}@endif</td>
						<td>{{ number_format($salary->basesalary) }}</td>
						<td>{{ number_format($salary->balancesalary) }}</td>
						<td>{{ number_format($salary->overtime) }}</td>
						<td>{{ number_format($salary->summerspecial) }}</td>
						<td>{{ number_format($salary->basetransportation) }}</td>
						<td>{{ number_format($salary->transportationbalance) }}</td>
						<td>{{ number_format($salary->biztransportation) }}</td>
						<td>{{ number_format($salary->reimbursement) }}</td>
						<td>{{ number_format($salary->biztrip) }}</td>
						<td>{{ number_format($salary->biztripallowance) }}</td>
						<td>{{ number_format($salary->Total-$salary->healthinsurance-$salary->nursinginsurance-$salary->pension-$salary->Employmentinsurance ) }}</td>
						<td>{{ number_format($salary->basetransportation + $salary->transportationbalance + $salary->biztransportation + $salary->reimbursement + $salary->biztrip + $salary->biztripallowance) }}</td>
						<td>{{ number_format($salary->Total + $salary->basetransportation + $salary->transportationbalance)}}
						<td>{{ number_format($salary->basesalary + $salary->balancesalary + $salary->overtime + $salary->summerspecial + $salary->basetransportation + $salary->transportationbalance + $salary->biztransportation + $salary->reimbursement + $salary->biztrip + $salary->biztripallowance) }}</td>
						<td>{{ number_format($salary->healthinsurance) }}</td>
						<td>{{ number_format($salary->nursinginsurance) }}</td>
						<td>{{ number_format($salary->pension) }}</td>
						<td>{{ number_format($salary->Employmentinsurance) }}</td>
						<td>{{ number_format($salary->healthinsurance + $salary->nursinginsurance + $salary->pension + $salary->Employmentinsurance) }}</td>
						<td>{{ number_format($salary->incometax) }}</td>
						<td>{{ number_format($salary->inhabitanttax) }}</td>
						<td>{{ number_format($salary->deducationbalance) }}</td>
						<td>{{ number_format($salary->healthinsurance + $salary->nursinginsurance + $salary->pension + $salary->Employmentinsurance + $salary->incometax + $salary->inhabitanttax + $salary->deducationbalance) }}</td>
						<td>{{ number_format($salary->adjustment) }}</td>
						<td>{{ number_format($salary->transferamount) }}</td>
						<td>{{ $salary->admin->id }}</td>
					</tr>
					@empty
					@endforelse
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>


@endsection