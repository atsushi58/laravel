@extends('layouts.app')

@section('title', '新規給与登録')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>新規給与登録</h3></div>
            <div class="panel-body">
    {{ Form::open(['route' => 'admin.salary.store', 'method' => 'post', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-6">
                {{ Form::label('admin_id', '名前', ['class' => 'col-2 col-form-label']) }}
                {{ Form::select('admin_id', $admins, null, ['class' => 'form-control col-4']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                {{ Form::label('year', '年', ['class' => 'col-2 col-form-label']) }}
                {{ Form::select('year', array('2015' => '2015', '2016' => '2016', '2017' => '2017', '2018' => '2018', '2019' => '2019', '2020' => '2020'), date('Y'),  ['class' => 'form-control col-4']) }}
            </div>
            <div class="col-md-6">
                {{ Form::label('month', '月', ['class' => 'col-2 col-form-label']) }}
                {{ Form::select('month', array('1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12'), date('m')+1 ,  ['class' => 'form-control col-4']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                {{ Form::label('basesalary', '通勤交通費調整') }}
                {{ Form::input('basesalary', 'basesalary', null, ['class' => 'form-control col-4']) }}
            </div>
            <div class="col-md-6">
                {{ Form::label('basetransportation', '通勤交通費') }}
                {{ Form::input('basetransportation', 'basetransportation', null, ['class' => 'form-control col-4']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
</div>
@endsection