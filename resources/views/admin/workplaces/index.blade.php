@extends('layouts.app')

@section('title', '勤務地マスタ')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>勤務地マスタ</h3></div>
        	<div class="panel-body">
				<table class="table table-hover table-striped">
					<thead>
					<tr>
						<th class="col-md-2">アクション</th>
						<th class="col-md-2">勤務地名</th>
						<th class="col-md-7">昼休憩(～時間以上)</th>
						<th class="col-md-1"></th>
					</tr>
					</thead>
					<tbody>
					@forelse ($workplaces as $workplace)
					<tr>
						<td><a href="{{ route('admin.workplace.edit', $workplace->id) }}">編集</a> | 
						    <form action="{{ route('admin.workplace.destroy', $workplace->id) }}" id="form_{{ $workplace->id }}" method="post" style="display:inline">
						    {{ csrf_field() }}
				    		{{ method_field('delete') }}
				      		<a href="#" data-id="{{ $workplace->id }}" onclick="deleteWorkplace(this);">削除</a></form></td>
						<td>{{ $workplace->name }}</td>
						<td>{{ $workplace->lunchbreaksetting }}</td>
						<td></td>
					</tr>
					@empty

					@endforelse
					<tr>
						<td colspan="4">
						{{ Form::open(['route' => 'admin.workplace.store', 'method' => 'post']) }}
						<div class="form-group row">
						<div class="col-md-2"></div>
						<div class="col-md-2">
						{{ Form::input('name', 'name', null, ['class' => 'form-control']) }}
						</div>
						<div class="col-md-2">
						{{ Form::input('lunchbreaksetting', 'lunchbreaksetting', null, ['class' => 'form-control']) }}
						</div>
						<div class="col-md-5"></div>
						<div class="col-md-1">
						{{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
				        {{ Form::close() }}
				        </div>
				        </div>
				        </td>
				    </tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script>
function deleteWorkplace(e) {
  'use strict';

  if (confirm('本当に削除しますか？')) {
    document.getElementById('form_' + e.dataset.id).submit();
  }
}
</script>


@endsection