@extends('layouts.app')

@section('title', '源泉徴収税額表')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>源泉徴収税額表</h3></div>
        	<div class="panel-body">
				<table class="table table-hover table-striped">
					<thead>
					<tr>
						<th class="col-md-1">アクション</th>
						<th class="col-md-1">以上</th>
						<th class="col-md-1">未満</th>
						<th class="col-md-1">甲(扶養家族0人)</th>
						<th class="col-md-1">甲(扶養家族1人)</th>
						<th class="col-md-1">甲(扶養家族2人)</th>
						<th class="col-md-1">甲(扶養家族3人)</th>
						<th class="col-md-1">甲(扶養家族4人)</th>
						<th class="col-md-1">甲(扶養家族5人)</th>
						<th class="col-md-1">甲(扶養家族6人)</th>
						<th class="col-md-1">甲(扶養家族7人)</th>
						<th class="col-md-1">乙</th>
					</tr>
					</thead>
					<tbody>
					@forelse ($taxes as $tax)
					<tr>
						<td><a href="{{ route('admin.tax.edit', $tax->id) }}">編集</a> | 
						    <form action="{{ route('admin.tax.destroy', $tax->id) }}" id="form_{{ $tax->id }}" method="post" style="display:inline">
						    {{ csrf_field() }}
				    		{{ method_field('delete') }}
				      		<a href="#" data-id="{{ $tax->id }}" onclick="deleteTax(this);">削除</a></form></td>
				      	<td>{{ $tax->kara }}</td>
				      	<td>{{ $tax->made }}</td>
				      	<td>{{ $tax->zerodependants }}</td>
				      	<td>{{ $tax->onedependants }}</td>
				      	<td>{{ $tax->twodependants }}</td>
				      	<td>{{ $tax->threedependants }}</td>
				      	<td>{{ $tax->fourdependants }}</td>
				      	<td>{{ $tax->fivedependants }}</td>
				      	<td>{{ $tax->sixdependants }}</td>
				      	<td>{{ $tax->sevendependants }}</td>
				      	<td>{{ $tax->otsu }}</td>
					</tr>
					@empty
					@endforelse
					<tr>
						<td colspan="12">
						{{ Form::open(['route' => 'admin.tax.store', 'method' => 'post']) }}
						<div class="form-group row">
						<div class="col-md-1"></div>
						<div class="col-md-1">
						{{ Form::input('kara', 'kara', null, ['class' => 'form-control']) }}
						</div>
						<div class="col-md-1">
						{{ Form::input('made', 'made', null, ['class' => 'form-control']) }}
						</div>
						<div class="col-md-1">
						{{ Form::input('zerodependants', 'zerodependants', null, ['class' => 'form-control']) }}
						</div>
						<div class="col-md-1">
						{{ Form::input('onedependants', 'onedependants', null, ['class' => 'form-control']) }}
						</div>
						<div class="col-md-1">
						{{ Form::input('twodependants', 'twodependants', null, ['class' => 'form-control']) }}
						</div>
						<div class="col-md-1">
						{{ Form::input('threedependants', 'threedependants', null, ['class' => 'form-control']) }}
						</div>
						<div class="col-md-1">
						{{ Form::input('fourdependants', 'fourdependants', null, ['class' => 'form-control']) }}
						</div>
						<div class="col-md-1">
						{{ Form::input('fivedependants', 'fivedependants', null, ['class' => 'form-control']) }}
						</div>
						<div class="col-md-1">
						{{ Form::input('sixdependants', 'sixdependants', null, ['class' => 'form-control']) }}
						</div>
						<div class="col-md-1">
						{{ Form::input('sevendependants', 'sevendependants', null, ['class' => 'form-control']) }}
						</div>	
						<div class="col-md-1">
						{{ Form::input('otsu', 'otsu', null, ['class' => 'form-control']) }}
						</div>						
						</div>
						<div class="form-group row">
						<div class="col-md-1"></div>
						<div class="col-md-1">
						{{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
				        {{ Form::close() }}
				        </div>
				        </div>
				        </td>
				    </tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script>
function deleteTax(e) {
  'use strict';
  if (confirm('本当に削除しますか？')) {
    document.getElementById('form_' + e.dataset.id).submit();
  }
}
</script>
@endsection