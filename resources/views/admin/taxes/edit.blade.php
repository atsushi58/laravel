@extends('layouts.app')

@section('title', '健康保険・厚生年金保険料額表の編集')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>健康保険・厚生年金保険料額表の編集</h3></div>
        	<div class="panel-body">
				<table class="table table-hover table-striped">
					<thead>
					<tr>
						<th class="col-md-1">アクション</th>
						<th class="col-md-1">以上</th>
						<th class="col-md-1">未満</th>
						<th class="col-md-1">甲(扶養家族0人)</th>
						<th class="col-md-1">甲(扶養家族1人)</th>
						<th class="col-md-1">甲(扶養家族2人)</th>
						<th class="col-md-1">甲(扶養家族3人)</th>
						<th class="col-md-1">甲(扶養家族4人)</th>
						<th class="col-md-1">甲(扶養家族5人)</th>
						<th class="col-md-1">甲(扶養家族6人)</th>
						<th class="col-md-1">甲(扶養家族7人)</th>
						<th class="col-md-1">乙</th>
					</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="12">
							{{ Form::open(['route' => ['admin.tax.update', $tax->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
							<div class="form-group row">
								<div class="col-md-1"></div>
								<div class="col-md-1">
								{{ Form::input('kara', 'kara', $tax->kara, ['class' => 'form-control']) }}
								</div>
								<div class="col-md-1">
								{{ Form::input('made', 'made', $tax->made, ['class' => 'form-control']) }}
								</div>
								<div class="col-md-1">
								{{ Form::input('zerodependants', 'zerodependants', $tax->zerodependants, ['class' => 'form-control']) }}
								</div>
								<div class="col-md-1">
								{{ Form::input('onedependants', 'onedependants', $tax->onedependants, ['class' => 'form-control']) }}
								</div>
								<div class="col-md-1">
								{{ Form::input('twodependants', 'twodependants', $tax->twodependants, ['class' => 'form-control']) }}
								</div>
								<div class="col-md-1">
								{{ Form::input('threedependants', 'threedependants', $tax->threedependants, ['class' => 'form-control']) }}
								</div>
								<div class="col-md-1">
								{{ Form::input('fourdependants', 'fourdependants', $tax->fourdependants, ['class' => 'form-control']) }}
								</div>
								<div class="col-md-1">
								{{ Form::input('fivedependants', 'fivedependants', $tax->fivedependants, ['class' => 'form-control']) }}
								</div>
								<div class="col-md-1">
								{{ Form::input('sixdependants', 'sixdependants', $tax->sixdependants, ['class' => 'form-control']) }}
								</div>
								<div class="col-md-1">
								{{ Form::input('sevendependants', 'sevendependants', $tax->sevendependants, ['class' => 'form-control']) }}
								</div>	
								<div class="col-md-1">
								{{ Form::input('otsu', 'otsu', $tax->otsu, ['class' => 'form-control']) }}
								</div>						
								<div class="col-md-1">
								{{ Form::submit('変更', ['class' => 'btn btn-primary']) }}
							    {{ Form::close() }}
							    </div>
						    </div>
						    </td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection