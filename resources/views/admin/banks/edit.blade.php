@extends('layouts.app')

@section('title', '銀行マスタの編集')

@section('content')

<div class="container">
	<div class="panel panel-default">
	<div class="panel-heading"><h3>銀行マスタの編集</h3></div>
    	<div class="panel-body">
			{{ Form::open(['route' => ['banks.store'], 'post' => 'patch', 'class' => 'form-horizontal']) }}
			<div class="form-group row">
				<div class="col-md-2">{{ Form::label('bankcode', '銀行コード') }}</div>
				<div class="col-md-4">{{ Form::input('bankcode', 'bankcode', $bank->bankcode, ['class' => 'form-control']) }}</div>
				<div class="col-md-2">{{ Form::label('name', '銀行名') }}</div>
				<div class="col-md-4">{{ Form::input('name', 'name', $bank->name, ['class' => 'form-control']) }}</div>
			</div>
			<div class="form-group row">
				<div class="col-md-1">
				{{ Form::submit('変更', ['class' => 'btn btn-primary']) }}
	        	{{ Form::close() }}
	        	</div>
	        </div>
		</div>
	</div>
</div>
@endsection