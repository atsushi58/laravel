@extends('layouts.app')

@section('title', '銀行マスタ')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>銀行マスタ</h3></div>
        	<div class="panel-body">
        	<a href="{{ route('banks.create') }}"><button type="button" class="btn btn-default navbar-btn">新規銀行登録</button></a>
				<table class="minitable display" cellspacing="0" width="100%">
					<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>銀行コード</th>
						<th>銀行名</th>
					</tr>
					</thead>
					<tbody>
					@forelse ($banks as $bank)
					<tr>
						<td><a href="{{ route('banks.edit', $bank->id) }}">編集</a>
						    </td>
						<td>{{ $bank->id }}</td>
				      	<td>{{ sprintf('%04d',$bank->bankcode) }}</td>
						<td>{{ $bank->name }}</td>
					</tr>
					@empty
					@endforelse
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection