
@extends('layouts.app')

@section('title', '時給・交通費一覧')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>時給・交通費一覧</h3></div>
    	    <div class="panel-body">
				<a href="{{ route('payments.create') }}"><button type="button" class="btn btn-default navbar-btn">時給・交通費登録</button></a>
				<table class="minitable display" cellspacing="0" width="100%">
					<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>スタッフ名</th>
						<th>勤務地</th>
						<th>時給</th>
						<th>交通費</th>
						<th>ルート</th>
					</tr>
					</thead>
					<tbody>
					@forelse($payments as $payment)
					<tr>
						<td><a href="{{ route('payments.edit', $payment->id) }}">編集</a></td>
						<td>{{ $payment->id }}</td>
						<td>{{ $payment->admin->name }}</td>
						<td>{{ $payment->workplace->name }}</td>
						<td>{{ $payment->payment }}</td>
						<td>{{ $payment->transportation }}</td>
						<td>{{ $payment->route }}</td>
					</tr>
					@empty
					@endforelse
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection