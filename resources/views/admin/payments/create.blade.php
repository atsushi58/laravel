@extends('layouts.app')

@section('title', '新規時給・交通費登録')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>新規時給・交通費登録</h3></div>
            <div class="panel-body">
    {{ Form::open(['route' => 'payments.store', 'method' => 'post', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::label('admin_id', 'バイト') }}
            </div>
            <div class="col-md-4">
                {{ Form::select('admin_id', $admins, null, ['class' => 'form-control']) }}
            </div>
            <div class="col-md-2">
                {{ Form::label('workplace_id', '勤務地') }}
            </div>
            <div class="col-md-4">
                {{ Form::select('workplace_id', $workplaces, null, ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::label('payment', '時給') }}
            </div>
            <div class="col-md-4">
                {{ Form::input('payment', 'payment', null, ['class' => 'form-control']) }}
            </div>
            <div class="col-md-2">
                {{ Form::label('transportation', '交通費') }}
            </div>
            <div class="col-md-4">
                {{ Form::input('transportation', 'transportation', null, ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::label('route', 'ルート') }}
            </div>
            <div class="col-md-4">
                {{ Form::input('route', 'route', null, ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
</div>
@endsection