@extends('layouts.app')

@section('title', '時給・交通費の編集')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>時給・交通費の編集</h3></div>
        	<div class="panel-body">
				<table class="table table-hover table-striped">
					<thead>
					<tr>
						<th class="col-md-2">アクション</th>
						<th class="col-md-2">スタッフ名</th>
						<th class="col-md-2">勤務地</th>
						<th class="col-md-2">時給</th>
						<th class="col-md-2">交通費</th>
						<th class="col-md-2">ルート</th>
					</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="6">
							{{ Form::open(['route' => ['payments.update', $payment->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
							<div class="form-group row">
								<div class="col-md-2"></div>
								<div class="col-md-2">
								{{ Form::select('admin_id', $admins, $payment->admin_id, ['class' => 'form-control']) }}
								</div>
								<div class="col-md-2">
								{{ Form::select('workplace_id', $workplaces, $payment->workplace_id, ['class' => 'form-control']) }}
								</div>
								<div class="col-md-2">
								{{ Form::input('payment', 'payment', $payment->payment, ['class' => 'form-control']) }}
								</div>
								<div class="col-md-2">
								{{ Form::input('transportation', 'transportation', $payment->transportation, ['class' => 'form-control']) }}
								</div>
								<div class="col-md-2">
								{{ Form::input('route', 'route', $payment->route, ['class' => 'form-control']) }}
								</div>
								<div class="col-md-2">
								{{ Form::submit('変更', ['class' => 'btn btn-primary']) }}
							    {{ Form::close() }}
							    </div>
						    </div>
						    </td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection