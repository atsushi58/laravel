@extends('layouts.app')

@section('title', '健康保険・厚生年金保険料額表の編集')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>健康保険・厚生年金保険料額表の編集</h3></div>
        	<div class="panel-body">
				<table class="table table-hover table-striped">
					<thead>
					<tr>
						<th class="col-md-2">アクション</th>
						<th class="col-md-1">等級</th>
						<th class="col-md-1">標準報酬</th>
						<th class="col-md-2">報酬月額</th>
						<th class="col-md-2">健康保険(折半額)</th>
						<th class="col-md-2">介護保険(折半額)</th>
						<th class="col-md-2">厚生年金(折半額)</th>
					</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="7">
							{{ Form::open(['route' => ['admin.insurance.update', $insurance->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
							<div class="form-group row">
								<div class="col-md-2"></div>
								<div class="col-md-1">
								{{ Form::input('rank', 'rank', $insurance->rank, ['class' => 'form-control']) }}
								</div>
								<div class="col-md-1">
								{{ Form::input('basewage', 'basewage', $insurance->basewage, ['class' => 'form-control']) }}
								</div>
								<div class="col-md-1">
								{{ Form::input('minwage', 'minwage', $insurance->minwage, ['class' => 'form-control']) }}
								</div>
								<div class="col-md-1">
								{{ Form::input('maxwage', 'maxwage', $insurance->maxwage, ['class' => 'form-control']) }}
								</div>
								<div class="col-md-2">
								{{ Form::input('healthinsurance', 'healthinsurance', $insurance->healthinsurance, ['class' => 'form-control']) }}
								</div>
								<div class="col-md-2">
								{{ Form::input('nursinginsurance', 'nursinginsurance', $insurance->nursinginsurance, ['class' => 'form-control']) }}
								</div>
								<div class="col-md-2">
								{{ Form::input('pension', 'pension', $insurance->pension, ['class' => 'form-control']) }}
								</div>
								<div class="col-md-1">
								{{ Form::submit('変更', ['class' => 'btn btn-primary']) }}
							    {{ Form::close() }}
							    </div>
						    </div>
						    </td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection