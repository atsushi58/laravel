@extends('layouts.app')

@section('title', '健康保険・厚生年金保険料額表')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>健康保険・厚生年金保険料額表</h3></div>
        	<div class="panel-body">
				<table class="table table-hover table-striped">
					<thead>
					<tr>
						<th class="col-md-2">アクション</th>
						<th class="col-md-1">等級</th>
						<th class="col-md-1">標準報酬</th>
						<th class="col-md-2">報酬月額</th>
						<th class="col-md-2">健康保険(折半額)</th>
						<th class="col-md-2">介護保険(折半額)</th>
						<th class="col-md-2">厚生年金(折半額)</th>
					</tr>
					</thead>
					<tbody>
					@forelse ($insurances as $insurance)
					<tr>
						<td><a href="{{ route('admin.insurance.edit', $insurance->id) }}">編集</a> | 
						    <form action="{{ route('admin.insurance.destroy', $insurance->id) }}" id="form_{{ $insurance->id }}" method="post" style="display:inline">
						    {{ csrf_field() }}
				    		{{ method_field('delete') }}
				      		<a href="#" data-id="{{ $insurance->id }}" onclick="deleteInsurance(this);">削除</a></form></td>
						<td>{{ $insurance->rank }}</td>
						<td>{{ $insurance->basewage }}</td>
						<td>{{ $insurance->minwage }}～{{ $insurance->maxwage }}</td>
						<td>{{ $insurance->healthinsurance }}</td>
						<td>{{ $insurance->nursinginsurance }}</td>
						<td>{{ $insurance->pension }}</td>
					</tr>
					@empty

					@endforelse
					<tr>
						<td colspan="7">
						{{ Form::open(['route' => 'admin.insurance.store', 'method' => 'post']) }}
						<div class="form-group row">
						<div class="col-md-2"></div>
						<div class="col-md-1">
						{{ Form::input('rank', 'rank', null, ['class' => 'form-control']) }}
						</div>
						<div class="col-md-1">
						{{ Form::input('basewage', 'basewage', null, ['class' => 'form-control']) }}
						</div>
						<div class="col-md-1">
						{{ Form::input('minwage', 'minwage', null, ['class' => 'form-control']) }}
						～
						</div>
						<div class="col-md-1">
						{{ Form::input('maxwage', 'maxwage', null, ['class' => 'form-control']) }}
						</div>
						<div class="col-md-2">
						{{ Form::input('healthinsurance', 'healthinsurance', null, ['class' => 'form-control']) }}
						</div>
						<div class="col-md-2">
						{{ Form::input('nursinginsurance', 'nursinginsurance', null, ['class' => 'form-control']) }}
						</div>
						<div class="col-md-2">
						{{ Form::input('pension', 'pension', null, ['class' => 'form-control']) }}
						</div>
						</div>
						<div class="form-group row">
						<div class="col-md-2"></div>
						<div class="col-md-10">
						{{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
				        {{ Form::close() }}
				        </div>
				        </div>
				        </td>
				    </tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script>
function deleteInsurance(e) {
  'use strict';

  if (confirm('本当に削除しますか？')) {
    document.getElementById('form_' + e.dataset.id).submit();
  }
}
</script>


@endsection