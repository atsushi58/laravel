@extends('layouts.app')

@section('title', 'シフトの編集')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>シフトの編集</h3></div>
            <div class="panel-body">
            {{ Form::open(['route' => ['admin.shift.update', $shift->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
                <div class="form-group row">
                    <div class="col-md-2">
                    {{ Form::label('admin_id', 'スタッフ', ['class' => 'col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::select('admin_id', $admins, $shift->admin_id, ['class' => 'form-control']) }}
                    </div>
                    <div class="col-md-2">
                    {{ Form::label('workday', '勤務日', ['class' => 'col-2 col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::input('date', 'workday', $shift->workday, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                    {{ Form::label('workplace_id', '勤務地', ['class' => 'col-2 col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::select('workplace_id', $workplaces, $shift->workplace_id, ['class' => 'form-control col-4']) }}
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                    {{ Form::label('start', '開始予定', ['class' => 'col-2 col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::select('start', $worktimes, $shift->start, ['class' => 'form-control col-4']) }}
                    </div>
                    <div class="col-md-2">
                    {{ Form::label('end', '終了予定', ['class' => 'col-2 col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::select('end', $worktimes, $shift->end, ['class' => 'form-control col-4']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                    {{ Form::label('primarytask_id', 'タスク1', ['class' => 'col-2 col-form-label']) }}
                    </div>
                    <div class="col-md-4">    
                    {{ Form::select('primarytask_id', $tasks, $shift->primarytask_id, ['class' => 'form-control col-4']) }}
                    </div>
                    <div class="col-md-2">
                    {{ Form::label('secondarytask_id', 'タスク2', ['class' => 'col-2 col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::select('secondarytask_id', $tasks, $shift->secondarytask_id, ['class' => 'form-control col-4']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                    {{ Form::label('actual_start', '勤務開始', ['class' => 'col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::input('datetime', 'actual_start', $shift->actual_start, ['class' => 'form-control']) }}
                    </div>
                    <div class="col-md-2">
                    {{ Form::label('actual_end', '勤務終了', ['class' => 'col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::datetime('actual_end', $shift->actual_end, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                    {{ Form::label('break', '休憩時間', ['class' => 'col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::input('break', 'break', $shift->break, ['class' => 'form-control']) }}
                    </div>
                    <div class="col-md-2">
                    {{ Form::label('paybalance', '調整額', ['class' => 'col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::input('paybalance', 'paybalance', $shift->paybalance, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                    {{ Form::label('transportationbalance', '交通費調整', ['class' => 'col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::input('transportationbalance', 'transportationbalance', $shift->transportationbalance, ['class' => 'form-control']) }}
                    </div>
                    <div class="col-md-2">
                    {{ Form::label('comment', '備考', ['class' => 'col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::textarea('comment', $shift->comment, ['class' => 'form-control', 'rows' => '3']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                    {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                    {{ Form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection