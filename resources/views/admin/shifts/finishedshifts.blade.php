@extends('layouts.app')

@section('title', 'シフト一覧')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>{{ $title }}の終了シフト一覧</h3>
    	@if(isset($date))
    	<p><a href="{{ route('admin.finishedshifts', [$workplace_id, $date->copy()->subDay()->format('Y-m-d') ]) }}"><<前日のシフト</a> | 
    	@foreach($workplaces as $key => $value)
    	<a href="{{ route('admin.finishedshifts', [$key, $date->format('Y-m-d') ]) }}">{{ $value }}のシフト(同日)</a> | 
    	@endforeach
    	<a href="{{ route('admin.finishedshifts', [$workplace_id, $date->copy()->addDay()->format('Y-m-d') ]) }}">翌日のシフト>></a></p>
    	@endif
    	</div>
        <div class="panel-body">
			<form class="form-horizontal">
			  <div class="form-group">
			    <label for="name" class="control-label col-xs-1">ビュー : </label>
			    <div class="col-xs-3">
					<select onChange="top.location.href=value" class="form-control">
						<option value="#"></option>
						<option value="{{ route('admin.finishedshifts.all') }}">全ての終了シフト</option>
						@if(isset($workplace_id))
						@foreach($twoweeks as $twoweek)
						<option value="{{ route('admin.finishedshifts', [$workplace_id, $twoweek]) }}">{{ $twoweek }}</option>
						@endforeach
						@endif
					</select>
				</div>
			  </div>
			</form>
			<table class="table table-hover table-striped">
				<thead>
				<tr>
					<th class="col-md-1">アクション</th>
					<th class="col-md-1">勤務日</th>
					<th class="col-md-1">スタッフ名</th>
					<th class="col-md-1">勤務地</th>
					<th class="col-md-1">開始時間</th>
					<th class="col-md-1">終了時間</th>
					<th class="col-md-1">休憩</th>
					<th class="col-md-1">労働時間</th>
					<th class="col-md-1">時給</th>
					<th class="col-md-1">手当(夏季+超過)</th>
					<th class="col-md-1">支給総額</th>
					<th class="col-md-1">交通費総額</th>
				</tr>
				</thead>
				<tbody>
				@forelse ($shifts as $shift)
				<tr>
					<td>
						<a href="{{ route('admin.shift.show', $shift->id) }}">詳細</a> |
						<a href="{{ route('admin.finishedshift.edit', $shift->id) }}">編集</a>
						</td>
			      	<td>{{ $shift->workday }}</td>
			      	<td>{{ $shift->admin->name }}</td>
			      	<td>{{ $shift->workplace->name }}</td>
			      	<td>{{ $shift->actual_start }}</td>
					<td>{{ $shift->actual_end }}</td>
					<td>{{ $shift->break }}</td>
					<td>{{ floor($shift->worktime/60) }}時間{{ $shift->worktime%60 }}分</td>
					<td>{{ $shift->hourlypay }}</td>
					<td>{{ $shift->overtime + $shift->summerspecial }}</td>
					<td>{{ $shift->basepay }}</td>
					<td>{{ $shift->transportation + $shift->transportationbalance }}</td>
				</tr>
				@empty

				@endforelse
				</tbody>
			</table>
			</div>
		</div>
	</div>
</div>
@endsection