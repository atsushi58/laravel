@extends('layouts.app')

@section('title', 'シフトの編集')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>シフトの編集</h3></div>
            <div class="panel-body">
            {{ Form::open(['route' => ['admin.finishedshift.update', $shift->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}

                <div class="form-group row">
                    <div class="col-md-2">
                    {{ Form::label('actual_start', '勤務開始', ['class' => 'col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::input('datetime', 'actual_start', $shift->actual_start, ['class' => 'form-control']) }}
                    </div>
                    <div class="col-md-2">
                    {{ Form::label('actual_end', '勤務終了', ['class' => 'col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::datetime('actual_end', $shift->actual_end, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                    {{ Form::label('break', '休憩時間', ['class' => 'col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::input('break', 'break', $shift->break, ['class' => 'form-control']) }}
                    </div>
                    <div class="col-md-2">
                    {{ Form::label('paybalance', '調整額', ['class' => 'col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::input('paybalance', 'paybalance', $shift->paybalance, ['class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                    {{ Form::label('transportationbalance', '交通費調整', ['class' => 'col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::input('transportationbalance', 'transportationbalance', $shift->transportationbalance, ['class' => 'form-control']) }}
                    </div>
                    <div class="col-md-2">
                    {{ Form::label('comment', '備考', ['class' => 'col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::textarea('comment', $shift->comment, ['class' => 'form-control', 'rows' => '3']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                    {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                    {{ Form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection