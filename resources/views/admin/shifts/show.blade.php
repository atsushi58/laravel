@extends('layouts.app')

@section('title', 'シフトの詳細')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>シフトの詳細</h3></div>
            <div class="panel-body">
                <a href="{{ route('admin.shift.edit', $shift->id) }}"><button type="button" class="btn btn-default navbar-btn">シフト編集</button></a>
                <div class="row">
                    <div class="col-md-2"><label>氏名</label></div>
                    <div class="col-md-4">{{ $shift->admin->name }}</div>
                    <div class="col-md-2"><label>勤務日</label></div>
                    <div class="col-md-4">{{ $shift->workday }}</div>
                </div>
                <div class="row">
                    <div class="col-md-2"><label>勤務地</label></div>
                    <div class="col-md-4">{{ $shift->workplace->name }}</div>
                    <div class="col-md-2"></div>
                    <div class="col-md-4"></div>
                </div>
                <div class="row">
                    <div class="col-md-2"><label>開始予定</label></div>
                    <div class="col-md-4">{{ $shift->starttime->worktime }}</div>
                    <div class="col-md-2"><label>終了予定</label></div>
                    <div class="col-md-4">{{ $shift->endtime->worktime }}</div>
                </div>
                <div class="row">
                    <div class="col-md-2"><label>タスク1</label></div>
                    <div class="col-md-4">{{ $shift->primarytask->name }}</div>
                    <div class="col-md-2"><label>タスク2</label></div>
                    <div class="col-md-4">{{ $shift->secondarytask->name }}</div>
                </div>
                <div class="row">
                    <div class="col-md-2"><label>勤務開始</label></div>
                    <div class="col-md-4">{{ $shift->actual_start }}</div>
                    <div class="col-md-2"><label>勤務終了</label></div>
                    <div class="col-md-4">{{ $shift->actual_end }}</div>
                </div>
                <div class="row">
                    <div class="col-md-2"><label>休憩時間</label></div>
                    <div class="col-md-4">{{ $shift->break }}</div>
                    <div class="col-md-2"><label>時給</label></div>
                    <div class="col-md-4">{{ $shift->hourlypay }}</div>
                </div>                      
                <div class="row">
                    <div class="col-md-2"><label>超過時間手当</label></div>
                    <div class="col-md-4">{{ $shift->overtime }}</div>
                    <div class="col-md-2"><label>夏季特別手当</label></div>
                    <div class="col-md-4">{{ $shift->summerspecial }}</div>
                </div>                      
                <div class="row">
                    <div class="col-md-2"><label>勤務時間</label></div>
                    <div class="col-md-4">{{ floor($shift->worktime/60) }}時間{{ $shift->worktime%60 }}分</div>
                    <div class="col-md-2"><label>調整額</label></div>
                    <div class="col-md-4">{{ $shift->paybalance }}</div>
                </div>                      
                <div class="row">
                    <div class="col-md-2"><label>合計支給額</label></div>
                    <div class="col-md-4">{{ $shift->Basepay }}</div>
                    <div class="col-md-2"><label>交通費</label></div>
                    <div class="col-md-4">{{ $shift->transportation }}</div>
                </div>     
                <div class="row">
                    <div class="col-md-2"><label>交通費調整</label></div>
                    <div class="col-md-4">{{ $shift->transportationbalance }}</div>
                    <div class="col-md-2"><label>備考</label></div>
                    <div class="col-md-4">{{ $shift->comment }}</div>
                </div>      
            </div>
        </div>
    </div>
</div>
@endsection