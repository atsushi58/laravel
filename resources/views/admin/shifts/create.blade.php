@extends('layouts.app')

@section('title', '新規予定シフト')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>新規予定シフト</h3></div>
            <div class="panel-body">
            {{ Form::open(['route' => 'admin.shift.store', 'method' => 'post', 'class' => 'form-horizontal']) }}
            {{ Form::hidden('confirm', '1')}}
                <div class="form-group row">
                    <div class="col-md-2">
                    {{ Form::label('admin_id', 'スタッフ', ['class' => 'col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::select('admin_id', $admins, null, ['class' => 'form-control']) }}
                    </div>
                    <div class="col-md-2">
                    {{ Form::label('workday', '勤務日', ['class' => 'col-2 col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::date('workday', date('Y-m-d'), ['class' => 'form-control col-4']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                    {{ Form::label('workplace_id', '勤務地', ['class' => 'col-2 col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::select('workplace_id', $workplaces, null, ['class' => 'form-control col-4']) }}
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                    {{ Form::label('start', '開始予定', ['class' => 'col-2 col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::select('start', $worktimes, null, ['class' => 'form-control col-4']) }}
                    </div>
                    <div class="col-md-2">
                    {{ Form::label('end', '終了予定', ['class' => 'col-2 col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::select('end', $worktimes, null, ['class' => 'form-control col-4']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-2">
                    {{ Form::label('primarytask_id', 'タスク1', ['class' => 'col-2 col-form-label']) }}
                    </div>
                    <div class="col-md-4">    
                    {{ Form::select('primarytask_id', $tasks, null, ['class' => 'form-control col-4']) }}
                    </div>
                    <div class="col-md-2">
                    {{ Form::label('secondarytask_id', 'タスク2', ['class' => 'col-2 col-form-label']) }}
                    </div>
                    <div class="col-md-4">
                    {{ Form::select('secondarytask_id', $tasks, null, ['class' => 'form-control col-4']) }}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-xs-2">
                    {{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
                    {{ Form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection