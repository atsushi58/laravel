@extends('layouts.app')

@section('title', 'シフト一覧')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>{{ $title }}シフト一覧</h3>
    	@if(isset($date))
    	<p><a href="{{ route('admin.shifts', [$workplace_id, $date->copy()->subDay()->format('Y-m-d') ]) }}"><<前日のシフト</a> | 
    	@foreach($workplaces as $key => $value)
    	<a href="{{ route('admin.shifts', [$key, $date->format('Y-m-d') ]) }}">{{ $value }}のシフト(同日)</a> | 
    	@endforeach
    	<a href="{{ route('admin.shifts', [$workplace_id, $date->copy()->addDay()->format('Y-m-d') ]) }}">翌日のシフト>></a></p>
    	@endif
    	</div>
        <div class="panel-body">
			<form class="form-horizontal">
			  <div class="form-group">
			    <label for="name" class="control-label col-md-1">ビュー: </label>		
			    <div class="col-md-3">
					<select onChange="top.location.href=value" class="form-control">
						<option value="#"></option>
						<option value="{{ route('admin.shifts.bymonth', [date('Y'), date('m')]) }}">今月のシフト</option>
						@if(isset($workplace_id))
						@foreach($twoweeks as $twoweek)
						<option value="{{ route('admin.shifts', [$workplace_id, $twoweek]) }}">{{ $twoweek }}</option>
						@endforeach
						@endif
					</select>
				</div>
			  </div>
			</form>
			<div class="row">
				<div class="col-md-2"><label>総労働時間</label></div>
				<div class="col-md-4">{{ $totalworktime or '' }}</div>
				<div class="col-md-2"><label>総支給額</label></div>
				<div class="col-md-4">{{ $totalpay or '' }}</div>
			</div>
			<div class="row">
				<div class="col-md-2"><label>レンタル総労働時間</label></div>
				<div class="col-md-4">{{ $totalrentalworktime or '' }}</div>
				<div class="col-md-2"><label>総支給額</label></div>
				<div class="col-md-4">{{ $totalpay or '' }}</div>
			</div>
			<a href="{{ route('admin.shift.create') }}"><button type="button" class="btn btn-default navbar-btn">新規予定シフト</button></a>
			@if( Route::currentRouteName() == 'admin.shifts.bymonth')
			<a href="{{ route('admin.shifts.inserthourlypay', [$year, $month]) }}"><button type="button" class="btn btn-default navbar-btn">時給入力</button></a>
			@endif
			<table class="datatable display nowrap" cellspacing="0" width="100%">
				<thead>
				<tr>
					<th>アクション</th>
					<th>確定</th>
					<th>勤務日</th>
					<th>id</th>
					<th>スタッフ名</th>
					<th>勤務地</th>
					<th>開始予定</th>
					<th>終了予定</th>
					<th>タスク1</th>
					<th>タスク2</th>
					<th>開始時間</th>
					<th>終了時間</th>
					<th>休憩</th>
					<th>労働時間</th>
					<th>時給</th>
					<th>手当(夏季+超過)</th>
					<th>支給総額</th>
					<th>交通費総額</th>
				</tr>
				</thead>
				<tbody>
				@forelse ($shifts as $shift)
				<tr>
					<td>
						<form action="{{ route('admin.shift.confirming', $shift->id) }}" id="confirmform_{{ $shift->id }}" method="post" style="display:inline">
					    {{ csrf_field() }}
			    		{{ method_field('patch') }}
			      		<a href="#" data-id="{{ $shift->id }}" onclick="confirmShifts(this);">確定</a></form> | <form action="{{ route('admin.shift.declining', $shift->id) }}" id="declineform_{{ $shift->id }}" method="post" style="display:inline">
					    {{ csrf_field() }}
			    		{{ method_field('patch') }}
			      		<a href="#" data-id="{{ $shift->id }}" onclick="declineShifts(this);">却下</a></form> | 
						<a href="{{ route('admin.shift.edit', $shift->id) }}">編集</a></td>
			      	<td>@if($shift->confirm == 0){{ '未確定' }}@elseif($shift->confirm == 1){{ '確定' }}@elseif($shift->confirm == 2){{ '却下' }}@endif</a></td>
			      	<td>{{ $shift->workday }}</td>
			      	<td>{{ $shift->admin->id }}</td>
			      	<td>{{ $shift->admin->name }}</td>
			      	<td>{{ $shift->workplace->name }}</td>
			      	<td>{{ $shift->starttime->worktime or '' }}</td>
					<td>{{ $shift->endtime->worktime or '' }}</td>
					<td>{{ $shift->primarytask->name }}</td>
					<td>{{ $shift->secondarytask->name or '' }}</td>
					<td>{{ $shift->actual_start }}</td>
					<td>{{ $shift->actual_end }}</td>
					<td>{{ $shift->break }}</td>
					<td>{{ floor($shift->worktime/60) }}時間{{ $shift->worktime%60 }}分</td>
					<td>{{ $shift->hourlypay }}</td>
					<td>{{ $shift->Overtimepay + $shift->summerspecial }}</td>
					<td>{{ $shift->basepay }}</td>
					<td>{{ $shift->transportation + $shift->transportationbalance }}</td>
				</tr>
				@empty

				@endforelse
				</tbody>
			</table>
			</div>
		</div>
	</div>
</div>
<script>
function confirmShifts(e) {
  'use strict';

  if (confirm('確定しますか？')) {
    document.getElementById('confirmform_' + e.dataset.id).submit();
  }
}
function declineShifts(e) {
  'use strict';

  if (confirm('却下しますか？')) {
    document.getElementById('declineform_' + e.dataset.id).submit();
  }
}
</script>
@endsection