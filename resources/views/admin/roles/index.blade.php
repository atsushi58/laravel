@extends('layouts.app')

@section('title', '役割マスタ')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>役割マスタ</h3></div>
        	<div class="panel-body">
				<table class="table table-hover table-striped">
					<thead>
					<tr>
						<th class="col-xs-2">アクション</th>
						<th class="col-xs-9">役割名</th>
						<th class="col-xs-1"></th>
					</tr>
					</thead>
					<tbody>
					@forelse ($roles as $role)
					<tr>
						<td><a href="{{ route('roles.edit', $role->id) }}">編集</a></td>
						<td>{{ $role->name }}</td>
						<td></td>
					</tr>
					@empty
					@endforelse
					<tr>
						<td colspan="3">
						{{ Form::open(['route' => 'roles.store', 'method' => 'post']) }}
						<div class="form-group row">
						<div class="col-md-2"></div>
						<div class="col-md-9">
						{{ Form::input('name', 'name', null, ['class' => 'form-control']) }}
						</div>
						<div class="col-md-1">
						{{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
				        {{ Form::close() }}
				        </div>
				        </div>
				        </td>
				    </tr>
					</tbody>
				</table>
			</div></div></div>


@endsection