@extends('layouts.app')

@section('title', '役割マスタの編集')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>役割マスタの編集</h3></div>
        	<div class="panel-body">
				<table class="table table-hover table-striped">
					<thead>
					<tr>
						<th class="col-xs-2">アクション</th>
						<th class="col-xs-9">役割名</th>
						<th class="col-xs-1"></th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td colspan="4">
						{{ Form::open(['route' => ['admin.role.update', $role->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
						<div class="form-group row">
						<div class="col-md-2"></div>
						<div class="col-md-9">
						{{ Form::input('name', 'name', $role->name, ['class' => 'form-control']) }}
						</div>
						<div class="col-md-1">
						{{ Form::submit('変更', ['class' => 'btn btn-primary']) }}
				        {{ Form::close() }}
				        </div>
				        </div>
				        </td>
				    </tr>
					</tbody>
				</table>
@endsection