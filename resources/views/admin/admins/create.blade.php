@extends('layouts.app')

@section('title', '新規スタッフ登録')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>新規スタッフ登録</h3></div>
            <div class="panel-body">
    {{ Form::open(['route' => 'admin.admin.store', 'method' => 'post', 'class' => 'form-horizontal']) }}
    {{ Form::hidden('active', '1') }}
    {{ Form::hidden('password', '$2y$10$huhO9cEKKeLFJfvioFz5eeyyPe4H5/NouLJYwabAyZIB8Qxf8GVj6')}}
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::label('name', '名前') }}
            </div>
            <div class="col-md-4">
                {{ Form::input('name', 'name', null, ['class' => 'form-control']) }}
            </div>
            <div class="col-md-2">
                {{ Form::label('kana', 'カナ') }}
            </div>
            <div class="col-md-4">
                {{ Form::input('kana', 'kana', null, ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::label('tel', '電話番号') }}
            </div>
            <div class="col-md-4">
                {{ Form::input('tel', 'tel', null, ['class' => 'form-control']) }}
            </div>
            <div class="col-md-2">
                {{ Form::label('email', 'メールアドレス') }}
            </div>
            <div class="col-md-4">
                {{ Form::input('email', 'email', null, ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::label('birthday', '生年月日') }}
            </div>
            <div class="col-md-4">
                {{ Form::date('birthday', null, ['class' => 'form-control']) }}
            </div>
            <div class="col-md-2">
                {{ Form::label('role_id', '役割') }}
            </div>
            <div class="col-md-4">
                {{ Form::select('role_id', $roles, null, ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::label('reference', '紹介元') }}
            </div>
            <div class="col-md-4">
                {{ Form::input('reference', 'reference', null, ['class' => 'form-control']) }}
            </div>
            <div class="col-md-6">
                {{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
</div>
@endsection