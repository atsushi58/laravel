@extends('layouts.app')

@section('title', 'スタッフ編集')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>スタッフ編集</h3></div>
            <div class="panel-body">
    @if(Route::currentRouteName() == 'staff.user.edit')
    {{ Form::open(['route' => ['staff.user.update'], 'method' => 'patch', 'class' => 'form-horizontal']) }}
    @elseif(Route::currentRouteName() == 'admin.admin.edit')
    {{ Form::open(['route' => ['admin.admin.update', $admin->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
    @endif
        <div class="form-group row">
            <div class="col-md-6">
                {{ Form::label('name', '名前', ['class' => 'col-2 col-form-label']) }}
                {{ Form::input('name', 'name', $admin->name, ['class' => 'form-control col-4']) }}
            </div>
            <div class="col-md-6">
                {{ Form::label('kana', 'カナ', ['class' => 'col-2 col-form-label']) }}
                {{ Form::input('kana', 'kana', $admin->kana, ['class' => 'form-control col-4']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                {{ Form::label('tel', '電話番号') }}
                {{ Form::input('tel', 'tel', $admin->tel, ['class' => 'form-control']) }}
            </div>
            <div class="col-md-6">
                {{ Form::label('email', 'メールアドレス') }}
                {{ Form::input('email', 'email', $admin->email, ['class' => 'form-control col-4']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                {{ Form::label('active', '現役') }}
                {{ Form::select('active', ['1' => '現役', '0' => 'OB/OG'], $admin->active, ['class' => 'form-control']) }}
            </div>
            <div class="col-md-6">
                {{ Form::label('birthday', '生年月日') }}
                {{ Form::date('birthday', $admin->birthday, ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="form-group row">
            @if($admin->role_id == 1 || 2)
            <div class="col-md-6">
                {{ Form::label('role_id', '役割') }}
                {{ Form::select('role_id', $roles, $admin->role_id, ['class' => 'form-control']) }}
            </div>
            @else
            <div class="col-md-6">
            </div>
            @endif
            <div class="col-md-6">
                {{ Form::label('reference', '紹介元') }}
                {{ Form::input('reference', 'reference', $admin->reference, ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
</div>
@endsection