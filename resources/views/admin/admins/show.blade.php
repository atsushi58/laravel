@extends('layouts.app')

@section('title', 'スタッフ詳細')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>{{ $admin->name }}の詳細</h3></div>
    	    <div class="panel-body">
    	    	@if(Route::currentRouteName() == 'staff.user')
				<a href="{{ route('staff.user.edit') }}"><button type="button" class="btn btn-default navbar-btn">情報編集</button></a>
				@elseif(Route::currentRouteName() == 'admin.admin.show')
				<a href="{{ route('admin.admin.edit', $admin->id) }}"><button type="button" class="btn btn-default navbar-btn">スタッフ情報編集</button></a>
				@endif
				<div class="row">
					<div class="col-md-2"><label>氏名</label></div>
					<div class="col-md-4">{{ $admin->name }}</div>
					<div class="col-md-2"><label>カナ</label></div>
					<div class="col-md-4">{{ $admin->kana }}</div>
				</div>
				<div class="row">
					<div class="col-md-2"><label>ニックネーム</label></div>
					<div class="col-md-4">{{ $admin->nickname }}</div>
					<div class="col-md-2"><label>電話番号</label></div>
					<div class="col-md-4">{{ $admin->tel }}</div>
				</div>
				<div class="row">					
					<div class="col-md-2"><label>メールアドレス</label></div>
					<div class="col-md-4">{{ $admin->email }}</div>
					<div class="col-md-2"><label>現役</label></div>
					<div class="col-md-4">@if($admin->active == 1){{ '現役' }}@else{{ 'OB/OG' }}@endif</div>
				</div>
				<div class="row">					
					<div class="col-md-2"><label>生年月日</label></div>
					<div class="col-md-4">{{ date('Y年n月j日', strtotime($admin->birthday)) }}</div>
					<div class="col-md-2"><label>役割</label></div>
					<div class="col-md-4">{{ $admin->role->name }}</div>
				</div>
				<div class="row">						
					<div class="col-md-2"><label>紹介元</label></div>
					<div class="col-md-4">{{ $admin->reference }}</div>
				</div>
			</div>
		</div>
	</div>
</div>
@if(Route::currentRouteName() == 'staff.user' || Auth::user()->role_id == 1)
<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>税金等の詳細</h3></div>
    	    <div class="panel-body">
    	    	@if(Auth::user()->role_id == 1)
				<a href="{{ route('admin.admin.edittax', $admin->id) }}"><button type="button" class="btn btn-default navbar-btn">税金等編集</button></a>
				@endif
				<div class="row">
					<div class="col-md-2"><label>基本給</label></div>
					<div class="col-md-4">{{ number_format($admin->basesalary) }}円</div>
					<div class="col-md-2"><label>基本通勤交通費</label></div>
					<div class="col-md-4">{{ number_format($admin->basetransportation) }}円</div>
				</div>
				<div class="row">
					<div class="col-md-2"><label>添乗日当</label></div>
					<div class="col-md-4">{{ number_format($admin->tcpayment) }}円</div>
					<div class="col-md-2"><label>浦和時給</label></div>
					<div class="col-md-4">{{ number_format($admin->urawapayment) }}円</div>
				</div>
				<div class="row">
					<div class="col-md-2"><label>新宿時給</label></div>
					<div class="col-md-4">{{ number_format($admin->shinjukupayment) }}円</div>
					<div class="col-md-2"><label>河口湖時給</label></div>
					<div class="col-md-4">{{ number_format($admin->kawaguchikopayment)}}円</div>
				</div>
				<div class="row">
					<div class="col-md-2"><label>屋久島時給</label></div>
					<div class="col-md-4">{{ number_format($admin->yakushimapaymen) }}円</div>
					<div class="col-md-2"></div>
					<div class="col-md-4"></div>
				</div>
				<div class="row">
					<div class="col-md-2"><label>社保等級</label></div>
					<div class="col-md-4">{{ $admin->insurancerank }}等級</div>
					<div class="col-md-2"><label>源泉徴収区分</label></div>
					<div class="col-md-4">@if($admin->taxsection == 0){{ '乙' }}@elseif($admin->taxsection == 1){{ '甲(扶養0人)' }}@elseif($admin->taxsection == 2){{ '甲(扶養1人)' }}@elseif($admin->taxsection == 3){{ '甲(扶養2人)' }}@elseif($admin->taxsection == 4){{ '甲(扶養3人)' }}@elseif($admin->taxsection == 5){{ '甲(扶養4人)' }}@elseif($admin->taxsection == 6){{ '甲(扶養5人)' }}@elseif($admin->taxsection == 7){{ '甲(扶養6人)' }}@elseif($admin->taxsection == 8){{ '甲(扶養7人)' }}@endif</div>
				</div>
				<div class="row">
					<div class="col-md-2"><label>住民税</label></div>
					<div class="col-md-4">{{ number_format($admin->inhabitanttax) }}円</div>
					<div class="col-md-2"><label>介護保険</label></div>
					<div class="col-md-4">@if($admin->nursinginsurance == 1){{ 'あり' }}@elseif($admin->nursinginsurance == 0){{ 'なし' }}@endif</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>振込先の詳細</h3></div>
    	    <div class="panel-body">
    	    	@if(Route::currentRouteName() == 'staff.user' && isset($admin->bankaccount))
    	    	<a href="{{ route('staff.user.editbankaccount') }}"><button type="button" class="btn btn-default navbar-btn">振込先の編集</button></a>
    	    	@elseif(Route::currentRouteName() == 'admin.admin.show' && isset($admin->bankaccount))
    	    	<a href="{{ route('admin.admin.editbankaccount', $admin->id) }}"><button type="button" class="btn btn-default navbar-btn">振込先の編集</button></a>
    	    	@elseif(Route::currentRouteName() == 'staff.user')
				<a href="{{ route('staff.user.createbankaccount') }}"><button type="button" class="btn btn-default navbar-btn">振込先の追加</button></a>
				@elseif(Route::currentRouteName() == 'admin.admin.show')
				<a href="{{ route('admin.admin.createbankaccount', $admin->id) }}"><button type="button" class="btn btn-default navbar-btn">振込先の追加</button></a>
				@endif
				<div class="row">
					<div class="col-md-2"><label>銀行名</label></div>
					<div class="col-md-4">{{ isset($admin->bankaccount->bank->name) ? $admin->bankaccount->bank->name . '銀行' : '' }}</div>
					<div class="col-md-2"><label>支店名</label></div>
					<div class="col-md-4">{{ isset($admin->bankaccount->branch) ? $admin->bankaccount->branch . '支店' : '' }}</div>
				</div>
				<div class="row">
					<div class="col-md-2"><label>口座番号</label></div>
					<div class="col-md-4">{{ isset($admin->bankaccount->accountno) ? $admin->bankaccount->accountno : '' }}</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endif
<script>
function deleteStaff(e) {
  'use strict';

  if (confirm('本当に削除しますか？')) {
    document.getElementById('form_' + e.dataset.id).submit();
  }
}
</script>



@endsection