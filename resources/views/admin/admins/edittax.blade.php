@extends('layouts.app')

@section('title', 'スタッフ編集')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>税金等編集</h3></div>
            <div class="panel-body">
    {{ Form::open(['route' => ['admin.admin.update', $admin->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-6">
                {{ Form::label('basesalary', '基本給', ['class' => 'col-2 col-form-label']) }}
                {{ Form::input('basesalary', 'basesalary', $admin->basesalary, ['class' => 'form-control col-4']) }}
            </div>
            <div class="col-md-6">
                {{ Form::label('basetransportation', '基本通勤交通費', ['class' => 'col-2 col-form-label']) }}
                {{ Form::input('basetransportation', 'basetransportation', $admin->basetransportation, ['class' => 'form-control col-4']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                {{ Form::label('insurancerank', '社保等級') }}
                {{ Form::input('insurancerank', 'insurancerank', $admin->insurancerank, ['class' => 'form-control']) }}
            </div>
            <div class="col-md-6">
                {{ Form::label('taxsection', '源泉徴収区分') }}
                {{ Form::select('taxsection', ['0' => '乙', '1' => '甲(扶養0人)', '2' => '甲(扶養1人)', '3' => '甲(扶養2人)', '4' => '甲(扶養3人)', '5' => '甲(扶養4人)', '6' => '甲(扶養5人)', '7' => '甲(扶養6人)', '8' => '甲(扶養7人)'], $admin->taxsection, ['class' => 'form-control col-4']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                {{ Form::label('inhabitanttax', '住民税') }}
                {{ Form::input('inhabitanttax', 'inhabitanttax', $admin->inhabitanttax, ['class' => 'form-control col-4']) }}
            </div>
            <div class="col-md-6">
                {{ Form::label('nursinginsurance', '介護保険') }}
                {{ Form::select('nursinginsurance', ['0' => 'なし', '1' => 'あり'], $admin->nursinginsurance, ['class' => 'form-control']) }}
            </div>
            <div class="col-md-6">
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
</div>
@endsection