@extends('layouts.app')

@section('title', '振込先編集')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>振込先編集</h3></div>
            <div class="panel-body">
    @if(Route::currentRouteName() == 'staff.user.editbankaccount')
    {{ Form::open(['route' => ['staff.user.updatebankaccount'], 'method' => 'patch', 'class' => 'form-horizontal']) }}
    @elseif(Route::currentRouteName() == 'admin.admin.editbankaccount')
    {{ Form::open(['route' => ['admin.admin.updatebankaccount', $bankaccount->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
    @endif
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::label('bank_id', '銀行名', ['class' => 'col-2 col-form-label']) }}
            </div>
            <div class="col-md-4">
                {{ Form::select('bank_id', $banks, $bankaccount->bank_id, ['class' => 'form-control col-4']) }}
            </div>
            <div class="col-md-2">
                {{ Form::label('branch', '支店名', ['class' => 'col-2 col-form-label']) }}
            </div>
            <div class="col-md-4">
                {{ Form::input('branch', 'branch', $bankaccount->branch, ['class' => 'form-control col-4']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::label('accountno', '口座番号') }}
            </div>
            <div class="col-md-4">
                {{ Form::input('accountno', 'accountno', $bankaccount->accountno, ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
</div>
@endsection