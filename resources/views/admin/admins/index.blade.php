@extends('layouts.app')

@section('title', 'スタッフ一覧')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>スタッフ一覧</h3></div>
    	    <div class="panel-body">
				<form class="form-horizontal">
				  <div class="form-group">
				    <label for="name" class="control-label col-xs-1">ビュー: </label>
				    <div class="col-md-3">
						<select onChange="top.location.href=value" class="form-control">
							<option value="#"></option>
							<option value="{{ route('admin.admins.all') }}">全スタッフ一覧</option>
							<option value="{{ route('admin.admins')}}">現役スタッフ一覧</option>
						</select>
					</div>
				  </div>
				</form>
				<a href="{{ route('admin.admin.create') }}"><button type="button" class="btn btn-default navbar-btn">新規スタッフ登録</button></a>
				<table class="datatable display nowrap" cellspacing="0" width="100%">
					<thead>
					<tr>
						<th>アクション</th>
						<th>ID</th>
						<th>スタッフ名</th>
						<th>カナ</th>
						<th>役割</th>
						<th>メール</th>
						<th>電話番号</th>
						<th>現役・OB/OG</th>
						<th>紹介元</th>
						<th>浦和の時給</th>
						<th>新宿の時給</th>
						<th>屋久島の時給</th>
						<th>税金区分</th>
					</tr>
					</thead>
					<tbody>
					@forelse($admins as $admin)
					<tr>
						<td><a href="{{ route('admin.admin.show', $admin->id) }}">詳細</a> |  
						    <form action="{{ route('admin.admin.destroy', $admin->id) }}" id="form_{{ $admin->id }}" method="post" style="display:inline">
						    {{ csrf_field() }}
				    		{{ method_field('delete') }}
				      		<a href="#" data-id="{{ $admin->id }}" onclick="deleteStaff(this);">削除</a></form>
				      	</td>
						<td>{{ $admin->id }}</td>
						<td>{{ $admin->name }}</td>
						<td>{{ $admin->kana }}</td>
						<td>{{ $admin->role->name }}</td> 
						<td>{{ $admin->email }}</td>
						<td>{{ $admin->tel }}</td>
						<td><?php if($admin->active == 1){echo '現役';}else{echo 'OB/OG';} ?></td>
						<td>{{ $admin->reference }}</td>
						<td>{{ $admin->UrawaPayment }}</td>
						<td>{{ $admin->ShinjukuPayment }}</td>
						<td>{{ $admin->YakushimaPayment }}</td>
						<td>{{ $admin->taxsection }}</td>
					</tr>
					@empty
					@endforelse
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script>
function deleteStaff(e) {
  'use strict';

  if (confirm('本当に削除しますか？')) {
    document.getElementById('form_' + e.dataset.id).submit();
  }
}
</script>



@endsection