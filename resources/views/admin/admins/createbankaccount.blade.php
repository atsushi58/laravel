@extends('layouts.app')

@section('title', '振込先追加')

@section('content')

<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>振込先追加</h3></div>
            <div class="panel-body">
    @if(Route::currentRouteName() == 'staff.user.createbankaccount')
    {{ Form::open(['route' => ['staff.user.storebankaccount'], 'method' => 'post', 'class' => 'form-horizontal']) }}
    @elseif(Route::currentRouteName() == 'admin.admin.createbankaccount')
    {{ Form::open(['route' => ['admin.admin.storebankaccount'], 'method' => 'post', 'class' => 'form-horizontal']) }}
    @endif
    {{ Form::hidden('admin_id', $id) }}
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::label('bank_id', '銀行名', ['class' => 'col-2 col-form-label']) }}
            </div>
            <div class="col-md-4">
                {{ Form::select('bank_id', $banks, null, ['class' => 'form-control col-4']) }}
            </div>
            <div class="col-md-2">
                {{ Form::label('branch', '支店名', ['class' => 'col-2 col-form-label']) }}
            </div>
            <div class="col-md-4">
                {{ Form::input('branch', 'branch', null, ['class' => 'form-control col-4']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::label('accountno', '口座番号') }}
            </div>
            <div class="col-md-4">
                {{ Form::input('accountno', 'accountno', null, ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-2">
                {{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
</div>
@endsection