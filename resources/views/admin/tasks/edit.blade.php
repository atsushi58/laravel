@extends('layouts.app')

@section('title', 'タスクの編集')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>タスクの編集</h3></div>
    <div class="panel-body">
		{{ Form::open(['route' => ['tasks.update', $task->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}</td>
		<div class="form-group row">
		<div class="col-md-2">{{ Form::label('name', 'タスク名') }}</div>
		<div class="col-md-4">{{ Form::input('name', 'name', $task->name, ['class' => 'form-control']) }}</div>
		<div class="col-md-2">{{ Form::label('name', '部門') }}</div>
		<div class="col-md-4">{{ Form::select('division_id', $divisions, $task->division_id, ['class' => 'form-control']) }}</div>
		</div>
		<div class="form-group row">
			<div class="col-md-2">
			{{ Form::submit('変更', ['class' => 'btn btn-primary']) }}
            {{ Form::close()}}
        </div>
        </div>
    </div>
</div>
</div>
@endsection