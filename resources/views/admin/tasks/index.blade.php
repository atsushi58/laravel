@extends('layouts.app')

@section('title', 'タスクマスタ')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>タスクマスタ</h3></div>
        	<div class="panel-body">
        	<a href="{{ route('tasks.create') }}"><button type="button" class="btn btn-default navbar-btn">新規タスク</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
				<tr>
					<th>アクション</th>
					<th>id</th>
					<th>タスク名</th>
					<th>部門</th>
				</tr>
				</thead>
				<tbody>
				@forelse ($tasks as $task)
				<tr>
					<td><a href="{{ route('tasks.edit', $task->id) }}">編集</a></td>
					<td>{{ $task->id }}</td>
					<td>{{ $task->name }}</td>
					<td>{{ $task->division->name or '' }}</td>
				</tr>
				@empty

				@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection