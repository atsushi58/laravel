@extends('layouts.app')

@section('title', 'タスクの追加')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>タスクの追加</h3></div>
    <div class="panel-body">
		{{ Form::open(['route' => ['tasks.store'], 'method' => 'post', 'class' => 'form-horizontal']) }}
		{{ Form::hidden('active', 1) }}
		<div class="form-group row">
		<div class="col-md-2">{{ Form::label('name', 'タスク名') }}</div>
		<div class="col-md-4">{{ Form::input('name', 'name', null, ['class' => 'form-control']) }}</div>
		<div class="col-md-2">{{ Form::label('name', '部門') }}</div>
		<div class="col-md-4">{{ Form::select('division_id', $divisions, null, ['class' => 'form-control']) }}</div>
		</div>
		<div class="form-group row">
			<div class="col-md-2">
			{{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
            {{ Form::close()}}
        </div>
        </div>
    </div>
</div>
</div>
@endsection