@extends('layouts.default')

@section('title', '開始・終了時間の編集')

@section('content')

<h1>開始・終了時間の編集</h1>

<table class="table table-hover table-striped">
	<thead>
	<tr>
		<th class="col-md-2">アクション</th>
		<th class="col-md-1">有効</th>
		<th class="col-md-8">開始・終了時間</th>
		<th class="col-md-1"></th>
	</tr>
	</thead>
	<tbody>
	<tr>
		<td><div class="form-group">
		{{ Form::open(['route' => ['admin.worktime.update', $worktime->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}</td>
		<td>{{ Form::select('active', ['1' => '有効', '0' => '無効'], $worktime->active, ['class' => 'form-control']) }}</td>
		<td>{{ Form::time('worktime', $worktime->worktime, ['class' => 'form-control']) }}</td>
		<td>{{ Form::submit('変更', ['class' => 'btn btn-primary']) }}
            {{ Form::close()}}</td>
	</tbody>
</table>
@endsection