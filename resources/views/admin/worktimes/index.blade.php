@extends('layouts.app')

@section('title', '開始・終了時間マスタ')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>開始・終了時間マスタ</h3></div>
        	<div class="panel-body">
			<table class="table table-hover table-striped">
				<thead>
				<tr>
					<th class="col-md-2">アクション</th>
					<th class="col-md-1">有効</th>
					<th class="col-md-8">開始・終了時間</th>
					<th class="col-md-1"></th>
				</tr>
				</thead>
				<tbody>
				@forelse ($worktimes as $worktime)
				<tr>
					<td><a href="{{ route('admin.worktime.edit', $worktime->id) }}">編集</a> | 
					    <form action="{{ route('admin.worktime.destroy', $worktime->id) }}" id="form_{{ $worktime->id }}" method="post" style="display:inline">
					    {{ csrf_field() }}
			    		{{ method_field('delete') }}
			      		<a href="#" data-id="{{ $worktime->id }}" onclick="deleteWorktime(this);">削除</a></form></td>
			      	<td><?php if($worktime->active == 1){echo '有効';}else{echo '無効';}; ?></td>
					<td>{{ $worktime->worktime }}</td>
					<td></td>
				</tr>
				@empty

				@endforelse
			    <tr>
					<td colspan="4">
					{{ Form::open(['route' => 'admin.worktime.store', 'method' => 'post', 'class' => 'form-horizontal']) }}
					{{ Form::hidden('active', '1') }}
					<div class="form-group row">
					<div class="col-md-2"></div>
					<div class="col-md-1"></div>
					<div class="col-md-8">
					{{ Form::time('worktime', 'worktime', null, ['class' => 'form-control']) }}
					</div>
					<div class="col-md-1">
					{{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
		            {{ Form::close()}}
					</div>
			        </div>
			        </td>
				</tr>
				</tbody>
			</table>
<script>
function deleteWorktime(e) {
  'use strict';

  if (confirm('本当に削除しますか？')) {
    document.getElementById('form_' + e.dataset.id).submit();
  }
}
</script>


@endsection