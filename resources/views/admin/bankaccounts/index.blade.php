@extends('layouts.app')

@section('title', '振込先マスタ')

@section('content')

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-heading"><h3>振込先マスタ</h3></div>
        	<div class="panel-body">
        	<a href="{{ route('bankaccounts.create') }}"><button type="button" class="btn btn-default navbar-btn">新規振込先登録</button></a>
				<table class="minitable display" cellspacing="0" width="100%">
					<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>スタッフ名</th>
						<th>銀行コード</th>
						<th>銀行名</th>
						<th>支店コード</th>
						<th>支店名</th>
						<th>口座番号</th>
					</tr>
					</thead>
					<tbody>
					@forelse ($bankaccounts as $bankaccount)
					<tr>
						<td><a href="{{ route('bankaccounts.edit', $bankaccount->id) }}">編集</a>
						    </td>
						<td>{{ $bankaccount->id }}</td>
						<td>{{ $bankaccount->admin->name }}</td>
						<td>@if(!empty($bankaccount->bank->bankcode)){{ sprintf('%04d',$bankaccount->bank->bankcode) }}@endif</td>
						<td>{{ $bankaccount->bank->name or ''}}</td>
						<td>{{ sprintf('%03d',$bankaccount->branchcode) }}</td>
						<td>{{ $bankaccount->branch }}</td>
						<td>{{ $bankaccount->accountno }}</td>
					</tr>
					@empty
					@endforelse
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection