@extends('layouts.app')

@section('title', '振込先マスタの編集')

@section('content')

<div class="container">
	<div class="panel panel-default">
	<div class="panel-heading"><h3>銀行マスタの編集</h3></div>
    	<div class="panel-body">
			{{ Form::open(['route' => ['bankaccounts.update', $bankaccount->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
			<div class="form-group row">
				<div class="col-md-2">{{ Form::label('admin_id', 'スタッフ名') }}</div>
				<div class="col-md-4">{{ Form::select('admin_id', $admins, $bankaccount->admin_id, ['class' => 'form-control']) }}</div>
				<div class="col-md-2">{{ Form::label('bank_id', '銀行名') }}</div>
				<div class="col-md-4">{{ Form::select('bank_id', $banks, $bankaccount->bank_id, ['class' => 'form-control']) }}</div>
			</div>
			<div class="form-group row">
				<div class="col-md-2">{{ Form::label('branchcode', '支店コード') }}</div>
				<div class="col-md-4">{{ Form::input('branchcode', 'branchcode', $bankaccount->branchcode, ['class' => 'form-control']) }}</div>
				<div class="col-md-2">{{ Form::label('branch', '支店名') }}</div>
				<div class="col-md-4">{{ Form::input('branch', 'branch', $bankaccount->branch, ['class' => 'form-control']) }}</div>
			</div>
			<div class="form-group row">
				<div class="col-md-2">{{ Form::label('accountno', '口座番号') }}</div>
				<div class="col-md-4">{{ Form::input('accountno', 'accountno', $bankaccount->accountno, ['class' => 'form-control']) }}</div>
			</div>
			<div class="form-group row">
				<div class="col-md-1">
				{{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
	        	{{ Form::close() }}
	        	</div>
	        </div>
		</div>
	</div>
</div>
@endsection