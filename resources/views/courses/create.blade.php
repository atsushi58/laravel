@extends('layouts.default')

@section('title', '新規登山コース')

@section('content')
<h1>新規登山コース</h1>
<div class="jumbotron">
    {{ Form::open(['action' => 'CoursesController@store', 'method' => 'post', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-xs-6">
                {{ Form::label('coursearea_id', 'コースエリア', ['class' => 'col-2 col-form-label']) }}
                {{ Form::select('coursearea_id', $courseareas, null, ['class' => 'form-control col-4']) }}
            </div>
            <div class="col-xs-6">
                {{ Form::label('courselength_id', 'コース期間', ['class' => 'col-2 col-form-label']) }}
                {{ Form::select('courselength_id', $courselengths, null, ['class' => 'form-control col-4']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-xs-6">
                {{ Form::label('depplace_id', '出発地', ['class' => 'col-2 col-form-label']) }}
                {{ Form::select('depplace_id', $depplaces, null, ['class' => 'form-control col-4']) }}
            </div>
            <div class="col-xs-6">
                {{ Form::label('name', 'コース名', ['class' => 'col-2 col-form-label']) }}
                {{ Form::input('name', null, null, ['class' => 'form-control col-4']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-xs-6">
                {{ Form::label('memo', 'メモ', ['class' => 'col-2 col-form-label']) }}
                {{ Form::textarea('memo', null, ['class' => 'form-control col-4']) }}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-xs-2">
            {{ Form::submit('追加', ['class' => 'btn btn-primary']) }}
            {{ Form::close()}}
            </div>
        </div>
@endsection