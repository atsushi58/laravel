@extends('layouts.default')

@section('title', '登山コースの編集')

@section('content')
<h1>登山コースの編集</h1>
<form method="post" action="{{ url('/courses', $course->id) }}">
{{ csrf_field() }}
{{ method_field('patch') }}
<div class="jumbotron">
    <div class="form-horizontal">
        <div class="form-group">
            <label for="coursearea_id" class="col-xs-2 control-label">コースエリア</label>
            <div class="col-xs-4">
            {{Form::select('coursearea_id', $courseareas, $course->coursearea_id, ['class' => 'form-control'])}}
            </div>
            <label for="name" class="col-xs-2 control-label">登山コース名</label>
            <div class="col-xs-4">
            <input type="text" class="form-control" name="name" id="name" placeholder="登山コース名" value="{{ old('name', $course->name) }}">
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="col-xs-2 control-label">コース期間</label>
            <div class="col-xs-4">
            {{Form::select('coursearea_id', $courseareas, $course->coursearea_id, ['class' => 'form-control'])}}
            </div>
            <label for="name" class="col-xs-2 control-label">メモ</label>
            <div class="col-xs-4">
            <input type="text" class="form-control" name="memo" id="memo" placeholder="メモ" value="{{ old('memo', $course->memo) }}">
            </div>
        </div>
    </div>
  <p>
    <button class="btn btn-default" type="submit">保存</button>
  </p>
</form>
@endsection