@extends('layouts.default')

@section('title', '登山コースマスタ')

@section('content')

<h1>登山コースマスタ</h1>

<form class="form-horizontal">
  <div class="form-group">
    <label for="name" class="control-label col-xs-1">ビュー : </label>
    <div class="col-xs-3">
		<select onChange="top.location.href=value" class="form-control">
			<option value="#"></option>
			<option value="(移動したい先URL)">(移動したい先名前)</option>
			<option value="(移動したい先URL)">(移動したい先名前)</option>
		</select>
	</div>
  </div>
</form>

<a href="{{ url('/courses/create') }}"><button type="button" class="btn btn-default navbar-btn">新規登山コース</button></a>


<table class="table table-hover table-striped">
	<thead>
	<tr>
		<th>アクション</th>
		<th>コースエリア</th>
		<th>コース名</th>
		<th>メモ</th>
	</tr>
	</thead>
	<tbody>
	@forelse ($courses as $course)
	<tr>
		<td><a href="{{ url('/courses', $course->id) }}">詳細</a> | 
		    <a href="{{ action('CoursesController@edit', $course->id) }}">編集</a> | 
		    <form action="{{ action('CoursesController@destroy', $course->id) }}" id="form_{{ $course->id }}" method="post" style="display:inline">
		    {{ csrf_field() }}
    		{{ method_field('delete') }}
      		<a href="#" data-id="{{ $course->id }}" onclick="deleteCourse(this);">削除</a></form></td>
		<td>{{ $course->coursearea->name }}</td>
		<td>{{ $course->name }}</td>
		<td>{{ $course->memo }}</td>
	</tr>
	@empty

	@endforelse
	</tbody>
</table>
<script>
function deleteCourse(e) {
  'use strict';

  if (confirm('本当に削除しますか？')) {
    document.getElementById('form_' + e.dataset.id).submit();
  }
}
</script>


@endsection