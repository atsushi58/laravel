@extends('layouts.default')

@section('title', '登山コース詳細')

@section('content')
<h1>{{ $course->name }}詳細</h1>
<h2>スケジュール</h2>
<p>スケジュール追加</p>
<table class="table table-hover table-striped">
	<thead>
	<tr>
		<th>日</th>
		<th>コースタイム(h)</th>
		<th>距離(km)</th>
		<th>累積上り(km)</th>
		<th>累積下り(km)</th>
		<th>スケジュール内容</th>
	</tr>
	</thead>
	<tbody>
	@forelse ($course->schedules as $schedule)
	<tr>
		<td>{{ $schedule->date }}</td>
		<td>{{ $schedule->coursetime }}</td>
		<td>{{ $schedule->distance }}</td>
		<td>{{ $schedule->ascend }}</td>
		<td>{{ $schedule->descend }}</td>
		<td>{{ $schedule->schedule }}</td>

	</tr>
	@empty
	@endforelse
	</tbody>
	</table>
@endsection