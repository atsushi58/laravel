@extends('layouts.default')

@section('title', 'イベント期間')

@section('content')

<h1>イベント期間</h1>

<a href="{{ url('/courselengths/create') }}"><button type="button" class="btn btn-default navbar-btn">新規イベント期間</button></a>


<table class="table table-hover table-striped">
	<thead>
	<tr>
		<th>アクション</th>
		<th>イベント期間</th>
	</tr>
	</thead>
	<tbody>
	@forelse ($courselengths as $courselength)
	<tr>
		<td><a href="{{ url('/courselengths', $courselength->id) }}">詳細</a> | 
		    <a href="{{ action('CourselengthsController@edit', $courselength->id) }}">編集</a> | 
			<form action="{{ action('CourselengthsController@destroy', $courselength->id) }}" id="form_{{ $courselength->id }}" method="post" style="display:inline">
		    {{ csrf_field() }}
    		{{ method_field('delete') }}
      		<a href="#" data-id="{{ $courselength->id }}" onclick="deleteCourselength(this);">削除</a></form></td>
		<td>{{ $courselength->name }}</td>
	</tr>
	@empty

	@endforelse
	</tbody>
</table>
<script>
function deleteCourselength(e) {
  'use strict';

  if (confirm('本当に削除しますか？')) {
    document.getElementById('form_' + e.dataset.id).submit();
  }
}
</script>


@endsection