@extends('layouts.default')

@section('title', '新規イベント期間')

@section('content')
<h1>イベント期間</h1>
<form method="post" action="{{ url('/courselengths') }}">
{{ csrf_field() }}
<div class="jumbotron">
  <div class="form-horizontal">
    <div class="form-group">
      <label for="name" class="col-xs-2 control-label">イベント期間</label>
        <div class="col-xs-4">
          <input type="name" name="name" placeholder="イベント期間">
        </div>
    </div>
  <p>
    <button class="btn btn-default" type="submit">保存</button>
  </p>
</form>
@endsection