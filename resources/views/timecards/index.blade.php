@extends('layouts.timecards')

@section('title', 'タイムカード')

@section('content')
<div class="container">
<h1>タイムカード：{{ $workplace->name }}</h1>

<form class="form-horizontal">
  <div class="form-group">
    <label for="name" class="control-label col-xs-1">ビュー : </label>
    <div class="col-xs-3">
		<select onChange="top.location.href=value" class="form-control">
			<option value=""></option>
			@foreach($workplaces as $place)
			<option value="/timecards/{{ $place->id }}">{{ $place->name }}のタイムカード</option>
			@endforeach
		</select>
	</div>
  </div>
</form>

<table class="table table-hover table-striped">
	<thead>
	<tr>
		<th class="col-xs-2">名前</th>
		<th class="col-xs-2">出勤予定</th>
		<th class="col-xs-2">退勤予定</th>
		<th class="col-xs-2">出勤時間</th>
		<th class="col-xs-2">退勤時間</th>
		<th class="col-xs-2">出勤/退勤</th>
	</tr>
	</thead>
	<tbody>
	@forelse ($shiftToday as $shift)
	<tr>
		<td class="lead">{{ $shift->admin->name }}</p></td>
		<td class="lead">{{ date('H:i', strtotime($shift->starttime->worktime)) }}</td>
		<td class="lead">{{ date('H:i', strtotime($shift->endtime->worktime)) }}</td>
		@if(!isset($shift->actual_start))
		<td></td>
		@else
		<td class="lead">{{ date('H:i', strtotime($shift->actual_start)) }}</td>
		@endif
		@if(!isset($shift->actual_end))
		<td></td>
		@else
		<td class="lead">{{ date('H:i', strtotime($shift->actual_end)) }}</td>
		@endif
		<td>
			@if(!isset($shift->actual_start))
			{{ Form::open(['route' => ['timecards.updatestart', $shift->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
			{{ Form::submit('出勤', ['class' => 'btn btn-primary btn-lg']) }}
			{{ Form::close()}}
			@elseif(empty($shift->actual_end))
			{{ Form::open(['route' => ['timecards.updateend', $shift->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
			{{ Form::submit('退勤', ['class' => 'btn btn-danger btn-lg']) }}
            {{ Form::close()}}
            @endif
      	</td>
	</tr>
	@empty

	@endforelse
	</tbody>
</table>
</div>
@endsection