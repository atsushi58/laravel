@extends('layouts.timecards')

@section('title', '添乗シフト')

@section('content')
<div class="container">
<h1>直近の添乗シフト</h1>

<form class="form-horizontal">
  <div class="form-group">
    <label for="name" class="control-label col-xs-1">ビュー : </label>
    <div class="col-md-3">
		<select onChange="top.location.href=value" class="form-control">
			<option value=""></option>
			@foreach($workplaces as $place)
			<option value="/timecards/{{ $place->id }}">{{ $place->name }}のタイムカード</option>
			@endforeach
		</select>
	</div>
  </div>
</form>

<table class="table table-hover table-striped">
	<thead>
	<tr>
		<th class="col-md-2">名前</th>
		<th class="col-md-2">出発日</th>
		<th class="col-md-2">ツアー</th>
	</tr>
	</thead>
	<tbody>
	@forelse ($shiftToday as $shift)
	<tr>
		<td class="lead">{{ $shift->admin->name }}</p></td>
		<td>{{ $shift->workday }}</td>
		<td>{{ $shift->event->course->name or '' }}</td>
	</tr>
	@empty

	@endforelse
	</tbody>
</table>
</div>
@endsection