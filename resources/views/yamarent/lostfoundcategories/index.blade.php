@extends('layouts.app')

@section('title', '忘れ物カテゴリ')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>忘れ物カテゴリ</h3></div>
		<div class="panel-body">
			<a href="{{ route('lostfoundcategories.create') }}"><button type="button" class="btn btn-default navbar-btn">新規忘れ物カテゴリ</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>忘れ物カテゴリ</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($lostfoundcategories as $lostfoundcategory)
					<tr>
						<td>
							<a href="{{ route('lostfoundcategories.edit', $lostfoundcategory->id) }}">編集</a></td>
						<td>{{ $lostfoundcategory->id }}</td>
					    <td>{{ $lostfoundcategory->name }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection