@extends('layouts.app')

@section('title', '問い合わせプロセス')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>問い合わせプロセス</h3></div>
		<div class="panel-body">
			<a href="{{ route('lostprocesses.create') }}"><button type="button" class="btn btn-default navbar-btn">新規問い合わせプロセス</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>問い合わせプロセス</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($lostprocesses as $lostprocess)
					<tr>
						<td>
							<a href="{{ route('lostprocesses.edit', $lostprocess->id) }}">編集</a></td>
						<td>{{ $lostprocess->id }}</td>
					    <td>{{ $lostprocess->name }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection