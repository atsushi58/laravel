@extends('layouts.app')

@section('title', '忘れ物プロセス')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>忘れ物プロセス</h3></div>
		<div class="panel-body">
			<a href="{{ route('foundprocesses.create') }}"><button type="button" class="btn btn-default navbar-btn">新規忘れ物プロセス</button></a>
			<table class="minitable display" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>アクション</th>
						<th>id</th>
						<th>忘れ物プロセス名</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($foundprocesses as $foundprocess)
					<tr>
						<td>
							<a href="{{ route('foundprocesses.edit', $foundprocess->id) }}">編集</a></td>
						<td>{{ $foundprocess->id }}</td>
					    <td>{{ $foundprocess->name }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection