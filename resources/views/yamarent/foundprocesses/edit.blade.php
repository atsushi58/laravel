@extends('layouts.app')

@section('title', '忘れ物プロセス編集')

@section('content')

<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading"><h3>忘れ物プロセス編集</h3></div>
    <div class="panel-body">
        {{ Form::open(['route' => ['foundprocesses.update', $foundprocess->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
        <div class="form-group row">
            <div class="col-md-2">{{ Form::label('name', '忘れ物プロセス名')}}</div>
            <div class="col-md-4">{{ Form::input('name', 'name', $foundprocess->name, ['class' => 'form-control']) }}</div>
            <div class="col-md-2">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
            </div>
        </div>
    </div>
    </div>
</div>
@endsection