@extends('layouts.app')

@section('title', '忘れ物編集')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>忘れ物編集</h3></div>
        <div class="panel-body">
        {{ Form::open(['route' => ['founds.update', $found->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('lostfoundcategory_id', '忘れ物カテゴリ', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::select('lostfoundcategory_id', $categories, $found->lostfoundcategory_id, ['class' => 'form-control']) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('name', '顧客名', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('name', 'name', $found->name, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('tel', '顧客電話番号', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('tel', 'tel', $found->tel, ['class' => 'form-control']) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('orderid', 'レンタルSOYid', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('orderid', 'orderid', $found->orderid, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('item', '忘れ物名', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('item', 'item', $found->item, ['class' => 'form-control']) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('comment', 'メモ', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::textarea('comment', $found->comment, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection