@extends('layouts.app')

@section('title', '忘れ物伝票番号登録')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>忘れ物忘れ物伝票番号登録</h3></div>
        <div class="panel-body">
        {{ Form::open(['route' => ['founds.updatevouchar', $found->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('voucharno', '送付伝票番号', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('voucharno', 'voucharno', $found->voucharno, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection