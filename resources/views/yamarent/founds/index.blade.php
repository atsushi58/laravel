@extends('layouts.app')

@section('title', '忘れ物一覧')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>忘れ物一覧</h3></div>
		<div class="panel-body">
           <div class="row">
                {{ Form::open(['method' => 'get']) }} 
                <div class="col-md-2">
			<a href="{{ route('founds.create') }}"><button type="button" class="btn btn-default">新規忘れ物</button></a>
                </div>
                <div class="col-md-6">
                    {{ Form::input('検索する', 'search', null, ['class' => 'form-control']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::submit('検索', ['class' => 'btn btn-primary']) }}
                    {{ Form::close() }}
                </div>
            </div>
            <div class="paginate">
                {{ $founds->appends(['search' => $search])->links() }}
            </div>
                <div class="table-responsive">
    	    	<table class="table nowrap table-hover table-striped">
    	    		<thead>
					<tr>
						<th>アクション</th>
						<th>登録日</th>
						<th>id</th>
						<th>忘れ物カテゴリ名</th>
						<th>忘れ物プロセス</th>
						<th>顧客名</th>
						<th>電話番号</th>
						<th>レンタルSOYid</th>
						<th>忘れ物名</th>
						<th>メモ</th>
						<th>送付伝票番号</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($founds as $found)
					<tr>
						<td>
							<a href="{{ route('founds.edit', $found->id) }}">編集</a> | <a href="{{ route('founds.editprocess', $found->id) }}">ステータス変更</a> | <a href="{{ route('founds.editvouchar', $found->id) }}">送付伝票番号登録</a>
						</td>
						<td>{{ $found->created_at->format('Y年m月d日') }}</td>
						<td>{{ $found->id }}</td>
					    <td>{{ $found->lostfoundcategory->name or '' }}</td>
					    <td>{{ $found->foundprocess->name or '' }}</td>
					    <td>{{ $found->name }}</td>
					    <td>{{ $found->tel }}</td>
					    <td>{{ $found->orderid }}</td>
					    <td>{{ $found->item }}</td>
					    <td>{{ $found->comment }}</td>
					    <td>{{ $found->voucharno }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
		</div>
	</div>
</div>
@endsection