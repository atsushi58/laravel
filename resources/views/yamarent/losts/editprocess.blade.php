@extends('layouts.app')

@section('title', '問い合わせステータス変更')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>問い合わせステータス変更</h3></div>
        <div class="panel-body">
        {{ Form::open(['route' => ['losts.updateprocess', $lost->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('lostprocess_id', '忘れ物ステータス', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::select('lostprocess_id', $processes, $lost->lostprocess_id, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection