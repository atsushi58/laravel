@extends('layouts.app')

@section('title', '問い合わせ編集')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>問い合わせ編集</h3></div>
        <div class="panel-body">
        {{ Form::open(['route' => ['losts.update', $lost->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('lostlostcategory_id', '忘れ物カテゴリ', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::select('lostfoundcategory_id', $categories, $lost->lostlostcategory_id, ['class' => 'form-control']) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('name', '顧客名', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('name', 'name', $lost->name, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('tel', '顧客電話番号', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('tel', 'tel', $lost->tel, ['class' => 'form-control']) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('orderid', 'レンタルSOYid', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('orderid', 'orderid', $lost->orderid, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('item', '忘れ物名', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('item', 'item', $lost->item, ['class' => 'form-control']) }}
                </div>
                <div class="col-md-2">
                    {{ Form::label('memo', 'メモ', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::textarea('memo', $lost->memo, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection