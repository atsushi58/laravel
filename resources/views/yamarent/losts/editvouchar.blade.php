@extends('layouts.app')

@section('title', '問い合わせ送付伝票番号登録')

@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading"><h3>問い合わせ送付伝票番号登録</h3></div>
        <div class="panel-body">
        {{ Form::open(['route' => ['losts.updatevouchar', $lost->id], 'method' => 'patch', 'class' => 'form-horizontal']) }}
            <div class="form-group row">
                <div class="col-md-2">
                    {{ Form::label('voucharno', '送付伝票番号', ['class' => 'form-label']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::input('voucharno', 'voucharno', $lost->voucharno, ['class' => 'form-control']) }}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-2">
                {{ Form::submit('保存', ['class' => 'btn btn-primary']) }}
                {{ Form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection