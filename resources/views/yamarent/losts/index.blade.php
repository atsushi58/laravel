@extends('layouts.app')

@section('title', '問い合わせ一覧')

@section('content')
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading"><h3>問い合わせプロセス</h3></div>
		<div class="panel-body">
			<div class="row">
                {{ Form::open(['method' => 'get']) }} 
                <div class="col-md-2">
				<a href="{{ route('losts.create') }}"><button type="button" class="btn btn-default">新規問い合わせ</button></a>
				</div>
                <div class="col-md-6">
                    {{ Form::input('検索する', 'search', null, ['class' => 'form-control']) }}
                </div>
                <div class="col-md-4">
                    {{ Form::submit('検索', ['class' => 'btn btn-primary']) }}
                    {{ Form::close() }}
                </div>
        	</div>
        <div class="paginate">
            {{ $losts->appends(['search' => $search])->links() }}
        </div>
                <div class="table-responsive">
    	    	<table class="table nowrap table-hover table-striped">
				<thead>
					<tr>
						<th>アクション</th>
						<th>登録日</th>
						<th>id</th>
						<th>忘れ物カテゴリ名</th>
						<th>問い合わせプロセス</th>
						<th>顧客名</th>
						<th>電話番号</th>
						<th>レンタルSOYid</th>
						<th>忘れ物名</th>
						<th>メモ</th>
						<th>送付伝票番号</th>
					</tr>
				</thead>
				<tbody>
					@forelse ($losts as $lost)
					<tr>
						<td>
							<a href="{{ route('losts.edit', $lost->id) }}">編集</a> | <a href="{{ route('losts.editprocess', $lost->id) }}">ステータス変更</a> | <a href="{{ route('losts.editvouchar', $lost->id) }}">送付伝票番号登録</a>
						</td>
						<td>{{ $lost->created_at->format('Y年m月d日') }}</td>
						<td>{{ $lost->id }}</td>
					    <td>{{ $lost->lostfoundcategory->name or '' }}</td>
					    <td>{{ $lost->lostprocess->name or '' }}</td>
					    <td>{{ $lost->name }}</td>
					    <td>{{ $lost->tel }}</td>
					    <td>{{ $lost->orderid }}</td>
					    <td>{{ $lost->item }}</td>
					    <td>{{ $lost->comment }}</td>
					    <td>{{ $lost->voucharno }}</td>
					</tr>
					@empty
					@endforelse
				</tbody>
			</table>
		</div>
		</div>
	</div>
</div>
@endsection