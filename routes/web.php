<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', 'Auth\LoginController@showLoginForm');


Route::get('/top', 'Yamakaraweb\PagesController@index')->name('yamakaraweb.toppage');
Route::get('/mypage', 'Yamakaraweb\PagesController@mypage')->name('yamakaraweb.mypage');
Route::get('/tour/{course_slug}', 'Yamakaraweb\PagesController@tourpage')->name('yamakaraweb.tour');
Route::get('/tourcategory/{category_slug}', 'Yamakaraweb\PagesController@categorypage')->name('yamakaraweb.tourcategory');
Route::get('/tourtag/{tag_slug}', 'Yamakaraweb\PagesController@tagpage')->name('yamakaraweb.tourtag');
Route::get('/allevents', 'Yamakaraweb\PagesController@allevents')->name('yamakaraweb.allevents');
Route::get('/inquiry', 'Yamakaraweb\PagesController@inquiry')->name('yamakaraweb.inquiry');
Route::post('/inquiry', 'Yamakaraweb\PagesController@storeinquiry')->name('yamakaraweb.inquiry.store');
Route::get('/logout', 'Auth\LoginController@logout')->name('user.logout');
Auth::routes();

Route::prefix('admin')->group(function(){
	Auth::routes();
	Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
	Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
	Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
	Route::get('/register', 'Auth\AdminRegisterController@showRegisterForm')->name('admin.register');
	Route::post('/register', 'Auth\AdminRegisterController@create')->name('admin.register.submit');

	Route::get('/admins', 'Admin\AdminsController@index')->name('admin.admins');
	Route::get('/admins/all', 'Admin\AdminsController@all')->name('admin.admins.all');
	Route::get('/admins/create', 'Admin\AdminsController@create')->name('admin.admin.create');
	Route::post('/admins/store', 'Admin\AdminsController@store')->name('admin.admin.store');
	Route::get('/admins/{id}/createbankaccount', 'Admin\AdminsController@createbankaccount')->name('admin.admin.createbankaccount');
	Route::get('/admins/{id}', 'Admin\AdminsController@show')->name('admin.admin.show');
	Route::get('/admins/{id}/edit', 'Admin\AdminsController@edit')->name('admin.admin.edit');
	Route::get('/admins/{id}/edittax', 'Admin\AdminsController@edittax')->name('admin.admin.edittax');
	Route::post('/admins/storebankaccount', 'Admin\AdminsController@storebankaccount')->name('admin.admin.storebankaccount');
	Route::get('/admins/{id}/editbankaccount', 'Admin\AdminsController@editbankaccount')->name('admin.admin.editbankaccount');
	Route::patch('/admins/{id}', 'Admin\AdminsController@update')->name('admin.admin.update');
	Route::patch('admin/{bankaccount}/updatebankaccount', 'Admin\AdminsController@updatebankaccount')->name('admin.admin.updatebankaccount');
	Route::delete('/admins/{id}', 'Admin\AdminsController@destroy')->name('admin.admin.destroy');

	Route::resource('banks', 'Admin\BanksController', ['only' => ['index', 'create', 'store', 'edit', 'update']]);

	Route::resource('bankaccounts', 'Admin\BankaccountsController', ['only' => ['index', 'create', 'store', 'edit', 'update']]);

	Route::resource('payments', 'Admin\PaymentsController', ['only' => ['index', 'create', 'store', 'edit', 'update']]);

	

	Route::get('/insurances', 'Admin\InsurancesController@index')->name('admin.insurances');
	Route::get('/insurances/{id}/edit', 'Admin\InsurancesController@edit')->name('admin.insurance.edit');
	Route::post('/insurances', 'Admin\InsurancesController@store')->name('admin.insurance.store');
	Route::patch('/insurances/{id}', 'Admin\InsurancesController@update')->name('admin.insurance.update');
	Route::delete('/insurances/{id}', 'Admin\InsurancesController@destroy')->name('admin.insurance.destroy');

	Route::resource('roles', 'Admin\RolesController');

	Route::get('/salary/{id}/{year}/{month}', 'Admin\SalariesController@index')->name('admin.salary');
	Route::get('/salaryrenew/{id}/{year}/{month}', 'Admin\SalariesController@renew')->name('admin.salaryrenew');
	Route::get('/salarylist/{year}/{month}', 'Admin\SalariesController@salarylist')->name('admin.salarylist');
	Route::get('/unconfirmedsalarylist/{year}/{month}', 'Admin\SalariesController@unconfirmedsalarylist')->name('admin.unconfirmedsalarylist');
	Route::get('/salary/create/{user_id}/{year}/{month}', 'Admin\SalariesController@create')->name('admin.salary.create');
	Route::get('/salary/createfulltime', 'Admin\SalariesController@createfulltime')->name('admin.salary.create.fulltime');
	Route::post('/salary', 'Admin\SalariesController@store')->name('admin.salary.store');
	Route::get('/salary/{id}/edit', 'Admin\SalariesController@edit')->name('admin.salary.edit');
	Route::patch('/salary/{id}', 'Admin\SalariesController@update')->name('admin.salary.update');
	Route::patch('/salaryreleaseaccountingconfirm/{id}', 'Admin\SalariesController@releaseaccountingconfirm')->name('admin.salary.releaseaccountingconfirm');

	Route::get('/shifts/{workplace_id}/{date}', 'Admin\ShiftsController@index')->name('admin.shifts');
	Route::get('/finishedshifts/{workplace_id}/{date}', 'Admin\ShiftsController@finishedshifts')->name('admin.finishedshifts');
	Route::get('/shiftsbymonth/{year}/{month}', 'Admin\ShiftsController@shiftsbymonth')->name('admin.shifts.bymonth');
	Route::get('/inserthourlypay/{year}/{month}', 'Admin\ShiftsController@inserthourlypay')->name('admin.shifts.inserthourlypay');
	Route::get('/shifts/unconfirmed', 'Admin\ShiftsController@unconfirmed')->name('admin.shifts.unconfirmed');
	Route::get('/shifts/create', 'Admin\ShiftsController@create')->name('admin.shift.create');
	Route::get('/shifts/{id}', 'Admin\ShiftsController@show')->name('admin.shift.show');
	Route::get('/shift/{id}/edit', 'Admin\ShiftsController@edit')->name('admin.shift.edit');
	Route::patch('/shift/{id}', 'Admin\ShiftsController@update')->name('admin.shift.update');
	Route::get('/finishedshift/{id}/edit', 'Admin\ShiftsController@finishededit')->name('admin.finishedshift.edit');
	Route::patch('/finishedshift/{id}/update', 'Admin\ShiftsController@finishedupdate')->name('admin.finishedshift.update');
	Route::post('/shifts', 'Admin\ShiftsController@store')->name('admin.shift.store');
	Route::patch('/confirmshift/{id}', 'Admin\ShiftsController@confirming')->name('admin.shift.confirming');
	Route::patch('/declineshift/{id}', 'Admin\ShiftsController@declining')->name('admin.shift.declining');
	Route::delete('/deleteshift/{id}', 'Admin\ShiftsController@destroy')->name('admin.shift.destroy');

	Route::get('/staffs', 'AdminController@allstaffs')->name('admin.staffs');
	Route::get('/staff/create', 'AdminController@create')->name('admin.staff.create');
	Route::post('/staff', 'AdminController@store')->name('admin.staff.store');
	Route::patch('/staff/{id}', 'AdminController@update')->name('admin.staff.update');
	Route::delete('/staff/{id}', 'AdminController@destroy')->name('admin.staff.destroy');

	Route::get('/staff/{id}/edit', 'AdminController@edit')->name('admin.staff.edit');
	Route::delete('/staff/{id}/edit', 'AdminController@destroy')->name('admin.staff.destrpy');

	Route::resource('tasks', 'Admin\TasksController');

	Route::get('/taxes', 'Admin\TaxesController@index')->name('admin.taxes');
	Route::get('/tax/{id}/edit', 'Admin\TaxesController@edit')->name('admin.tax.edit');
	Route::post('/tax', 'Admin\TaxesController@store')->name('admin.tax.store');
	Route::patch('/tax/{id}', 'Admin\taxesController@update')->name('admin.tax.update');
	Route::delete('/tax/{id}', 'Admin\taxesController@destroy')->name('admin.tax.destroy');

	Route::resource('workplaces', 'Admin\WorkplacesController@index');

	Route::get('/worktimes', 'Admin\WorktimesController@index')->name('admin.worktimes');
	Route::get('/worktimes/{id}/edit', 'Admin\WorktimesController@edit')->name('admin.worktime.edit');
	Route::post('/worktimes', 'Admin\WorktimesController@store')->name('admin.worktime.store');
	Route::patch('/worktimes/{id}', 'Admin\WorktimesController@update')->name('admin.worktime.update');
	Route::delete('/worktimes/{id}', 'Admin\WorktimesController@destroy')->name('admin.worktime.destroy');
});

Route::prefix('staff')->group(function(){
	Route::get('/changepassword', 'Staff\PasswordController@changepassword')->name('staff.password.changepassword');
	Route::patch('/changepassword', 'Staff\PasswordController@updatepassword')->name('staff.password.updatepassword');

	Route::get('/salary/{year}/{month}', 'Staff\SalariesController@index')->name('staff.salary');

	Route::get('/shifts/thisweek', 'Staff\ShiftsController@thisweek')->name('staff.shifts.thisweek');
	Route::get('/shifts/nextweek', 'Staff\ShiftsController@nextweek')->name('staff.shifts.nextweek');
	Route::get('shifts/confirmed/{workplace}/{date}', 'Staff\ShiftsController@confirmed')->name('Staff.shifts.confirmed'); 
	Route::get('/shifts/{id}/nextweekedit', 'Staff\ShiftsController@nextweekedit')->name('staff.shift.nextweekedit');
	Route::get('/shifts/nextweekcreate/{date}', 'Staff\ShiftsController@nextweekcreate')->name('staff.shift.nextweekcreate');
	Route::post('/shifts', 'Staff\ShiftsController@nextweekstore')->name('staff.shift.nextweekstore');
	Route::patch('/shifts/{id}', 'Staff\ShiftsController@nextweekupdate')->name('staff.shift.nextweekupdate');
	Route::delete('/shifts/{id}', 'Staff\ShiftsController@nextweekdestroy')->name('staff.shift.nextweekdestroy');

	Route::get('/staffs', 'Staff\StaffsController@index')->name('staff.staffs');
	Route::get('/staffs/{id}', 'Staff\StaffsController@show')->name('staff.staff.show');

	Route::get('/staffmails', 'Staff\StaffmailsController@index')->name('staff.staffmails');
	Route::get('/staffmails/create', 'Staff\StaffmailsController@create')->name('staff.staffmail.create');
	Route::post('/staffmails', 'Staff\StaffmailsController@store')->name('staff.staffmail.store');

	Route::get('/staffmailcategories', 'Staff\StaffmailcategoriesController@index')->name('staff.staffmailcategories');
	Route::post('/staffmailcategory', 'Staff\StaffmailcategoriesController@store')->name('staff.staffmailcategory.store');
	Route::get('/staffmailcategory/{id}/edit', 'Staff\StaffmailcategoriesController@edit')->name('staff.staffmailcategory.edit');
	Route::patch('/staffmailcategory/{id}', 'Staff\StaffmailcategoriesController@update')->name('staff.staffmailcategory.update');
	Route::delete('/Staffmailcategory/{id}', 'Staff\StaffmailcategoriesController@destroy')->name('staff.staffmailcategory.destroy');

	Route::get('/user', 'Staff\UserController@index')->name('staff.user');
	Route::get('/user/edit', 'Staff\UserController@edit')->name('staff.user.edit');
	Route::patch('/user/update', 'Staff\UserController@update')->name('staff.user.update');
	Route::get('/user/editbankaccount', 'Staff\UserController@editbankaccount')->name('staff.user.editbankaccount');
	Route::patch('/user/updatebankaccount', 'Staff\UserController@updatebankaccount')->name('staff.user.updatebankaccount');
	Route::get('/user/createbankaccount', 'Staff\UserController@createbankaccount')->name('staff.user.createbankaccount');
	Route::post('/user/storebankaccount', 'Staff\UserController@storebankaccount')->name('staff.user.storebankaccount');

	Route::get('/yamakara/event/{id}/createeventprice', 'Yamakara\EventsController@createeventprice')->name('yamakara.event.createeventprice');
	Route::post('/yamakara/event/storeeventprice', 'Yamakara\EventsController@storeeventprice')->name('yamakara.event.storeeventprice');

	Route::get('/yamakara/inquiries', 'Yamakara\InquiriesController@index')->name('yamakara.inquiries');
	Route::get('/yamakara/inquiry/{id}', 'Yamakara\InquiriesController@show')->name('yamakara.inquiry.show');
	Route::get('/yamakara/mailmagazines', 'Yamakara\MailmagazinesController@index')->name('yamakara.mailmagazines');
	Route::get('/yamakara/mailmagazines/create', 'Yamakara\MailmagazinesController@create')->name('yamakara.mailmagazine.create');
	Route::post('/yamakara/mailmagazines/send', 'Yamakara\MailmagazinesController@send')->name('yamakara.mailmagazine.send');

	Route::get('/yamakara/topsliders', 'Yamakara\TopslidersController@index')->name('yamakara.topsliders');
	Route::post('/yamakara/topslider', 'Yamakara\TopslidersController@store')->name('yamakara.topslider.store');
	Route::delete('/yamakara/topslider/{id}', 'Yamakara\TopslidersController@destroy')->name('yamakara.topslider.destroy');
});

Route::prefix('yamarent')->group(function(){
	Route::resource('founds', 'Yamarent\FoundsController', ['except' => ['destroy', 'show']]);
	Route::get('founds/{id}/editprocess', 'Yamarent\FoundsController@editprocess')->name('founds.editprocess');
	Route::patch('founds/{id}/updateprocess', 'Yamarent\FoundsController@updateprocess')->name('founds.updateprocess');
	Route::get('founds/{id}/editvouchar', 'Yamarent\FoundsController@editvouchar')->name('founds.editvouchar');
	Route::patch('founds/{id}/updatevouchar', 'Yamarent\FoundsController@updatevouchar')->name('founds.updatevouchar');
	Route::get('losts/{id}/editprocess', 'Yamarent\LostsController@editprocess')->name('losts.editprocess');
	Route::patch('losts/{id}/updateprocess', 'Yamarent\LostsController@updateprocess')->name('losts.updateprocess');
	Route::get('losts/{id}/editvouchar', 'Yamarent\LostsController@editvouchar')->name('losts.editvouchar');
	Route::patch('losts/{id}/updatevouchar', 'Yamarent\LostsController@updatevouchar')->name('losts.updatevouchar');
	Route::resource('foundprocesses', 'Yamarent\FoundprocessesController');
	Route::resource('losts', 'Yamarent\LostsController');
	Route::resource('lostfoundcategories', 'Yamarent\LostfoundcategoriesController');
	Route::resource('lostprocesses', 'Yamarent\LostprocessesController');
});

Route::prefix('yamakara')->group(function(){
	Route::resource('balancetypes', 'Yamakara\BalancetypesController');
	Route::resource('balances', 'Yamakara\BalancesController');
	Route::get('balancesbymonth/{year}/{month}', 'Yamakara\BalancesController@bymonth')->name('balances.bymonth');
	Route::get('balances/inbyuser/{user_id}', 'Yamakara\BalancesController@inbyuser')->name('balances.inbyuser');
	Route::post('balances/inbyuserstore', 'Yamakara\BalancesController@inbyuserstore')->name('balances.inbyuserstore');
	Route::get('balances/inbyuserwm/{user_id}', 'Yamakara\BalancesController@inbyuserwm')->name('balances.inbyuserwm');
	Route::post('balances/inbyuserstorewm', 'Yamakara\BalancesController@inbyuserstorewm')->name('balances.inbyuserstorewm');
	Route::get('balances/createpay/{user_id}', 'Yamakara\BalancesController@createpay')->name('balances.createpay');
	Route::post('balances/storepay', 'Yamakara\BalancesController@storepay')->name('balances.storepay');
	Route::get('balances/createpaywm/{user_id}', 'Yamakara\BalancesController@createpaywm')->name('balances.createpaywm');
	Route::post('balances/storepaywm', 'Yamakara\BalancesController@storepaywm')->name('balances.storepaywm');
	Route::get('balances/createcxl/{balance_id}', 'Yamakara\BalancesController@createcxl')->name('balances.createcxl');
	Route::post('balances/storecxl/{balance_id}', 'Yamakara\BalancesController@storecxl')->name('balances.storecxl');

	Route::resource('clients', 'Yamakara\ClientsController', ['except' => ['destroy']]);
	Route::get('clients/select', 'Yamakara\ClientsController@selectmerge')->name('clients.selectmerge');
	Route::get('thisyearpaid/{amount}', 'Yamakara\ClientsController@thisyearpaid')->name('clients.thisyearpaid');
	Route::get('thisyearattend/{times}', 'Yamakara\ClientsController@thisyearattend')->name('clients.thisyearattend');
	Route::get('clients/createeventattendance/{id}', 'Yamakara\ClientsController@createeventattendance')->name('clients.createeventattendance');
	Route::post('clients/storeeventattendance/', 'Yamakara\ClientsController@storeeventattendance')->name('clients.storeeventattendance');

	Route::resource('contactlists', 'Yamakara\ContactlistsController');
	Route::get('contactlists/createcontact/{contactlist_id}', 'Yamakara\ContactlistsController@createcontact')->name('contactlists.createcontact');
	Route::post('contactlists/storecontact', 'Yamakara\ContactlistsController@storecontact')->name('contactlists.storecontact');
	Route::get('contactlists/editcontact/{id}', 'Yamakara\ContactlistsController@editcontact')->name('contactlists.editcontact');
	Route::patch('contactlists/updatecontact/{id}', 'Yamakara\ContactlistsController@updatecontact')->name('contactlists.updatecontact');

	Route::resource('courses', 'Yamakara\CoursesController', ['except' => ['destroy']]);
	Route::get('coursescreatecontacts/{course_id}', 'Yamakara\CoursesController@createcontacts')->name('courses.createcontacts');
	Route::post('coursestorecontacts/{course_id}', 'Yamakara\CoursesController@storecontacts')->name('courses.storecontacts');
	Route::get('coursesdestroycontacts/{course_id}', 'Yamakara\CoursesController@destroycontacts')->name('courses.destroycontacts');
	Route::post('courseupdatecontacts/{course_id}', 'Yamakara\CoursesController@updatecontacts')->name('courses.updatecontacts');
	Route::get('/yamakara/course/{id}/createevent', 'Yamakara\CoursesController@createevent')->name('courses.createevent');
	Route::post('/yamakara/course/storeevent', 'Yamakara\CoursesController@storeevent')->name('courses.storeevent');
	Route::get('/yamakara/coursesbyarea/{id}', 'Yamakara\CoursesController@coursesbyarea')->name('courses.byarea');

	Route::resource('courseareas', 'Yamakara\CourseareasController', ['except' => ['destroy']]);
	
	Route::resource('courselengths', 'Yamakara\CourselengthsController', ['except' => ['destroy']]);
	
	Route::resource('depplaces', 'Yamakara\DepplacesController', ['except' => ['destroy']]);

	Route::resource('events', 'Yamakara\EventsController', ['except' => ['destroy']]);
	Route::get('events/createeventattendance/{id}', 'Yamakara\EventsController@createeventattendance')->name('events.createeventattendance');
	Route::get('eventourts/createeventattendancewm/{id}', 'Yamakara\EventsController@createeventattendancewm')->name('events.createeventattendancewm');
	Route::post('evetournts/storeeventattendance/{id}', 'Yamakara\EventsController@storeeventattendance')->name('events.storeeventattendance');
	Route::post('events/storeeventattendancewm/{id}', 'Yamakara\EventsController@storeeventattendancewm')->name('events.storeeventattendancewm');
	Route::patch('eventsadmincheck/{id}', 'Yamakara\EventsController@admincheck')->name('events.admincheck');
	Route::get('eventsbyadmin/{admin_id}', 'Yamakara\EventsController@eventsbyadmin')->name('events.byadmin');
	Route::get('eventsbymonth/{year}/{month}', 'Yamakara\EventsController@eventsbymonth')->name('events.bymonth');
	Route::get('eventsbyyear/{year}', 'Yamakara\EventsController@eventsbyyear')->name('events.byyear');
	Route::get('events/{id}/tcset/', 'Yamakara\EventsController@tcset')->name('events.tcset');
	Route::post('events/tcstore', 'Yamakara\EventsController@tcstore')->name('events.tcstore');
	Route::get('eventsbystatus/{id}', 'Yamakara\EventsController@eventsbystatus')->name('events.bystatus');
	Route::get('depevents/{id}', 'Yamakara\EventsController@depevents')->name('events.depevents');
	Route::get('cancelevents/{id}', 'Yamakara\EventsController@cancelevents')->name('events.cancelevents');

	Route::get('eventscreatecontacts/{event_id}', 'Yamakara\EventsController@createcontacts')->name('events.createcontacts');
	Route::post('eventsstorecontacts', 'Yamakara\EventsController@storecontacts')->name('events.storecontacts');
	Route::get('eventscreatecontactmemo/{event_id}/{contactlist_id}', 'Yamakara\EventsController@createcontactmemo')->name('events.createcontactmemo');
	Route::post('eventsstorecontactmemo/{event_id}', 'Yamakara\EventsController@storecontactmemo')->name('events.storecontactmemo');

	Route::resource('eventattendances', 'Yamakara\EventattendancesController', ['except' => ['destroy']]);
	Route::get('eventattendances/{year}/{date}', 'Yamakara\EventattendancesController@index')->name('eventattendances.index');
	Route::get('eventattendancebydate/{date}', 'Yamakara\EventattendancesController@bydate')->name('eventattendances.bydate');
	Route::get('eventattendancebypoint/{year}/{date}', 'Yamakara\EventattendancesController@point')->name('eventattendances.point');
	Route::get('multipleeventattendance', 'Yamakara\EventattendancesController@multiple')->name('eventattendances.multiple');
	Route::patch('canceleventattendances/{id}', 'Yamakara\EventattendancesController@cancel')->name('eventattendances.cancel');
	Route::patch('attendeventattendances/{id}', 'Yamakara\EventattendancesController@attend')->name('eventattendances.attend');
	Route::patch('attendcheck/{id}', 'Yamakara\EventattendancesController@attendcheck')->name('eventattendances.attendcheck');
	Route::get('eventattendances/{id}/createrental', 'Yamakara\EventattendancesController@createrental')->name('eventattendances.createrental');
	Route::post('eventattendances/storerental', 'Yamakara\EventattendancesController@storerental')->name('eventattendances.storerental');
	Route::patch('rentalpack/{id}', 'Yamakara\EventattendancesController@rentalpack')->name('eventattendances.rentalpack');
	Route::patch('rentaldeliver/{id}', 'Yamakara\EventattendancesController@rentaldeliver')->name('eventattendances.rentaldeliver');
	Route::patch('payeventattendances/{id}', 'Yamakara\EventattendancesController@inpay')->name('eventattendances.inpay');
	Route::get('unpaid/{date}', 'Yamakara\EventattendancesController@unpaid')->name('eventattendances.unpaid');
	Route::get('unpack', 'Yamakara\EventattendancesController@unpack')->name('eventattendances.rentalunpack');
	Route::get('undeliver', 'Yamakara\EventattendancesController@undeliver')->name('eventattendances.rentalundeliver');
	Route::get('booked/{date}', 'Yamakara\EventattendancesController@booked')->name('eventattendances.booked');
	Route::get('rentalall', 'Yamakara\EventattendancesController@rentalall')->name('eventattendances.rentalall');
	Route::get('editrental/{id}', 'Yamakara\EventattendancesController@editrental')->name('eventattendances.editrental');
	Route::patch('updaterental/{id}', 'Yamakara\EventattendancesController@updaterental')->name('eventattendances.updaterental');
	Route::get('zerotoone', 'Yamakara\EventattendancesController@zerotoone');

	Route::resource('eventattendancepaymentmethods', 'Yamakara\EventattendancepaymentmethodsController', ['except' => ['destroy']]);

	Route::resource('eventattendancepaymentstatuses', 'Yamakara\EventattendancepaymentstatusesController', ['except' => ['destroy']]);

	Route::resource('eventattendancestatuses', 'Yamakara\EventattendancestatusesController', ['except' => ['destroy']]);

	Route::resource('eventcosts', 'Yamakara\EventcostsController', ['except' => ['index']]);
	Route::get('eventcosts/{event_id}/create', 'Yamakara\EventcostsController@create')->name('eventcosts.create');
	Route::get('unpaideventcosts', 'Yamakara\EventcostsController@unpaid')->name('eventcosts.unpaid');
	Route::patch('payeventcosts/{id}', 'Yamakara\EventcostsController@pay')->name('eventcosts.pay');

	Route::resource('eventcosttypes', 'Yamakara\EventcosttypesController', ['except' => ['destroy']]);

	Route::resource('eventcostcategories', 'Yamakara\EventcostcategoriesController', ['except' => ['destroy']]);

	Route::resource('eventcostpaymentstatuses', 'Yamakara\EventcostpaymentstatusesController', ['except' => ['destroy']]);


	Route::resource('eventprices', 'Yamakara\EventpricesController', ['except' => ['create', 'destroy']]);
	Route::get('eventprices/{event_id}/create', 'Yamakara\EventpricesController@create')->name('eventprices.create');

	Route::resource('eventstatuses', 'Yamakara\EventstatusesController',  ['except' => ['destroy']]);

	Route::resource('eventbuses', 'Yamakara\EventbusesController',  ['except' => ['destroy']]);

	Route::resource('eventtypes', 'Yamakara\EventtypesController',  ['except' => ['destroy']]);

	Route::resource('inquirystatuses', 'Yamakara\InquirystatusesController', ['except' => ['destroy']]);

	Route::resource('inquirytypes', 'Yamakara\InquirytypesController', ['except' => ['destroy']]);

	Route::resource('photobookstatuses', 'Yamakara\PhotobookstatusesController', ['except' => ['show', 'destroy']]);

	Route::resource('rentaldeliveries', 'Yamakara\RentaldeliveriesController', ['except' => ['destroy']]);

	Route::get('referrals', 'Yamakara\ReferralsController@index')->name('referrals.index');
	Route::get('referralprepoint', 'Yamakara\ReferralsController@prepoint')->name('referrals.prepoint');
	Route::get('referralpoint/{id}', 'Yamakara\ReferralsController@point')->name('referrals.point');
	Route::get('referrals/{eventattendance_id}', 'Yamakara\ReferralsController@create')->name('referrals.create');
	Route::post('referrals/store', 'Yamakara\ReferralsController@store')->name('referrals.store');
	Route::get('referrals/{id}/edit', 'Yamakara\ReferralsController@edit')->name('referrals.edit');
	Route::patch('referrals/{id}/update', 'Yamakara\ReferralsController@update')->name('referrals.update');

	Route::resource('scenarios', 'Yamakara\ScenariosController', ['except' => ['index', 'create']]);
	Route::get('scenarios/{course_id}/create', 'Yamakara\ScenariosController@create')->name('scenarios.create');
	Route::get('scenarios/createcost/{id}', 'Yamakara\ScenariosController@createcost')->name('scenarios.createcost');
	Route::post('scenarios/storecost', 'Yamakara\ScenariosController@storecost')->name('scenarios.storecost');
	Route::get('scenarios/{scenariocost_id}/editcost', 'Yamakara\ScenariosController@editcost')->name('scenarios.editcost');
	Route::patch('scenarios/{scenariocost_id}/updatecost', 'Yamakara\ScenariosController@updatecost')->name('scenarios.updatecost');
	Route::delete('scenarios/{scenariocost_id}/destroycost', 'Yamakara\ScenariosController@destroycost')->name('scenarios.destroycost');

	Route::resource('schedules', 'Yamakara\SchedulesController', ['except' =>['index', 'create']]);
	Route::get('schedules/{course}/create', 'Yamakara\SchedulesController@create')->name('schedules.create');

	Route::resource('tctels', 'Yamakara\TctelsController', ['except' => ['destroy']]);

	Route::resource('tcreports', 'Yamakara\TcreportsController', ['except' => ['create', 'destroy']]);
	Route::get('tcreports/create/{event_id}', 'Yamakara\TcreportsController@create')->name('tcreports.create');
	Route::get('tcreports/createtimereport/{tcreport_id}', 'Yamakara\TcreportsController@createtimereport')->name('tcreports.createtimereport');
	Route::post('tcreports/storetimereport', 'Yamakara\TcreportsController@storetimereport')->name('tcreports.storetimereport');
	Route::get('tcreports/edittimereport/{tctimereport_id}', 'Yamakara\TcreportsController@edittimereport')->name('tcreports.edittimereport');
	Route::patch('tcreports/updatetimereport/{tctimereport_id}', 'Yamakara\TcreportsController@updatetimereport')->name('tcreports.updatetimereport');

	Route::resource('tourimages', 'Yamakara\TourimagesController', ['only' => ['store', 'destroy']]);
	Route::get('tourimages/{course_id}/create', 'Yamakara\TourimagesController@create')->name('tourimages.create');

	Route::resource('tourmailcategories', 'Yamakara\TourmailcategoriesController',  ['except' => ['destroy']]);
	Route::resource('tourmails', 'Yamakara\TourmailsController',  ['except' => ['destroy']]);
	Route::get('tourmails/create/{event_id}', 'Yamakara\TourmailsController@create')->name('tourmails.create');
	Route::get('tourmails/createallmail/{event_id}', 'Yamakara\TourmailsController@createallmail')->name('tourmails.createallmail');
	Route::post('tourmails/storeallmail', 'Yamakara\TourmailsController@storeallmail')->name('tourmails.storeallmail');
	Route::get('tourmails/createfullmail/{event_id}', 'Yamakara\TourmailsController@createfullmail')->name('tourmails.createfullmail');
	Route::post('tourmails/storefullmail', 'Yamakara\TourmailsController@storefullmail')->name('tourmails.storefullmail');

	Route::resource('tourmailtemplates', 'Yamakara\TourmailtemplatesController');
});

Route::prefix('yakushima')->group(function(){
	Route::resource('yakuactivities', 'Yakushima\YakuactivitiesController');
	Route::resource('yakuactivitytypes', 'Yakushima\YakuactivitytypesController');
	Route::get('yakuactivities/{activity_id}/guidefix', 'Yakushima\YakuactivitiesController@guidefix')->name('yakuactivities.guidefix');
	Route::post('yakuactivities/guidestore', 'Yakushima\YakuactivitiesController@guidestore')->name('yakuactivities.guidestore');
	Route::resource('yakuaccompanies', 'Yakushima\YakuaccompaniesController');
	Route::resource('yakucoursetypes', 'Yakushima\YakucoursetypesController');
	Route::get('yakucourseprices/{yakucourse_id}/{year}/{month}', 'Yakushima\YakucoursepricesController@index')->name('yakucourseprices.index');
	Route::get('yakucourseprices/{yakucourse_id}/{year}/{month}/create', 'Yakushima\YakucoursepricesController@create')->name('yakucouseprices.create');
	Route::get('yakucourseprices/createdefaultprice/{course_id}', 'Yakushima\YakucoursepricesController@createdefaultprice')->name('yakucourseprices.createdefaultprice');
	Route::get('yakucourseprices/create30advanceprice/{course_id}', 'Yakushima\YakucoursepricesController@create30advanceprice')->name('yakucourseprices.create30advanceprice');
	Route::get('yakucourseprices/create60advanceprice/{course_id}', 'Yakushima\YakucoursepricesController@create60advanceprice')->name('yakucourseprices.create60advanceprice');
	Route::get('yakucourseprices/createdateprice/{course_id}', 'Yakushima\YakucoursepricesController@createdateprice')->name('yakucourseprices.createdateprice');
	Route::post('yakucourseprices/store', 'Yakushima\YakucoursepricesController@store')->name('yakucourseprices.store');
	Route::resource('yakuflightarrangestatuses', 'Yakushima\YakuflightarrangestatusesController');
	Route::resource('yakuguides', 'Yakushima\YakuguidesController');
	Route::get('yakuguides/worklist/{date}', 'Yakushima\YakuguidesController@worklist')->name('yakuguides.worklist');
	Route::get('yakuguide/worklist/{id}/{year}/{month}', 'Yakushima\YakuguidesController@personworklist')->name('yakuguide.worklist');
	Route::get('yakuguides/worklist/{id}/up', 'Yakushima\YakuguidesController@worklistup')->name('yakuguides.worklistup');
	Route::get('yakuguides/createschedule/{id}', 'Yakushima\YakuguidesController@createschedule')->name('yakuguides.createschedule');
	Route::get('yakuguides/createscheduledate/{id}/{date}', 'Yakushima\YakuguidesController@createscheduledate')->name('yakuguides.createscheduledate');
	Route::post('yakuguides/storeschedule', 'Yakushima\YakuguidesController@storeschedule')->name('yakuguides.storeschedule');
	Route::get('yakuguides/editschedule/{id}', 'Yakushima\YakuguidesController@editschedule')->name('yakuguides.editschedule');
	Route::patch('yakuguides/updateschedule/{id}', 'Yakushima\YakuguidesController@updateschedule')->name('yakuguides.updateschedule');
	Route::resource('yakuguidescheduletypes', 'Yakushima\YakuguidescheduletypesController');
	Route::resource('yakuhostelarrangestatuses', 'Yakushima\YakuhostelarrangestatusesController');
	Route::resource('yakuhosteltypes', 'Yakushima\YakuhosteltypesController');
	Route::resource('yakushiparrangestatuses', 'Yakushima\YakushiparrangestatusesController');
	Route::resource('yakucometokojs', 'Yakushima\YakucometokojsController');
	Route::resource('yakucometokums', 'Yakushima\YakucometokumsController');
	Route::resource('yakucomerequests', 'Yakushima\YakucomerequestsController');

	Route::resource('yakucourses', 'Yakushima\YakucoursesController');
	Route::get('yakucourses/createoptionactivity/{id}', 'Yakushima\YakucoursesController@createoptionactivity')->name('yakucourses.createoptionactivity');
	Route::post('yakucourses/storeoptionactivity', 'Yakushima\YakucoursesController@storeoptionactivity')->name('yakucourses.storeoptionactivity');
	Route::patch('yakucourses/detachoptionactivity/{course_id}/{activitytype_id}', 'Yakushima\YakucoursesController@detachoptionactivity')->name('yakucourses.detachoptionactivity');
	Route::resource('yakudepplaces', 'Yakushima\YakudepplacesController');
	Route::resource('yakueventattendances', 'Yakushima\YakueventattendancesController', ['only' => ['index', 'show', 'store', 'edit', 'update']]);
	Route::get('yakueventattendances/{yakucourse_id}/create', 'Yakushima\YakueventattendancesController@create')->name('yakueventattendances.create');
	Route::get('yakueventattendances/{id}/editaccompany', 'Yakushima\YakueventattendancesController@editaccompany')->name('yakueventattendances.editaccompany');
	Route::patch('yakueventattendances/{id}/updateaccompany', 'Yakushima\YakueventattendancesController@updateaccompany')->name('yakueventattendances.updateaccompany');
	Route::resource('yakudefaultactivities', 'Yakushima\YakudefaultactivitiesController', ['only' => ['store', 'edit', 'update', 'destroy']]);
	Route::resource('yakuhostels', 'Yakushima\YakuhostelsController');
	Route::resource('yakuschedules', 'Yakushima\YakuschedulesController');
	Route::get('yakudefaultactivities/{yakucourse_id}/create', 'Yakushima\YakudefaultactivitiesController@create')->name('yakudefaultactivities.create');
	Route::resource('yakudefaulthostelarranges', 'Yakushima\YakudefaulthostelarrangesController', ['only' => ['store', 'edit', 'update', 'destroy']]);
	Route::get('yakudefaulthostelarranges/{yakucourse_id}/create', 'Yakushima\YakudefaulthostelarrangesController@create')->name('yakudefaulthostelarranges.create');
	Route::resource('yakureturnfromkojs', 'Yakushima\YakureturnfromkojsController');
	Route::resource('yakureturnfromkums', 'Yakushima\YakureturnfromkumsController');
	Route::resource('yakureturnrequests', 'Yakushima\YakureturnrequestsController');	
	Route::resource('yakutourimages', 'Yakushima\YakutourimagesController');		
});

Route::get('/timecards/{id}', 'TimecardsController@index')->name('timecards');
Route::patch('/timecardsstart/{id}', 'TimecardsController@updatestart')->name('timecards.updatestart');
Route::patch('/timecardsend/{id}', 'TimecardsController@updateend')->name('timecards.updateend');
Route::get('/tourlist', 'Yamakaraweb\PagesController@tourlist')->name('yamakaraweb.tourlist');
Route::get('/attendanceform/{id}', 'Yamakaraweb\PagesController@attendanceform')->name('yamakaraweb.eventattendance');
Route::post('/storeeventattendance', 'Yamakaraweb\PagesController@storeattendance')->name('yamakaraweb.storeeventattendance');
Route::get('/createrental/{user_id}/{eventprice_id}', 'Yamakaraweb\PagesController@createrental')->name('yamakaraweb.createrental');
Route::patch('/updaterental/{eventattendance_id}', 'Yamakaraweb\PagesController@updaterental')->name('yamakaraweb.updaterental');
Route::get('/attendcheck/{user_id}/{eventprice_id}', 'Yamakaraweb\PagesController@attendcheck')->name('yamakaraweb.attendcheck');

Route::get('yakuattendanceform/{yakucourse_id}/{date}', 'Yamakaraweb\YakupagesController@yakuattendanceform')->name('yakushimaweb.attendanceform');
Route::post('storeyakueventattendance', 'Yamakaraweb\YakupagesController@storeyakueventattendance')->name('yakushimaweb.storeeventattendance');

Route::patch('/updateattend/{eventattendance_id}', 'Yamakaraweb\PagesController@updateattend')->name('yamakaraweb.updateattend');
Route::get('/cancelform/{user_id}/{eventprice_id}', 'Yamakaraweb\PagesController@cancelform')->name('yamakaraweb.cancelform');
Route::patch('/confirmcancel/{eventattendance_id}', 'Yamakaraweb\PagesController@confirmcancel')->name('yamakaraweb.confirmcancel');

Auth::routes();